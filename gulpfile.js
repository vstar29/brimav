var elixir = require('laravel-elixir');

var gulp=require('gulp');
var templateCache = require('gulp-angular-templatecache');
require('gulp-ng-annotate');
require('laravel-elixir-ng-annotate')

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */



/*----Vstar s--*/

/*elixir(function(mix)
{
    mix.annotate(["tether.min.js","bootstrap.min.js","mdb.min.js","scripts.js"],'public/assets/vstarjsanotate.js','public/assets/js').scripts('vstarjsanotate.js','public/assets/vstarscripts.js','public/assets')
});

elixir(function(mix) {
    mix.styles([
       
        "bootstrap.min.css",
        "style.css",
        "vstarvibe_style.css",
    ], 'public/assets/vstarstyles.css', 'public/assets/css');


});*/




/*SCRIPTS MIX */


/*--priority scripts--*/

/*elixir(function(mix) {
    mix.scripts([
       
       "theme/jquery.min.js",
        "angularjs/angular.min.js",
    
       
        
    ], 'public/scripts/jangular.js', 'public/scripts');
});*/


/*SCRIPTS MIX */

/*==---Theme scripts--==*/

elixir(function(mix) {
    mix.scripts([
       
        "modernizr.js",
        "bootstrap.js",
        "jquery.easing.1.3.js",
        "skrollr.min.js",
        "smooth-scroll.js",
        "wow.min.js",
        "page-scroll.js",
        "jquery.parallax-1.1.3.js",
        "jquery.tools.min.js",
        "moment.min.js",
        "main.js",
        "jquery.jplayer.min.js",
        "owl.carousel.min.js",
        "jquery.scrollme.min.js"
        
    ], 'public/scripts/templatescripts.js', 'public/scripts/theme');
});

/*==---Angular scripts--==*/
    
elixir(function(mix) {
    mix.scripts([
       

       "angular-ui-router.min.js",
       "angular-animate.min.js",
       "angular-breadcrumb.min.js",
       "angular-sanitize.min.js",
       "dirPagination.js",
       "angular-moment.min.js",
       "angular-base64.js",
       "angular-cookies.js",
        "ng-infinite-scroll.min.js",
        "ngToast.min.js"

    ], 'public/scripts/angularjs.js', 'public/scripts/angularjs');

});


/*--BRIMAV SCRIPTS COMBINE--*/

elixir(function(mix)
{
    mix.scripts(
    ['jangular.js','templatescripts.js','angularjs.js',],
    'public/scripts/brimav_script.js','public/scripts');
})



/*==---ui Bootstrap scripts--==*/

elixir(function(mix) {
    mix.scripts([
       
        "ui-bootstrap.min.js",
        "ui-bootstrap-tpls-2.4.0.min.js"
    ], 'public/scripts/angularui.js', 'public/scripts/angular-ui');
});

/*--revolution slider script--*/

elixir(function(mix) {
    mix.scripts([

        "jquery.revolution.js", 
    ], 'public/scripts/revolution.js', 'public/scripts/theme');
});




/*==---Intel Input script--==*/

elixir(function(mix)
{
    mix.scripts( "intlTelInput.min.js",'public/scripts/intlTelInputmin.js', 'public/scripts/theme')
});



/*==---CK editor script--==*/

/*elixir(function(mix)
{
    mix.scripts(["ckeditor.js","jquery.js"],'public/scripts/ckeditor/ckeditor.min.js', 'public/scripts/ckeditor')
});*/




/*==---Home + log in scripts--==*/

elixir(function(mix)
{
    mix.annotate( ["App.js","services/HomeService.js","controllers/HomeController.js"],'public/shared/app/homesannote.js','public/shared/app').scripts('homesannote.js','public/shared/app/homescripts.js','public/shared/app')
});

/*==---Registration scripts--==*/

elixir(function(mix)
{
    mix.annotate(["App.js","services/HomeService.js","controllers/HomeController.js","services/RegistrationService.js","controllers/RegistrationController.js"],'public/shared/app/registrationannote.js','public/shared/app').scripts('registrationannote.js','public/shared/app/registrationscripts.js','public/shared/app')
});





/*==---Admin scripts--==*/


elixir(function(mix)
{
    mix.annotate([

        "App.js",
        "services/AdminShareService.js",
        "services/AdminAdvertiserService.js",
        "services/AdminAdvertService.js",
        "services/AdminCouponService.js",
         "services/AdminBannerTypeService.js",
        "services/AdminPaymentService.js",
        "services/AdminProfileService.js",
        "services/AdminPublisherService.js",
         "controllers/ShareController.js",
        "controllers/HomeController.js",
        "controllers/AdvertController.js",
        "controllers/AdvertiserController.js",
        "controllers/CouponController.js",
         "controllers/BannerTypeController.js",
        "controllers/PaymentController.js",
        "controllers/PublisherController.js",
        "controllers/ProfileController.js"

        ],
        'public/areas/admin/app/adminannotate.js','public/areas/admin/app').scripts('adminannotate.js','public/areas/admin/app/adminscripts.js','public/areas/admin/app')
});


/*==---Advertiser scripts--==*/


elixir(function(mix)
{
    mix.annotate([
        "App.js",
        "services/AdvertiserShareService.js",
        "services/AdvertiserHomeService.js",
        "services/AdvertiserAdvertService.js",
        "services/AdvertiserProfileService.js",
        "controllers/ShareController.js",
        "controllers/HomeController.js",
        "controllers/AdvertController.js",
        "controllers/ProfileController.js"
       
        ],
        'public/areas/advertiser/app/advertiserannotate.js','public/areas/advertiser/app').scripts(['advertiserannotate.js','audioMetadata.js'],'public/areas/advertiser/app/advertiserscripts.js','public/areas/advertiser/app')
});


/*==---Publisher scripts--==*/


elixir(function(mix)
{
    mix.annotate([
        "App.js",
        "services/PublisherShareService.js",
        "services/PublisherHomeService.js",
        "services/PublisherAdvertService.js",
        "services/PublisherProfileService.js",
        "services/PublisherPaymentService.js",
        "services/PublisherBannerService.js",
         "services/PublisherMediumService.js",
        "controllers/ShareController.js",
        "controllers/HomeController.js",
        "controllers/AdvertController.js",
        "controllers/PaymentController.js",
        "controllers/ProfileController.js",
        "controllers/BannerController.js",
        "controllers/MediumController.js" 
        ],
        'public/areas/publisher/app/publisherannotate.js','public/areas/publisher/app').scripts('publisherannotate.js','public/areas/publisher/app/publisherscripts.js','public/areas/publisher/app')
});



/*CSS MIX */

elixir(function(mix) {
    mix.styles([
       
        "animate.css",
        "bootstrap.css",
        "font-awesome.min.css",
        "extralayers.css",
        "settings.css",
        "magnific-popup.css",
        "text-effect.css",
        "myanimate.css",
        "ngToast.css",
        "ngToast-animations.min.css",
        "responsive.css",
        "style.css",
        "owl.transitions.css",
        "owl.carousel.css",
        "site.css",
        "jplayer.css",
    ], 'public/css/templatestyle.css', 'public/css/theme');


});



/*--reg extra--*/
elixir(function(mix) {
    mix.styles([

        "regextra.css", 
    ], 'public/css/regextra.css', 'public/css/theme');
});

 
   
   /*---VERSION MIXING--*/
elixir(function(mix) {
     
    mix.version(["scripts/templatescripts.js","scripts/brimav_script.js","scripts/angularjs.js","scripts/jangular.js","scripts/angularui.js","scripts/revolution.js", "shared/app/homescripts.js","shared/app/registrationscripts.js","scripts/intlTelInputmin.js","areas/admin/app/adminscripts.js","areas/publisher/app/publisherscripts.js","areas/advertiser/app/advertiserscripts.js","css/templatestyle.css"]);

});




/*----TEMPLTES CACHING---*/

gulp.task('publisher_template_view_cache', function () {
  return gulp.src('public/areas/publisher/views/**/*.blade.php')
    .pipe(templateCache(
        {filename:'publisher_templates.js',
        module:'PublisherShareModule',
       transformUrl:function(url)
       {
          url =url.replace('.blade.php','');
          return 'publisher'+url;
       } }))
    .pipe(gulp.dest('public/areas/publisher/app'));
});

gulp.task('advertiser_template_view_cache', function () {
  return gulp.src('public/areas/advertiser/views/**/*.blade.php')
    .pipe(templateCache(
        {filename:'advertiser_templates.js',
        module:'AdvertiserShareModule',
       transformUrl:function(url)
       {
          url =url.replace('.blade.php','');
          return 'advertiser'+url;
       } }))
    .pipe(gulp.dest('public/areas/advertiser/app'));
});

gulp.task('shared_template_view_cache', function () {
  return gulp.src('public/shared/views/**/*.blade.php')
    .pipe(templateCache(
        {filename:'shared_templates.js',
        module:'HomeShareModule',
       transformUrl:function(url)
       {
          url =url.replace('.blade.php','');
          return 'home/'+url;
       } }))
    .pipe(gulp.dest('public/shared/app'));
});




elixir(function(mix)
{
    mix.task('advertiser_template_view_cache');
    mix.task('publisher_template_view_cache');
     mix.task('shared_template_view_cache');

})