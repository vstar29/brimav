<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Balances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Balances', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('balance');
            $table->integer('publisher_id')->unsigned();
            $table->timestamps();

            $table->foreign('publisher_id')->references('id')->on('Publishers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Balances');
    }
}
