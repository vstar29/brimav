<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PublisherWebsites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('publisher_id')->unsigned();
            $table->string('websitename')->unique();
            $table->string('websiteurl')->unique();
            $table->string('category');
            $table->string('status')->default('Pending');
            $table->integer('prize')->unsigned();
            $table->timestamps();
            $table->foreign('publisher_id')->references('id')->on('Publishers')->onUpdate('cascade')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('websites');
    }
}
