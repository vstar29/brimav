<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Advertlinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AdvertLinks', function(Blueprint $table)
        {
             $table->increments('id');
             $table->integer('advert_id')->unsigned();
             $table->integer('publisher_id')->unsigned();
             $table->string('websitename');
             $table->string('link');
             $table->string('status');
            $table->integer('prize');
             $table->timestamps();

             $table->foreign('advert_id')->references('id')->on('adverts')->onUpdate('cascade')->onDelete('cascade');

              $table->foreign('publisher_id')->references('id')->on('publishers')->onUpdate('cascade')->onDelete('cascade');
              

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('AdvertLinks');
    }
}
