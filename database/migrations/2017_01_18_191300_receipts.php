<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class Receipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    

        public function up()
        {
            Schema::create('Receipts', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('advert_id')->unsigned();
                $table->integer('advertiser_id')->unsigned();
                $table->timestamps();
                $table->string('receipt_number');
                $table->string('cardtype');
                $table->string('cardnumber');
                $table->string('reference');
                $table->string('cardtoken');

                
                $table->integer('websitenumber')->unsigned();
                $table->foreign('advert_id')->references('id')->on('Adverts')->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('advertiser_id')->references('id')->on('Advertisers')->onUpdate('cascade')->onDelete('cascade');
                
            });

        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Receipts');
    }
}