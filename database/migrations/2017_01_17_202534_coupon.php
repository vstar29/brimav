<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Coupon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Coupons', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code')->unique();
            $table->integer('percent');
            $table->dateTime('start');
            $table->dateTime('stop');
            $table->string('adverttype');
            $table->string('name')->unique();
            $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Coupons');
    }
}
