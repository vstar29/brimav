<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PublisherTvRadio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tvs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('publisher_id')->unsigned();
            $table->string('channel_name');
            $table->string('category');
            $table->string('status')->default('Pending');
            $table->string('certificate');
            $table->string('channel_frequency');
            $table->integer('prize')->unsigned();
            $table->timestamps();
            $table->foreign('publisher_id')->references('id')->on('Publishers')->onUpdate('cascade')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tvs');
    }
}
