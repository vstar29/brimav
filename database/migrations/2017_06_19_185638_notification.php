<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Notifications',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('sender_id')->unsigned();
            $table->integer('receiver_id')->unsigned();
            $table->integer('advert_id')->unsigned();
            $table->integer('status');
            $table->string('content');
            $table->string('advert_link');
            $table->timestamps('created_at');
            $table->foreign('advert_id')->references('id')->on('Adverts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sender_id')->references('id')->on('Users')->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('receiver_id')->references('id')->on('Users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Notifications');
    }
}
