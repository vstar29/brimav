<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Publishers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('Publishers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('fullname');
            $table->string('banker')->nullable();
            $table->string('accountnumber')->nullable();
            $table->string('accountname')->nullable();
            $table->string('status');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Publishers');
    }
}
