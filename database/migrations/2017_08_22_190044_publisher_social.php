<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PublisherSocial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Socials', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('publisher_id')->unsigned();
            $table->string('type');
            $table->string('handler');
            $table->string('status')->default('Pending');
            $table->integer('prize')->unsigned();
            $table->integer('followers')->unsigned();
            $table->timestamps();
            $table->foreign('publisher_id')->references('id')->on('Publishers')->onUpdate('cascade')->onDelete('cascade');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Social');
    }
}
