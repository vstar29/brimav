<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('Users', function(Blueprint $table)
        {
             $table->increments('id');
             $table->string('username');
             $table->string('email');
             $table->string('password');
             $table->string('phonenumber')->nullable();
             $table->boolean('emailverification');
             $table->string('status');
             $table->string('verificationtoken');
             $table->string('picture')->nullable();
             $table->string('country');
             $table->string('city');
             $table->string('currency');
             $table->string('remember_token')->nullable();
             $table->integer('userable_id');
             $table->string('userable_type');
             $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Users');
    }
}
