<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Adverts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
          Schema::create('Adverts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('destination');
             $table->string('duration');
            $table->string('youtube');
            $table->string('itunes');
            $table->string('type');
            $table->string('songurl')->nullable();
            $table->string('attachment')->nullable();
            $table->string('imageurl');
            $table->string('genre')->nullable();
            $table->string('status');
            $table->string('totalamount')->nullable();
             $table->integer('discount')->default(0);
            $table->boolean('payment')->nullable();
            $table->integer('advertiser_id')->unsigned();
             $table->timestamps();

            $table->foreign('advertiser_id')->references('id')->on('Advertisers')->onUpdate('cascade')->onDelete('cascade');
              

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Adverts');
    }
}
