<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TvsProgramme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Programmes',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('publisher_id')->unsigned();
            $table->integer('tv_id')->unsigned();
            $table->string('name');
            $table->string('status')->default('Pending');
            $table->string('start_time');
            $table->string('stop_time');
            $table->string('category');
            $table->string('days');
            $table->string('interval');
            $table->integer('prize')->unsigned();
            $table->timestamps();
            $table->foreign('publisher_id')->references('id')->on('Publishers')->onUpdate('cascade')->onDelete('cascade'); 
             $table->foreign('tv_id')->references('id')->on('tvs')->onUpdate('cascade')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop();
    }
}
