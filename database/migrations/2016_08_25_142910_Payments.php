<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('Payments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('amount');
            $table->string('accountnumber');
            $table->integer('publisher_id')->unsigned();
            $table->string('status');
            $table->timestamps();

            $table->foreign('publisher_id')->references('id')->on('Publishers')->onUpdate('cascade')->onDelete('cascade');
     

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Payments');
    }
}
