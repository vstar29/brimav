			/*====---
 publisher BANNER  SERVICE--=====*/


 publisherBannerApp.service('PublisherBannerService',function($http,SharedMethods)
{
	var bannerBase="api/publisherbannerAPI";
	 


	/*--get all  banner s---*/
	this.getAll=function()
	{
		return $http.get(bannerBase);
	};


	/*--get  banner  by id---*/
	this.getBannerbyid=function(id)
	{
		
		return $http.get(bannerBase +'/getbannerbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting banner  details");
			var BANNER={};
			return BANNER;
		})
	};


	/*--create banner ---*/
	this.postBanner=function(data)
	{
		var request=$http(
			{
				url:bannerBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit banner---*/
	this.putBanner=function(data)
	{
		var request=$http(
			{
				url:bannerBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete banne---*/

    this.deleteBanner = function (id) {
        var request=$http(
        {
            url:bannerBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

   /*--get all  banner typess---*/
	this.getBannerTypes=function()
	{
		return $http.get(bannerBase+'/getbannertypes');
	};


	/*-get all publisher websites--*/
	this.getWebsites=function()
	{
		return $http.get('api/publisherwebsiteAPI').then(function(pl)
		{
			return pl.data;
		},function(error)
		{
			return {};
		});
	};
   
})