
/*----====== PUBLISHER PROFILE SERVICE ======----*/

 
publisherProfileApp.service('PublisherProfileService', function ($http, SharedMethods) {

    var profileBase = "api/publisherprofileAPI";
   

    /*--===== Get  Basic details ====--*/
   
    this.getPublisherBasicDetails = function () {
        return $http.get(profileBase).then(function (response) {
            return response.data;
            
        },
       function (error) {
           var publisher = {};
           return publisher;
       });
    };
    

    /*--===== Edit Basic details ====--*/
  
  this.putEditpublisherBasicDetails=function(PublisherDetails)
  {
   
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editpublisherbasicdetails",
        data:PublisherDetails
    });
    return request;
  };



   /*--===== Change Avatar ====--*/
  
  this.putUploadavatar=function(avatar)
  {

        var request = $http({
            method: "post",
            url:profileBase +"/uploadavatar",
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            formData.append("avatar" ,avatar);              
            return formData;            
            },
        });
        return request;
    };



    /*--===== Edit Account details ====--*/
  
  this.putEditpublisherAccountDetails=function(PublisherDetails)
  {
   
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editpublisheraccountdetails",
        data:PublisherDetails
    });
    return request;
  };


    /*--===== Change Password ====--*/
    
    this.changepassword = function (Passworddetails) {
      
        var request = $http({
            method: 'put',
            url: profileBase + "/changepassword",
            data: Passworddetails
        });
        return request;
    };

})