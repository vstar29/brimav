			/*====---publisher Medium SERVICE--=====*/


/*-----social medium--*/
 publisherSocialApp.service('PublisherSocialService',function($http,SharedMethods)
{
	var socialBase="api/publishersocialAPI";
	 


	/*--get all  socials---*/
	this.getAll=function()
	{
		return $http.get(socialBase);
	};


	/*--get  social  by id---*/
	this.getSocialbyid=function(id)
	{
		
		return $http.get(socialBase +'/getsocialbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting social medium  details");
			var social={};
			return social;
		})
	};


	/*--create Social ---*/
	this.postSocial=function(data)
	{
		var request=$http(
			{
				url:socialBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit social---*/
	this.putSocial=function(data)
	{
		var request=$http(
			{
				url:socialBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete social---*/

    this.deleteSocial = function (id) {
        var request=$http(
        {
            url:socialBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };
   
})


 /*-----------------------------------------------------------------------------------*/


/*-----website medium--*/
 publisherWebsiteApp.service('PublisherWebsiteService',function($http,SharedMethods)
{
	var websiteBase="api/publisherwebsiteAPI";
	 


	/*--get all  websites---*/
	this.getAll=function()
	{
		return $http.get(websiteBase);
	};


	/*--get  social  by id---*/
	this.getWebsitebyid=function(id)
	{
		
		return $http.get(websiteBase +'/getwebsitebyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem : Refresh page");
			var social={};
			return social;
		})
	};


	/*--add new websites ---*/
	this.postWebsite=function(data)
	{
		var request=$http(
			{
				url:websiteBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit websites---*/
	this.putWebsite=function(data)
	{
		var request=$http(
			{
				url:websiteBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete website---*/

    this.deleteWebsite = function (id) {
        var request=$http(
        {
            url:websiteBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

 
   
})




 /*-----------------------------------------------------------------------------------*/


/*-----tv-radio medium--*/
 publisherTvApp.service('PublisherTvService',function($http,SharedMethods)
{
	var tvBase="api/publishertvAPI";
	 


	/*--get all  station---*/
	this.getAll=function()
	{
		return $http.get(tvBase);
	};


	/*--get  station  by id---*/
	this.getTvbyid=function(id)
	{
		
		return $http.get(tvBase +'/gettvbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem : Refresh page");
			var social={};
			return social;
		})
	};



	
	/*--add new station ---*/
	this.postTv=function(data)
	{
		var request=$http(
			{
				url:tvBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit tv---*/
	this.putTv=function(data)
	{
		var request=$http(
			{
				url:tvBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete tv---*/

    this.deleteTv = function (id) {
        var request=$http(
        {
            url:tvBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

 
})



 /*-----------------------------------------------------------------------------------*/


/*-----program service--*/
 publisherProgrammesApp.service('PublisherProgrammesService',function($http,SharedMethods)
{
	var tvBase="api/publishertvAPI";
	var programmeBase="api/publisherprogrammeAPI";
	 


	/*--get all  programmes---*/
	this.getAll=function()
	{
		return $http.get(programmeBase);
	};

	/*--get all  station---*/
	this.getTvs=function()
	{
		return $http.get(tvBase);
	};



	/*--get  programme  by id---*/
	this.getProgrammebyid=function(id)
	{
		
		return $http.get(programmeBase +'/getprogrammebyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem : Refresh page");
			var social={};
			return social;
		})
	};




   /*--add new programme ---*/
	this.postProgramme=function(data)
	{
		var request=$http(
			{
				url:programmeBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit programme---*/
	this.putProgramme=function(data)
	{
		var request=$http(
			{
				url:programmeBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete programme---*/

    this.deleteProgramme = function (id) {
        var request=$http(
        {
            url:programmeBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

 
   
})