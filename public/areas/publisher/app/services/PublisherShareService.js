﻿/*-------========== PUBLISHER SHARED SERVICE =======-------*/

/*--==== Get  Logged Inuser Details ====--*/

publisherShareApp.service('ShareService', function ($http) {
    //Get current admin info
    this.getLoggedInPublisher = function () {

        var request=$http({
            url:'api/publisherprofileAPI/shared',
            method:'get',
            cache:true
        });

        return request;

    };


    /*--===== Mark notification as read ====--*/

    this.markRead = function () {
        var request=$http({
            url:'api/markread',
            method:'get',
        });

        return request;
       
    };
})


