
 /*----====== PUBLISHER ADVERTS SERVICE ======----*/

publisherAdvertApp.service("PublisherAdvertService", function ($http, $rootScope, SharedMethods) {

    var advertBase = "api/publisheradvertAPI";
    
  
    /*--===== Get completed Adverts ====--*/
    
    this.getCompletedAdverts = function () {
        return $http.get(advertBase+'/completedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get Uncompleted Adverts ====--*/
    
    this.getUncompletedAdverts = function () {
        return $http.get(advertBase+'/uncompletedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };


    /*--===== Get  Advert by ID ====--*/

    this.getAdvertbyid = function (id) {
        return $http.get(advertBase +'/advertbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            //SharedMethods.createErrorToast("Problem getting Advert details");
            var advert = {};
            return advert;
        });
    };

    /*--===== Get  Advertlink by a Publisher ====--*/

    this.getAdvertlink = function (id) {
        return $http.get(advertBase +'/advertlink/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var advert = {};
            return advert;
        });
    };




     /*--===== Put Advert Link ====--*/

    this.putadvertlink = function (linkdetails) {
        var request = $http({
            method: "put",
            url: advertBase + "/insertadvertlink/",
            data:linkdetails
        });
        return request;
    };


});