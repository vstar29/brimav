publisherPaymentApp.service('publisherPaymentService', function($http,SharedMethods)
{

var payBase="api/publisherpaymentAPI";



/*--===== Get  All  Payments ====--*/

    this.getAllPayments = function () {
        return $http.get(payBase).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting payment details");
            var Payments = {};
            return Payments;
        });
    };

    /*---Request for Payments===*/
    this.requestPayment=function(data)
    {
        var request=$http(
        {
            'url':payBase+'/requestpayment',
             method:'post',
             data:data
        });
        return request;
    }

  
});