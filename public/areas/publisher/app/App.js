/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var publisherShareApp = angular.module('PublisherShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','angularMoment','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
       
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {
           
            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }

        } 
        else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }

    /* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
    
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="glyphicon glyphicon-ok jp-white alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning  alert-danger margin-right-04"></i>' + cont
        });
    }
    
    /* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }



})
.config(function ($urlRouterProvider, $stateProvider, $locationProvider, $breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider) {
    
    
    //modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'right',
        timeout: 4000,
        maxNumber: 3,
        animation: 'fade',
        dismissButton: true,
    });

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    $breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/publisher/home" target="_self">Home&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp; &#xf18e &nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });

    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
    
    //make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    //$urlRouterProvider.otherwise('/404');

    $locationProvider.html5Mode(true);
})


/*J PLAYER DIRECTIVE*/


.directive('jplayer', function()
{
    return{
        restrict:'E',
        templateUrl:'publisheradvert/player',
        scope:{Advert:'=advertdetails',
                audio:'=song'},
        link:function(scope,elem,attr)
        {
            
            scope.onplay=false;
            scope.onpause=false;
            var player=jQuery('#jplayer'),
            fixmp4,
            fixmp4_id, 
            ignore_timeupdate,
            cssSelectorAncestor= "#jp_container",
            myControl =
            {
                progress: jQuery("#jp-progress-slider"),
                volume: jQuery("#jp-volume-slider")
            }
        
            player.jPlayer
            ({ 
                
                swfPath: 'scripts/theme/jquery.jplayer.swf',
                solution: 'html, flash',
                supplied: 'mp3',
                preload: 'metadata',
                volume: 0.8,
                muted: false,
                wmode: "window",
                keyEnabled: true,
                cssSelectorAncestor: "#jp_container",

                ready: function () 
                {
                    player.jPlayer("setMedia", {mp3: attr.audio})
                },

               
                timeupdate: function(event) {

                myControl.progress.slider("option","value", event.jPlayer.status.currentPercentAbsolute);
            },

            volumechange: function(event) 
            {
                    if(event.jPlayer.options.muted) 
                    {
                        myControl.volume.slider("value", 0);
                    } 
                    else {
                        myControl.volume.slider("option","value", event.jPlayer.options.volume);
                    }
            }
                
            });
            
             var playerData = player.data("jPlayer");

            /*-------Create the progress slider control-----*/
            myControl.progress.slider({
            animate: "fast",
            max: 100,
            range: "min",
            step: 0.1,
            value : 0,
            slide: function(event, ui) {
                var sp = playerData.status.seekPercent;
                if(sp > 0) {
                
                    if(fixmp4) {
                        ignore_timeupdate = true;
                        clearTimeout(fixmp4_id);
                        fixmp4_id = setTimeout(function() {
                            ignore_timeupdate = false;
                        },1000);
                    }

                    // Move the play-head to the value and factor in the seek percent.
                    player.jPlayer("playHead", ui.value * (100 / sp));
                } else {
                    
                    // Create a timeout to reset this slider to zero.
                    setTimeout(function() {
                        myControl.progress.slider("option","value", 0);
                    }, 0);
                }
            }
        });

        /*---Create the volume slider control---*/
        myControl.volume.slider({
            animate: "fast",
            max: 1,
            range: "min",
            step: 0.01,
            value : $.jPlayer.prototype.options.volume,
            slide: function(event, ui) {
                player.jPlayer("option", "muted", false);
                player.jPlayer("option", "volume", ui.value);
            }
        });
            
            
       scope.play=function()
       {
         scope.onplay=true;
         scope.onpause=false;
       }

        scope.pause=function()
       {
         player.jPlayer('pause');
         scope.onpause=true;
         scope.onplay=false;
       }

      
        
        }
        
    };
})


/*MULTI  SELECT FOR BANNER TYPE DIRECTICE */
.directive('multiselect', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        template:
                "<div class='multi_select'>" +
                   
                    "<ul class='  ' >" +
                        "<li  data-ng-repeat='option in options'><a data-ng-click='toggleSelectItem(option)' class='pointer'><span data-ng-class='getClassName(option)'></span> [[option.name]]  </a></li>" +
                    "</ul>" +
                "</div>",

        controller: function ($scope) {

    

            $scope.toggleSelectItem = function (option) {
                  var intIndex = -1;

                angular.forEach($scope.model, function (item, index) {
                         
                    if (index == option.id ) {
                           
                        intIndex = index;
                    }

                });

                if (intIndex >0) {
                   delete $scope.model[option.id];
                  
                 
                }
                else {

                    $scope.model[option.id]=option.name;

                }
            };

           $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.id) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };
        }
    };

})


.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})


.run(function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
});




/*===================== HOME SPA ====================*/
var publisherHomeApp = angular.module('PublisherHomeModule', ['PublisherShareModule','PublisherAdvertModule'])
 
.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
    //debugger; //break point
   
    $urlRouterProvider.otherwise('/publisher/home');
   
    $stateProvider
    .state('home', {
        url: '/publisher/home',
        templateUrl: 'publisherhome/home',
        controller: 'HomeController'
    });
})
.run(function ($rootScope) {
    $rootScope.homePage = true;
    $rootScope.advertview = false;
});





    /*===================== PROFILE SPA ====================*/
   var publisherProfileApp = angular.module('PublisherProfileModule', ['PublisherShareModule'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/publisher/profile');
        
        $stateProvider
        // Template for the Profile Pages
        .state('profile', {
            abstract: true,
            url: '/publisher/profile',
            templateUrl: 'publisherprofile/home',
            controller: 'ProfileTemplateController',
            resolve: {
                Publisher: function (PublisherProfileService) {
                    return PublisherProfileService.getPublisherBasicDetails(); 
                }
            }
        })

        //Profile (Display Publisher Profile)
        .state('profile.basic', {
            url: '',
            templateUrl: 'publisherprofile/basicdetails',
            controller: 'BasicDetailsController',
            resolve: {
                Publisher: function (Publisher) {
                    return Publisher;
                }
            },
            ncyBreadcrumb: {
            label: 'Profile'
        }
            
        })
       
        // Edit mode for Publisher Basic Details
        .state('profile.editbasic', {
            url: '/edit',
            templateUrl: 'publisherprofile/editbasic',
            controller: 'EditBasicProfileController',
            resolve: {
                Publisher: function (Publisher) {
                    return Publisher;
                }
            },
            ncyBreadcrumb: {
                label: 'Edit Details',
                parent: 'profile.basic'
            }
                
        })


        //Profile (Display Publisher Account Details)
        .state('profile.accountdetails', {
            url: '/account',
            templateUrl: 'publisherprofile/accountdetails',
            controller: 'AccountDetailsController',
            resolve: {
                Publisher: function (Publisher) {
                    return Publisher;
                }
            },
            ncyBreadcrumb: {
            label: 'Account',
            parent:'profile.basic'
        }
            
        })
       
        // Edit mode for Publisher Account Details
        .state('profile.editaccountdetails', {
            url: '/edit/account',
            templateUrl: 'publisherprofile/editaccount',
            controller: 'EditAccountProfileController',
            resolve: {
                Publisher: function (Publisher) {
                    return Publisher;
                }
            },
            ncyBreadcrumb: {
                label: 'Edit Details',
                parent: 'profile.basic'
            }
                
        })



         // Change Password
        .state('profile.changepassword', {
            url: '/password',
            templateUrl: 'publisherprofile/changepassword',
            controller: 'ChangePasswordController',
            resolve: {
                Publisher: function (Publisher) {
                    return Publisher;
                }
            },
            ncyBreadcrumb: {
                label: 'Change Password',
                parent: 'profile.basic'
            }
        })  

    })
    .run(function ($rootScope) {
    $rootScope.profilePage = true;
    
   });



/*===================== ADVERT SPA ========================*/
var publisherAdvertApp = angular.module('PublisherAdvertModule',['PublisherShareModule'])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {

    var startSymbol= $interpolateProvider.startSymbol('[[');
    var endSymbol= $interpolateProvider.endSymbol(']]');
     denormalizeTemplate = (startSymbol == '{{' || endSymbol  == '}}')
            ? identity
            : function denormalizeTemplate(template) {
              return template.replace(/\{\{/g, startSymbol).replace(/}}/g, endSymbol);
        },
        NG_ATTR_BINDING = /^ngAttr[A-Z]/;
    $urlRouterProvider.otherwise('/publisher/advert');

    $stateProvider
     // Get List of adverts
    .state('advert', {
        url: '/publisher/advert',
        abstract:true,
        templateUrl: 'publisheradvert/home',
        controller: 'AdvertTemplateController',
        resolve:{
            
            UnconfirmedAdverts:function(PublisherAdvertService)
            {
                return PublisherAdvertService.getUncompletedAdverts();
            },

            ConfirmedAdverts:function(PublisherAdvertService)
            {
               return PublisherAdvertService.getCompletedAdverts();
            }
        }
        
    })

    // Get List of uncompleted adverts
    .state('advert.unconfirmed', {
        url: '',
        params: { returnState: "advert" },
        templateUrl: 'publisheradvert/uncompleted',
        controller: 'UnConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'Advert'
        },
        resolve:
        {
            UnconfirmedAdverts:function(UnconfirmedAdverts)
            {
                return UnconfirmedAdverts;
            }
        }
    })
       
       

    // Get List of completed adverts
    
     .state('advert.confirmed', {
        url: '/published',
        templateUrl: 'publisheradvert/completed',
        controller: 'ConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'completed',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            ConfirmedAdverts:function(ConfirmedAdverts)
            {
                return ConfirmedAdverts;
            }
        }
        
    })


    // Advert Details
    .state('details' , {
        url: '/publisher/advert/details/:id',
        params:{returnState:'advert.unconfirmed',id:null},
        templateUrl: 'publisheradvert/details',
        controller: 'AdvertDetailsController',
        ncyBreadcrumb: {
            label: 'Details',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            Advert:function(PublisherAdvertService, $stateParams)
            {
                return PublisherAdvertService.getAdvertbyid($stateParams.id);
            }

        }
    });

       
})
.run(function ($rootScope) {
    $rootScope.advertPage = true;
});


/*===================== PAYMENTS SPA ====================*/
var publisherPaymentApp = angular.module('PublisherPaymentModule', ['PublisherShareModule'])
.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/publisher/payment');

    $stateProvider
    //Payment's Homepage 
    .state('payment', {
        url: '/publisher/payment',
        templateUrl: 'publisherpayment/home',
        controller: 'PaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Payment'
        }
    })


})
.run(function($rootScope)
{
$rootScope.paymentPage=true;
});



/*===================== BANNER SPA ====================*/


var publisherBannerApp=angular.module('PublisherBannerModule',['PublisherShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/banner');

    $stateProvider.

    state('banner',
    {
        url:'/publisher/banner',
        templateUrl:'publisherbanner/home',
        controller:'PublisherBannerHomeController',
        
         ncyBreadcrumb:
        {
            label:'banner'
        }
    })

    .state('create',{

        url:'/publisher/banner/create',
        templateUrl:'publisherbanner/create',
        controller:'BannerCreatecontroller',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'banner'
        }
    })

    .state('edit',
    {
        url:'/publisher/banner/edit/:id',
        params:{id:null},
        templateUrl:'publisherbanner/edit',
        controller:'BannerEditcontroller',
        resolve:
        {
            Banner:function(PublisherBannerService,$stateParams)
            {
                
                return PublisherBannerService.getBannerbyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'banner'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.bannerPage=true;
});



/*===================== SOCIAL MEDIUM SPA ====================*/


var publisherSocialApp=angular.module('PublisherSocialModule',['PublisherShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/social');

    $stateProvider.

    state('social',
    {
        url:'/publisher/social',
        templateUrl:'publishersocial/home',
        controller:'SocialHomeController',
        
         ncyBreadcrumb:
        {
            label:'social'
        }
    })

    .state('create',{

        url:'/publisher/social/create',
        templateUrl:'publishersocial/create',
        controller:'SocialCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'social'
        }
    })

    .state('edit',
    {
        url:'/publisher/social/edit/:id',
        params:{id:null},
        templateUrl:'publishersocial/edit',
        controller:'SocialEditController',
        resolve:
        {
            Social:function(PublisherSocialService,$stateParams)
            {
                
                return PublisherSocialService.getSocialbyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'social'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.socialPage=true;
});




/*===================== WEBSITE MEDIUM SPA ====================*/


var publisherWebsiteApp=angular.module('PublisherWebsiteModule',['PublisherShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/website');

    $stateProvider.

    state('website',
    {
        url:'/publisher/website',
        templateUrl:'publisherwebsite/home',
        controller:'WebsiteHomeController',
        
         ncyBreadcrumb:
        {
            label:'website'
        }
    })

    .state('create',{

        url:'/publisher/website/create',
        templateUrl:'publisherwebsite/create',
        controller:'WebsiteCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'website'
        }
    })

    .state('edit',
    {
        url:'/publisher/website/edit/:id',
        params:{id:null},
        templateUrl:'publisherwebsite/edit',
        controller:'WebsiteEditController',
        resolve:
        {
            Website:function(PublisherWebsiteService,$stateParams)
            {
                
                return PublisherWebsiteService.getWebsitebyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'website'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.websitePage=true;
});


/*===================== TV MEDIUM SPA ====================*/


var publisherTvApp=angular.module('PublisherTvModule',['PublisherShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/tv');

    $stateProvider.

    state('tv',
    {
        url:'/publisher/tv',
        templateUrl:'publishertv/home',
        controller:'TvHomeController',
        
         ncyBreadcrumb:
        {
            label:'website'
        }
    })

    .state('create',{

        url:'/publisher/tv/create',
        templateUrl:'publishertv/create',
        controller:'TvCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'tv'
        }
    })

    .state('edit',
    {
        url:'/publisher/tv/edit/:id',
        params:{id:null},
        templateUrl:'publishertv/edit',
        controller:'TvEditController',
        resolve:
        {
            Tv:function(PublisherTvService,$stateParams)
            {
                
                return PublisherTvService.getTvbyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'tv'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.tvPage=true;
});




/*===================== PROGRAMMES SPA ====================*/


var publisherProgrammesApp=angular.module('PublisherProgrammesModule',['PublisherShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/programme');

    $stateProvider.

    state('programme',
    {
        url:'/publisher/programme',
        templateUrl:'publisherprogramme/home',
        controller:'ProgrammesHomeController',
         ncyBreadcrumb:
        {
            label:'programme'
        }
    })

    .state('create',{

        url:'/publisher/programme/create',
        templateUrl:'publisherprogramme/create',
        controller:'ProgrammeCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'programme'
        }
    })

    .state('edit',
    {
        url:'/publisher/programme/edit/:id',
        params:{id:null},
        templateUrl:'publisherprogramme/edit',
        controller:'ProgrammeEditController',
        resolve:
        {
            Programme:function(PublisherProgrammesService,$stateParams)
            {
                
                return PublisherProgrammesService.getProgrammebyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'tv'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.programmePage=true;
});


