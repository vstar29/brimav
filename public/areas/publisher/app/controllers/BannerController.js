 						/*====---Publisher BANNER  controller===---*/

/*---banner types  home controller---*/

 publisherBannerApp.controller('PublisherBannerHomeController',function(PublisherBannerService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner  | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$scope.deleting=false;


 	loadBanners();
 	function loadBanners()
 	{
 		var getBanners=PublisherBannerService.getAll();
 		getBanners.then(function(pl)
 		{
 			$scope.Banners=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Banners ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletebanner=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Banner " ,
            bodyMessage: "Are you sure you want to delete this Banner?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'BannerDeletecontroller',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---Banner type  create controller---*/
  publisherBannerApp.controller('BannerCreatecontroller',function(PublisherBannerService,Websites,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner  | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$rootScope.SubPageHeading='Create';
 	$scope.Websites=Websites;
 	

 	/*--get all banner types and websites from db--*/
 	loadBannerTypes();
 	function loadBannerTypes()
 	{
 		var getBannerTypes=PublisherBannerService.getBannerTypes();
 		getBannerTypes.then(function(pl)
 		{
 			$scope.BannerTypes=pl.data.BannerTypes;
 			$scope.Websites=pl.data.Websites;
 		},
 		function(error)
 		{
 			//SharedMethods.createErrorToast('Problem getting Banner Types');
 		})
 	}
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newbanner={
 		'name':$scope.Name,
 		'type':$scope.Type,
 		'prize':$scope.Prize,
 		'websites':$scope.website
 		};
 		

	 	var postBanner=PublisherBannerService.postBanner($scope.newbanner);
	 	postBanner.then(function(pl)
	 	{
	 		$state.go('banner',{},{reload:true});
	 		SharedMethods.createSuccessToast('Banner created succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---Banner   edit controller---*/
  publisherBannerApp.controller('BannerEditcontroller',function(PublisherBannerService,Banner, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner | Brimav';
 	$rootScope.PageHeading='Banner';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Banner=Banner;
 	$scope.Banner2={};
	for(var key in Banner)
 	{
 		$scope.Banner2[key]=$scope.Banner[key];
 	}

 
 	/*--get all banner types and websites from db--*/
 	loadBannerTypes();
 	function loadBannerTypes()
 	{
 		var getBannerTypes=PublisherBannerService.getBannerTypes();
 		getBannerTypes.then(function(pl)
 		{
 			$scope.BannerTypes=pl.data.BannerTypes;
 			$scope.Websites=pl.data.Websites;

 		},
 		function(error)
 		{
 			//SharedMethods.createErrorToast('Problem getting Banner Types');
 		})
 	}

 	$scope.edit=function()
 	{

 		$scope.editing=true

 		$scope.newbanner={
	    'id':$scope.Banner2.id,
 		'name':$scope.Banner2.name,
 		'type':$scope.Banner2.bannertype_id,
 		'prize':$scope.Banner2.prize,
 		'website':$scope.Banner2.website
 		};



       
	 	var putBanner=PublisherBannerService.putBanner($scope.newbanner);
	 	putBanner.then(function(pl)
	 	{
            $state.go('banner',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Banner  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---Banner   delete controller---*/
 publisherBannerApp.controller('BannerDeletecontroller',function(PublisherBannerService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner | Brimav';
 	$rootScope.PageHeading='Banner';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteBanner=PublisherBannerService.deleteBanner($scope.DeleteDetails.id);
 		deleteBanner.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('banner', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Banner deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Banner Type');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 })