 						/*====---Publisher Medium  controller===---*/

/*---social  medium controller---*/

 publisherSocialApp.controller('SocialHomeController',function(PublisherSocialService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials  | Brimav';
 	$rootScope.PageHeading='Social';
 	$scope.deleting=false;


 	loadSocials();
 	function loadSocials()
 	{
 		var getSocials=PublisherSocialService.getAll();
 		getSocials.then(function(pl)
 		{
 			$scope.Socials=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Socials ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletesocial=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Social " ,
            bodyMessage: "Are you sure you want to delete this Social Account?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'SocialDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---SOCIAL  create controller---*/
  publisherSocialApp.controller('SocialCreateController',function(PublisherSocialService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials  | Brimav';
 	$rootScope.PageHeading='Socials';
 	$rootScope.SubPageHeading='Create';
 	$scope.Categories=Categories;
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newsocial={
 		'handler':$scope.Handler,
 		'type':$scope.Type,
 		'prize':$scope.Prize,
 		'followers':$scope.Followers,
 		};
 		

	 	var postSocial=PublisherSocialService.postSocial($scope.newsocial);
	 	postSocial.then(function(pl)
	 	{
	 		$state.go('social',{},{reload:true});
	 		SharedMethods.createSuccessToast('Social medium added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---Social   edit controller---*/
  publisherSocialApp.controller('SocialEditController',function(PublisherSocialService,Social, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials | Brimav';
 	$rootScope.PageHeading='Socials';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Social=Social;
 	$scope.Categories=Categories;
 	
 

 	$scope.edit=function()
 	{

 		$scope.editing=true

		$scope.newsocial={
		'id':$scope.Social.id,
 		'handler':$scope.Social.handler,
 		'type':$scope.Social.type,
 		'prize':$scope.Social.prize,
 		'followers':$scope.Social.followers};

       
	 	var putSocial=PublisherSocialService.putSocial($scope.newsocial);
	 	putSocial.then(function(pl)
	 	{
            $state.go('social',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Medium  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---Social   delete controller---*/
 publisherSocialApp.controller('SocialDeleteController',function(PublisherSocialService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials| Brimav';
 	$rootScope.PageHeading='Social';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteSocial=PublisherSocialService.deleteSocial($scope.DeleteDetails.id);
 		deleteSocial.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('social', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Medium deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Medium');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 })



/*---------------------------------------------------------------------------------------------------------*/



 /*---WEBSITE MEDIUM CONTROLLER---*/

 publisherWebsiteApp.controller('WebsiteHomeController',function(PublisherWebsiteService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Websites| Brimav';
 	$rootScope.PageHeading='Website';
 	$scope.deleting=false;


 	loadWebsites();
 	function loadWebsites()
 	{
 		var getWebsites=PublisherWebsiteService.getAll();
 		getWebsites.then(function(pl)
 		{
 			$scope.Websites=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Websites ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletewebsite=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Website " ,
            bodyMessage: "Are you sure you want to delete this Website?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'WebsiteDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---WEBSITES  create controller---*/
  publisherWebsiteApp.controller('WebsiteCreateController',function(PublisherWebsiteService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Websites  | Brimav';
 	$rootScope.PageHeading='Website';
 	$rootScope.SubPageHeading='Create';
 	$scope.Categories=Categories;
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newwebsite={
 		'websitename':$scope.Websitename,
 		'websiteurl':$scope.Websiteurl,
 		'prize':$scope.Prize,
 		'category':$scope.Category,
 		};
 		

	 	var postWebsite=PublisherWebsiteService.postWebsite($scope.newwebsite);
	 	postWebsite.then(function(pl)
	 	{
	 		$state.go('website',{},{reload:true});
	 		SharedMethods.createSuccessToast('Website added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---Website   edit controller---*/
  publisherWebsiteApp.controller('WebsiteEditController',function(PublisherWebsiteService,Website, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Social Medium | Brimav';
 	$rootScope.PageHeading='Social';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Website=Website;
 	$scope.Categories=Categories;
 	
 

 	$scope.edit=function()
 	{

 		$scope.editing=true

		$scope.newwebsite={
		'id':$scope.Website.id,
 		'websitename':$scope.Website.websitename,
 		'websiteurl':$scope.Website.websiteurl,
 		'prize':$scope.Website.prize,
 		'category':$scope.Website.category,
 		};
 		
       
	 	var putWebsite=PublisherWebsiteService.putWebsite($scope.newwebsite);
	 	putWebsite.then(function(pl)
	 	{
            $state.go('website',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Website  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---Website  delete controller---*/
 publisherWebsiteApp.controller('WebsiteDeleteController',function(PublisherWebsiteService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Medium | Brimav';
 	$rootScope.PageHeading='Website';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deletewebsite=PublisherWebsiteService.deleteWebsite($scope.DeleteDetails.id);
 		deletewebsite.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('website', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Website deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Medium');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 })


/*----------------------------------------------------------------------------------------*/



 /*---	TV MEDIUM CONTROLLER---*/

publisherTvApp.controller('TvHomeController',function(PublisherTvService,SharedMethods, $uibModal,$scope,$state,$rootScope)
{
 	$rootScope.title='Tv-Radio  | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$scope.deleting=false;


 	loadStations();
 	function loadStations()
 	{
 		var getTvs=PublisherTvService.getAll();
 		getTvs.then(function(pl)
 		{
 			$scope.Tvs=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Stations ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletetv=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Station " ,
            bodyMessage: "Are you sure you want to delete this Station?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'TvDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---Tv  create controller---*/
  publisherTvApp.controller('TvCreateController',function(PublisherTvService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Tv-Radio  | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Create';
 	$scope.Categories=Categories;
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newstation={
 		'name':$scope.Name,
 		'frequency':$scope.Frequency,
 		'prize':$scope.Prize,
 		'category':$scope.Category,
 		};
 		

	 	var postTv=PublisherTvService.postTv($scope.newstation);
	 	postTv.then(function(pl)
	 	{
	 		$state.go('tv',{},{reload:true});
	 		SharedMethods.createSuccessToast('Station added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---Tv   edit controller---*/
  publisherTvApp.controller('TvEditController',function(PublisherTvService,Tv, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Tv-Radio | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Tv=Tv;
 	$scope.Categories=Categories;
 	
 

 	$scope.edit=function()
 	{

 		$scope.editing=true

		$scope.newstation={
		'id':$scope.Tv.id,
 		'name':$scope.Tv.channel_name,
 		'frequency':$scope.Tv.channel_frequency,
 		'prize':$scope.Tv.prize,
 		'category':$scope.Tv.category,
 		};
 		
       
	 	var putTv=PublisherTvService.putTv($scope.newstation);
	 	putTv.then(function(pl)
	 	{
            $state.go('tv',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Station  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---Tv  delete controller---*/
 publisherTvApp.controller('TvDeleteController',function(PublisherTvService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Medium | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deletetv=PublisherTvService.deleteTv($scope.DeleteDetails.id);
 		deletetv.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('tv', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Station deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Station');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
})


/*------------------------------------TV PROGRAMS CONTROLLERS--------------*/



publisherProgrammesApp.controller('ProgrammesHomeController',function(PublisherProgrammesService,SharedMethods, $uibModal,$scope,$state,$rootScope)
{
 	$rootScope.title='Tv-Radio  | Brimav';
 	$rootScope.PageHeading='Programmes';
 	$scope.deleting=false;


 	loadProgrammes();
 	function loadProgrammes()
 	{
 		var getProgrammes=PublisherProgrammesService.getAll();
 		getProgrammes.then(function(pl)
 		{
 			$scope.Programmes=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Programmes, Refresh ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deleteprogramme=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Programme " ,
            bodyMessage: "Are you sure you want to delete this Programme?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'ProgrammeDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---Programme  create controller---*/
publisherProgrammesApp.controller('ProgrammeCreateController',function(PublisherProgrammesService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Programmes  | Brimav';
 	$rootScope.PageHeading='Programmes';
 	$rootScope.SubPageHeading='Create';

 	loadStations();
 	function loadStations()
 	{
 		var getTvs=PublisherProgrammesService.getTvs();
 		getTvs.then(function(pl)
 		{
 			$scope.Stations=pl.data;
 			
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem : Refresh page ');
 		})
 	}
 	 $scope.Days = {};
 	 $scope.Options=Options;
 	 $scope.Categories=Categories;
 	
 	
 	$scope.create=function()
 	{
 		var selectedDays="";
 		for(var x in $scope.Days)
 		{
 			selectedDays+=$scope.Days[x]+",";
 		}
 		$scope.creating=true;
		$scope.newprograme={
 		'name':$scope.Name,
 		'days':selectedDays,
 		'category':$scope.Category,
 		'start_time':$scope.Start_time,
 		'stop_time':$scope.Stop_time,
 		'station':$scope.Station,
 		'interval':$scope.Interval,
 		'price':$scope.Price
 		};

 	
	 	var postProgramme=PublisherProgrammesService.postProgramme($scope.newprograme);
	 	postProgramme.then(function(pl)
	 	{
	 		$state.go('programme',{},{reload:true});
	 		SharedMethods.createSuccessToast('Programme added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---Program   edit controller---*/
  publisherProgrammesApp.controller('ProgrammeEditController',function(PublisherProgrammesService,Programme, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Programmes | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Programme=Programme;
 	$scope.Days = {};
 	$scope.Categories=Categories;
 	$scope.Options=Options;
 	
 	loadStations();
 	function loadStations()
 	{
 		var getTvs=PublisherProgrammesService.getTvs();
 		getTvs.then(function(pl)
 		{
 			$scope.Stations=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem : Refresh page ');
 		})
 	}
 

 	$scope.edit=function()
 	{
 		var selectedDays="";
 		for(var x in $scope.Days)
 		{
 			selectedDays+=$scope.Days[x]+",";
 		}

 		$scope.editing=true

		$scope.newprogramme={
		'id':$scope.Programme.id,
 		'name':$scope.Programme.name,
 		'days':selectedDays,
 		'category':$scope.Programme.category,
 		'start_time':$scope.Programme.start_time,
 		'stop_time':$scope.Programme.stop_time,
 		'station':$scope.Programme.tv_id,
 		'interval':$scope.Programme.interval,
 		'price':$scope.Programme.price
 		};
 		
       
	 	var putProgramme=PublisherProgrammesService.putProgramme($scope.newprogramme);
	 	putProgramme.then(function(pl)
	 	{
            $state.go('programme',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Programme  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---Programme  delete controller---*/
 publisherProgrammesApp.controller('ProgrammeDeleteController',function(PublisherProgrammesService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Programmes | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteprogramme=PublisherProgrammesService.deleteProgramme($scope.DeleteDetails.id);
 		deleteprogramme.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('programme', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Programme deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Programme');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
})


 /*---availlable categories---*/
 var Categories=["Economy","Education","Arts","Entertainment","Health","Others","Religion","Politics","Sports","Technology","Fashion"];
 var Options=[{id:1,name:"Sunday"},{id:2,name:"Monday"},{id:3,name:"Tuesday"},{id:4,name:"Wednessday"},{id:5,name:"Thursday"},{id:6,name:"Friday"},{id:7,name:"Saturday"}];





