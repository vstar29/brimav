
/*-------========== publisher ADVERTS CONTROLLER =======-------*/
/*--==== Adert Template Controller ====--*/

publisherAdvertApp.controller('AdvertTemplateController', function ($rootScope,PublisherAdvertService,$uibModal, $state, $scope) {
    $rootScope.headerClass = "normal-page-title";

    //Set default filter, sort parameters
    $scope.Filterview = "unconfirmed";
    $scope.sortKey = "date";
    $scope.reverse=false;
    
    $rootScope.advertview = true;
    
   
     $scope.$watch('Filterview', function (filterkey) {
        $state.go('advert.'+filterkey); // Go to the state selected
        $rootScope.Filterview=filterkey; //set the Filter to the param passed
    });

    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }


    // put advert link 

    $scope.putadvertlink = function (id,returnState) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var getLink=PublisherAdvertService.getAdvertlink(id);
        getLink.then(function(response)
        {
            $scope.link=response;
            var Details = {
            id : id,
            title: "Insert Link " ,
            class: "btn-adgold",
            returnState:returnState,
            oldlink:$scope.link
        };
        

        var modalInstance = $uibModal.open({
            templateUrl: 'publisheradvert/insertlink',
            controller: 'PutAdvertLinkController',
            resolve: {
                Details: function () {
                    return Details;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
    
        }, function () {
            
        });
            
           
        },

        function(error)
        {

        })
        
    };
});
   

/*--==== UnConpleted Advert Controller ====--*/

publisherAdvertApp.controller('UnConfirmedAdvertController', function ($scope, UnconfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Unpublished Adverts";
    $rootScope.SubPageHeading = null;
     $rootScope.Filterview = "unconfirmed";

    $scope.contentLoading = true; // show loading icon
    $scope.UnconfirmedAdvert=UnconfirmedAdverts;
    

    

    });


/*--==== Conpleted Advert Controller ====--*/

publisherAdvertApp.controller('ConfirmedAdvertController', function ($scope, ConfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Published Adverts";
    $rootScope.SubPageHeading = null;
     $rootScope.Filterview = "confirmed";

    $scope.contentLoading = true; // show loading icon
    $scope.ConfirmedAdverts=ConfirmedAdverts; 
    });




/*--==== Put AdvertLink Controller ====--*/

publisherAdvertApp.controller('PutAdvertLinkController', function ($scope,$state,$cookies, Details, PublisherAdvertService, $rootScope, $uibModal, $uibModalInstance, SharedMethods) {


    $scope.class=Details.class;
    $scope.title=Details.title;
    $scope.Link=Details.oldlink;
   

    // put advert link

    $scope.insertadvertlink = function () {
        $scope.editing=true;
        SharedMethods.keepModalOpen($scope);
        $scope.editing = true;

        $scope.linkDetails = {
            id:Details.id,
            link:$scope.Link,
        };

        var PutAdvertLink=PublisherAdvertService.putadvertlink($scope.linkDetails);
        PutAdvertLink.then(function()
        {   
            $uibModalInstance.close();
            $state.go(Details.returnState);
            $cookies.remove('Publisher_profile');

            SharedMethods.createSuccessToast('Successfully');
        },
        function(error)
        {
            SharedMethods.keepModalOpen($scope);
            $scope.Link="";
            SharedMethods.showValidationErrors($scope,error);
        })

        .finally(function()
        {
            $scope.editing = false;
        });

    };

    $scope.cancel=function()
    {
        $uibModalInstance.close();
    }
    
    
});



/*--==== Advert Details Controller ====--*/

publisherAdvertApp.controller('AdvertDetailsController', function ($scope,SharedMethods, $stateParams, $rootScope,$uibModal, $stateParams, Advert) {
    $scope.attachment=false;
    $scope.Advert = Advert;
    if( $scope.Advert.attachment==null || $scope.Advert.attachment==undefined || $scope.Advert.attachment=="")
    {
        $scope.attachment=false;
    }
    else
    {
         $scope.attachment=true;
    }
    $scope.returnState=$stateParams.returnState;
  
    $rootScope.title =  "Advert | Brimav";
    $rootScope.PageHeading = " Advert ";
    $rootScope.SubPageHeading = "Details" ;

});
 

