﻿/*-----====== ADMIN PROFILE CONTROLLER =====----*/

/*---==== Profile TemplateController =====--*/

publisherProfileApp.controller('ProfileTemplateController', function ($rootScope,$cacheFactory, $scope, ShareData, Publisher,PublisherProfileService,SharedMethods) {
    $scope.Publisher = Publisher;
    $rootScope.headerClass = "normal-page-title";
    $scope.Black=true;




    //Get publisher Picture
    if ($scope.Publisher.user.picture == null || $scope.Publisher.user.picture == undefined || $scope.Publisher.user.picture == "")
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisher.user.picture;
         $scope.Black=false;
    }


/*---- Avatar Upload &  preview ----*/

   $scope.stepsModel=[];
    $scope.imageUpload = function(event)
    {
        
          var avatar = event.target.files[0]; //FileList object

        if(avatar.type.match ("image/jpeg") || avatar.type.match ("image/png") || avatar.type.match ("image/gif") )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(avatar);
             $scope.avatar=avatar;
             var uploadAvatar=PublisherProfileService.putUploadavatar($scope.avatar);
             uploadAvatar.then(function(response)
             {
                $rootScope.Avatar=response.data;
                  var httpCache=$cacheFactory.get('$http');
                  httpCache.remove('api/publisherprofileAPI/shared');
             },
             function(error)
             {
                SharedMethods.createErrorToast('File too large(max:1.5mb)');
             })
        } 
         else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
            $rootScope.Avatar=e.target.result;
        });
    };

});


/*---==== Basic Details Controller =====--*/

publisherProfileApp.controller('BasicDetailsController', function ($scope,$templateCache, $rootScope, ShareData) {
    $rootScope.title = $scope.Publisher.user.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Basic Details";
    $rootScope.activeView = "basic";
    
   
});


/*---====Edit Basic Details Controller =====--*/

publisherProfileApp.controller("EditBasicProfileController", function ($scope,$cacheFactory, PublisherProfileService, SharedMethods, $rootScope, $state) {
    
    $scope.editing = false;

    $rootScope.title = "Edit Profile | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit Details";
    $rootScope.activeView = "basic";
 
    $scope.save = function () {
        $scope.editing = true;
        $scope.newPublisherdetails = {
            username: $scope.Publisher.user.username,
            email: $scope.Publisher.user.email,
            phonenumber: $scope.Publisher.user.phonenumber,
            
        };


        var PutPublisher = PublisherProfileService.putEditpublisherBasicDetails($scope.newPublisherdetails);
        PutPublisher.then(function (pl) {
             var httpCache=$cacheFactory.get('$http');
            httpCache.remove('api/publisherprofileAPI/shared');
            //$rootScope.$broadcast("updateShareuserPublisher", $scope.newPublisherdetails);

            $state.go('profile.basic', {}, { reload: true });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong>  updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});


/*---==== Account Details Controller =====--*/

publisherProfileApp.controller('AccountDetailsController', function ($scope, $rootScope, ShareData) {
    
    $rootScope.title = $scope.Publisher.user.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Account Details";
    $rootScope.activeView = "account";
});


/*---====Edit Account Details Controller =====--*/

publisherProfileApp.controller("EditAccountProfileController", function ($scope, PublisherProfileService, SharedMethods, $rootScope, $state) {
    
    $scope.editing = false;

    $rootScope.title = "Edit Profile | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit  Details";
    $rootScope.activeView = "account";
    $scope.Banks=Banks ;
 
    $scope.update = function () {

        $scope.editing = true;
        $scope.Publisherdetails = {
            banker: $scope.Publisher.banker,
            accountname: $scope.Publisher.accountname,
            accountnumber: $scope.Publisher.accountnumber,
        };


        var PutPublisher = PublisherProfileService.putEditpublisherAccountDetails($scope.Publisherdetails);
        PutPublisher.then(function (pl) {
           
            $state.go('profile.accountdetails', {}, { reload: true });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong>  updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});


 /*---====Change Password Controller =====--*/   

publisherProfileApp.controller('ChangePasswordController', function ($scope, PublisherProfileService, ShareData, $rootScope, $state, SharedMethods) {
    $rootScope.title = "Change Password | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.PageHeading = "Profile";
    $rootScope.headerClass = "normal-page-title";
    $rootScope.InnerHeading = "Change Password";
    $rootScope.activeView = "password";

    $scope.editing = false;

    $scope.change = function () {
        $scope.editing = true;
    
        $scope.passwordetails = {
            current_password: $scope.Currentpassword,
            password: $scope.Password,
            password_confirmation: $scope.Confirmpassword
        };
    
        var putpassword =PublisherProfileService.changepassword($scope.passwordetails)
        putpassword.then(function (pl) {
            $scope.Currentpassword = "";
            $scope.Password = "";
            $scope.Confirmpassword = "";
            $scope.fieldErrors = null;
            SharedMethods.createSuccessToast('<strong>Password</strong>  changed successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});


  var Banks=["Eco Bank","First Bank","GT Bank", "UBA","Zenith Bank"];