﻿		/*---====== publisher HomeController===-----*/
publisherHomeApp.controller('HomeController', function ($scope, $state,HomeService, $rootScope) {

    $rootScope.title = "Home | Brimav";

    getAdvert();

    function getAdvert()
    {
    	var fetchAdvert=HomeService.getAdvertwithoutlink();
    	fetchAdvert.then(function(response)
    	{
    		$scope.Adverts=response;
            
    	},
    	function(error)
    	{

    	})
    };

    $scope.go=function(id)
    {
    	$state.go ('details',{id:id,returnState:'home'})
    }

});


