/*==================== Payment Controller ====================*/
// PaymetListController
publisherPaymentApp.controller('PaymentsHomeController', function ($scope,SharedMethods, $stateParams, publisherPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "Payments";
    $rootScope.SubPageHeading = null;
    $scope.viewState=$stateParams.viewState;

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
        
        var promiseGetPayments = publisherPaymentService.getAllPayments();
        promiseGetPayments.then(function (pl) {
            $scope.Payments = pl;
            
        },
        function (errorPl) {
            $scope.error = errorPl;
        })
        .finally(function () {
            $scope.contentLoading = false;
        });
    }
    /*$scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };*/
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };

    /*--Payment request---*/
    $scope.requestPayment=function()
    {
        $scope.requesting=true;
        $scope.data=
        {
            'amount':$scope.Amount
        };
        var request=publisherPaymentService.requestPayment($scope.data);
        request.then(function()
        {
            var httpCache=$cacheFactory.get('$http');
            httpCache.remove('api/publisherprofileAPI/shared');
            SharedMethods.createSuccessToast("Successfull, payment will be processed within 24hrs");
            jQuery('#payment').modal('hide');
        },
        function(error)
        {
            SharedMethods.showValidationErrors($scope,error);
        })

        .finally(function()
        {
            $scope.requesting=false;
        })
    }
});




/*--==== Publisher Details Controller ====--*/

publisherPaymentApp.controller('PublisherDetailsController', function ($scope, $state, $stateParams, SharedMethods,publisherPaymentService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Payments";
    $rootScope.activeView="publisherdetails";

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = "data:image/jpeg;base64," + $scope.Publisherbasicdetails.picture;
        }

        $scope.reverse=false; // for fa plus and minus control
        $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

    $scope.save=function()
    {
        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };

         var putbalance = publisherPaymentService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            
            $state.go('publisherdetails.account');
            $scope.reverse=false;
            $scope.collapse=true;
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };

});


/*--==== Publisher Account Details Controller ====--*/
publisherPaymentApp.controller('AccountDetailsController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
    $rootScope.activeView="account";
   
   
});



/*--==== Publisher Payments Details Controller ====--*/
publisherPaymentApp.controller('PublisherPaymentsHomeController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
});

 