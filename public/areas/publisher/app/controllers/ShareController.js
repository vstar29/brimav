﻿/*-------========== PUBLISHER SHARED CONTROLLER =======-------*/

/*--==== Get  Logged In-User Details ====--*/
publisherShareApp.controller('PublisherShareController', function ($scope,$state, $window, $rootScope,ShareService, ShareData) {
    
    loadPublisherInfo();
    function loadPublisherInfo() {
        
        var promiseGetPublisherInfo = ShareService.getLoggedInPublisher();

        promiseGetPublisherInfo.then(function (pl)
        {
         var Publisherdetails = pl.data;
          $scope.Publisher = pl.data.Publisher;
          $scope.Notifications=pl.data.Notifications
          $scope.Avatar = $scope.Publisher.user.picture;
         
        },
        function (errorPl) {
            $scope.error = errorPl;
           
            
        });
    };

     $scope.markRead=function()
    {
        if($scope.Notifications.length>0)
        {
           var markRead = ShareService.markRead();

          markRead.then(function (pl)
          {

          });
        }
       
    }

    

});

  
