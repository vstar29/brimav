/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var publisherShareApp = angular.module('PublisherShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','angularMoment','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', ["ngToast", "$uibModalStack", function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
       
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {
           
            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }

        } 
        else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }

    /* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
    
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="glyphicon glyphicon-ok jp-white alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning  alert-danger margin-right-04"></i>' + cont
        });
    }
    
    /* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }



}])
.config(["$urlRouterProvider", "$stateProvider", "$locationProvider", "$breadcrumbProvider", "ngToastProvider", "$uibModalProvider", "$urlMatcherFactoryProvider", "$interpolateProvider", function ($urlRouterProvider, $stateProvider, $locationProvider, $breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider) {
    
    
    //modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'right',
        timeout: 4000,
        maxNumber: 3,
        animation: 'fade',
        dismissButton: true,
    });

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    $breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/publisher/home" target="_self">Home&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp; &#xf18e &nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });

    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
    
    //make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    //$urlRouterProvider.otherwise('/404');

    $locationProvider.html5Mode(true);
}])


/*J PLAYER DIRECTIVE*/


.directive('jplayer', function()
{
    return{
        restrict:'E',
        templateUrl:'publisheradvert/player',
        scope:{Advert:'=advertdetails',
                audio:'=song'},
        link:function(scope,elem,attr)
        {
            
            scope.onplay=false;
            scope.onpause=false;
            var player=jQuery('#jplayer'),
            fixmp4,
            fixmp4_id, 
            ignore_timeupdate,
            cssSelectorAncestor= "#jp_container",
            myControl =
            {
                progress: jQuery("#jp-progress-slider"),
                volume: jQuery("#jp-volume-slider")
            }
        
            player.jPlayer
            ({ 
                
                swfPath: 'scripts/theme/jquery.jplayer.swf',
                solution: 'html, flash',
                supplied: 'mp3',
                preload: 'metadata',
                volume: 0.8,
                muted: false,
                wmode: "window",
                keyEnabled: true,
                cssSelectorAncestor: "#jp_container",

                ready: function () 
                {
                    player.jPlayer("setMedia", {mp3: attr.audio})
                },

               
                timeupdate: function(event) {

                myControl.progress.slider("option","value", event.jPlayer.status.currentPercentAbsolute);
            },

            volumechange: function(event) 
            {
                    if(event.jPlayer.options.muted) 
                    {
                        myControl.volume.slider("value", 0);
                    } 
                    else {
                        myControl.volume.slider("option","value", event.jPlayer.options.volume);
                    }
            }
                
            });
            
             var playerData = player.data("jPlayer");

            /*-------Create the progress slider control-----*/
            myControl.progress.slider({
            animate: "fast",
            max: 100,
            range: "min",
            step: 0.1,
            value : 0,
            slide: function(event, ui) {
                var sp = playerData.status.seekPercent;
                if(sp > 0) {
                
                    if(fixmp4) {
                        ignore_timeupdate = true;
                        clearTimeout(fixmp4_id);
                        fixmp4_id = setTimeout(function() {
                            ignore_timeupdate = false;
                        },1000);
                    }

                    // Move the play-head to the value and factor in the seek percent.
                    player.jPlayer("playHead", ui.value * (100 / sp));
                } else {
                    
                    // Create a timeout to reset this slider to zero.
                    setTimeout(function() {
                        myControl.progress.slider("option","value", 0);
                    }, 0);
                }
            }
        });

        /*---Create the volume slider control---*/
        myControl.volume.slider({
            animate: "fast",
            max: 1,
            range: "min",
            step: 0.01,
            value : $.jPlayer.prototype.options.volume,
            slide: function(event, ui) {
                player.jPlayer("option", "muted", false);
                player.jPlayer("option", "volume", ui.value);
            }
        });
            
            
       scope.play=function()
       {
         scope.onplay=true;
         scope.onpause=false;
       }

        scope.pause=function()
       {
         player.jPlayer('pause');
         scope.onpause=true;
         scope.onplay=false;
       }

      
        
        }
        
    };
})


/*MULTI  SELECT FOR BANNER TYPE DIRECTICE */
.directive('multiselect', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        template:
                "<div class='multi_select'>" +
                   
                    "<ul class='  ' >" +
                        "<li  data-ng-repeat='option in options'><a data-ng-click='toggleSelectItem(option)' class='pointer'><span data-ng-class='getClassName(option)'></span> [[option.name]]  </a></li>" +
                    "</ul>" +
                "</div>",

        controller: ["$scope", function ($scope) {

    

            $scope.toggleSelectItem = function (option) {
                  var intIndex = -1;

                angular.forEach($scope.model, function (item, index) {
                         
                    if (index == option.id ) {
                           
                        intIndex = index;
                    }

                });

                if (intIndex >0) {
                   delete $scope.model[option.id];
                  
                 
                }
                else {

                    $scope.model[option.id]=option.name;

                }
            };

           $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.id) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };
        }]
    };

}])


.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})


.run(["$rootScope", "SharedMethods", function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
}]);




/*===================== HOME SPA ====================*/
var publisherHomeApp = angular.module('PublisherHomeModule', ['PublisherShareModule','PublisherAdvertModule'])
 
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
    //debugger; //break point
   
    $urlRouterProvider.otherwise('/publisher/home');
   
    $stateProvider
    .state('home', {
        url: '/publisher/home',
        templateUrl: 'publisherhome/home',
        controller: 'HomeController'
    });
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.homePage = true;
    $rootScope.advertview = false;
}]);





    /*===================== PROFILE SPA ====================*/
   var publisherProfileApp = angular.module('PublisherProfileModule', ['PublisherShareModule'])
    .config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/publisher/profile');
        
        $stateProvider
        // Template for the Profile Pages
        .state('profile', {
            abstract: true,
            url: '/publisher/profile',
            templateUrl: 'publisherprofile/home',
            controller: 'ProfileTemplateController',
            resolve: {
                Publisher: ["PublisherProfileService", function (PublisherProfileService) {
                    return PublisherProfileService.getPublisherBasicDetails(); 
                }]
            }
        })

        //Profile (Display Publisher Profile)
        .state('profile.basic', {
            url: '',
            templateUrl: 'publisherprofile/basicdetails',
            controller: 'BasicDetailsController',
            resolve: {
                Publisher: ["Publisher", function (Publisher) {
                    return Publisher;
                }]
            },
            ncyBreadcrumb: {
            label: 'Profile'
        }
            
        })
       
        // Edit mode for Publisher Basic Details
        .state('profile.editbasic', {
            url: '/edit',
            templateUrl: 'publisherprofile/editbasic',
            controller: 'EditBasicProfileController',
            resolve: {
                Publisher: ["Publisher", function (Publisher) {
                    return Publisher;
                }]
            },
            ncyBreadcrumb: {
                label: 'Edit Details',
                parent: 'profile.basic'
            }
                
        })


        //Profile (Display Publisher Account Details)
        .state('profile.accountdetails', {
            url: '/account',
            templateUrl: 'publisherprofile/accountdetails',
            controller: 'AccountDetailsController',
            resolve: {
                Publisher: ["Publisher", function (Publisher) {
                    return Publisher;
                }]
            },
            ncyBreadcrumb: {
            label: 'Account',
            parent:'profile.basic'
        }
            
        })
       
        // Edit mode for Publisher Account Details
        .state('profile.editaccountdetails', {
            url: '/edit/account',
            templateUrl: 'publisherprofile/editaccount',
            controller: 'EditAccountProfileController',
            resolve: {
                Publisher: ["Publisher", function (Publisher) {
                    return Publisher;
                }]
            },
            ncyBreadcrumb: {
                label: 'Edit Details',
                parent: 'profile.basic'
            }
                
        })



         // Change Password
        .state('profile.changepassword', {
            url: '/password',
            templateUrl: 'publisherprofile/changepassword',
            controller: 'ChangePasswordController',
            resolve: {
                Publisher: ["Publisher", function (Publisher) {
                    return Publisher;
                }]
            },
            ncyBreadcrumb: {
                label: 'Change Password',
                parent: 'profile.basic'
            }
        })  

    }])
    .run(["$rootScope", function ($rootScope) {
    $rootScope.profilePage = true;
    
   }]);



/*===================== ADVERT SPA ========================*/
var publisherAdvertApp = angular.module('PublisherAdvertModule',['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {

    var startSymbol= $interpolateProvider.startSymbol('[[');
    var endSymbol= $interpolateProvider.endSymbol(']]');
     denormalizeTemplate = (startSymbol == '{{' || endSymbol  == '}}')
            ? identity
            : function denormalizeTemplate(template) {
              return template.replace(/\{\{/g, startSymbol).replace(/}}/g, endSymbol);
        },
        NG_ATTR_BINDING = /^ngAttr[A-Z]/;
    $urlRouterProvider.otherwise('/publisher/advert');

    $stateProvider
     // Get List of adverts
    .state('advert', {
        url: '/publisher/advert',
        abstract:true,
        templateUrl: 'publisheradvert/home',
        controller: 'AdvertTemplateController',
        resolve:{
            
            UnconfirmedAdverts:["PublisherAdvertService", function(PublisherAdvertService)
            {
                return PublisherAdvertService.getUncompletedAdverts();
            }],

            ConfirmedAdverts:["PublisherAdvertService", function(PublisherAdvertService)
            {
               return PublisherAdvertService.getCompletedAdverts();
            }]
        }
        
    })

    // Get List of uncompleted adverts
    .state('advert.unconfirmed', {
        url: '',
        params: { returnState: "advert" },
        templateUrl: 'publisheradvert/uncompleted',
        controller: 'UnConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'Advert'
        },
        resolve:
        {
            UnconfirmedAdverts:["UnconfirmedAdverts", function(UnconfirmedAdverts)
            {
                return UnconfirmedAdverts;
            }]
        }
    })
       
       

    // Get List of completed adverts
    
     .state('advert.confirmed', {
        url: '/published',
        templateUrl: 'publisheradvert/completed',
        controller: 'ConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'completed',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            ConfirmedAdverts:["ConfirmedAdverts", function(ConfirmedAdverts)
            {
                return ConfirmedAdverts;
            }]
        }
        
    })


    // Advert Details
    .state('details' , {
        url: '/publisher/advert/details/:id',
        params:{returnState:'advert.unconfirmed',id:null},
        templateUrl: 'publisheradvert/details',
        controller: 'AdvertDetailsController',
        ncyBreadcrumb: {
            label: 'Details',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            Advert:["PublisherAdvertService", "$stateParams", function(PublisherAdvertService, $stateParams)
            {
                return PublisherAdvertService.getAdvertbyid($stateParams.id);
            }]

        }
    });

       
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.advertPage = true;
}]);


/*===================== PAYMENTS SPA ====================*/
var publisherPaymentApp = angular.module('PublisherPaymentModule', ['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/publisher/payment');

    $stateProvider
    //Payment's Homepage 
    .state('payment', {
        url: '/publisher/payment',
        templateUrl: 'publisherpayment/home',
        controller: 'PaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Payment'
        }
    })


}])
.run(["$rootScope", function($rootScope)
{
$rootScope.paymentPage=true;
}]);



/*===================== BANNER SPA ====================*/


var publisherBannerApp=angular.module('PublisherBannerModule',['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/banner');

    $stateProvider.

    state('banner',
    {
        url:'/publisher/banner',
        templateUrl:'publisherbanner/home',
        controller:'PublisherBannerHomeController',
        
         ncyBreadcrumb:
        {
            label:'banner'
        }
    })

    .state('create',{

        url:'/publisher/banner/create',
        templateUrl:'publisherbanner/create',
        controller:'BannerCreatecontroller',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'banner'
        }
    })

    .state('edit',
    {
        url:'/publisher/banner/edit/:id',
        params:{id:null},
        templateUrl:'publisherbanner/edit',
        controller:'BannerEditcontroller',
        resolve:
        {
            Banner:["PublisherBannerService", "$stateParams", function(PublisherBannerService,$stateParams)
            {
                
                return PublisherBannerService.getBannerbyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'banner'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.bannerPage=true;
}]);



/*===================== SOCIAL MEDIUM SPA ====================*/


var publisherSocialApp=angular.module('PublisherSocialModule',['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/social');

    $stateProvider.

    state('social',
    {
        url:'/publisher/social',
        templateUrl:'publishersocial/home',
        controller:'SocialHomeController',
        
         ncyBreadcrumb:
        {
            label:'social'
        }
    })

    .state('create',{

        url:'/publisher/social/create',
        templateUrl:'publishersocial/create',
        controller:'SocialCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'social'
        }
    })

    .state('edit',
    {
        url:'/publisher/social/edit/:id',
        params:{id:null},
        templateUrl:'publishersocial/edit',
        controller:'SocialEditController',
        resolve:
        {
            Social:["PublisherSocialService", "$stateParams", function(PublisherSocialService,$stateParams)
            {
                
                return PublisherSocialService.getSocialbyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'social'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.socialPage=true;
}]);




/*===================== WEBSITE MEDIUM SPA ====================*/


var publisherWebsiteApp=angular.module('PublisherWebsiteModule',['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/website');

    $stateProvider.

    state('website',
    {
        url:'/publisher/website',
        templateUrl:'publisherwebsite/home',
        controller:'WebsiteHomeController',
        
         ncyBreadcrumb:
        {
            label:'website'
        }
    })

    .state('create',{

        url:'/publisher/website/create',
        templateUrl:'publisherwebsite/create',
        controller:'WebsiteCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'website'
        }
    })

    .state('edit',
    {
        url:'/publisher/website/edit/:id',
        params:{id:null},
        templateUrl:'publisherwebsite/edit',
        controller:'WebsiteEditController',
        resolve:
        {
            Website:["PublisherWebsiteService", "$stateParams", function(PublisherWebsiteService,$stateParams)
            {
                
                return PublisherWebsiteService.getWebsitebyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'website'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.websitePage=true;
}]);


/*===================== TV MEDIUM SPA ====================*/


var publisherTvApp=angular.module('PublisherTvModule',['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/tv');

    $stateProvider.

    state('tv',
    {
        url:'/publisher/tv',
        templateUrl:'publishertv/home',
        controller:'TvHomeController',
        
         ncyBreadcrumb:
        {
            label:'website'
        }
    })

    .state('create',{

        url:'/publisher/tv/create',
        templateUrl:'publishertv/create',
        controller:'TvCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'tv'
        }
    })

    .state('edit',
    {
        url:'/publisher/tv/edit/:id',
        params:{id:null},
        templateUrl:'publishertv/edit',
        controller:'TvEditController',
        resolve:
        {
            Tv:["PublisherTvService", "$stateParams", function(PublisherTvService,$stateParams)
            {
                
                return PublisherTvService.getTvbyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'tv'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.tvPage=true;
}]);




/*===================== PROGRAMMES SPA ====================*/


var publisherProgrammesApp=angular.module('PublisherProgrammesModule',['PublisherShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/publisher/programme');

    $stateProvider.

    state('programme',
    {
        url:'/publisher/programme',
        templateUrl:'publisherprogramme/home',
        controller:'ProgrammesHomeController',
         ncyBreadcrumb:
        {
            label:'programme'
        }
    })

    .state('create',{

        url:'/publisher/programme/create',
        templateUrl:'publisherprogramme/create',
        controller:'ProgrammeCreateController',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'programme'
        }
    })

    .state('edit',
    {
        url:'/publisher/programme/edit/:id',
        params:{id:null},
        templateUrl:'publisherprogramme/edit',
        controller:'ProgrammeEditController',
        resolve:
        {
            Programme:["PublisherProgrammesService", "$stateParams", function(PublisherProgrammesService,$stateParams)
            {
                
                return PublisherProgrammesService.getProgrammebyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'tv'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.programmePage=true;
}]);



/*-------========== PUBLISHER SHARED SERVICE =======-------*/

/*--==== Get  Logged Inuser Details ====--*/

publisherShareApp.service('ShareService', ["$http", function ($http) {
    //Get current admin info
    this.getLoggedInPublisher = function () {

        var request=$http({
            url:'api/publisherprofileAPI/shared',
            method:'get',
            cache:true
        });

        return request;

    };


    /*--===== Mark notification as read ====--*/

    this.markRead = function () {
        var request=$http({
            url:'api/markread',
            method:'get',
        });

        return request;
       
    };
}])



	/*---====== publisher Home Service===-----*/

publisherHomeApp.service('HomeService',["$http", function($http)
{
	 var advertBase = "api/publisheradvertAPI";

	/*--===== Get Advert without link ====--*/

	this.getAdvertwithoutlink=function()
	{
		return $http.get(advertBase +'/advertswithoutlink').then(function(response)
		{
			return response.data
		},
		function(error)
		{
			var advert={};
			return advert;
		})
	}
}])

 /*----====== PUBLISHER ADVERTS SERVICE ======----*/

publisherAdvertApp.service("PublisherAdvertService", ["$http", "$rootScope", "SharedMethods", function ($http, $rootScope, SharedMethods) {

    var advertBase = "api/publisheradvertAPI";
    
  
    /*--===== Get completed Adverts ====--*/
    
    this.getCompletedAdverts = function () {
        return $http.get(advertBase+'/completedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get Uncompleted Adverts ====--*/
    
    this.getUncompletedAdverts = function () {
        return $http.get(advertBase+'/uncompletedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };


    /*--===== Get  Advert by ID ====--*/

    this.getAdvertbyid = function (id) {
        return $http.get(advertBase +'/advertbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            //SharedMethods.createErrorToast("Problem getting Advert details");
            var advert = {};
            return advert;
        });
    };

    /*--===== Get  Advertlink by a Publisher ====--*/

    this.getAdvertlink = function (id) {
        return $http.get(advertBase +'/advertlink/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var advert = {};
            return advert;
        });
    };




     /*--===== Put Advert Link ====--*/

    this.putadvertlink = function (linkdetails) {
        var request = $http({
            method: "put",
            url: advertBase + "/insertadvertlink/",
            data:linkdetails
        });
        return request;
    };


}]);

/*----====== PUBLISHER PROFILE SERVICE ======----*/

 
publisherProfileApp.service('PublisherProfileService', ["$http", "SharedMethods", function ($http, SharedMethods) {

    var profileBase = "api/publisherprofileAPI";
   

    /*--===== Get  Basic details ====--*/
   
    this.getPublisherBasicDetails = function () {
        return $http.get(profileBase).then(function (response) {
            return response.data;
            
        },
       function (error) {
           var publisher = {};
           return publisher;
       });
    };
    

    /*--===== Edit Basic details ====--*/
  
  this.putEditpublisherBasicDetails=function(PublisherDetails)
  {
   
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editpublisherbasicdetails",
        data:PublisherDetails
    });
    return request;
  };



   /*--===== Change Avatar ====--*/
  
  this.putUploadavatar=function(avatar)
  {

        var request = $http({
            method: "post",
            url:profileBase +"/uploadavatar",
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            formData.append("avatar" ,avatar);              
            return formData;            
            },
        });
        return request;
    };



    /*--===== Edit Account details ====--*/
  
  this.putEditpublisherAccountDetails=function(PublisherDetails)
  {
   
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editpublisheraccountdetails",
        data:PublisherDetails
    });
    return request;
  };


    /*--===== Change Password ====--*/
    
    this.changepassword = function (Passworddetails) {
      
        var request = $http({
            method: 'put',
            url: profileBase + "/changepassword",
            data: Passworddetails
        });
        return request;
    };

}])
publisherPaymentApp.service('publisherPaymentService', ["$http", "SharedMethods", function($http,SharedMethods)
{

var payBase="api/publisherpaymentAPI";



/*--===== Get  All  Payments ====--*/

    this.getAllPayments = function () {
        return $http.get(payBase).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting payment details");
            var Payments = {};
            return Payments;
        });
    };

    /*---Request for Payments===*/
    this.requestPayment=function(data)
    {
        var request=$http(
        {
            'url':payBase+'/requestpayment',
             method:'post',
             data:data
        });
        return request;
    }

  
}]);
			/*====---
 publisher BANNER  SERVICE--=====*/


 publisherBannerApp.service('PublisherBannerService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var bannerBase="api/publisherbannerAPI";
	 


	/*--get all  banner s---*/
	this.getAll=function()
	{
		return $http.get(bannerBase);
	};


	/*--get  banner  by id---*/
	this.getBannerbyid=function(id)
	{
		
		return $http.get(bannerBase +'/getbannerbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting banner  details");
			var BANNER={};
			return BANNER;
		})
	};


	/*--create banner ---*/
	this.postBanner=function(data)
	{
		var request=$http(
			{
				url:bannerBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit banner---*/
	this.putBanner=function(data)
	{
		var request=$http(
			{
				url:bannerBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete banne---*/

    this.deleteBanner = function (id) {
        var request=$http(
        {
            url:bannerBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

   /*--get all  banner typess---*/
	this.getBannerTypes=function()
	{
		return $http.get(bannerBase+'/getbannertypes');
	};


	/*-get all publisher websites--*/
	this.getWebsites=function()
	{
		return $http.get('api/publisherwebsiteAPI').then(function(pl)
		{
			return pl.data;
		},function(error)
		{
			return {};
		});
	};
   
}])
			/*====---publisher Medium SERVICE--=====*/


/*-----social medium--*/
 publisherSocialApp.service('PublisherSocialService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var socialBase="api/publishersocialAPI";
	 


	/*--get all  socials---*/
	this.getAll=function()
	{
		return $http.get(socialBase);
	};


	/*--get  social  by id---*/
	this.getSocialbyid=function(id)
	{
		
		return $http.get(socialBase +'/getsocialbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting social medium  details");
			var social={};
			return social;
		})
	};


	/*--create Social ---*/
	this.postSocial=function(data)
	{
		var request=$http(
			{
				url:socialBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit social---*/
	this.putSocial=function(data)
	{
		var request=$http(
			{
				url:socialBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete social---*/

    this.deleteSocial = function (id) {
        var request=$http(
        {
            url:socialBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };
   
}])


 /*-----------------------------------------------------------------------------------*/


/*-----website medium--*/
 publisherWebsiteApp.service('PublisherWebsiteService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var websiteBase="api/publisherwebsiteAPI";
	 


	/*--get all  websites---*/
	this.getAll=function()
	{
		return $http.get(websiteBase);
	};


	/*--get  social  by id---*/
	this.getWebsitebyid=function(id)
	{
		
		return $http.get(websiteBase +'/getwebsitebyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem : Refresh page");
			var social={};
			return social;
		})
	};


	/*--add new websites ---*/
	this.postWebsite=function(data)
	{
		var request=$http(
			{
				url:websiteBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit websites---*/
	this.putWebsite=function(data)
	{
		var request=$http(
			{
				url:websiteBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete website---*/

    this.deleteWebsite = function (id) {
        var request=$http(
        {
            url:websiteBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

 
   
}])




 /*-----------------------------------------------------------------------------------*/


/*-----tv-radio medium--*/
 publisherTvApp.service('PublisherTvService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var tvBase="api/publishertvAPI";
	 


	/*--get all  station---*/
	this.getAll=function()
	{
		return $http.get(tvBase);
	};


	/*--get  station  by id---*/
	this.getTvbyid=function(id)
	{
		
		return $http.get(tvBase +'/gettvbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem : Refresh page");
			var social={};
			return social;
		})
	};



	
	/*--add new station ---*/
	this.postTv=function(data)
	{
		var request=$http(
			{
				url:tvBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit tv---*/
	this.putTv=function(data)
	{
		var request=$http(
			{
				url:tvBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete tv---*/

    this.deleteTv = function (id) {
        var request=$http(
        {
            url:tvBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

 
}])



 /*-----------------------------------------------------------------------------------*/


/*-----program service--*/
 publisherProgrammesApp.service('PublisherProgrammesService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var tvBase="api/publishertvAPI";
	var programmeBase="api/publisherprogrammeAPI";
	 


	/*--get all  programmes---*/
	this.getAll=function()
	{
		return $http.get(programmeBase);
	};

	/*--get all  station---*/
	this.getTvs=function()
	{
		return $http.get(tvBase);
	};



	/*--get  programme  by id---*/
	this.getProgrammebyid=function(id)
	{
		
		return $http.get(programmeBase +'/getprogrammebyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem : Refresh page");
			var social={};
			return social;
		})
	};




   /*--add new programme ---*/
	this.postProgramme=function(data)
	{
		var request=$http(
			{
				url:programmeBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit programme---*/
	this.putProgramme=function(data)
	{
		var request=$http(
			{
				url:programmeBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete programme---*/

    this.deleteProgramme = function (id) {
        var request=$http(
        {
            url:programmeBase+'/delete/'+id,
            method:'get'
        });
        return request;
     
   };

 
   
}])
/*-------========== PUBLISHER SHARED CONTROLLER =======-------*/

/*--==== Get  Logged In-User Details ====--*/
publisherShareApp.controller('PublisherShareController', ["$scope", "$state", "$window", "$rootScope", "ShareService", "ShareData", function ($scope,$state, $window, $rootScope,ShareService, ShareData) {
    
    loadPublisherInfo();
    function loadPublisherInfo() {
        
        var promiseGetPublisherInfo = ShareService.getLoggedInPublisher();

        promiseGetPublisherInfo.then(function (pl)
        {
         var Publisherdetails = pl.data;
          $scope.Publisher = pl.data.Publisher;
          $scope.Notifications=pl.data.Notifications
          $scope.Avatar = $scope.Publisher.user.picture;
         
        },
        function (errorPl) {
            $scope.error = errorPl;
           
            
        });
    };

     $scope.markRead=function()
    {
        if($scope.Notifications.length>0)
        {
           var markRead = ShareService.markRead();

          markRead.then(function (pl)
          {

          });
        }
       
    }

    

}]);

  

		/*---====== publisher HomeController===-----*/
publisherHomeApp.controller('HomeController', ["$scope", "$state", "HomeService", "$rootScope", function ($scope, $state,HomeService, $rootScope) {

    $rootScope.title = "Home | Brimav";

    getAdvert();

    function getAdvert()
    {
    	var fetchAdvert=HomeService.getAdvertwithoutlink();
    	fetchAdvert.then(function(response)
    	{
    		$scope.Adverts=response;
            
    	},
    	function(error)
    	{

    	})
    };

    $scope.go=function(id)
    {
    	$state.go ('details',{id:id,returnState:'home'})
    }

}]);




/*-------========== publisher ADVERTS CONTROLLER =======-------*/
/*--==== Adert Template Controller ====--*/

publisherAdvertApp.controller('AdvertTemplateController', ["$rootScope", "PublisherAdvertService", "$uibModal", "$state", "$scope", function ($rootScope,PublisherAdvertService,$uibModal, $state, $scope) {
    $rootScope.headerClass = "normal-page-title";

    //Set default filter, sort parameters
    $scope.Filterview = "unconfirmed";
    $scope.sortKey = "date";
    $scope.reverse=false;
    
    $rootScope.advertview = true;
    
   
     $scope.$watch('Filterview', function (filterkey) {
        $state.go('advert.'+filterkey); // Go to the state selected
        $rootScope.Filterview=filterkey; //set the Filter to the param passed
    });

    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }


    // put advert link 

    $scope.putadvertlink = function (id,returnState) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var getLink=PublisherAdvertService.getAdvertlink(id);
        getLink.then(function(response)
        {
            $scope.link=response;
            var Details = {
            id : id,
            title: "Insert Link " ,
            class: "btn-adgold",
            returnState:returnState,
            oldlink:$scope.link
        };
        

        var modalInstance = $uibModal.open({
            templateUrl: 'publisheradvert/insertlink',
            controller: 'PutAdvertLinkController',
            resolve: {
                Details: function () {
                    return Details;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
    
        }, function () {
            
        });
            
           
        },

        function(error)
        {

        })
        
    };
}]);
   

/*--==== UnConpleted Advert Controller ====--*/

publisherAdvertApp.controller('UnConfirmedAdvertController', ["$scope", "UnconfirmedAdverts", "$rootScope", "$uibModal", function ($scope, UnconfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Unpublished Adverts";
    $rootScope.SubPageHeading = null;
     $rootScope.Filterview = "unconfirmed";

    $scope.contentLoading = true; // show loading icon
    $scope.UnconfirmedAdvert=UnconfirmedAdverts;
    

    

    }]);


/*--==== Conpleted Advert Controller ====--*/

publisherAdvertApp.controller('ConfirmedAdvertController', ["$scope", "ConfirmedAdverts", "$rootScope", "$uibModal", function ($scope, ConfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Published Adverts";
    $rootScope.SubPageHeading = null;
     $rootScope.Filterview = "confirmed";

    $scope.contentLoading = true; // show loading icon
    $scope.ConfirmedAdverts=ConfirmedAdverts; 
    }]);




/*--==== Put AdvertLink Controller ====--*/

publisherAdvertApp.controller('PutAdvertLinkController', ["$scope", "$state", "$cookies", "Details", "PublisherAdvertService", "$rootScope", "$uibModal", "$uibModalInstance", "SharedMethods", function ($scope,$state,$cookies, Details, PublisherAdvertService, $rootScope, $uibModal, $uibModalInstance, SharedMethods) {


    $scope.class=Details.class;
    $scope.title=Details.title;
    $scope.Link=Details.oldlink;
   

    // put advert link

    $scope.insertadvertlink = function () {
        $scope.editing=true;
        SharedMethods.keepModalOpen($scope);
        $scope.editing = true;

        $scope.linkDetails = {
            id:Details.id,
            link:$scope.Link,
        };

        var PutAdvertLink=PublisherAdvertService.putadvertlink($scope.linkDetails);
        PutAdvertLink.then(function()
        {   
            $uibModalInstance.close();
            $state.go(Details.returnState);
            $cookies.remove('Publisher_profile');

            SharedMethods.createSuccessToast('Successfully');
        },
        function(error)
        {
            SharedMethods.keepModalOpen($scope);
            $scope.Link="";
            SharedMethods.showValidationErrors($scope,error);
        })

        .finally(function()
        {
            $scope.editing = false;
        });

    };

    $scope.cancel=function()
    {
        $uibModalInstance.close();
    }
    
    
}]);



/*--==== Advert Details Controller ====--*/

publisherAdvertApp.controller('AdvertDetailsController', ["$scope", "SharedMethods", "$stateParams", "$rootScope", "$uibModal", "$stateParams", "Advert", function ($scope,SharedMethods, $stateParams, $rootScope,$uibModal, $stateParams, Advert) {
    $scope.attachment=false;
    $scope.Advert = Advert;
    if( $scope.Advert.attachment==null || $scope.Advert.attachment==undefined || $scope.Advert.attachment=="")
    {
        $scope.attachment=false;
    }
    else
    {
         $scope.attachment=true;
    }
    $scope.returnState=$stateParams.returnState;
  
    $rootScope.title =  "Advert | Brimav";
    $rootScope.PageHeading = " Advert ";
    $rootScope.SubPageHeading = "Details" ;

}]);
 


/*==================== Payment Controller ====================*/
// PaymetListController
publisherPaymentApp.controller('PaymentsHomeController', ["$scope", "SharedMethods", "$stateParams", "publisherPaymentService", "$rootScope", function ($scope,SharedMethods, $stateParams, publisherPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "Payments";
    $rootScope.SubPageHeading = null;
    $scope.viewState=$stateParams.viewState;

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
        
        var promiseGetPayments = publisherPaymentService.getAllPayments();
        promiseGetPayments.then(function (pl) {
            $scope.Payments = pl;
            
        },
        function (errorPl) {
            $scope.error = errorPl;
        })
        .finally(function () {
            $scope.contentLoading = false;
        });
    }
    /*$scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };*/
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };

    /*--Payment request---*/
    $scope.requestPayment=function()
    {
        $scope.requesting=true;
        $scope.data=
        {
            'amount':$scope.Amount
        };
        var request=publisherPaymentService.requestPayment($scope.data);
        request.then(function()
        {
            var httpCache=$cacheFactory.get('$http');
            httpCache.remove('api/publisherprofileAPI/shared');
            SharedMethods.createSuccessToast("Successfull, payment will be processed within 24hrs");
            jQuery('#payment').modal('hide');
        },
        function(error)
        {
            SharedMethods.showValidationErrors($scope,error);
        })

        .finally(function()
        {
            $scope.requesting=false;
        })
    }
}]);




/*--==== Publisher Details Controller ====--*/

publisherPaymentApp.controller('PublisherDetailsController', ["$scope", "$state", "$stateParams", "SharedMethods", "publisherPaymentService", "$rootScope", "$stateParams", "PublisherDetails", function ($scope, $state, $stateParams, SharedMethods,publisherPaymentService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Payments";
    $rootScope.activeView="publisherdetails";

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = "data:image/jpeg;base64," + $scope.Publisherbasicdetails.picture;
        }

        $scope.reverse=false; // for fa plus and minus control
        $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

    $scope.save=function()
    {
        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };

         var putbalance = publisherPaymentService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            
            $state.go('publisherdetails.account');
            $scope.reverse=false;
            $scope.collapse=true;
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };

}]);


/*--==== Publisher Account Details Controller ====--*/
publisherPaymentApp.controller('AccountDetailsController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
    $rootScope.activeView="account";
   
   
}]);



/*--==== Publisher Payments Details Controller ====--*/
publisherPaymentApp.controller('PublisherPaymentsHomeController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
}]);

 
/*-----====== ADMIN PROFILE CONTROLLER =====----*/

/*---==== Profile TemplateController =====--*/

publisherProfileApp.controller('ProfileTemplateController', ["$rootScope", "$cacheFactory", "$scope", "ShareData", "Publisher", "PublisherProfileService", "SharedMethods", function ($rootScope,$cacheFactory, $scope, ShareData, Publisher,PublisherProfileService,SharedMethods) {
    $scope.Publisher = Publisher;
    $rootScope.headerClass = "normal-page-title";
    $scope.Black=true;




    //Get publisher Picture
    if ($scope.Publisher.user.picture == null || $scope.Publisher.user.picture == undefined || $scope.Publisher.user.picture == "")
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisher.user.picture;
         $scope.Black=false;
    }


/*---- Avatar Upload &  preview ----*/

   $scope.stepsModel=[];
    $scope.imageUpload = function(event)
    {
        
          var avatar = event.target.files[0]; //FileList object

        if(avatar.type.match ("image/jpeg") || avatar.type.match ("image/png") || avatar.type.match ("image/gif") )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(avatar);
             $scope.avatar=avatar;
             var uploadAvatar=PublisherProfileService.putUploadavatar($scope.avatar);
             uploadAvatar.then(function(response)
             {
                $rootScope.Avatar=response.data;
                  var httpCache=$cacheFactory.get('$http');
                  httpCache.remove('api/publisherprofileAPI/shared');
             },
             function(error)
             {
                SharedMethods.createErrorToast('File too large(max:1.5mb)');
             })
        } 
         else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
            $rootScope.Avatar=e.target.result;
        });
    };

}]);


/*---==== Basic Details Controller =====--*/

publisherProfileApp.controller('BasicDetailsController', ["$scope", "$templateCache", "$rootScope", "ShareData", function ($scope,$templateCache, $rootScope, ShareData) {
    $rootScope.title = $scope.Publisher.user.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Basic Details";
    $rootScope.activeView = "basic";
    
   
}]);


/*---====Edit Basic Details Controller =====--*/

publisherProfileApp.controller("EditBasicProfileController", ["$scope", "$cacheFactory", "PublisherProfileService", "SharedMethods", "$rootScope", "$state", function ($scope,$cacheFactory, PublisherProfileService, SharedMethods, $rootScope, $state) {
    
    $scope.editing = false;

    $rootScope.title = "Edit Profile | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit Details";
    $rootScope.activeView = "basic";
 
    $scope.save = function () {
        $scope.editing = true;
        $scope.newPublisherdetails = {
            username: $scope.Publisher.user.username,
            email: $scope.Publisher.user.email,
            phonenumber: $scope.Publisher.user.phonenumber,
            
        };


        var PutPublisher = PublisherProfileService.putEditpublisherBasicDetails($scope.newPublisherdetails);
        PutPublisher.then(function (pl) {
             var httpCache=$cacheFactory.get('$http');
            httpCache.remove('api/publisherprofileAPI/shared');
            //$rootScope.$broadcast("updateShareuserPublisher", $scope.newPublisherdetails);

            $state.go('profile.basic', {}, { reload: true });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong>  updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);


/*---==== Account Details Controller =====--*/

publisherProfileApp.controller('AccountDetailsController', ["$scope", "$rootScope", "ShareData", function ($scope, $rootScope, ShareData) {
    
    $rootScope.title = $scope.Publisher.user.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Account Details";
    $rootScope.activeView = "account";
}]);


/*---====Edit Account Details Controller =====--*/

publisherProfileApp.controller("EditAccountProfileController", ["$scope", "PublisherProfileService", "SharedMethods", "$rootScope", "$state", function ($scope, PublisherProfileService, SharedMethods, $rootScope, $state) {
    
    $scope.editing = false;

    $rootScope.title = "Edit Profile | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit  Details";
    $rootScope.activeView = "account";
    $scope.Banks=Banks ;
 
    $scope.update = function () {

        $scope.editing = true;
        $scope.Publisherdetails = {
            banker: $scope.Publisher.banker,
            accountname: $scope.Publisher.accountname,
            accountnumber: $scope.Publisher.accountnumber,
        };


        var PutPublisher = PublisherProfileService.putEditpublisherAccountDetails($scope.Publisherdetails);
        PutPublisher.then(function (pl) {
           
            $state.go('profile.accountdetails', {}, { reload: true });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong>  updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);


 /*---====Change Password Controller =====--*/   

publisherProfileApp.controller('ChangePasswordController', ["$scope", "PublisherProfileService", "ShareData", "$rootScope", "$state", "SharedMethods", function ($scope, PublisherProfileService, ShareData, $rootScope, $state, SharedMethods) {
    $rootScope.title = "Change Password | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.PageHeading = "Profile";
    $rootScope.headerClass = "normal-page-title";
    $rootScope.InnerHeading = "Change Password";
    $rootScope.activeView = "password";

    $scope.editing = false;

    $scope.change = function () {
        $scope.editing = true;
    
        $scope.passwordetails = {
            current_password: $scope.Currentpassword,
            password: $scope.Password,
            password_confirmation: $scope.Confirmpassword
        };
    
        var putpassword =PublisherProfileService.changepassword($scope.passwordetails)
        putpassword.then(function (pl) {
            $scope.Currentpassword = "";
            $scope.Password = "";
            $scope.Confirmpassword = "";
            $scope.fieldErrors = null;
            SharedMethods.createSuccessToast('<strong>Password</strong>  changed successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);


  var Banks=["Eco Bank","First Bank","GT Bank", "UBA","Zenith Bank"];
 						/*====---Publisher BANNER  controller===---*/

/*---banner types  home controller---*/

 publisherBannerApp.controller('PublisherBannerHomeController',["PublisherBannerService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(PublisherBannerService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner  | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$scope.deleting=false;


 	loadBanners();
 	function loadBanners()
 	{
 		var getBanners=PublisherBannerService.getAll();
 		getBanners.then(function(pl)
 		{
 			$scope.Banners=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Banners ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletebanner=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Banner " ,
            bodyMessage: "Are you sure you want to delete this Banner?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'BannerDeletecontroller',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---Banner type  create controller---*/
  publisherBannerApp.controller('BannerCreatecontroller',["PublisherBannerService", "Websites", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherBannerService,Websites,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner  | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$rootScope.SubPageHeading='Create';
 	$scope.Websites=Websites;
 	

 	/*--get all banner types and websites from db--*/
 	loadBannerTypes();
 	function loadBannerTypes()
 	{
 		var getBannerTypes=PublisherBannerService.getBannerTypes();
 		getBannerTypes.then(function(pl)
 		{
 			$scope.BannerTypes=pl.data.BannerTypes;
 			$scope.Websites=pl.data.Websites;
 		},
 		function(error)
 		{
 			//SharedMethods.createErrorToast('Problem getting Banner Types');
 		})
 	}
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newbanner={
 		'name':$scope.Name,
 		'type':$scope.Type,
 		'prize':$scope.Prize,
 		'websites':$scope.website
 		};
 		

	 	var postBanner=PublisherBannerService.postBanner($scope.newbanner);
	 	postBanner.then(function(pl)
	 	{
	 		$state.go('banner',{},{reload:true});
	 		SharedMethods.createSuccessToast('Banner created succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---Banner   edit controller---*/
  publisherBannerApp.controller('BannerEditcontroller',["PublisherBannerService", "Banner", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherBannerService,Banner, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner | Brimav';
 	$rootScope.PageHeading='Banner';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Banner=Banner;
 	$scope.Banner2={};
	for(var key in Banner)
 	{
 		$scope.Banner2[key]=$scope.Banner[key];
 	}

 
 	/*--get all banner types and websites from db--*/
 	loadBannerTypes();
 	function loadBannerTypes()
 	{
 		var getBannerTypes=PublisherBannerService.getBannerTypes();
 		getBannerTypes.then(function(pl)
 		{
 			$scope.BannerTypes=pl.data.BannerTypes;
 			$scope.Websites=pl.data.Websites;

 		},
 		function(error)
 		{
 			//SharedMethods.createErrorToast('Problem getting Banner Types');
 		})
 	}

 	$scope.edit=function()
 	{

 		$scope.editing=true

 		$scope.newbanner={
	    'id':$scope.Banner2.id,
 		'name':$scope.Banner2.name,
 		'type':$scope.Banner2.bannertype_id,
 		'prize':$scope.Banner2.prize,
 		'website':$scope.Banner2.website
 		};



       
	 	var putBanner=PublisherBannerService.putBanner($scope.newbanner);
	 	putBanner.then(function(pl)
	 	{
            $state.go('banner',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Banner  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---Banner   delete controller---*/
 publisherBannerApp.controller('BannerDeletecontroller',["PublisherBannerService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherBannerService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner | Brimav';
 	$rootScope.PageHeading='Banner';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteBanner=PublisherBannerService.deleteBanner($scope.DeleteDetails.id);
 		deleteBanner.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('banner', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Banner deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Banner Type');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 }])
 						/*====---Publisher Medium  controller===---*/

/*---social  medium controller---*/

 publisherSocialApp.controller('SocialHomeController',["PublisherSocialService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(PublisherSocialService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials  | Brimav';
 	$rootScope.PageHeading='Social';
 	$scope.deleting=false;


 	loadSocials();
 	function loadSocials()
 	{
 		var getSocials=PublisherSocialService.getAll();
 		getSocials.then(function(pl)
 		{
 			$scope.Socials=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Socials ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletesocial=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Social " ,
            bodyMessage: "Are you sure you want to delete this Social Account?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'SocialDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---SOCIAL  create controller---*/
  publisherSocialApp.controller('SocialCreateController',["PublisherSocialService", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherSocialService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials  | Brimav';
 	$rootScope.PageHeading='Socials';
 	$rootScope.SubPageHeading='Create';
 	$scope.Categories=Categories;
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newsocial={
 		'handler':$scope.Handler,
 		'type':$scope.Type,
 		'prize':$scope.Prize,
 		'followers':$scope.Followers,
 		};
 		

	 	var postSocial=PublisherSocialService.postSocial($scope.newsocial);
	 	postSocial.then(function(pl)
	 	{
	 		$state.go('social',{},{reload:true});
	 		SharedMethods.createSuccessToast('Social medium added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---Social   edit controller---*/
  publisherSocialApp.controller('SocialEditController',["PublisherSocialService", "Social", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherSocialService,Social, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials | Brimav';
 	$rootScope.PageHeading='Socials';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Social=Social;
 	$scope.Categories=Categories;
 	
 

 	$scope.edit=function()
 	{

 		$scope.editing=true

		$scope.newsocial={
		'id':$scope.Social.id,
 		'handler':$scope.Social.handler,
 		'type':$scope.Social.type,
 		'prize':$scope.Social.prize,
 		'followers':$scope.Social.followers};

       
	 	var putSocial=PublisherSocialService.putSocial($scope.newsocial);
	 	putSocial.then(function(pl)
	 	{
            $state.go('social',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Medium  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---Social   delete controller---*/
 publisherSocialApp.controller('SocialDeleteController',["PublisherSocialService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherSocialService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Socials| Brimav';
 	$rootScope.PageHeading='Social';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteSocial=PublisherSocialService.deleteSocial($scope.DeleteDetails.id);
 		deleteSocial.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('social', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Medium deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Medium');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 }])



/*---------------------------------------------------------------------------------------------------------*/



 /*---WEBSITE MEDIUM CONTROLLER---*/

 publisherWebsiteApp.controller('WebsiteHomeController',["PublisherWebsiteService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(PublisherWebsiteService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Websites| Brimav';
 	$rootScope.PageHeading='Website';
 	$scope.deleting=false;


 	loadWebsites();
 	function loadWebsites()
 	{
 		var getWebsites=PublisherWebsiteService.getAll();
 		getWebsites.then(function(pl)
 		{
 			$scope.Websites=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Websites ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletewebsite=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Website " ,
            bodyMessage: "Are you sure you want to delete this Website?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'WebsiteDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---WEBSITES  create controller---*/
  publisherWebsiteApp.controller('WebsiteCreateController',["PublisherWebsiteService", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherWebsiteService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Websites  | Brimav';
 	$rootScope.PageHeading='Website';
 	$rootScope.SubPageHeading='Create';
 	$scope.Categories=Categories;
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newwebsite={
 		'websitename':$scope.Websitename,
 		'websiteurl':$scope.Websiteurl,
 		'prize':$scope.Prize,
 		'category':$scope.Category,
 		};
 		

	 	var postWebsite=PublisherWebsiteService.postWebsite($scope.newwebsite);
	 	postWebsite.then(function(pl)
	 	{
	 		$state.go('website',{},{reload:true});
	 		SharedMethods.createSuccessToast('Website added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---Website   edit controller---*/
  publisherWebsiteApp.controller('WebsiteEditController',["PublisherWebsiteService", "Website", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherWebsiteService,Website, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Social Medium | Brimav';
 	$rootScope.PageHeading='Social';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Website=Website;
 	$scope.Categories=Categories;
 	
 

 	$scope.edit=function()
 	{

 		$scope.editing=true

		$scope.newwebsite={
		'id':$scope.Website.id,
 		'websitename':$scope.Website.websitename,
 		'websiteurl':$scope.Website.websiteurl,
 		'prize':$scope.Website.prize,
 		'category':$scope.Website.category,
 		};
 		
       
	 	var putWebsite=PublisherWebsiteService.putWebsite($scope.newwebsite);
	 	putWebsite.then(function(pl)
	 	{
            $state.go('website',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Website  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---Website  delete controller---*/
 publisherWebsiteApp.controller('WebsiteDeleteController',["PublisherWebsiteService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherWebsiteService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Medium | Brimav';
 	$rootScope.PageHeading='Website';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deletewebsite=PublisherWebsiteService.deleteWebsite($scope.DeleteDetails.id);
 		deletewebsite.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('website', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Website deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Medium');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 }])


/*----------------------------------------------------------------------------------------*/



 /*---	TV MEDIUM CONTROLLER---*/

publisherTvApp.controller('TvHomeController',["PublisherTvService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(PublisherTvService,SharedMethods, $uibModal,$scope,$state,$rootScope)
{
 	$rootScope.title='Tv-Radio  | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$scope.deleting=false;


 	loadStations();
 	function loadStations()
 	{
 		var getTvs=PublisherTvService.getAll();
 		getTvs.then(function(pl)
 		{
 			$scope.Tvs=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Stations ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletetv=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Station " ,
            bodyMessage: "Are you sure you want to delete this Station?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'TvDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---Tv  create controller---*/
  publisherTvApp.controller('TvCreateController',["PublisherTvService", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherTvService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Tv-Radio  | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Create';
 	$scope.Categories=Categories;
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newstation={
 		'name':$scope.Name,
 		'frequency':$scope.Frequency,
 		'prize':$scope.Prize,
 		'category':$scope.Category,
 		};
 		

	 	var postTv=PublisherTvService.postTv($scope.newstation);
	 	postTv.then(function(pl)
	 	{
	 		$state.go('tv',{},{reload:true});
	 		SharedMethods.createSuccessToast('Station added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---Tv   edit controller---*/
  publisherTvApp.controller('TvEditController',["PublisherTvService", "Tv", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherTvService,Tv, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Tv-Radio | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Tv=Tv;
 	$scope.Categories=Categories;
 	
 

 	$scope.edit=function()
 	{

 		$scope.editing=true

		$scope.newstation={
		'id':$scope.Tv.id,
 		'name':$scope.Tv.channel_name,
 		'frequency':$scope.Tv.channel_frequency,
 		'prize':$scope.Tv.prize,
 		'category':$scope.Tv.category,
 		};
 		
       
	 	var putTv=PublisherTvService.putTv($scope.newstation);
	 	putTv.then(function(pl)
	 	{
            $state.go('tv',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Station  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---Tv  delete controller---*/
 publisherTvApp.controller('TvDeleteController',["PublisherTvService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherTvService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Medium | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deletetv=PublisherTvService.deleteTv($scope.DeleteDetails.id);
 		deletetv.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('tv', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Station deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Station');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
}])


/*------------------------------------TV PROGRAMS CONTROLLERS--------------*/



publisherProgrammesApp.controller('ProgrammesHomeController',["PublisherProgrammesService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(PublisherProgrammesService,SharedMethods, $uibModal,$scope,$state,$rootScope)
{
 	$rootScope.title='Tv-Radio  | Brimav';
 	$rootScope.PageHeading='Programmes';
 	$scope.deleting=false;


 	loadProgrammes();
 	function loadProgrammes()
 	{
 		var getProgrammes=PublisherProgrammesService.getAll();
 		getProgrammes.then(function(pl)
 		{
 			$scope.Programmes=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Programmes, Refresh ');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deleteprogramme=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Programme " ,
            bodyMessage: "Are you sure you want to delete this Programme?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'publishershared/modal/modalpopup',
 		 	controller:'ProgrammeDeleteController',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---Programme  create controller---*/
publisherProgrammesApp.controller('ProgrammeCreateController',["PublisherProgrammesService", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherProgrammesService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Programmes  | Brimav';
 	$rootScope.PageHeading='Programmes';
 	$rootScope.SubPageHeading='Create';

 	loadStations();
 	function loadStations()
 	{
 		var getTvs=PublisherProgrammesService.getTvs();
 		getTvs.then(function(pl)
 		{
 			$scope.Stations=pl.data;
 			
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem : Refresh page ');
 		})
 	}
 	 $scope.Days = {};
 	 $scope.Options=Options;
 	 $scope.Categories=Categories;
 	
 	
 	$scope.create=function()
 	{
 		var selectedDays="";
 		for(var x in $scope.Days)
 		{
 			selectedDays+=$scope.Days[x]+",";
 		}
 		$scope.creating=true;
		$scope.newprograme={
 		'name':$scope.Name,
 		'days':selectedDays,
 		'category':$scope.Category,
 		'start_time':$scope.Start_time,
 		'stop_time':$scope.Stop_time,
 		'station':$scope.Station,
 		'interval':$scope.Interval,
 		'price':$scope.Price
 		};

 	
	 	var postProgramme=PublisherProgrammesService.postProgramme($scope.newprograme);
	 	postProgramme.then(function(pl)
	 	{
	 		$state.go('programme',{},{reload:true});
	 		SharedMethods.createSuccessToast('Programme added succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---Program   edit controller---*/
  publisherProgrammesApp.controller('ProgrammeEditController',["PublisherProgrammesService", "Programme", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherProgrammesService,Programme, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Programmes | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Programme=Programme;
 	$scope.Days = {};
 	$scope.Categories=Categories;
 	$scope.Options=Options;
 	
 	loadStations();
 	function loadStations()
 	{
 		var getTvs=PublisherProgrammesService.getTvs();
 		getTvs.then(function(pl)
 		{
 			$scope.Stations=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem : Refresh page ');
 		})
 	}
 

 	$scope.edit=function()
 	{
 		var selectedDays="";
 		for(var x in $scope.Days)
 		{
 			selectedDays+=$scope.Days[x]+",";
 		}

 		$scope.editing=true

		$scope.newprogramme={
		'id':$scope.Programme.id,
 		'name':$scope.Programme.name,
 		'days':selectedDays,
 		'category':$scope.Programme.category,
 		'start_time':$scope.Programme.start_time,
 		'stop_time':$scope.Programme.stop_time,
 		'station':$scope.Programme.tv_id,
 		'interval':$scope.Programme.interval,
 		'price':$scope.Programme.price
 		};
 		
       
	 	var putProgramme=PublisherProgrammesService.putProgramme($scope.newprogramme);
	 	putProgramme.then(function(pl)
	 	{
            $state.go('programme',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Programme  edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---Programme  delete controller---*/
 publisherProgrammesApp.controller('ProgrammeDeleteController',["PublisherProgrammesService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(PublisherProgrammesService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Programmes | Brimav';
 	$rootScope.PageHeading='Tv-Radio';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteprogramme=PublisherProgrammesService.deleteProgramme($scope.DeleteDetails.id);
 		deleteprogramme.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('programme', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Programme deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Programme');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
}])


 /*---availlable categories---*/
 var Categories=["Economy","Education","Arts","Entertainment","Health","Others","Religion","Politics","Sports","Technology","Fashion"];
 var Options=[{id:1,name:"Sunday"},{id:2,name:"Monday"},{id:3,name:"Tuesday"},{id:4,name:"Wednessday"},{id:5,name:"Thursday"},{id:6,name:"Friday"},{id:7,name:"Saturday"}];





