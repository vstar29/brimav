

<!--Request for payment-->
<div class=" col-md-4 pull-left" style="margin-top: -20px;">
    <button class="btn btn-medium btn-create" data-toggle="modal" data-target="#payment"><i class="fa fa-money"></i> Request for Payment</button>
</div>

<!--/Request for payment-->
<!-- Search -->
<div class="form-group pull-right col-md-4 hidden-sm-down ">
    <div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/loader.gif" />
    </div>
    <div class="col-md-10">
        <input type="text" ng-model="SearchText" class=" pull-right form-control input-round tablesearch" placeholder="&#xf002;">
    </div>
    
</div>
<!-- Loading Image -->
<!--<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>-->

<!-- List of All Payments -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('date')">
                        <label>Date</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='date'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='date'"></span>
                    </th>

                      <th>
                          <label>Account Name</label>
                       
                    </th>
                   
                    <th >
                        <label>Account No</label>
                        
                    </th>

                  

                
                     <th ng-click="sort('amount')">
                          <label>Amount(NGN)</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='amount' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='amount' && sortKey != ''"></span>
                       
                    </th>

                
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="Payment in filteredItems = (Payments | orderBy:sortKey:reverse |filter:SearchText |itemsPerPage:10)">

                    <td >[[Payment.created_at | amDateFormat:'MMM Do, YYYY']] </td>
                   
                    
                    <td >[[Payment.publisher.accountname]]</td>

                    <td >[[Payment.publisher.accountnumber]]</td>

                    <td class="green">[[Payment.amount ]]</td>
                   
                   
                </tr>


                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Payments found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>






