
<div class="col-md-12">
    <div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
        <form name="formEdit" novalidate ng-submit="create()" class="form-horizontal">
            <div class="form-horizontal">
                <div class="validation-summary-errors" ng-show="validationErrors">
                    <ul>
                        <li ng-repeat="error in validationErrors">[[error]]</li>
                    </ul>
                </div>


                 <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Channel Name</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <input type="text" name="name" id="name" class="form-control width-70" placeholder="Vibes Fm" ng-model="Name">
                        <span ng-show="fieldErrors.name" class="field-validation-error remain2 white-space-pre">[[fieldErrors.name]]</span>
                    </div>
                </div>


                 <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Frequency</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <input type="text" name="frequency" id="frequency" class="form-control width-70" placeholder="DSTV:100, Startime:226" ng-model="Frequency">
                        <span ng-show="fieldErrors.frequency" class="field-validation-error remain2 white-space-pre">[[fieldErrors.frequency]]</span>
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Category</label>
                    <div class="col-md-9 leftfifteen">
                   
                        <select class="form-control width-70" id="category"
                        name="category" ng-model="Category" ng-options="cat for cat in Categories | orderBy">
                            <option value="">Select</option>
                            
                        </select>
                        <span ng-show="fieldErrors.category" class="field-validation-error remain2 white-space-pre">[[fieldErrors.category]]</span>

                        
                        
                    </div>
                </div>
               
                <div class="form-group margin-four-bottom">
                   <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Price(NGN)</label>
                    <div class="col-md-9 leftfifteen">
                        <input type="text" id="prize" name="prize" class="form-control width-70" ng-model="Prize"
                               ng-class="{'input-validation-error': fieldErrors.prize }" />
                        <span ng-show="fieldErrors.prize" class="field-validation-error remain2 white-space-pre">[[fieldErrors.prize]]</span>
                    </div>
                </div>

            
                <div class="form-group pull-right ">
                    <button class="btn btn-default btn-round btn-medium" ui-sref="social" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                        Save
                       
                        <img ng-show="creating" class="loader loader-small display-inline-block ng-hide">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>