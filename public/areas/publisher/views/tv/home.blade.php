
<!-- Search -->
<div class="form-group pull-right col-md-4 ">
    <input type="text" ng-model="SearchText" class=" pull-right form-control input-round tablesearch" placeholder="&#xf002;">
</div>

<div class=" " style="margin-left: 15px;">
    <a ui-sref="create"> <button style="margin-top: -2px;" class="btn btn-medium captitallize " > Add<i class="fa fa-plus"></i></button></a>
</div>
<!-- Loading Image -->
<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>

<!-- List of All Social -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('name')">
                        <label>Name</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='name'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='name'"></span>
                    </th>
                   

                    <th>
                        <label>Frequency</label>
                        
                    </th>


                    <th>
                        <label>Category</label>
                        
                    </th>

                    <th>
                        <label>Price</label>
                        
                    </th>



                    <th class="text-right">
                         <label>Actions</label>
                    </th>

                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="Tv in filteredItems = (Tvs | orderBy:sortKey:reverse |filter:SearchText |itemsPerPage:10)">
              
                    <td>[[Tv.channel_name]]</td>
                    <td>[[Tv.channel_frequency]]</td>
                    <td>[[Tv.category]]</td>
                    <td>[[Tv.prize]]</td>
            
                    <td class="text-right">
                        <a ui-sref="edit({id:Tv.id})">
                            <i class="fa fa-pencil i-extra-small-box i-rounded i-bordered" uib-tooltip="Edit" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                       
                        <a ng-click="deletetv(Tv.id)">
                            <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Delete" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Station yet</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>