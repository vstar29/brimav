<div class="col-md-12">

    <div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
        <form name="formEdit" novalidate ng-submit="edit()" class="form-horizontal">
            <div class="form-horizontal">
                <div class="validation-summary-errors" ng-show="validationErrors">
                    <ul>
                        <li ng-repeat="error in validationErrors">[[error]]</li>
                    </ul>
                </div>

                
                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Type</label>
                    <div class="col-md-9 leftfifteen">
                   
                        <select disabled="true" contenteditable="false" class="form-control width-70" id="type"
                        name="type" ng-model="Social.type">
                            <option  value="Facebook">Facebook</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Youtube">Youtube</option>
                             <option value="BBM Channel">BBM Channel</option>
                        </select>
                        
                    </div>
                </div>
               

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Social Account Url</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <input type="text" name="handler" id="handler" class="form-control width-70" ng-model="Social.handler">
                        <span ng-show="fieldErrors.handler" class="field-validation-error remain2 white-space-pre">[[fieldErrors.handler]]</span>
                    </div>
                </div>

                  <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Numbers of Followers</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <input type="text" name="followers" id="followers" class="form-control width-70" ng-model="Social.followers">
                        <span ng-show="fieldErrors.followers" class="field-validation-error remain2 white-space-pre">[[fieldErrors.followers]]</span>
                    </div>
                </div>


                <div class="form-group margin-four-bottom">
                   <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Price(NGN)</label>
                    <div class="col-md-9 leftfifteen">
                        <input type="text" id="prize" name="prize" class="form-control width-70" ng-model="Social.prize"
                               ng-class="{'input-validation-error': fieldErrors.prize }" />
                        <span ng-show="fieldErrors.prize" class="field-validation-error remain2 white-space-pre">[[fieldErrors.prize]]</span>
                    </div>
                </div>
               
            
                <div class="form-group pull-right ">
                    <button class="btn btn-default btn-round btn-medium" ui-sref="banner" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                        Save
                       
                        <img ng-show="editing" class="loader loader-small display-inline-block ng-hide">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>