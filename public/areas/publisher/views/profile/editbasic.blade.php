<div class="wow fadeIn col-md-12 ng-scope " data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="save()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>



            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">UserName</label>
                <div class="col-md-9">
               
                    <input type="text" id="username" name="username" maxlength="17" class="form-control width-80 margin-one" ng-model="Publisher.user.username"
                           ng-class="{'input-validation-error': fieldErrors.username }" />
                    <span ng-show="fieldErrors.username" class="field-validation-error remain2 white-space-pre">[[fieldErrors.username]]</span>
                    
                </div>
            </div>
           

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Email</label>
                <div class="col-md-9">
                    <input type="email" id="email" name="email" class="form-control width-80" ng-model="Publisher.user.email"
                           ng-class="{'input-validation-error': fieldErrors.email }" />
                    <span ng-show="fieldErrors.email" class="field-validation-error remain2 white-space-pre">[[fieldErrors.email]]</span>
                </div>
            </div>

            

            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Phone Number</label>
                <div class="col-md-9">
                    <input type="tel" id="phonenumber" name="phonenumber" class="form-control width-80" ng-model="Publisher.user.phonenumber"
                           ng-class="{'input-validation-error': fieldErrors.phonenumber }" />
                    <span ng-show="fieldErrors.phonenumber" class="field-validation-error remain2 white-space-pre">[[fieldErrors.phonenumber]]</span>
                </div>
            </div>


            <div class="form-group text-right margin-four-bottom">
                <button class="btn btn-default btn-medium" ui-sref="profile.basic" ng-disabled="editing">Cancel</button>
                <button type="submit" class="btn btn-medium bg-gold no-margin-bottom" ">
                    Save
                    
                    <img ng-show="editing" class="display-inline-block remain remain2" src="/uploads/images/theme/ajax_loader.gif">
                </button>
            </div>
        </div>
    </form>
</div>


