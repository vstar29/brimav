<div id="generalContent" class="ng-hide ng-cloak  margin-top-three" ng-show="!loadingView">
    <div id="generalPublisherInfo" class="wow fadeIn col-md-9 pull-right div-inbetween margin-one-bottom" data-wow-duration="300ms">  
    </div>


    <!-- My Tab-->
     <h3 class=" col-md-10 margin-two center-col">[[InnerHeading]]</h3>
    <div class="col-md-12 my-tab ng-cloak">
        <div class="col-md-3">
            <div class="box center-adjust">
                <div class=" center-col publisherprofile-user-img">
    
                        <img class="profile-user-img  img-responsive center-col publisherprofile-user-img img-circle" ng-src="[[stepsModel=='' && Picture ||stepsModel]]"/>

                   
                    <label ng-class="{'black-text':Black,'white-text':!Black}" class="overlapseicon  display-block position-absolute info padding-two"  for="avatar">Change
                        <input type="file" accept="image/*" onchange="angular.element(this).scope().imageUpload(event)" name="avatar" id="avatar" class="hiddeninput">  
                    </label> 
                </div>
                <h3 class="text-center">[[Publisher.user.username]]</h3>
                <p class="text-center"><a href="[[Publisher.websiteurl]]">[[Publisher.websiteurl]]</a></p>
                <ul class="list-group list-group-unbordered extradetails">
                    <li class="list-group-item">
                      <b>Balance <span class="pull-right green">[[Publisher.balance.balance |currency:"":0]] NGN</span></b>
                    </li>
                    <li class="list-group-item">
                      <b>Phone No</b> <a style="font-size: 12px;" class="pull-right">[[Publisher.user.phonenumber]]</a>
                    </li>
              </ul>

             </div>
        </div>

        <div class="tab-style3">
        <div class="col-md-8 col-sm-12 desktopprofile-nav">
            <!-- tab navigation -->
            <ul class="nav nav-tabs nav-tabs-light text-left profileslink">
                <li ng-class="{'active': activeView == 'basic'}"  ><a ui-sref="profile.basic">Basic</a></li>
                <li ng-class="{'active': activeView == 'account'}"><a ui-sref="profile.accountdetails">Account Details</a></li>
                <li ng-class="{'active': activeView == 'password'}"><a ui-sref="profile.changepassword">Change Password</a></li>
            </ul>
        </div>

        <div class="panel-group toggles mobileprofile">
        <!-- toggle item -->
            <div class="panel panel-default" id="collapse-one">
                <div role="tablist" id="type1-headingOne" class="panel-heading">
                    <a data-toggle="collapse" data-parent="#collapse-one" href="#s"><h4 class="panel-title bg-dark-gray white-text">My Profile<span class="pull-right"><i class="fa fa-plus"></i></span></h4></a>
                </div>
                <div id="s" class="panel-collapse collapse">
                    <div class="">
                        <ul class="nav nav-tabs nav-tabs-light mobileprofile-nav">
                            <li ng-class="{'active': activeView == 'basic'}"  ><a ui-sref="profile.basic">Basic</a></li>
                            <li ng-class="{'active': activeView == 'account'}"><a ui-sref="profile.accountdetails">Account Details</a></li>
                             <li ng-class="{'active': activeView == 'password'}"><a ui-sref="profile.changepassword">Change Password</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class=" col-md-9 tab-content padding-three ">
            <!-- Nested View Loading Spinner -->
            <div class="nestedLoadingHolder centerAbsolute ng-hide" ng-show="loadingNestedView">
                <div class="spinner spinner2">
                     <img src="/uploads/images/theme/ajax_loader.gif" style="width: 70px; height: 70px;" />
                </div>
            </div>
            <!-- End Loading Spinner-->
            <div class="row">
                <div ui-view class="tab-pane ng-cloak center-col fade in active col-md-9 col-sm-12"></div>
            </div>
                    
        </div>
    </div>
    </div>
</div>
    