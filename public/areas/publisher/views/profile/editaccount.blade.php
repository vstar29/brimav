<div class="wow fadeIn col-md-12" data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="update()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>



            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Bank</label>
                <div class="col-md-9">
                   
                     <input type="text" ng-required="true" id="banker" name="banker" placeholder="Banker"  class="form-control width-80" ng-model="Publisher.banker"
                                   ng-class="{'input-validation-error': fieldErrors.banker }" />
                                   <span ng-show="fieldErrors.prize" class="field-validation-error ">[[fieldErrors.banker]]</span>
                    
                </div>
            </div>
           

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Account name</label>
                <div class="col-md-9">
                    <input type="text" id="accountname" name="accountname" class="form-control width-80" ng-model="Publisher.accountname"
                           ng-class="{'input-validation-error': fieldErrors.accountname }" />
                    <span ng-show="fieldErrors.email" class="field-validation-error remain2 white-space-pre">[[fieldErrors.accountname]]</span>
                </div>
            </div>

            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Account number</label>
                <div class="col-md-9">
                    <input type="tel" id="accountnumber" name="accountnumber" class="form-control width-80" ng-model="Publisher.accountnumber"
                           ng-class="{'input-validation-error': fieldErrors.accountnumber }" />
                    <span ng-show="fieldErrors.accountnumber" class="field-validation-error remain2 white-space-pre">[[fieldErrors.accountnumber]]</span>
                </div>
            </div>


            <div class="form-group text-right margin-four-bottom">
                <button class="btn btn-default btn-medium" ui-sref="profile.basic" ng-disabled="editing">Cancel</button>
                <button type="submit" class="btn btn-medium white-text bg-gold no-margin-bottom" ">
                    Save
                    <!--<span ng-show="editing" class="loader display-inline-block"></span>-->
                    <img ng-show="editing" class="display-inline-block remain remain2 loading loader" src="/uploads/images/theme/ajax_loader.gif">
                </button>
            </div>
        </div>
    </form>
</div>


