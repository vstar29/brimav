<div id="generalContent" class="ng-hide ng-cloak col-md-12  margin-top-three" ng-show="!loadingView" >

		<div class="col-md-8 margin-top-three" style="margin-top: 30px;">

			<h5 ng-if="Adverts.length > 1">Hello <span ng-controller="PublisherShareController">[[Publisher.user.username]]</span>, you have [[Adverts.length]] adverts to publish</h5>

			<h5 ng-if="Adverts.length == 1">Hello <span ng-controller="PublisherShareController">[[Publisher.user.username]]</span>, you have [[Adverts.length]] advert to publish</h5>
			
		
			<div ng-repeat="Advert in Adverts"  class="pull-right col-md-4 gray-round-border bg-gray  margin-three no-padding" style="margin-top: 30px !important; float: left !important;">
				
				<div class=" col-md-12 fade-header-icon-bg" style="height: 22px;">
					<a title="View" ng-click="go(Advert.advert_id,Advert.type)" class=" pointer pull-right"><i class="fa fa-arrow-right"></i></a>
				</div>
				
				<div class="col-md-12 margin-fifteenneg">
					<label class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
			           <label>Date :</label>
			        </label>
			        <label class=" no-padding-left letter-spacing-1 font-14 black-text">
			            [[Advert.created_at | amDateFormat:'MMM D']]
			        </label>
		        </div>
		        
		        <div class="col-md-12 margin-fifteenneg">
			        <label class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
			           <label>Title :</label>
			        </label>
			        <label class=" no-padding-left letter-spacing-1 font-14 black-text">
			            [[Advert.title |limitTo:30]]
			        </label>
		        </div>
		        <div class="col-md-12 margin-fifteenneg">
			        <label class="col-md-4 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
			           <label>Amount:</label>
			        </label>
			        <label class=" margin-two-left letter-spacing-1 font-14 green">
			             [[Advert.totalamount |currency:"":0]] NGN
			        </label>
		        </div> 

			</div>
			 <a href="publisher/advert" target="_self"> More </a>  

			<div ng-if="Adverts.length == 0" class="col-md-8 welcome-guide">
		
			<h4 class="text-center" > Brimav Guide </h4>
				<p>Brimav helps you to earn a living from your websites  by serving you with adverts.</p>
				<p> The process is simple and automated, you get mailed anytime there is an advert for you, login to your dashboard and click on Adverts, list of your unpublished adverts will be display, click on anyone to view the details.
				<p> Copy the content and publish on your websites, return the url of the published content to brimav by inserting the url to the corresponding advert.</p>

				<p> Your Brimav wallet get credited when the advertiser confirm the link.</p>
				<p>Advert is automatically confirm if Advertiser failed to confirm advert within 48hrs.</p>

				<p> You can withdraw your money to any Bank, any Time.</p>

				<p> Brimav charged 9.7% for any advert transaction on the platform.</p>

				<p>Live chart is avallable Mon - Friday (8:00 AM -8:00PM). Support mail: <span class='green'>support@brimav.com</span></p>

			</div>
		</div>

		<div class="col-md-4 row">

		<p class="pull-right" style="font-weight: bold;
	    margin-top: 70px;
	    font-size: 15px;">Increase your chances of getting more adverts by <a href="publisher/banner"  target="_self">CREATING </a>type  of Banners you supported</p>
		</div>
	</div>
