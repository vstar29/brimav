
<!-- Advert Details -->
<div class="col-md-12">                  
    <div class="col-md-8 ng-cloak center-col bg-gray pubadvertdetails" data-wow-duration="300ms">
        <div ng-if="Advert.type=='music' && Advert.songurl!='null' && Advert.songurl!='undefined'" class="form-group  margin-one-bottom">
            <jplayer audio="[[Advert.songurl]]"  advertdetails="Advert" ></jplayer>

        </div><br/>
                
        <div class="form-group  margin-one-bottom">
            <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Title</label>

            <input type="text" id="title" name="title" class="form-control width-80 input-round medium-input margin-one" ng-model="Advert.title"
         /> 
               
        </div>
       

        <div class="form-group sm-no-padding margin-one-bottom">
           <div ng-if="Advert.type =='music'||Advert.type=='others'" class="pull-right col-md-6">
               <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1" style="margin-bottom: 15px;">Featured image</label>

                <input type="text" id="album art" name="album art" class="form-control width-60 input-round medium-input margin-one" ng-model="Advert.imageurl" />
            </div> 

            <div ng-if="Advert.type=='others' && Advert.attachment !='null' && Advert.attachment !='undefined'" class="form-group sm-no-padding margin-one-bottom">
                <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Attachment</label>
                <br>
                <a href="[[Advert.attachment]]" uib-tooltip="Download" style="cursor: pointer;" tooltip-placement="bottom" tooltip-trigger="mouseenter" target="_self"><img src="/uploads/images/theme/msword.jpg" style="width:30px;height:30px"></a>
            </div>

            <div ng-if="Advert.genre !='' && Advert.genre !='undefined'">
                <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Genres</label>

                    <select id="genres" name="genres" class="form-control width-30 input-round medium-input margin-one" >
                        <option value="Advert.genre" selected>[[Advert.genre]] </option>
                    </select> 
            </div>

             <div ng-if="Advert.type =='banner'">
                <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Banner Code</label>

                <input type="text" value="<a href='[[Advert.destination]]' rel='noopener' target='_blank'><img src='[[Advert.imageurl]]'></a>" id="destination" name="destination" class="form-control width-80 input-round medium-input margin-one" />     
            </div>
        </div>

        <div ng-if="Advert.type =='music'||Advert.type=='others'" class="form-group sm-no-padding ">
            <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Content</label>
            
                <textarea id="description" rows="14" name="description" class="form-control width-80 input-round medium-input margin-one" ng-model="Advert.description" placeholder="Describe your song and the artist"  ></textarea>
        </div>




        <!--Return Section-->
        <div class="col-md-12 pull-left">
            <button class="btn btn-default btn-medium" ui-sref="[[returnState]]"><i class="fa fa-angle-left"></i>Back to Adverts </button>
        </div>
    </div>
</div>
