<div id="generalContent" class="ng-hide ng-cloak  margin-top-three" ng-show="!loadingView">
<!-- Search -->
<div class="form-group col-md-4 pull-right" style="margin-right: 16px;">
		    <input type="text" ng-model="SearchText" class=" form-control input-round tablesearch pull-right" placeholder="&#xf002;">
		</div>

<!-- Loading Image -->

<!-- View Completed/ Uncompleted Advert -->
<div class=" col-md-4 pull-left">
    <select ng-model="Filterview">
    
     <option value="confirmed" name="advert.completed" >PUBLISHED</option>  
     <option value="unconfirmed" name="advert.uncompleted" >UNPUBLISHED</option> 
    </select>
</div>


<div class="col-md-12 ng-cloak">
<div class="nestedLoadingHolder centerAbsolute ng-hide  ng-show="loadingNestedView">
    <img class="width-30" src="/uploads/images/theme/loadingwhite.gif">
</div>
    <div ui-view></div>
</div>

</div>