<div style="background-color:white !important">
	<div class="modal-header">
	<h4 class="modal-title">[[title]]</h4>
	</div>
	<form ng-submit="insertadvertlink()" id="insert">
		 <div class="modal-body">
		   <input type="text"  name="link" class="center-col form-control width-60 input-round medium-input margin-two" placeholder="[[fieldErrors.link]]" ng-model="Link" ng-class="{'input-validation-error': fieldErrors.link,'red': fieldErrors.link }" /> 
		</div>
		<div class="modal-footer">
		    <button form="insert" type="submit" class="btn [[class]] btn-round btn-small">Ok
		    <img ng-show="deleting" class="loader loader-small display-inline-block ng-hide">
		    </button> 
		</div>
	</form>
	<button title="Close (Esc)" type="button" ng-click="cancel()" class=" mfp-close">×</button>
</div>

