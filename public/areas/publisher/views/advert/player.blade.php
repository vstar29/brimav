<div id="jplayer" class="jp-jplayer"></div>
<div id="jp_container">
	<div class="jp-gui ui-widget ui-widget-content ui-corner-all">
		<div class="form-group col-md-12">
			<div class="  pull-left">
				<img ng-src="[[Advert.imageurl]]" class="jp-albumart">
			</div>
		

			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1" style=" margin-left: 2px;">[[Advert.title | limitTo:37]]</label></br/>
					<label class="control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1"><i class="fa fa-user jp-adgold"></i> [[Advert.advertiser.user.username | limitTo:17]]</label>
				</div>
				<div class="form-group">
					<div id="jp-progress-slider" class="jp-progress-slider" style="    height: 7px;"></div>
					<div class="jp-duration"></div>
					<div class="jp-current-time"></div>
				</div>
					
				
			</div>
				<div class="jp-control">
				<ul class="list-group list-group-unbordered">
					<button uib-tooltip="play" tooltip-placement="top" tooltip-trigger="mouseenter" class=" jp-control-button btn jp-play no-margin  btn-small margin-half-bottom" ng-click="play()"  ng-class="{'btn-adgold':onplay,'btn-jplay':!onplay}" ><i class="fa fa-play jp-white" ></i></button>

					<button uib-tooltip="pause" tooltip-placement="top" tooltip-trigger="mouseenter" class="btn  jp-control-button  no-margin btn-small margin-half-bottom" ng-click="pause()" ng-class="{'btn-adgold':onpause,'btn-jplay':!onpause}" ><i class="fa fa-pause jp-white" ></i></button>
			
					
					<a href="[[Advert.songurl]]" target="_self" class="margin-half-bottom">
					<button uib-tooltip="download" tooltip-placement="top" tooltip-trigger="mouseenter" class="btn jp-control-button  no-margin btn-jplay  btn-small no-margin-bottom" ><i class="fa fa-download jp-white margin-ten" ></i></button></a>
					
				</ul>
				</div>
				
			</div>
				
		</div>

		
	</div>

</div>


