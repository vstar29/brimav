
<!-- List All Completed  Adverts -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-hover margin-three">
            <thead>
                <tr class="sortable">
                   
                    <th ng-click="sort('date')">
                        <label>Date</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='date'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='date'"></span>
                    </th>
                    <th ng-click="sort('title')">
                       <label>Title</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='title'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey != 'title'"></span>
                    </th>


                    <th ng-click="sort('status')">
                          <label>Status</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='status'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='status'"></span>
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="ConfirmedAdvert in filteredItems = (ConfirmedAdverts | orderBy:sortKey:!reverse |filter:SearchText |itemsPerPage:10)">
               
                    <td>[[ConfirmedAdvert.created_at | amDateFormat:'MMM Do YYYY']]</td>
                    <td>[[ConfirmedAdvert.title]]</td>
                    <td>[[ConfirmedAdvert.status]]</td>
                    <td class="text-right">
                       

                        <a ng-click="putadvertlink(ConfirmedAdvert.id,'advert.confirmed')">
                            <i class="fa  fa-chain i-extra-small-box i-rounded i-bordered" uib-tooltip="Put Link" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                         <a  ui-sref="details({id:ConfirmedAdvert.advert_id,returnState:'advert.confirmed'})">
                            <i class="fa  fa-eye i-extra-small-box i-rounded i-bordered" uib-tooltip="View details" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                       


                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No  Published advert found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>



