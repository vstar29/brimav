
<div class="col-md-12">
    <div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
        <form name="formEdit" novalidate ng-submit="create()" class="form-horizontal">
            <div class="form-horizontal">
                <div class="validation-summary-errors" ng-show="validationErrors">
                    <ul>
                        <li ng-repeat="error in validationErrors">[[error]]</li>
                    </ul>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Name</label>
                    <div class="col-md-9 leftfifteen">
                   
                        <input type="text" id="name" name="name" class="form-control width-70 margin-one" ng-model="Name"
                               ng-class="{'input-validation-error': fieldErrors.name }" />
                        <span ng-show="fieldErrors.name" class="field-validation-error remain2 white-space-pre">[[fieldErrors.name]]</span>
                        
                    </div>
                </div>
               

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Type</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <select id="type" name="type" class="form-control width-70" ng-model="Type" 
                               ng-class="{'input-validation-error': fieldErrors.banner_type }"  ng-options="type.id  as  type.name +' ('+ type.size+')' for type in BannerTypes track by type.id" >
                               <option value="">..Select Type..</option>

                       </select>
                        <span ng-show="fieldErrors.banner_type" class="field-validation-error remain2 white-space-pre">[[fieldErrors.banner_type]]</span>
                    </div>
                </div>


                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Type</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <select id="website" name="website" class="form-control width-70" ng-model="Website" 
                               ng-class="{'input-validation-error': fieldErrors.website }"  ng-options="website.websiteurl  as  website.websitename  for website in Websites track by website.websiteurl" >
                               <option value="">..Select Websites..</option>

                       </select>
                        <span ng-show="fieldErrors.banner_type" class="field-validation-error remain2 white-space-pre">[[fieldErrors.website]]</span>
                    </div>
                </div>


                <div class="form-group margin-four-bottom">
                   <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Price(NGN)</label>
                    <div class="col-md-9 leftfifteen">
                        <input type="text" id="prize" name="prize" class="form-control width-70" ng-model="Prize"
                               ng-class="{'input-validation-error': fieldErrors.prize }" />
                        <span ng-show="fieldErrors.prize" class="field-validation-error remain2 white-space-pre">[[fieldErrors.prize]]</span>
                    </div>
                </div>

                

                <div class="form-group pull-right ">
                    <button class="btn btn-default btn-round btn-medium" ui-sref="banner" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                        Save
                       
                        <img ng-show="creating" class="loader loader-small display-inline-block ng-hide">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>