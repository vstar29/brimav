<!Doctype html>
<html class="no-js" data-ng-app="@yield('module')" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
       <meta name="theme-color" content="#1e313e"  />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="72x72" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="114x114" href="/uploads/images/logo/favicon.ico">


         <style>
            .ng-cloak
            {
                display: none !important;
            }

        </style>
        
        <!--styles and scripts>-->

        
        
         <script type="text/javascript" src="/scripts/brimav_script.js"></script>

        <link rel="stylesheet" type="text/css" href="/css/templatestyle.css">
        
     

        <script src="{{ elixir('scripts/angularui.js')}}"></script>

        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">



    <!--End styles and scripts>-->

    </head>
    <body class="ng-cloak bg-body">
        
        <!-- navigation panel -->
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-white nav-dark-transparent bg-gold " role="navigation">
            <div class="container">
                <div class="row">
                
                    <!-- logo -->
                    <div class="col-md-2 pull-left logo-responsive"><a class="logo-light" target="_self" href="https://brimav.com"><img alt="" src="/uploads/images/logo/brimavwhite.png" class="logo" /></a><a class="logo-dark" target="_self" href="https://brimav.com"><img alt="" src="/uploads/images/logo/brimavcolor.png" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and user menu  -->
                    <div class="fade-header-icon-bg col-md-2 no-padding-left hidden-sm-down search-cart-header pull-right">
                        
                     
                        <div class="top-cart">
                            <!-- user menu -->
                            <a class="shopping-cart">
                                <i class="fa  fa-user"></i>
                                <div class="subtitle"> Profile</div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- user menu content -->
                            <div ng-controller="PublisherShareController" class="ng-cloak cart-content">
                                <ul class="cart-list">
                                    <li>
                                        
                                        <div class="pull-right">
                                            <span class="username text-uppercase">H!  [[Publisher.user.username]]</span><br/>
                                            <span style="font-size: 12px !important">Bal:[[Publisher.balance.balance |currency:"":0]] NGN</span>
                                        </div>
                                            <img style="width:70px;height:70px;"  class="pull-left"  alt="pics" ng-src="[[Avatar]]">
   
                                    </li>
                                </ul>
                                
                                <p class="buttons">
                                    <a href="publisher/profile" target="_self" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr" ng-class="{'active':profilePage}">Profile</a>
                                    <a href="api/auth/logout" target="_self" class="btn-danger btn btn-very-small no-margin-bottom margin-seven no-margin-right pull-right">Signout</a>

                                </p>
                            </div>
                        </div>
                            <!-- end user menu-->

                    </div>
                    <!-- end search  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                        <button id="toggleside" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#side" style=" background-color: #575757;"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu menu-mobile-pub text-right">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class=" hidden-sm-down fade-header-icon-bg nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->
                                

                                  <li class="dropdown" ng-controller="PublisherShareController"><a ng-click="markRead()" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell "></i><span ng-if="Notifications.length>0" class="notify_count">[[Notifications.length]] </span></a>
                                     
                                    
                                         <ul style="padding: 10px;background: rgb(0, 0, 0);"  class="dropdown-menu">
                                        <li style="text-transform: none;color: white;text-align: center;" ng-if="Notifications.length==0">No new notification</li>
                                             
                                            <li style="border-bottom: solid 1px rgba(185, 185, 185, 0.11);" ng-repeat="notification in Notifications">
                                                <a target="_self" style="text-transform: capitalize;" href ="[[notification.advert_link]]"  class="dropdown-item"> [[notification.content]] <span style="color: #3c763d;" class="pull-right">[[notification.created_at|amTimeAgo]]</span>
                                                </a>

                                            </li>
                                        
                                         </ul>
                                     
                                </li>
                            
                                <!-- end menu item -->
                               
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>
        <!--end navigation panel --> 

        <!--side bar nav-->

            <nav class="sidebar hidden-sm-down navbar-fixed-top  " role="navigation">
            <div class="container">
                <div class="row">

                    
                    
                    <!-- main menu -->
                    <div class="col-md-8 slideInLeft no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse" id="side">
                            <ul style="    margin-top: 90px;" id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->


                                
                                <li ><a href="publisher/home" ng-class="{'active':homePage}" target="_self"><i class="fa  fa-dedent"></i> Dashboard </a></li>

    
                                 <li><a href="publisher/website" ng-class="{'active':websitePage}" target="_self"><i class="fa fa-globe"></i> Websites </a></li>


                                <li><a href="publisher/tv"   ng-class="{'active':tvsPage}" target="_self"><i class="fa  fa-desktop"></i> Tv/Radio  </a></li>

                                 <li><a href="publisher/programme"  ng-class="{'active':programmePage}" target="_self"> <i class="fa fa-building-o"></i> Programmes </a></li>

                                 
                                <li><a href="publisher/social" ng-class="{'active':socialPage}" target="_self"><i class="fa  fa-twitter"></i> Social </a>
                                </li>


                                <li><a href="publisher/advert" ng-class="{'active':advertPage && advertview}" target="_self"><i class="fa fa-bullhorn"></i> Adverts </a></li>


                                 <li><a href="publisher/payment" ng-class="{'active':paymentPage}" target="_self"><i class="fa fa-money"></i> Payments </a></li>


                                  <li><a href="publisher/banner" ng-class="{'active':bannerPage}" target="_self"><i class="fa  fa-calendar-o"></i> Banners </a></li>


                                 <li class=""><a href="publisher/profile" ng-class="{'active':profilePage}" target="_self"><i class="fa fa-user"></i> Profile </a></li>

                                  <li class=""><a href="api/auth/logout"  target="_self"><i class="fa fa-sign-out"></i> Logout </a></li>


                            


                                <!-- end menu item -->
                               
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
            <div class="position-absolute" style="bottom: 10px;left:12px">
                 <span class="text-lower"> Copyright © Vacvi  <?php echo date('Y'); ?></span>          
            </div>
        </nav>

        <!--End side bar nav-->

         <!-- head section -->
        <section ng-controller="PublisherShareController" ng-class="{'bg-gray':!loadingView}" class="content-top-margin  border-bottom-very-light " style="padding: 20px 0">
            <div class="container">

                <div ng-class="{'bg-active':Publisher.status=='ACTIVE','bg-inactive':Publisher.status=='INACTIVE'}" class="col-md-2 pull-left wow slideInUp wow fadeInUp" data-wow-duration="300ms" style=" padding: 10px 10px;border-radius: 4px; text-align: center; margin-left: 15%;">
                    <h3>STATUS : [[Publisher.status]]</h3>
                        
                        <!-- end page title -->
                    </div>
               
                <div class="col-md-2 pull-right wow slideInUp wow fadeInUp" data-wow-duration="300ms" style="background-color: #bdc13d !important;;padding: 10px 10px;border-radius: 4px; text-align: center;">
                <h3>BAL :  [[Publisher.balance.balance |currency:"":0]] NGN</h3>
                    
                    <!-- end page title -->
                </div>   
                        
                </div>
                </div>
            </div>
        </section>
        <!-- end head section -->
        

        <!-- content section -->
        <section class="wow  margin-two fadeIn slideInRight padding-one">
            <div class="container">
                <div class="row">
                    <!-- content  -->
                    <div class=" content col-md-11 ng-cloak ">
                        @yield('content')
                    </div>
                </div>
           </div> 
        </section>
        <!-- end content section -->
  
     


    
    <!-- footer -->
    <footer  class="bg-gold ">
             
        <div  class="container-fluid footer-bottom hidden">
                <div class="container">
                    <div class="row">
                      <!-- copyright -->
                        <div class="col-md-6 col-sm-6 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one">
                           &copy; <?php echo date('Y'); ?> powered by <a class="display-inline" target="_blank"  href="https://vacvi.com"><img style=" margin-left: -70px" src="/uploads/images/logo/vacvi-logo-white.png"></a> 
                        </div>
                        <!-- end copyright -->
                        <!-- logo -->
                        <div class="col-md-6 col-sm-6 col-xs-12 footer-logo text-right xs-text-center">
                            <a href="index.html"><img  src="/uploads/images/logo/brimavwhite.png" alt=""/></a>
                        </div>
                        <!-- end logo -->
                    </div>
                </div>
            </div>
          
   
        </footer>
         <!-- end footer -->

        <!-- notification-->
    <toast></toast>
    <!-- end notification-->

     <!-- View Loading Spinner -->
    <div ng-show="loadingView" class="spinner centerAbsolute">
        <img src="/uploads/images/theme/loadingcolor.gif">
    </div>
    <!-- End Loading Spinner-->



    <!-- Modal Loading Spinner -->
    <div id="loadingModalHolder" class="ng-hide">
        <div class="loadingModal modal-backdrop in">
            <div class="spinner">
                 <img src="/uploads/images/theme/loadingwhite.gif">
            </div>
        </div>
    </div>
    <!-- End Modal Loading Spinner--> 


<!---Modal-->
<div  ng-controller="PaymentsHomeController" class="modal zoom-anim-dialog fade" role="dialog" id="payment" ng-if="paymentPage" >
   
        <div class="modal-content col-md-3 center-col" style="margin-top:60px;
    background-color: white !important;">
            <div class="modal-header">
            <span> Payment Request</span>
            </div>
            <div class="modal-body">
                <form ng-submit="requestPayment()">
                    <div class="form-group">
                        <input type="text" placeholder="Amount" name="amount" id="amount" ng-Model="Amount">
                        <span ng-show="fieldErrors.amount" class="field-validation-error remain2 white-space-pre">[[fieldErrors.amount]]</span>
                    </div>
                    <button class="btn" type="submit"> Request <img ng-show="requesting" class="loader loader-small display-inline-block ng-hide" style="width: 2em; margin-bottom: 0; height: 2em;" /> </button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
     
    </div>

</div>
<!--/Modal-->

<!--SCRIPTS-->

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        
    <script type="text/javascript" src="scripts/theme/jquery.jplayer.min.js"></script>
    

    <script type="text/javascript" src="/areas/publisher/app/publisherscripts.js"></script>

    <script type="text/javascript" src="/areas/publisher/app/publisher_templates.js"></script>

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/588b53a3bab87e66427d107b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

jQuery('#toggleside').click(function()
{
    jQuery('.sidebar').toggleClass('hidden-sm-down');
});
</script>
<!--End of Tawk.to Script-->

<!--/SCRIPTS-->

    </body>
</html>