
<div class="col-md-12">
    <div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
        <form name="formEdit" novalidate ng-submit="create()" class="form-horizontal">
            <div class="form-horizontal">
                <div class="validation-summary-errors" ng-show="validationErrors">
                    <ul>
                        <li ng-repeat="error in validationErrors">[[error]]</li>
                    </ul>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Program Name</label>
                    <div class="col-md-9">
                       
                        <input type="text" name="name" id="name" class="form-control width-70" placeholder="Vibes 10" ng-model="Name">
                        <span ng-show="fieldErrors.name" class="field-validation-error remain2 white-space-pre">[[fieldErrors.name]]</span>
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Station</label>
                    <div class="col-md-9">
                       
                        <select id="station" name="station" class="form-control width-70" ng-model="Station" 
                               ng-class="{'input-validation-error': fieldErrors.banner_type }"  ng-options="station.id  as  station.channel_name  for station in Stations track by station.id" >
                               <option value="">..Select Station..</option>

                       </select>
                        <span ng-show="fieldErrors.banner_type" class="field-validation-error remain2 white-space-pre">[[fieldErrors.station]]</span>
                    </div>
                </div>
              
                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Days</label>
                    <div class="col-md-9 ">
                    <multiselect class="form-control width-70"  options="Options" model='Days'></multiselect>
                       
                        <span ng-show="fieldErrors.frequency" class="field-validation-error remain2 white-space-pre">[[fieldErrors.days]]</span>
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Programme Category</label>
                    <div class="col-md-9 leftfifteen">
                   
                        <select class="form-control width-70" id="category"
                        name="category" ng-model="Category" ng-options="cat for cat in Categories | orderBy">
                            <option value="">Select</option>
                            
                        </select>
                        <span ng-show="fieldErrors.category" class="field-validation-error remain2 white-space-pre">[[fieldErrors.category]]</span>
                        
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Start time</label>
                    <div class="col-md-9">
                       
                       <small style="font-size: 80%;color: #1e313e;" class="helper">08:30 AM</small>
                        <input  type="text" name="start_time" id="start_time" class="form-control width-70" ng-model="Start_time">
                        <span ng-show="fieldErrors.start_time" class="field-validation-error remain2 white-space-pre">[[fieldErrors.start_time]]</span>
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Stop time</label>
                    <div class="col-md-9">
                       
                       <small style="font-size: 80%;color: #1e313e;" class="helper">01:00 PM</small>
                        <input  type="text" name="stop_time" id="stop_time" class="form-control width-70" ng-model="Stop_time">
                        <span ng-show="fieldErrors.stop_time" class="field-validation-error remain2 white-space-pre">[[fieldErrors.stop_time]]</span>
                    </div>
                </div>

               

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Price</label>
                    <div class="col-md-9">

                    <div class="row">
                        
                        <div class="col-md-5">
                            <input id="pass" name="price" id="price" type="text" class="form-control " placeholder="9000" ng-model="Price" >
                        </div>
                       
                        <div class="col-md-4 p_adjust">
                            <select name="interval" id="interval"  class="form-control " ng-model="Interval" style=" height: 38px;">
                                <option value=""  >Select</option>
                                <option  value="Daily">Daily</option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                            </select>
                        </div>
                       
                    </div>
                   
                        <span ng-show="fieldErrors.frequency" class="field-validation-error remain2 white-space-pre">[[fieldErrors.price]]</span>
                    </div>
                </div>
            
                <div class="form-group pull-right ">
                    <button class="btn btn-default btn-round btn-medium" ui-sref="programme" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                        Save
                       
                        <img ng-show="creating" class="loader loader-small display-inline-block ng-hide">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>