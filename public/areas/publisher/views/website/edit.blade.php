<div class="col-md-12">

    <div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
        <form name="formEdit" novalidate ng-submit="edit()" class="form-horizontal">
            <div class="form-horizontal">
                <div class="validation-summary-errors" ng-show="validationErrors">
                    <ul>
                        <li ng-repeat="error in validationErrors">[[error]]</li>
                    </ul>
                </div>

                
                  <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Website Name</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <input type="text" name="name" id="name" class="form-control width-70"  ng-model="Website.websitename">
                        <span ng-show="fieldErrors.websitename" class="field-validation-error remain2 white-space-pre">[[fieldErrors.websitename]]</span>
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Website Url</label>
                    <div class="col-md-9 leftfifteen">
                       
                        <input type="text" name="websiteurl" id="websiteurl" class="form-control width-70" ng-model="Website.websiteurl">
                        <span ng-show="fieldErrors.websiteurl" class="field-validation-error remain2 white-space-pre">[[fieldErrors.websiteurl]]</span>
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                    <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Category</label>
                    <div class="col-md-9 leftfifteen">
                   
                        <select class="form-control width-70" id="category"
                        name="category" ng-model="Website.category" ng-options="cat for cat in Categories | orderBy">
                            <option value="">Select</option>
                            
                        </select>
                         <span ng-show="fieldErrors.category" class="field-validation-error remain2 white-space-pre">[[fieldErrors.category]]</span>
                        
                    </div>
                </div>

                <div class="form-group margin-four-bottom">
                   <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Price(NGN)</label>
                    <div class="col-md-9 leftfifteen">
                        <input type="text" id="prize" name="prize" class="form-control width-70" ng-model="Website.prize"
                               ng-class="{'input-validation-error': fieldErrors.prize }" />
                        <span ng-show="fieldErrors.prize" class="field-validation-error remain2 white-space-pre">[[fieldErrors.prize]]</span>
                    </div>
                </div>
            
                <div class="form-group pull-right ">
                    <button class="btn btn-default btn-round btn-medium" ui-sref="website" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                        Save
                       
                        <img ng-show="editing" class="loader loader-small display-inline-block ng-hide">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>