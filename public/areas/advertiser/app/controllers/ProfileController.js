﻿/*-----====== ADMIN PROFILE CONTROLLER =====----*/


/*---==== Profile TemplateController =====--*/

advertiserProfileApp.controller('ProfileTemplateController', function ($rootScope,$cacheFactory,$scope,AdvertiserProfileService, $scope, ShareData, Advertiser,SharedMethods) {
    $scope.Advertiser = Advertiser;
    $rootScope.headerClass = "normal-page-title";

    //Get advertiser Picture
    if ($scope.Advertiser.user.picture == null || $scope.Advertiser.user.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";
    else {
        $scope.Picture =  $scope.Advertiser.user.picture;
    }

/*---- Avatar Upload &  preview ----*/

   $scope.stepsModel=[];
    $scope.imageUpload = function(event)
    {
        
          var avatar = event.target.files[0]; //FileList object

        if(avatar.type.match ("image/jpeg") || avatar.type.match ("image/png") || avatar.type.match ("image/gif") )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(avatar);
             $scope.avatar=avatar;
             var uploadAvatar=AdvertiserProfileService.putUploadavatar($scope.avatar);
             uploadAvatar.then(function(response)
             {
                //$rootScope.Picture=response.data;
                $rootScope.Avatar=response.data;
                var httpCache=$cacheFactory.get('$http');
            httpCache.remove('api/advertiserprofileAPI/shared');
             },
             function(error)
             {
                SharedMethods.createErrorToast('file too large (max:1.4mb)');
             })
        } 
         else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
            $rootScope.Avatar=e.target.result;
        });
    };

});


/*---==== Basic Details Controller =====--*/

advertiserProfileApp.controller('BasicDetailsController', function ($scope, $rootScope, ShareData) {
    
    $rootScope.title = $scope.Advertiser.user.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Basic Details";
    $rootScope.activeView = "basic";
});


/*---====Edit Basic Details Controller =====--*/

advertiserProfileApp.controller("EditBasicProfileController", function ($scope,$cookies, AdvertiserProfileService, SharedMethods, $rootScope, $state) {
    
    $scope.editing = false;

    $rootScope.title = "Edit Profile | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit Basic Details";
    $rootScope.activeView = "basic";
    $scope.Advertiser2={};
    for(var key in $scope.Advertiser)
    {
        $scope.Advertiser2[key]=$scope.Advertiser[key];
    }
   

    $scope.save = function () {
        $scope.editing = true;
        $scope.newAdvertiserdetails = {
            username: $scope.Advertiser2.user.username,
            email: $scope.Advertiser2.user.email,
            facebook:$scope.Advertiser2.facebook,
            twitter:$scope.Advertiser2.twitter,
            phonenumber:$scope.Advertiser2.user.phonenumber,
         
        };

         $scope.toBroadcastdetails = {
            user:
            {
                username: $scope.Advertiser2.user.username,
                email: $scope.Advertiser2.user.email,
                phonenumber:$scope.Advertiser2.user.phonenumber,
            },
            facebook:$scope.Advertiser2.facebook,
            twitter:$scope.Advertiser2.twitter,
            
            country:$scope.Country,
        };

        var PutAdvertiser = AdvertiserProfileService.putEditadvertiserBasicDetails($scope.newAdvertiserdetails);
        PutAdvertiser.then(function (pl) {
             $cookies.remove('Advertiser_profile');
             $scope.$parent.Advertiser = $scope.toBroadcastdetails;
            //$rootScope.$broadcast("updateShareuserAdvertiser", $scope.newAdvertiserdetails);

            $state.go('profile.basic', {}, { reload: false });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong>  updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});


 /*---====Change Password Controller =====--*/   

advertiserProfileApp.controller('ChangePasswordController', function ($scope, AdvertiserProfileService, ShareData, $rootScope, $state, SharedMethods) {
    $rootScope.title = "Change Password | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.headerClass = "normal-page-title";
    $rootScope.InnerHeading = "Change Password";
    $rootScope.activeView = "password";

    $scope.editing = false;

    $scope.change = function () {
        $scope.editing = true;
    
        $scope.passwordetails = {
            current_password: $scope.Currentpassword,
            password: $scope.Password,
            password_confirmation: $scope.Confirmpassword
        };
        console.log($scope.passwordetails);
    
        var putpassword =AdvertiserProfileService.changepassword($scope.passwordetails)
        putpassword.then(function (pl) {
            $scope.Currentpassword = "";
            $scope.Password = "";
            $scope.Confirmpassword = "";
            $scope.fieldErrors = null;
            SharedMethods.createSuccessToast('<strong>Password</strong>  changed successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});