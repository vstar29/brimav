/*-------========== advertiser ADVERTS CONTROLLER =======-------*/

/*--==== Adert Template Controller ====--*/
;
advertiserAdvertApp.controller('AdvertTemplateController', function ($rootScope,$uibModal, $state, $scope) {
    $rootScope.headerClass = "normal-page-title";
     
    

    //Set default filter, sort parameters
    
    $scope.Filterview = "unconfirmed";
    $scope.sortKey = "date";
    $scope.reverse=false;
    $rootScope.advertview = true;


    $scope.$watch('Filterview', function (filterkey) {
    $state.go('advert.'+filterkey); // Go to the state selected
    $scope.Filterview=filterkey; //set the Filter to the param passed
    });

     /*--- Advert type selection---*/

    $scope.selectType=function()
    {
        
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        var modalInstance=$uibModal.open({

            templateUrl: 'advertiseradvert/create/adverttype',
            controller: 'ModalTerminator',

        });
        modalInstance.opened.then(function()
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        },function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        })
    }


    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    
    // confirm advert as complete 

    $scope.confirmadvert = function (id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var confirmationDetails = {
            id : id,
            headerContent: "Confirm Advert " ,
            bodyMessage: " Are you sure you want to confirm this Advert?",
            buttonText: "",
            buttonClass: "btn-adgold",
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'advertiseradvert/shared/multimodal',
            controller: 'ConfirmAdvertController',
            resolve: {
                ConfirmationDetails: function () {
                    return confirmationDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            
        });
    };
});
   



/*--==== Confirmed Advert Controller ====--*/

advertiserAdvertApp.controller('ConfirmedAdvertController', function ($scope, ConfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Confirmed Adverts";
    $rootScope.SubPageHeading = null;


    $scope.contentLoading = true; // show loading icon
    $scope.ConfirmedAdverts=ConfirmedAdverts;
    $scope.Type="music";


    // Delete advert

    $scope.deleteadvert = function (id,returnState) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            headerContent: "Add to Archive " ,
            bodyMessage: " Are you sure you want to add this Advert to archive ?",
            buttonText: "",
            buttonClass: "btn-danger",
            id:id,
            returnState:returnState
        };


        var modalInstance = $uibModal.open({
            templateUrl: 'advertiseradvert/shared/multimodal',
            controller: 'DeleteAdvertController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;}
                }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {

        }, function () {
            
        });


    };
    
    
});


/*--==== UnConfirmed Advert Controller ====--*/

advertiserAdvertApp.controller('UnConfirmedAdvertController', function ($scope, UnconfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Unconfirmed Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon
    $scope.UnconfirmedAdvert=UnconfirmedAdverts;
   


   
 
    });

    


/*--==== Advert Details Controller ====--*/

advertiserAdvertApp.controller('AdvertDetailsController', function ($scope, $stateParams, $rootScope,$uibModal, $stateParams, AdvertDetails) {

    //$scope.Advert = AdvertDetails.Advert;
    $scope.Advertlink=AdvertDetails;
    $scope.returnState=$stateParams.returnState;
  
    $rootScope.title =  "Advert | Brimav";
    $rootScope.PageHeading = " Advert ";
    $rootScope.SubPageHeading = "Links" ;

     // confirm advert as complete 

    $scope.confirmlink = function (link_id,publisher_id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var confirmationDetails = {
            headerContent: "Confirm Link " ,
            bodyMessage: " Are you sure you want to confirm this link ?",
            buttonText: "",
            buttonClass: "btn-adgold",
            returnView:'advertlinks({id:' +$stateParams.id +'})'
        };

        var confirmrequirements=
        {
            link_id:linkid,
            publisher_id:publisher_id
        }

        var modalInstance = $uibModal.open({
            templateUrl: 'advertiseradvert/shared/multimodal',
            controller: 'ConfirmAdvertLinkController',
            resolve: {
                ConfirmationDetails: function () {
                    return confirmationDetails;
                },
                 ConfirmRequirements: function () {
                    return confirmrequirements;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            
        });
    };


    

});
 

/*--==== Confirm Advert  Controller ====--*/

advertiserAdvertApp.controller('ConfirmAdvertController', function ($scope,$cookies, $uibModalInstance, AdvertiserAdvertService, $state, ConfirmationDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ConfirmationDetails.id;
    $scope.title = ConfirmationDetails.headerContent;
    $scope.body = ConfirmationDetails.bodyMessage;
    $scope.action = ConfirmationDetails.buttonText;
    $scope.class = ConfirmationDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdvertiserAdvertService.confirmadvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advert.unconfirmed', {}, { reload: true });
            $cookies.remove('Publisher_profile');
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was confirmed successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Advert links yet to complete. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});





/*--==== Confirm Advert Link  Controller ====--*/

advertiserAdvertApp.controller('ConfirmAdvertLinkController', function ($scope, ConfirmRequirements, $uibModalInstance, AdvertiserAdvertService, $state, ConfirmationDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.title = ConfirmationDetails.headerContent;
    $scope.body = ConfirmationDetails.bodyMessage;
    $scope.action = ConfirmationDetails.buttonText;
    $scope.class = ConfirmationDetails.buttonClass;
    $scope.ConfirmRequirements=ConfirmRequirements;
    $scope.returnView=ConfirmationDetails.returnView;
    

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdvertiserAdvertService.confirmadvertlink($scope.ConfirmRequirements);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
        $state.go('advertlinks', {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Link </strong>  was confirmed successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast(' Problem confirming <strong> Advert</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});




/*--==== NEW ADVERT CONTROLLERS AND FUNCTIONS ====--*/


/*--==== Song Upload Controller ====--*/

advertiserAdvertApp.controller('SongUploadController', function ($scope, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {

     $rootScope.activeView = "upload";
     $rootScope.advertview = true;
    /*Song Metadata View/Edit*/

    $scope.songPreview=function (event)
    {
        $rootScope.Albumart  = "";
        var song=event.target.files[0]
        $rootScope.song=song;

        var success_callback = function (metadata) 
        {
        
            $state.go('addadvert.metadata');
            $rootScope.Metadata=metadata;
            
            var reader = new FileReader();

            reader.onload = function(e) 
            {
                $rootScope.Albumart  = reader.result; 
                 
            }

            if($rootScope.Metadata.picture!="" && $rootScope.Metadata.picture!=undefined)
            {
               
                reader.readAsDataURL($rootScope.Metadata.picture);
            }
            else
            {
                $rootScope.Albumart="/uploads/images/theme/revolution1-slider-img-bg.jpg";
            }

        };
        var error_callback = function (e) {

        };
        parse_audio_metadata(song, success_callback, error_callback);

    }

    

    /*---- Song Upload Function ----*/
    $scope.songUpload = function()
    {
        $scope.uploading=true;
        var song= $rootScope.song;
        if( song!=undefined && song.type.match ("audio/mp3") || song.type.match ("audio/mpeg") && song.size <= 20000000 )
        {

           $scope.song=song;
           var postSong=  AdvertiserAdvertService.postSong($scope.song);
           postSong.then(function(response)
           {
             $scope.uploadedsonglink=response.data;
             $state.go('addadvert.music',{songurl:$scope.uploadedsonglink,type:'music'},{reload:false});
             $rootScope.progress="";
           },
           function(error)
           {
             $scope.error=true;
             SharedMethods.showValidationErrors($scope,error);
           })
           .finally(function()
           {
            $scope.uploadsong=false;
           });
        } 
         else
        {
            $scope.uploadsong=false;
            $state.go('addadvert.uploadsong');
            SharedMethods.createErrorToast('Select a valid mp3 file less than 15mb');
            $scope.label="Upload";
            
        } 
        
    }


    /*Verify Song Url*/
    $scope.verifySongrl=function()
    {
        $scope.editing=true;
        $scope.url=
        {
            url:$scope.Songurl
        };

        var verifyUrl=AdvertiserAdvertService.verifySongUrl($scope.url);
        verifyUrl.then(function(pl)
        {
            $state.go('addadvert.music',{songurl:pl.data,type:'music'});
        },
        function(error)
        {
            SharedMethods.showValidationErrors($scope, error)
        })
        .finally(function()
        {
            $scope.editing=false;
        })
    };

});

/*--==== Modal Terminator Controller ====--*/

advertiserAdvertApp.controller('ModalTerminator', function ($state, $window,$uibModalInstance,$scope) {

    $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    };

     /*---Advert Type Dictator---*/

    $scope.typeSelection=function()
    {
      if($scope.Type=="music")
        {
            $scope.close();
            $state.go('addadvert.uploadsong',{},{});
        }
        else if($scope.Type=="banner")
        {

            $scope.close();
            $state.go('addadvert.banner',{type:'banner'},{reload:false});
        }
        else
        {
            $scope.close();
            $state.go('addadvert.others',{type:'others'},{reload:false});
        }
        
   };

    $scope.print=function()
    {
        
        var url=$state.href('printreceipt',{printdetails:$scope.printdetails});
        $window.open(url,'blank');
      
    }
      

});




/*--==== Create Add Controller ====--*/

advertiserAdvertApp.controller('CreateAdvertController', function ($scope,$cookies, $state, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {


    $rootScope.title='Create Advert | Brimav';
    $rootScope.PageHeading="Create"
    $rootScope.SubPageHeading=null
    $scope.Defaultpics = "/uploads/images/theme/uploadicon.jpg";
    $scope.songurl=$stateParams.songurl;
    $rootScope.activeView = "basic";
    $rootScope.advertview = true;
    $scope.type=$stateParams.type;
    $scope.Genres=Genres;
    $scope.SelectedBannerType=[]

    if($scope.type=='banner')
    {
        loadBannerTypes();
   
    }

     function loadBannerTypes()
    {
        var getBannerTypes=AdvertiserAdvertService.getBannerTypes();
        getBannerTypes.then(function(pl)
        {
            $scope.BannerTypes=pl.data;

        },
        function(error)
        {
            //SharedMethods.createErrorToast('Problem getting Banner Types');
        })
    }
   

/*---- Image  preview & upload ----*/

    $scope.stepsModel = [];

    $scope.imageUpload = function(event)
    {
        $scope.stepsModel=[];
          var file = event.target.files[0]; //FileList object

        if(file.type.match ("image/jpeg") || file.type.match ("image/png") || file.type.match ("image/gif") )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
             $scope.featuredImage=file;
        } 
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
        });
    };




    /*---- Attachment  upload(pdf & doc x) ----*/


    $scope.fileUpload = function(event)
    {
      var file = event.target.files[0]; //FileList object
      
        if(file.type.match ("application/pdf") || file.type.match ("application/vnd.openxmlformats-officedocument.wordprocessingml.document") || file.type.match ("application/msword") && file.size <=5000000  )
        {
            $scope.Attachment=file;
        }
        
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

      
    $scope.create=function()
    {
        $scope.creating=true;
        
        $scope.newAdvert=
        {
            title:$scope.Title,
            description:$scope.Description,
            genre:$scope.Genre,
            song:$scope.songurl,
            type:$scope.type,
            attachment:$scope.Attachment,
            image:$scope.featuredImage,
            youtube:$scope.Youtube,
            destination:$scope.Destination,
            itunes:$scope.Itunes 
        };

        if($scope.SelectedBannerType=="" && $scope.type=='banner')
        {
             SharedMethods.createErrorToast('Select atleast one banner type ');
              $scope.continue=false;
              return false;
        }
         
        $cookies.putObject('selected_banner_type', $scope.SelectedBannerType);
         var postAdvert = AdvertiserAdvertService.postAdvert($scope.newAdvert);

        postAdvert.then(function (response) { 

        $state.go('addadvert.selectpublishers', {id:response.data,returnState:$scope.type,type:$scope.type,status:'new'});
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function()
        {
            $scope.creating = false;
        });
    }; 

     $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    }     

});


/*--==== Edit Advert Controller ====--*/

advertiserAdvertApp.controller('EditAdvertController', function (Advert,$scope,$cookies, $state, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {


    $rootScope.title='Create Advert | Brimav';
    $rootScope.PageHeading="Create"
    $rootScope.SubPageHeading=null
    $rootScope.activeView = "basic";
    $rootScope.advertview = true;

    $scope.Genres=Genres;
    $scope.Advert=Advert;
    $scope.SelectedBannerType=[]; //availlable only when type=banner;


    if($scope.Advert.type=="music")
    {
        $scope.returnView="editbasicinfo";
    }

    else if($scope.Advert.type=="others")
    {
        $scope.returnView="editothers";
    }
    else if($scope.Advert.type=="banner")
    {
        $scope.returnView="editbanner";
        loadBannerTypes();

    }

   

    function loadBannerTypes()
    {
        var getBannerTypes=AdvertiserAdvertService.getBannerTypes();
        getBannerTypes.then(function(pl)
        {
            $scope.BannerTypes=pl.data;

        },
        function(error)
        {
            //SharedMethods.createErrorToast('Problem getting Banner Types');
        })
    }


    

/*---- Image  preview & upload ----*/
  $scope.featuredImage=Advert.imageurl;
    $scope.stepsModel = [];

    $scope.imageUpload = function(event)
    {

        $scope.stepsModel=[];
          var file = event.target.files[0]; //FileList object

        if(file.type.match ("image/jpeg") || file.type.match ("image/png") || file.type.match ("image/gif") && file.size>0 )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
             $scope.featuredImage=file;
        } 
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
        });
    };




    /*---- Attachment  upload(pdf & doc x) ----*/

    $scope.Attachment=$scope.Advert.attachment;
    $scope.fileUpload = function(event)
    {
      var file = event.target.files[0]; //FileList object
      
        if(file.type.match ("application/pdf") || file.type.match ("application/vnd.openxmlformats-officedocument.wordprocessingml.document") || file.type.match ("application/msword") && file.size <=5000000 && file.size>0  )
        {
            $scope.Attachment=file;
        }
        
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

      
    $scope.create=function()
    {
        $scope.creating=true;
        $scope.newAdvert=
        {
            id:$stateParams.id,
            title:$scope.Advert.title,
            description:$scope.Advert.description,
            genre:$scope.Advert.genre,
            song:$scope.Advert.songurl,
            type:$scope.Advert.type,
            attachment:$scope.Attachment,
            image:$scope.featuredImage,
            youtube:$scope.Advert.youtube,
            destination:$scope.Advert.destination,
            itunes:$scope.Advert.itunes  
        };

          
        $cookies.putObject('selected_banner_type', $scope.SelectedBannerType);
         if($scope.SelectedBannerType=="" && $scope.Advert.type=='banner')
        {
             SharedMethods.createErrorToast('Select atleast one banner type ');
              $scope.continue=false;
              return false;
        }

         var putAdvert = AdvertiserAdvertService.putAdvert($scope.newAdvert);


        putAdvert.then(function (response) {  

        $state.go('addadvert.selectpublishers', {id:response.data,returnState:$scope.returnView,type:$scope.Advert.type,status:'edit'});
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function()
        {
            $scope.creating = false;
        });
    }; 

     $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    }     

   
});




/*--==== Select Advert Publishers Controller ====--*/

advertiserAdvertApp.controller('SelectAdvertPublishersController', function ($scope,$cookies, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {
 $rootScope.activeView = "selectpublisher";
 $scope.returnState=$stateParams.returnState;
 $scope.id=$stateParams.id;
 $scope.Type=$stateParams.type;
 var usd_naira="";
 var usd_ghs="";
 var bannertype=$cookies.getObject('selected_banner_type');
 $scope.Currency='NGN';
var offset=0;
 $scope.rate="1";


    if($scope.Type=='banner')
    {
        loadBannerPublishers(bannertype);
      
    }
    else
    {
         loadallPublishers();
        
    }
   

    function loadallPublishers()
    {
        var getPublishers=AdvertiserAdvertService.getallPublishers(offset);
        getPublishers.then(function(response)
        {
            $scope.Websites=response.Websites;
            $scope.Tvs=response.Tvs;
            $scope.Socials=response.Socials;
            usd_naira=response.Rates.usd_naira;
            usd_ghs=response.Rates.usd_ghs;
            offset+=30;
        })
    };

     function loadBannerPublishers(type)
    {

    
        var getPublishers=AdvertiserAdvertService.getBannerPublishers(type,offset);
        getPublishers.then(function(response)
        {
            $scope.Banners=response.Banners;
            offset+=30;
            usd_naira=response.Rates.usd_naira;
            usd_ghs=response.Rates.usd_ghs;
        })
    };



    $scope.$watch('Currency',function(n_currency)
    {
        $scope.Currency=n_currency;
        if(n_currency=="USD")
        {
             $scope.rate=1/usd_naira;
        }
        else
        {
             $scope.rate=1
        }

    });

        /*---get exchange rate in background--*/
         /*var promise=AdvertiserAdvertService.getExchangerate();
       promise.then(function(pl)
        {
            $scope.Rates=pl.rates;
            rate=1/parseInt($scope.Rates.NGN);
        })*/


    $scope.loadMorePublisher=function()
    {
        var loadmore=AdvertiserAdvertService.getallPublishers(offset);
        getPublishers.then(function(response)
        {
            if($scope.Type=='banner')
            {
                $scope.Banners=response;
            }
            else
            {
                $scope.Websites=response.Websites;
                $scope.Tvs=response.Tvs;
                $scope.Socials=response.Socials;
            }
            
            offset+=30;
        })
    }


    $scope.SelectedPublishers = {};
     $scope.noofwebsite=0;
    $scope.Total=0;

/*---- Total price calculation ----*/
    $rootScope.addTotalprize=function(publisherprize)
    {
        $scope.noofwebsite= $scope.noofwebsite +1;
        $scope.Total=$scope.Total + publisherprize;
    };
    
    $rootScope.substractTotalprize=function(publisherprize)
    {
        $scope.noofwebsite= $scope.noofwebsite - 1;
        $scope.Total=$scope.Total - publisherprize;     
    };

    $scope.save=function(){
        
        $scope.continue=true;
        if($scope.SelectedPublishers=="")
        {
             SharedMethods.createErrorToast('Select websites');
             $scope.continue=false;
        }
        else
        {
           
            var publishersplusamount=
            {
                id:$stateParams.id,
                publishers:$scope.SelectedPublishers,
                totalamount:$scope.Total,
                status:$stateParams.status
            };

            
            var putPublisherAndTotalamount=AdvertiserAdvertService.putPublisherAndTotalamount(publishersplusamount);
            putPublisherAndTotalamount.then(function(response)
            {
                
                $state.go('advertpayment', {id:response.data,no: $scope.noofwebsite,rate:1/usd_naira});
            },

            function(error)
            {
                SharedMethods.showValidationErrors($scope,error);
            })
            .finally(function()
            {
                $scope.continue=false;
            });
        }

    };

});


/*--==== Advert Payment Controller ====--*/

advertiserAdvertApp.controller('AdvertPaymentController', function ($scope, $cookies,$uibModal,$base64,Advert,$stateParams, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {

$rootScope.title='Payment | Brimav';
$rootScope.PageHeading="Create";
$rootScope.SubPageHeading="Payment";
$scope.Advert=Advert
$scope.WebsitesNo=$stateParams.no;
$scope.newprize=$scope.Advert.totalamount;
$scope.discount=0;
$scope.rate=$stateParams.rate;
getAdvertiser();

function getAdvertiser()
{
    var getAdvertiserdetails=AdvertiserAdvertService.getAdvertiserdetails();
    getAdvertiserdetails.then(function(response)
    {
        $scope.Advertiser=response.data;
        
    })
    .finally(function()
    {
        $scope.loading=false; 
    })
};


 /* Verify Coupon Code*/

    $scope.verifyCoupon=function()
    {
        $scope.creating=true;
        $scope.Details=
        {
            couponcode:$scope.Coupon,
            totalamount:$scope.Advert.totalamount,
            advertid:$scope.Advert.id
        };
     
        var verifycoupon=AdvertiserAdvertService.verifycoupon($scope.Details);
        verifycoupon.then(function(pl)
        {
            $scope.newDetails=pl.data;
            $scope.Advert.totalamount=$scope.newDetails.newprize;
            $scope.newprize=$scope.newDetails.newprize;
            $scope.discount=$scope.newDetails.newprize
        },
        function(error)
        {
           
            SharedMethods.createErrorToast('Invalid/Expired Coupon');
        })
        .finally(function()
        {
            $scope.creating=false;
        })

    }


 /* Payment Functions and Configuration*/
    try{
              var handler = SimplePay.configure({
        token: paymentConfirmation, 
        key: 'test_pu_805b105d76274ebda0fdb78b1e4d83c6',  
        image: 'https://brimav.com/uploads/images/logo/brimavcolor.png' 
    });
    }catch(exception)
    {
        
    }
 


    $scope.lunchpayment =function()
        {
          
           
           try{
                 handler.open(SimplePay.CHECKOUT, 
            {
                email: $scope.Advertiser.user.email,
                description: 'Payment for Digital Promotion',
                country: $scope.Advertiser.user.country,
                amount: $scope.newprize+'00',
                currency: $scope.Advertiser.user.currency,
                phone:$scope.Advertiser.user.phonenumber
            });
          }catch(exception)
           {
                alert("Connection error Reload");
           }
            
        };



    function paymentConfirmation(token)
    {
        $scope.loadingView=true;
        //$scope.username=$base64.encode('test_pr_5996c4f02397440fb3821199048827c6'+':');
        $scope.verificationdetails=
        {

            token : token,
            amount: $scope.newprize+'00',
            amount_currency :$scope.Advertiser.user.currency,
            id:$scope.Advert.id,
            websiteno:$scope.WebsitesNo,
            advertiser:$scope.Advertiser.id
            
        };
        
        
       var verifypayment=AdvertiserAdvertService.verifypayment($scope.verificationdetails);

        verifypayment.then(function(response)
        {
            $scope.receipt=response.data;
           
            
            $scope.printdetails=
            {   
                username: $scope.Advertiser.user.username,
                city: $scope.Advertiser.user.city,
                country: $scope.Advertiser.user.country,
                amount: $scope.newprize,
                totalamount: $scope.Advert.totalamount,
                discount:$scope.discount,
                currency:$scope.Advertiser.user.currency,
                cardtype:$scope.receipt.cardtype,
                cardnumber:$scope.receipt.cardnumber,
                phone: $scope.Advertiser.user.phonenumber,
                receiptnumber:'BRM'+$scope.receipt.receipt_number,
                date:$scope.receipt.created_at,
                websiteno:$scope.receipt.websitenumber,
                receiptid:$scope.receipt.id,
            };
            $scope.success($scope.printdetails);
            },
            function(error)
            {
                SharedMethods.createErrorToast('Promotion failed, Try again');
            })
        
        .finally(function()
        {
            $scope.loadingView=false;
        });
    }

    $scope.success=function(PrintDetails)
    {
          $cookies.remove('Publisher_profile');
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        var modalInstance=$uibModal.open({

            templateUrl: 'advertiseradvert/create/advertsuccess',
            controller: 'ReceiptController',
            resolve:
            {
               printdetails:function()
               {
                return PrintDetails;
               }
            }

        });
        modalInstance.opened.then(function()
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        },function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        })
    }


});




/*--==== Receipt preview/print Controller ====--*/

advertiserAdvertApp.controller('ReceiptController', function ($state,printdetails,AdvertiserAdvertService, $window,$uibModalInstance,$scope) {

   
    $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    };

    

    $scope.PrintDetails=printdetails;
   

    $scope.print=function()
    {
        //$state.go('printreceipt',{id:$scope.PrintDetails.receiptid},{});
       // var url=$state.href('printreceipt',{id:$scope.PrintDetails.receiptid},{});
        $window.open("/api/receipt/"+$scope.PrintDetails.receiptid,'blank');
        //window.location.href="/api/receipt/"+$scope.PrintDetails.receiptid;
      
    }


    $scope.download=function()
    {
        
        var downloadPDF=AdvertiserAdvertService.downloadPDF(
    $scope.PrintDetails.receiptid);       
    }
      

});


/*--==== Delete Advert  Controller ====--*/

advertiserAdvertApp.controller('DeleteAdvertController', function ($scope, $uibModalInstance, AdvertiserAdvertService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class = DeleteDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdvertiserAdvertService.deleteAdvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go(DeleteDetails.returnState, {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was added to Archive successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem adding <strong> Advert</strong> to Archive. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});


/*--==== Page Print Controller ====--*/

advertiserAdvertApp.controller('PagePrintController', function ($state,$rootScope,$stateParams, Receipt, $window,$scope) {

    $scope.PrintDetails=Receipt;
    $rootScope.title="Receipt | Brimav";
    
})




/*GENRES LIST */

var Genres=["Blues", "Hip-Hop","Rap", "Fuji","Jazz","Gospel","Rap/Hip-Hop","R&B","Reggae","Rock","Country","Others"];
