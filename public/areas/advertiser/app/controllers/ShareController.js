/*-------========== ADVERTISER SHARED CONTROLLER =======-------*/

/*--==== Get  Logged In-User Details ====--*/
advertiserShareApp.controller('AdvertiserShareController', function ($scope,$rootScope, ShareService, ShareData) {

  loadAdvertiserInfo();
    

    function loadAdvertiserInfo() {
        
        var promiseGetAdvertiserInfo = ShareService.getLoggedInAdvertiser();
        promiseGetAdvertiserInfo.then(function (pl)
        {
         

          var Advertiserdetails = pl.data;
          $scope.Advertiser = pl.data.Advertiser;
          $scope.Notifications=pl.data.Notifications;
          $scope.Avatar = $scope.Advertiser.user.picture;
          
        },
        function (errorPl) {
            $scope.error = errorPl;
           
            
        });
    };

     $scope.markRead=function()
    {
        if($scope.Notifications.length>0)
        {
           var markRead = ShareService.markRead();

          markRead.then(function (pl)
          {

          });
        }
       
    }

          
});

