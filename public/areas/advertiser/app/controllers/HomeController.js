﻿// HomeController
advertiserHomeApp.controller('HomeController', function ($scope, $uibModal, HomeService, $state, $rootScope) {

    $rootScope.title = "Home | Brimav";
    $scope.home=true;

    getAdvert();


     function getAdvert()
    {

    	var fetchAdvert=HomeService.getUnpaidadvert();
    	fetchAdvert.then(function(response)
    	{
    		$scope.Adverts=response;
           
    	},
    	function(error)
    	{

    	})
    };

    $scope.go=function(id,type)
    {
        
        if(type=="music")
        {
             
          $state.go('addadvert.editbasicinfo',{id:id},{reload:false});  
        }
        else if(type=="banner")
        {
            $state.go('addadvert.editbanner',{id:id},{reload:false});
        }
        else
        { 
            $state.go('addadvert.editothers',{id:id},{reload:false}); 
        }
    	
    };



    $scope.delete=function(id,type)
    {
        
    	var deleteAdvert=HomeService.deleteAdvert(id);
    	deleteAdvert.then(function(response)
    	{
    		$state.go('home',{},{reload:true});
    	},
    	function(error)
    	{
            alert("error");
    	})
    };


    $scope.selectType=function()
    {
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        var modalInstance=$uibModal.open({

            templateUrl: 'advertiseradvert/create/adverttype',
            controller: 'ModalTerminator',

        });
        modalInstance.opened.then(function()
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        },function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        })
    };



});


/*--==== Modal Terminator Controller ====--*/

advertiserHomeApp.controller('ModalTerminator', function ($state,$window,$uibModalInstance,$scope) {

    $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    };

     /*---Advert Type Dictator---*/

    $scope.typeSelection=function()
    {
        try
        {

              if($scope.Type=="music")
            {
                $scope.close();
                $state.go('addadvert.uploadsong',{},{});
            }
            else if($scope.Type=="banner")
            {

                $scope.close();
                $state.go('addadvert.banner',{type:'banner'},{reload:false});
            }
            else
            {
                $scope.close();
                $state.go('addadvert.others',{type:'others'},{reload:false});
            }

        }catch(Exception)
        {

        }

       
    };      

});