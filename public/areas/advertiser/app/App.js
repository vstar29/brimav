/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var advertiserShareApp = angular.module('AdvertiserShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','base64','angularMoment','infinite-scroll','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
       
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {
           
            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }

        } 
        else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }
    


    /* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
    
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="glyphicon glyphicon-ok alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning alert-danger margin-right-04"></i>' + cont
        });
    }
    
    /* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }
})
.config(function ($urlRouterProvider, $locationProvider, $breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider) {
    //debugger; //break point

    //modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'left',
        timeout: 4000,
        maxNumber: 3,
        animation: 'fade',
        dismissButton: true,
    });

   

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    /*$breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/advertiser/home" target="_self">Home&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp; &#xf18e &nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });*/

    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
    
    //make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
})
.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})




/*MULTI OPTIONS DROP DOWN SELECT DIRECTICE */
.directive('dropdownMultiselect', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            websites: '=',
            tvs: '=',
            socials:'=',
            currency:'=',
            rate:'='
        },
        templateUrl:"advertiseradvert/create/publisherlist",

        controller: function ($scope) {

            $scope.openDropdown = function () {

                $scope.open = !$scope.open;

            }
            $scope.toggleSelectItem = function (option,type) {
                var intIndex = -1;
                var intprize=0;
                var name="";
                switch(type)
                {
                    case "websites":
                    name=option.websitename;
                    break;
                    case "tvs":
                    name=option.channel_name;
                    break;
                      case "socials":
                    name=option.handler;
                    break;

                }

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.id && item==option.prize+","+option.publisher_id+","+name+","+type) {

                        intIndex = index;
                        intprize=option.prize;

                    }

                });

                if (intIndex >0) {
                   
                   delete $scope.model[option.id];
                  $scope.gettotalprize=$rootScope.substractTotalprize(intprize);
                }
                else {

                    $scope.model[option.id]=option.prize+","+option.publisher_id+","+name+","+type;
                    $scope.gettotalprize=$rootScope.addTotalprize(option.prize);
                }

            };

            $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.id) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };

            

        }
    };

})

/*Publishers banner selection */
.directive('publisherSelect', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            banners: '=',
            currency:'=',
            rate:'='
        },
        templateUrl:"advertiseradvert/create/bannerpublisherslist",

        controller: function ($scope) {

            $scope.openDropdown = function () {

                $scope.open = !$scope.open;

            }
            $scope.toggleSelectItem = function (option) {
                var intIndex = -1;
                var intprize=0;

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.name && item==option.prize+","+option.publisher_id) {

                        intIndex = index;
                        intprize=option.prize;

                    }

                });

                if (intIndex !=-1) {
                   
                   delete $scope.model[option.name];
                  
                }
                else {

                    $scope.model[option.name]=option.prize+","+option.publisher_id;
                    $scope.gettotalprize=$rootScope.addTotalprize(option.prize);

                }

            };

            $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.name) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };

            

        }
    };

})


/*MULTI  SELECT FOR BANNER TYPE DIRECTICE */
.directive('bannertypeMultiselect', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        template:
                "<div class='multi_select'>" +
                   
                    "<ul class='  ' >" +
                        "<li class='pointer' data-ng-repeat='option in options'><a data-ng-click='toggleSelectItem(option)'><span data-ng-class='getClassName(option)'></span> [[option.bannertype_name]]  ( [[option.size]] )</a></li>" +
                    "</ul>" +
                "</div>",

        controller: function ($scope) {

    

            $scope.selectAll = function () {

                $scope.model = [];

                angular.forEach($scope.options, function (item, index) {

                    $scope.model.push(item.id);
                   
                });

            };

            $scope.deselectAll = function () {

                $scope.model = [];
                
            };

            $scope.toggleSelectItem = function (option) {
                var intIndex = -1;

                angular.forEach($scope.model, function (item, index) {

                    if (item == option.id) {

                        intIndex = index;

                    }

                });

                if (intIndex >= 0) {
                   
                   var removedid= $scope.model.splice(intIndex, 1);
                  
                }
                else {

                    $scope.model.push(option.id);
                   

                    }

            };

            $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (item == option.id) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };

            

        }
    };

})




.run(function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
});




/*===================== HOME SPA ====================*/
var advertiserHomeApp = angular.module('AdvertiserHomeModule', ['AdvertiserShareModule','AdvertiserAdvertModule'])
 
.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
    //debugger; //break point
   
    $urlRouterProvider.otherwise('/advertiser/home');
   
    $stateProvider
    .state('home', {
        url: '/advertiser/home',
        templateUrl: 'advertiserhome/home',
        controller: 'HomeController'
    });
})
.run(function ($rootScope) {
    $rootScope.homePage = true;
    $rootScope.advertview = false;
});
   




    /*===================== PROFILE SPA ====================*/
   var advertiserProfileApp = angular.module('AdvertiserProfileModule', ['AdvertiserShareModule'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/advertiser/profile');
        
        $stateProvider
        // Template for the Profile Pages
        .state('profile', {
            abstract: true,
            url: '/advertiser/profile',
            templateUrl: 'advertiserprofile/home',
            controller: 'ProfileTemplateController',
            resolve: {
                Advertiser: function (AdvertiserProfileService) {
                    return AdvertiserProfileService.getAdvertiserBasicDetails(); 
                }
            }
        })

        //Profile (Display Advertiser Profile)
        .state('profile.basic', {
            url: '',
            templateUrl: 'advertiserprofile/basicdetails',
            controller: 'BasicDetailsController',
            resolve: {
                Advertiser: function (Advertiser) {
                    return Advertiser;
                }
            },
            ncyBreadcrumb: {
            label: 'Profile'
        }
            
        })
       
        // Edit mode for Advertiser Basic Details
        .state('profile.editbasic', {
            url: '/edit',
            templateUrl: 'advertiserprofile/editadvertiserbasicdetails',
            controller: 'EditBasicProfileController',
            resolve: {
                Advertiser: function (Advertiser) {
                    return Advertiser;
                }
            },
            ncyBreadcrumb: {
                label: 'Edit Basic Details',
                parent: 'profile.basic'
            }
                
        })

         // Change Password
        .state('profile.changepassword', {
            url: '/password',
            templateUrl: 'advertiserprofile/changepassword',
            controller: 'ChangePasswordController',
            resolve: {
                Advertiser: function (Advertiser) {
                    return Advertiser;
                }
            },
            ncyBreadcrumb: {
                label: 'Change Password',
                parent: 'profile.basic'
            }
        })  

    })
    .run(function ($rootScope) {
    $rootScope.profilePage = true;

    

});


/*===================== ADVERT SPA ========================*/
var advertiserAdvertApp = angular.module('AdvertiserAdvertModule',['AdvertiserShareModule'])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {

    /* multiple selector config*/
    var startSymbol= $interpolateProvider.startSymbol('[[');
    var endSymbol= $interpolateProvider.endSymbol(']]');
     denormalizeTemplate = (startSymbol == '{{' || endSymbol  == '}}')
            ? identity
            : function denormalizeTemplate(template) {
              return template.replace(/\{\{/g, startSymbol).replace(/}}/g, endSymbol);
        },
        NG_ATTR_BINDING = /^ngAttr[A-Z]/;

        $urlRouterProvider.otherwise('/advertiser/advert/');
     

    $stateProvider
     // Get List of adverts
    .state('advert', {
        url: '/advertiser/advert/',
        abstract:true,
        templateUrl: 'advertiseradvert/home',
        controller: 'AdvertTemplateController',
        resolve:{

            
            UnconfirmedAdverts:function(AdvertiserAdvertService)
            {
                return AdvertiserAdvertService.getUncompletedAdverts();
            },

            ConfirmedAdverts:function(AdvertiserAdvertService)
            {
               return AdvertiserAdvertService.getCompletedAdverts();
            }
        }
        
    })

    // Get List of uncompleted adverts
    .state('advert.unconfirmed', {
        url: '',
        params: { returnState: "advert" },
        templateUrl: 'advertiseradvert/uncompleted',
        controller: 'UnConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'Advert'
        },
        resolve:
        {
            
            UnconfirmedAdverts:function(UnconfirmedAdverts)
            {
                return UnconfirmedAdverts;
            }
        }
    })
       
       

    // Get List of completed adverts
    
     .state('advert.confirmed', {
        url: '',
        templateUrl: 'advertiseradvert/completed',
        controller: 'ConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'completed',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            ConfirmedAdverts:function(ConfirmedAdverts)
            {
                return ConfirmedAdverts;
            }
        }
        
    })


    // Get Advert Links
    .state('advertlinks' , {
        url: '/advertiser/advert/links/:id',
        params:{returnState:'advert.unconfirmed'},
        templateUrl: 'advertiseradvert/details',
        controller: 'AdvertDetailsController',
        ncyBreadcrumb: {
            label: 'Links',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            AdvertDetails:function(AdvertiserAdvertService, $stateParams)
            {
                return AdvertiserAdvertService.getAdvertlinksbyid($stateParams.id);
            }

        }
    })

     /* Add New Advert States and Controllers*/

    .state('addadvert', {
        url: '/advertiser/advert/create',
        templateUrl: 'advertiseradvert/create/home',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label: 'Create',
            parent: 'advert.unconfirmed'
        }
        
    })

     .state('addadvert.uploadsong', {
        url: '/upload',
        templateUrl: 'advertiseradvert/create/uploadsong',
        controller: 'SongUploadController',
        ncyBreadcrumb: {
            label:'upload song',
            parent: 'addadvert'
        }
    })

    .state('addadvert.others', {
        url: '/others',
        params:{type:'others'},
        templateUrl: 'advertiseradvert/create/others',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        }
    })

      .state('addadvert.music', {
        url: '/details',
        params:{songurl:null,type:'music'},
        templateUrl: 'advertiseradvert/create/basicinfo',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        }
    })

        .state('addadvert.banner', {
        url: '/banner',
        params:{type:'banner'},
        templateUrl: 'advertiseradvert/create/banneradvert',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label:'banner advert',
            parent: 'addadvert'
        }
    })


    .state('addadvert.editothers', {
        url: '/basic/:id',
        params:{id:null},
        templateUrl: 'advertiseradvert/create/editothers',
        controller: 'EditAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        },
         resolve:
        {
            Advert:function(AdvertiserAdvertService, $stateParams)
            {
    
                return AdvertiserAdvertService.getAdvertbyid($stateParams.id);
            }

        }
    })

    .state('addadvert.editbanner' , {
        url: '/banner/:id',
        params:{id:null},
        templateUrl: 'advertiseradvert/create/editbanner',
        controller: 'EditAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        },
         resolve:
        {
            Advert:function(AdvertiserAdvertService, $stateParams)
            {
    
                return AdvertiserAdvertService.getAdvertbyid($stateParams.id);
            }

        }
    })

    .state('addadvert.editbasicinfo' , {
        url: '/music/:id',
        params:{id:null},
        templateUrl: 'advertiseradvert/create/editbasicinfo',
        controller: 'EditAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        },
         resolve:
        {
            Advert:function(AdvertiserAdvertService, $stateParams)
            {
               
                return AdvertiserAdvertService.getAdvertbyid($stateParams.id);
            }

        }
        
    })

    .state('addadvert.metadata' , {
                url: '/metadata',
                templateUrl: 'advertiseradvert/create/songmetadata',
                controller: 'SongUploadController',
                ncyBreadcrumb: {
                    label:'Metadata',
                    parent: 'addadvert'
                }   
        })

    .state('addadvert.selectpublishers' , {
        url: '/:type/publishers/:id',
        params:{returnState:'basicinfo',status:'edit'},
        templateUrl: 'advertiseradvert/create/selectpublishers',
        controller: 'SelectAdvertPublishersController',
        ncyBreadcrumb: {
            label:'Websites',
            parent: 'addadvert'
        }
    })

   

     // New Advert Payment 
    .state('advertpayment' , {
        url: '/advertiser/advert/payment/:id/:no/:rate',
        params:{id:null,no:null,rate:null},
        templateUrl: 'advertiseradvert/create/advertpayment',
        controller: 'AdvertPaymentController',
        resolve:{
            Advert:function(AdvertiserAdvertService, $stateParams)
            {
                return AdvertiserAdvertService. getAdvertbyid($stateParams.id);
            }
        },
        ncyBreadcrumb: {
            label: 'Payment',
            parent: 'addadvert'
        }
    })


     // New Advert Payment 
    .state('printreceipt' , {
        url: '/receipt',
        params:{id:null},
        templateUrl: 'advertiseradvert/receipt/index',
        controller: 'PagePrintController',
        ncyBreadcrumb: {
            label: 'Receipt',
            parent: 'advert'
        },
        resolve:
        {
            Receipt:function($stateParams, AdvertiserAdvertService)
            {
               return AdvertiserAdvertService.getReceipt($stateParams.id);
            }
        }
        
    })



 
   
})
.run(function ($rootScope) {
    $rootScope.advertPage = true;
    
});
