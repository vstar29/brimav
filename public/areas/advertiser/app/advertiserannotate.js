/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var advertiserShareApp = angular.module('AdvertiserShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','base64','angularMoment','infinite-scroll','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', ["ngToast", "$uibModalStack", function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
       
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {
           
            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }

        } 
        else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }
    


    /* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
    
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="glyphicon glyphicon-ok alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning alert-danger margin-right-04"></i>' + cont
        });
    }
    
    /* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }
}])
.config(["$urlRouterProvider", "$locationProvider", "$breadcrumbProvider", "ngToastProvider", "$uibModalProvider", "$urlMatcherFactoryProvider", "$interpolateProvider", function ($urlRouterProvider, $locationProvider, $breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider) {
    //debugger; //break point

    //modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'left',
        timeout: 4000,
        maxNumber: 3,
        animation: 'fade',
        dismissButton: true,
    });

   

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    /*$breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/advertiser/home" target="_self">Home&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp; &#xf18e &nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });*/

    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
    
    //make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
}])
.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})




/*MULTI OPTIONS DROP DOWN SELECT DIRECTICE */
.directive('dropdownMultiselect', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            websites: '=',
            tvs: '=',
            socials:'=',
            currency:'=',
            rate:'='
        },
        templateUrl:"advertiseradvert/create/publisherlist",

        controller: ["$scope", function ($scope) {

            $scope.openDropdown = function () {

                $scope.open = !$scope.open;

            }
            $scope.toggleSelectItem = function (option,type) {
                var intIndex = -1;
                var intprize=0;
                var name="";
                switch(type)
                {
                    case "websites":
                    name=option.websitename;
                    break;
                    case "tvs":
                    name=option.channel_name;
                    break;
                      case "socials":
                    name=option.handler;
                    break;

                }

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.id && item==option.prize+","+option.publisher_id+","+name+","+type) {

                        intIndex = index;
                        intprize=option.prize;

                    }

                });

                if (intIndex >0) {
                   
                   delete $scope.model[option.id];
                  $scope.gettotalprize=$rootScope.substractTotalprize(intprize);
                }
                else {

                    $scope.model[option.id]=option.prize+","+option.publisher_id+","+name+","+type;
                    $scope.gettotalprize=$rootScope.addTotalprize(option.prize);
                }

            };

            $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.id) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };

            

        }]
    };

}])

/*Publishers banner selection */
.directive('publisherSelect', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            banners: '=',
            currency:'=',
            rate:'='
        },
        templateUrl:"advertiseradvert/create/bannerpublisherslist",

        controller: ["$scope", function ($scope) {

            $scope.openDropdown = function () {

                $scope.open = !$scope.open;

            }
            $scope.toggleSelectItem = function (option) {
                var intIndex = -1;
                var intprize=0;

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.name && item==option.prize+","+option.publisher_id) {

                        intIndex = index;
                        intprize=option.prize;

                    }

                });

                if (intIndex !=-1) {
                   
                   delete $scope.model[option.name];
                  $scope.gettotalprize=$rootScope.substractTotalprize(intprize);

                }
                else {

                    $scope.model[option.name]=option.prize+","+option.publisher_id;
                    $scope.gettotalprize=$rootScope.addTotalprize(option.prize);

                }

            };

            $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (index == option.name) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };

            

        }]
    };

}])


/*MULTI  SELECT FOR BANNER TYPE DIRECTICE */
.directive('bannertypeMultiselect', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        template:
                "<div class='multi_select'>" +
                   
                    "<ul class='  ' >" +
                        "<li class='pointer' data-ng-repeat='option in options'><a data-ng-click='toggleSelectItem(option)'><span data-ng-class='getClassName(option)'></span> [[option.bannertype_name]]  ( [[option.size]] )</a></li>" +
                    "</ul>" +
                "</div>",

        controller: ["$scope", function ($scope) {

    

            $scope.selectAll = function () {

                $scope.model = [];

                angular.forEach($scope.options, function (item, index) {

                    $scope.model.push(item.id);
                   
                });

            };

            $scope.deselectAll = function () {

                $scope.model = [];
                
            };

            $scope.toggleSelectItem = function (option) {
                var intIndex = -1;

                angular.forEach($scope.model, function (item, index) {

                    if (item == option.id) {

                        intIndex = index;

                    }

                });

                if (intIndex >= 0) {
                   
                   var removedid= $scope.model.splice(intIndex, 1);
                  
                }
                else {

                    $scope.model.push(option.id);
                   

                    }

            };

            $scope.getClassName = function (option) {

                var varClassName = '';

                angular.forEach($scope.model, function (item, index) {

                    if (item == option.id) {

                        varClassName = 'fa  fa-check green margin-right-04';

                    }

                });

                return (varClassName);

            };

            

        }]
    };

}])




.run(["$rootScope", "SharedMethods", function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
}]);




/*===================== HOME SPA ====================*/
var advertiserHomeApp = angular.module('AdvertiserHomeModule', ['AdvertiserShareModule','AdvertiserAdvertModule'])
 
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
    //debugger; //break point
   
    $urlRouterProvider.otherwise('/advertiser/home');
   
    $stateProvider
    .state('home', {
        url: '/advertiser/home',
        templateUrl: 'advertiserhome/home',
        controller: 'HomeController'
    });
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.homePage = true;
    $rootScope.advertview = false;
}]);
   




    /*===================== PROFILE SPA ====================*/
   var advertiserProfileApp = angular.module('AdvertiserProfileModule', ['AdvertiserShareModule'])
    .config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/advertiser/profile');
        
        $stateProvider
        // Template for the Profile Pages
        .state('profile', {
            abstract: true,
            url: '/advertiser/profile',
            templateUrl: 'advertiserprofile/home',
            controller: 'ProfileTemplateController',
            resolve: {
                Advertiser: ["AdvertiserProfileService", function (AdvertiserProfileService) {
                    return AdvertiserProfileService.getAdvertiserBasicDetails(); 
                }]
            }
        })

        //Profile (Display Advertiser Profile)
        .state('profile.basic', {
            url: '',
            templateUrl: 'advertiserprofile/basicdetails',
            controller: 'BasicDetailsController',
            resolve: {
                Advertiser: ["Advertiser", function (Advertiser) {
                    return Advertiser;
                }]
            },
            ncyBreadcrumb: {
            label: 'Profile'
        }
            
        })
       
        // Edit mode for Advertiser Basic Details
        .state('profile.editbasic', {
            url: '/edit',
            templateUrl: 'advertiserprofile/editadvertiserbasicdetails',
            controller: 'EditBasicProfileController',
            resolve: {
                Advertiser: ["Advertiser", function (Advertiser) {
                    return Advertiser;
                }]
            },
            ncyBreadcrumb: {
                label: 'Edit Basic Details',
                parent: 'profile.basic'
            }
                
        })

         // Change Password
        .state('profile.changepassword', {
            url: '/password',
            templateUrl: 'advertiserprofile/changepassword',
            controller: 'ChangePasswordController',
            resolve: {
                Advertiser: ["Advertiser", function (Advertiser) {
                    return Advertiser;
                }]
            },
            ncyBreadcrumb: {
                label: 'Change Password',
                parent: 'profile.basic'
            }
        })  

    }])
    .run(["$rootScope", function ($rootScope) {
    $rootScope.profilePage = true;

    

}]);


/*===================== ADVERT SPA ========================*/
var advertiserAdvertApp = angular.module('AdvertiserAdvertModule',['AdvertiserShareModule'])
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {

    /* multiple selector config*/
    var startSymbol= $interpolateProvider.startSymbol('[[');
    var endSymbol= $interpolateProvider.endSymbol(']]');
     denormalizeTemplate = (startSymbol == '{{' || endSymbol  == '}}')
            ? identity
            : function denormalizeTemplate(template) {
              return template.replace(/\{\{/g, startSymbol).replace(/}}/g, endSymbol);
        },
        NG_ATTR_BINDING = /^ngAttr[A-Z]/;

        $urlRouterProvider.otherwise('/advertiser/advert/');
     

    $stateProvider
     // Get List of adverts
    .state('advert', {
        url: '/advertiser/advert/',
        abstract:true,
        templateUrl: 'advertiseradvert/home',
        controller: 'AdvertTemplateController',
        resolve:{

            
            UnconfirmedAdverts:["AdvertiserAdvertService", function(AdvertiserAdvertService)
            {
                return AdvertiserAdvertService.getUncompletedAdverts();
            }],

            ConfirmedAdverts:["AdvertiserAdvertService", function(AdvertiserAdvertService)
            {
               return AdvertiserAdvertService.getCompletedAdverts();
            }]
        }
        
    })

    // Get List of uncompleted adverts
    .state('advert.unconfirmed', {
        url: '',
        params: { returnState: "advert" },
        templateUrl: 'advertiseradvert/uncompleted',
        controller: 'UnConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'Advert'
        },
        resolve:
        {
            
            UnconfirmedAdverts:["UnconfirmedAdverts", function(UnconfirmedAdverts)
            {
                return UnconfirmedAdverts;
            }]
        }
    })
       
       

    // Get List of completed adverts
    
     .state('advert.confirmed', {
        url: '',
        templateUrl: 'advertiseradvert/completed',
        controller: 'ConfirmedAdvertController',
        ncyBreadcrumb: {
            label: 'completed',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            ConfirmedAdverts:["ConfirmedAdverts", function(ConfirmedAdverts)
            {
                return ConfirmedAdverts;
            }]
        }
        
    })


    // Get Advert Links
    .state('advertlinks' , {
        url: '/advertiser/advert/links/:id',
        params:{returnState:'advert.unconfirmed'},
        templateUrl: 'advertiseradvert/details',
        controller: 'AdvertDetailsController',
        ncyBreadcrumb: {
            label: 'Links',
            parent: 'advert.unconfirmed'
        },
        resolve:
        {
            AdvertDetails:["AdvertiserAdvertService", "$stateParams", function(AdvertiserAdvertService, $stateParams)
            {
                return AdvertiserAdvertService.getAdvertlinksbyid($stateParams.id);
            }]

        }
    })

     /* Add New Advert States and Controllers*/

    .state('addadvert', {
        url: '/advertiser/advert/create',
        templateUrl: 'advertiseradvert/create/home',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label: 'Create',
            parent: 'advert.unconfirmed'
        }
        
    })

     .state('addadvert.uploadsong', {
        url: '/upload',
        templateUrl: 'advertiseradvert/create/uploadsong',
        controller: 'SongUploadController',
        ncyBreadcrumb: {
            label:'upload song',
            parent: 'addadvert'
        }
    })

    .state('addadvert.others', {
        url: '/others',
        params:{type:'others'},
        templateUrl: 'advertiseradvert/create/others',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        }
    })

      .state('addadvert.music', {
        url: '/details',
        params:{songurl:null,type:'music'},
        templateUrl: 'advertiseradvert/create/basicinfo',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        }
    })

        .state('addadvert.banner', {
        url: '/banner',
        params:{type:'banner'},
        templateUrl: 'advertiseradvert/create/banneradvert',
        controller: 'CreateAdvertController',
        ncyBreadcrumb: {
            label:'banner advert',
            parent: 'addadvert'
        }
    })


    .state('addadvert.editothers', {
        url: '/basic/:id',
        params:{id:null},
        templateUrl: 'advertiseradvert/create/editothers',
        controller: 'EditAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        },
         resolve:
        {
            Advert:["AdvertiserAdvertService", "$stateParams", function(AdvertiserAdvertService, $stateParams)
            {
    
                return AdvertiserAdvertService.getAdvertbyid($stateParams.id);
            }]

        }
    })

    .state('addadvert.editbanner' , {
        url: '/banner/:id',
        params:{id:null},
        templateUrl: 'advertiseradvert/create/editbanner',
        controller: 'EditAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        },
         resolve:
        {
            Advert:["AdvertiserAdvertService", "$stateParams", function(AdvertiserAdvertService, $stateParams)
            {
    
                return AdvertiserAdvertService.getAdvertbyid($stateParams.id);
            }]

        }
    })

    .state('addadvert.editbasicinfo' , {
        url: '/music/:id',
        params:{id:null},
        templateUrl: 'advertiseradvert/create/editbasicinfo',
        controller: 'EditAdvertController',
        ncyBreadcrumb: {
            label:'basic info',
            parent: 'addadvert'
        },
         resolve:
        {
            Advert:["AdvertiserAdvertService", "$stateParams", function(AdvertiserAdvertService, $stateParams)
            {
               
                return AdvertiserAdvertService.getAdvertbyid($stateParams.id);
            }]

        }
        
    })

    .state('addadvert.metadata' , {
                url: '/metadata',
                templateUrl: 'advertiseradvert/create/songmetadata',
                controller: 'SongUploadController',
                ncyBreadcrumb: {
                    label:'Metadata',
                    parent: 'addadvert'
                }   
        })

    .state('addadvert.selectpublishers' , {
        url: '/:type/publishers/:id',
        params:{returnState:'basicinfo',status:'edit'},
        templateUrl: 'advertiseradvert/create/selectpublishers',
        controller: 'SelectAdvertPublishersController',
        ncyBreadcrumb: {
            label:'Websites',
            parent: 'addadvert'
        }
    })

   

     // New Advert Payment 
    .state('advertpayment' , {
        url: '/advertiser/advert/payment/:id/:no/:rate',
        params:{id:null,no:null,rate:null},
        templateUrl: 'advertiseradvert/create/advertpayment',
        controller: 'AdvertPaymentController',
        resolve:{
            Advert:["AdvertiserAdvertService", "$stateParams", function(AdvertiserAdvertService, $stateParams)
            {
                return AdvertiserAdvertService. getAdvertbyid($stateParams.id);
            }]
        },
        ncyBreadcrumb: {
            label: 'Payment',
            parent: 'addadvert'
        }
    })


     // New Advert Payment 
    .state('printreceipt' , {
        url: '/receipt',
        params:{id:null},
        templateUrl: 'advertiseradvert/receipt/index',
        controller: 'PagePrintController',
        ncyBreadcrumb: {
            label: 'Receipt',
            parent: 'advert'
        },
        resolve:
        {
            Receipt:["$stateParams", "AdvertiserAdvertService", function($stateParams, AdvertiserAdvertService)
            {
               return AdvertiserAdvertService.getReceipt($stateParams.id);
            }]
        }
        
    })



 
   
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.advertPage = true;
    
}]);

/*-------========== ADVERTISER SHARED SERVICE =======-------*/

/*--==== Get  Logged Inuser Details ====--*/

advertiserShareApp.service('ShareService', ["$http", function ($http) {
    //Get current advertiser info
    this.getLoggedInAdvertiser = function () {

         var request=$http({
            url:'api/advertiserprofileAPI/shared',
            method:'get',
        });

        return request;


    };

    /*--===== Mark notification as read ====--*/

    this.markRead = function () {
        var request=$http({
            url:'api/markread',
            method:'get',
        });

        return request;
       
    };

    
}])
	/*---====== Advertiser Home Service===-----*/

advertiserHomeApp.service('HomeService',["$http", function($http)
{
	 var advertBase = "api/advertiseradvertAPI";

	/*--===== Get unpaid Advert  ====--*/

	this.getUnpaidadvert=function()
	{
		return $http.get(advertBase +'/unpaidadverts').then(function(response)
		{
			return response.data
		},
		function(error)
		{
			var advert={};
			return advert;
		})
	}

	this.deleteAdvert=function(id)
	{
		var request=$http({
			method:'delete',
			url:advertBase +'/removedvertbyid/'+id
		});
		return request;
		
	}
}])

 /*----====== advertiser ADVERTS SERVICE ======----*/

advertiserAdvertApp.service("AdvertiserAdvertService", ["$http", "$rootScope", "SharedMethods", function ($http,$rootScope,SharedMethods) {

    var advertBase = "api/advertiseradvertAPI";
    var profileBase = "api/advertiserprofileAPI";
    
  
    /*--===== Get completed Adverts ====--*/
    
    this.getCompletedAdverts = function () {
        return $http.get(advertBase+'/completedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get Uncompleted Adverts ====--*/
    
    this.getUncompletedAdverts = function () {
        return $http.get(advertBase+'/uncompletedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };


    /*--===== Get  Advertlinks by ID ====--*/

    this.getAdvertlinksbyid = function (id) {
        return $http.get(advertBase +'/advertlinksbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Advert details");
            var advert = {};
            return advert;
        });
    };


      /*--===== Get  Advert plus unique reference no ====--*/

    this. getAdvertplusrefence = function (id) {
        return $http.get(advertBase +'/advertplusreference/'+ id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var advert = {};
            return advert;
        });
    };



   

    /*----Get currency exchange rate--*/

    this.getExchangerate=function()
    {
        return $http.get('https://openexchangerates.org/api/latest.json?app_id=18a6af291e844b3882059aab994567f7').then(function(response)
        {
            return response.data;
        });
    };


      /*--===== Get  Advertiser details ====--*/

    this.getAdvertiserdetails = function () {
        return $http.get(profileBase);
    };

     /*--===== Get  Advertbyid ====--*/

    this.getAdvertbyid = function (id) {
        return $http.get(advertBase+"/advertbyid/"+id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var advert = {};
            return advert;
        });
    };



    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherbyid = function (id) {
        return $http.get(advertBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var pub = {};
            return pub;
        });
    };
    
   
   /*--===== Confirm Advert by ID ====--*/

    this.confirmadvert = function (id) {
        var request = $http({
            method: "get",
            url: advertBase + "/confirmadvert/" + id
        });
        return request;
    };


     /*--===== Confirm Advert Link ====--*/

    this.confirmadvertlink = function (confirmdetails) {
        var request = $http({
            method: "put",
            url: advertBase + "/confirmadvertlink",
            data:confirmdetails
        });
        return request;
    };


       /*--===== Get All Publishers ====--*/

    this.getallPublishers = function (offset) {
       return $http.get(advertBase+'/allpublishers/'+offset).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
   };


     /*--===== Get Banner Publishers ====--*/

    this.getBannerPublishers = function (type,offset) {
       return $http.get(advertBase+'/bannerpublishers/'+type+'/'+offset).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
   };


     /*--===== Verify Payment from Gateway ====--*/

   this.verifypayment = function (verificationdetails) {
      
        var request = $http({
            method: "post",
            /*headers: {'Authorization': 'Basic ' + credential},
            url: "https://api.simplepay.ng/v1/payments/verify",*/
            url:advertBase+'/verifypayment',
            data:verificationdetails,
        });

        return request;
    };



        /*--===== Verify Coupon ====--*/

   this.verifycoupon = function (verificationdetails) {
      
        var request = $http({
            method: "post",
            url: advertBase+'/verifycoupon',
            data:verificationdetails,
        });
        return request;
    };



     /*--===== Confirm Payment====--*/

    this.confirmpayment = function (confirmationdetails) {
        var request = $http({
            method: "put",
            url: advertBase + "/confirmpayment",
            data:confirmationdetails,
        });
        return request;
    };



    /*--===== Verify song url====--*/

    this.verifySongUrl = function (url) {
        var request = $http({
            method: "put",
            url: advertBase + "/verifysongurl",
            data:url,
        });
        return request;
    };
   
   /*--===== Post Song ====--*/

    this.postSong = function (song) {
        var request = $http({
            method: "post",
            url: advertBase + "/newsong",
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            formData.append("song" ,song);             
            return formData;            
            },
            eventHandlers:{ 
                progress:function(pl)
                { 
                    /* Upload Progress Bar Determinant*/
                    $rootScope.progress=pl;
                    $rootScope.percent=100*parseInt($rootScope.progress.loaded)/parseInt($rootScope.progress.total);
                    if($rootScope.total=0)
                    {
                        $rootScope.percent=0;
                    }

                    if ($rootScope.percent<25)
                    {
                        $rootScope.type="warning";
                    }

                    else if ($rootScope.percent >25 && $rootScope.percent <50)
                    {
                        $rootScope.type="info";
                    }
                    else if ($rootScope.percent>75 && $rootScope.percent <=100)
                    {
                        $rootScope.type="success";
                    }
                    else
                    {
                        $rootScope.type="danger";
                    }
                }
            },
           
        });
        return request;
    };






    /*--===== Put new Advert ====--*/

    this.postAdvert = function (advertdetails) {
        
        
       var request=$http({
            url:advertBase+'/newadvert',
            method:'post',
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            if(angular.isObject(advertdetails))
            {
                
                for(var key in advertdetails)
                {
                    var value=advertdetails[key];
                    formData.append(key,value);
                }
            }
          
                                                                    
            return formData;            
            },
        });
       return request;
   };


      /*--===== Edit  Advert ====--*/

   this.putAdvert = function (advertdetails) {
    
       var request=$http({
            url:advertBase+'/editadvert',
            method:'post',
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            if(angular.isObject(advertdetails))
            {
                
                for(var key in advertdetails)
                {
                    var value=advertdetails[key];
                    formData.append(key,value);
                }
            }
                                                        
                                     
            return formData;            
            },
        });
       return request;
   };

    /*--===== put Publisher And Totalamount====--*/

    this.putPublisherAndTotalamount = function (details) {
       
        var request = $http({
            method: "put",
            url: advertBase + "/totalamount",
            data:details,
        });
        return request;
    };

        /*--===== Download pdf receipt ====--*/

    this.downloadPDF = function (id) {
       
       return $http.get(advertBase +'/downloadpdf/' + id)
    };


      /*--===== Delete Advert ====--*/

    this.deleteAdvert = function (id) {
        var request=$http(
        {
            url:advertBase+'/removeadvertbyid/'+id,
            method:'delete'
        });
        return request;
     
   };


     /*--===== Get  Receipts ====--*/

    this.getReceipt = function (id) {
        return $http.get(advertBase+'receipt/'+id).then(function(pl)
            {
                return pl.data;
            },
            function(error)
            {
                var Receipt={}
                return Receipt;
            });
    };


     /*--get all  banner typess---*/
    this.getBannerTypes=function()
    {
        return $http.get(advertBase+'/getbannertypes');
    };


}]);

/*----====== ADMIN PROFILE SERVICE ======----*/

 
advertiserProfileApp.service('AdvertiserProfileService', ["$http", "SharedMethods", function ($http, SharedMethods) {

    var profileBase = "api/advertiserprofileAPI";
    

    /*--===== Get  Basic details ====--*/
   
    this.getAdvertiserBasicDetails = function () {
        return $http.get(profileBase).then(function (response) {
            return response.data;
            
        },
       function (error) {
           SharedMethods.createErrorToast("Problem retreiving your details, kindly refresh");
           var advertiser = {};
           return advertiser;
       });
    };
    

     /*--===== Change Avatar ====--*/
  
  this.putUploadavatar=function(avatar)
  {

        var request = $http({
            method: "post",
            url:profileBase +"/uploadavatar",
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            formData.append("avatar" ,avatar);              
            return formData;            
            },
        });
        return request;
    };
    

    /*--===== Edit Basic details ====--*/
  
  this.putEditadvertiserBasicDetails=function(AdvertiserDetails)
  {
   
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editadvertiserbasicdetails",
        data:AdvertiserDetails
    });
    return request;
  };


    /*--===== Change Password ====--*/
    
    this.changepassword = function (Passworddetails) {
      
        var request = $http({
            method: 'put',
            url: profileBase + "/changepassword",
            data: Passworddetails
        });
        return request;
    };

}])
/*-------========== ADVERTISER SHARED CONTROLLER =======-------*/

/*--==== Get  Logged In-User Details ====--*/
advertiserShareApp.controller('AdvertiserShareController', ["$scope", "$rootScope", "ShareService", "ShareData", function ($scope,$rootScope, ShareService, ShareData) {

  loadAdvertiserInfo();
    

    function loadAdvertiserInfo() {
        
        var promiseGetAdvertiserInfo = ShareService.getLoggedInAdvertiser();
        promiseGetAdvertiserInfo.then(function (pl)
        {
         

          var Advertiserdetails = pl.data;
          $scope.Advertiser = pl.data.Advertiser;
          $scope.Notifications=pl.data.Notifications;
          $scope.Avatar = $scope.Advertiser.user.picture;
          
        },
        function (errorPl) {
            $scope.error = errorPl;
           
            
        });
    };

     $scope.markRead=function()
    {
        if($scope.Notifications.length>0)
        {
           var markRead = ShareService.markRead();

          markRead.then(function (pl)
          {

          });
        }
       
    }

          
}]);


// HomeController
advertiserHomeApp.controller('HomeController', ["$scope", "$uibModal", "HomeService", "$state", "$rootScope", function ($scope, $uibModal, HomeService, $state, $rootScope) {

    $rootScope.title = "Home | Brimav";
    $scope.home=true;

    getAdvert();


     function getAdvert()
    {

    	var fetchAdvert=HomeService.getUnpaidadvert();
    	fetchAdvert.then(function(response)
    	{
    		$scope.Adverts=response;
           
    	},
    	function(error)
    	{

    	})
    };

    $scope.go=function(id,type)
    {
        
        if(type=="music")
        {
             
          $state.go('addadvert.editbasicinfo',{id:id},{reload:false});  
        }
        else if(type=="banner")
        {
            $state.go('addadvert.editbanner',{id:id},{reload:false});
        }
        else
        { 
            $state.go('addadvert.editothers',{id:id},{reload:false}); 
        }
    	
    };



    $scope.delete=function(id,type)
    {
        
    	var deleteAdvert=HomeService.deleteAdvert(id);
    	deleteAdvert.then(function(response)
    	{
    		$state.go('home',{},{reload:true});
    	},
    	function(error)
    	{
            alert("error");
    	})
    };


    $scope.selectType=function()
    {
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        var modalInstance=$uibModal.open({

            templateUrl: 'advertiseradvert/create/adverttype',
            controller: 'ModalTerminator',

        });
        modalInstance.opened.then(function()
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        },function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        })
    };



}]);


/*--==== Modal Terminator Controller ====--*/

advertiserHomeApp.controller('ModalTerminator', ["$state", "$window", "$uibModalInstance", "$scope", function ($state,$window,$uibModalInstance,$scope) {

    $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    };

     /*---Advert Type Dictator---*/

    $scope.typeSelection=function()
    {
        try
        {

              if($scope.Type=="music")
            {
                $scope.close();
                $state.go('addadvert.uploadsong',{},{});
            }
            else if($scope.Type=="banner")
            {

                $scope.close();
                $state.go('addadvert.banner',{type:'banner'},{reload:false});
            }
            else
            {
                $scope.close();
                $state.go('addadvert.others',{type:'others'},{reload:false});
            }

        }catch(Exception)
        {

        }

       
    };      

}]);
/*-------========== advertiser ADVERTS CONTROLLER =======-------*/

/*--==== Adert Template Controller ====--*/
;
advertiserAdvertApp.controller('AdvertTemplateController', ["$rootScope", "$uibModal", "$state", "$scope", function ($rootScope,$uibModal, $state, $scope) {
    $rootScope.headerClass = "normal-page-title";
     
    

    //Set default filter, sort parameters
    
    $scope.Filterview = "unconfirmed";
    $scope.sortKey = "date";
    $scope.reverse=false;
    $rootScope.advertview = true;


    $scope.$watch('Filterview', function (filterkey) {
    $state.go('advert.'+filterkey); // Go to the state selected
    $scope.Filterview=filterkey; //set the Filter to the param passed
    });

     /*--- Advert type selection---*/

    $scope.selectType=function()
    {
        
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        var modalInstance=$uibModal.open({

            templateUrl: 'advertiseradvert/create/adverttype',
            controller: 'ModalTerminator',

        });
        modalInstance.opened.then(function()
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        },function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        })
    }


    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    
    // confirm advert as complete 

    $scope.confirmadvert = function (id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var confirmationDetails = {
            id : id,
            headerContent: "Confirm Advert " ,
            bodyMessage: " Are you sure you want to confirm this Advert?",
            buttonText: "",
            buttonClass: "btn-adgold",
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'advertiseradvert/shared/multimodal',
            controller: 'ConfirmAdvertController',
            resolve: {
                ConfirmationDetails: function () {
                    return confirmationDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            
        });
    };
}]);
   



/*--==== Confirmed Advert Controller ====--*/

advertiserAdvertApp.controller('ConfirmedAdvertController', ["$scope", "ConfirmedAdverts", "$rootScope", "$uibModal", function ($scope, ConfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Confirmed Adverts";
    $rootScope.SubPageHeading = null;


    $scope.contentLoading = true; // show loading icon
    $scope.ConfirmedAdverts=ConfirmedAdverts;
    $scope.Type="music";


    // Delete advert

    $scope.deleteadvert = function (id,returnState) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            headerContent: "Add to Archive " ,
            bodyMessage: " Are you sure you want to add this Advert to archive ?",
            buttonText: "",
            buttonClass: "btn-danger",
            id:id,
            returnState:returnState
        };


        var modalInstance = $uibModal.open({
            templateUrl: 'advertiseradvert/shared/multimodal',
            controller: 'DeleteAdvertController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;}
                }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {

        }, function () {
            
        });


    };
    
    
}]);


/*--==== UnConfirmed Advert Controller ====--*/

advertiserAdvertApp.controller('UnConfirmedAdvertController', ["$scope", "UnconfirmedAdverts", "$rootScope", "$uibModal", function ($scope, UnconfirmedAdverts, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Unconfirmed Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon
    $scope.UnconfirmedAdvert=UnconfirmedAdverts;
   


   
 
    }]);

    


/*--==== Advert Details Controller ====--*/

advertiserAdvertApp.controller('AdvertDetailsController', ["$scope", "$stateParams", "$rootScope", "$uibModal", "$stateParams", "AdvertDetails", function ($scope, $stateParams, $rootScope,$uibModal, $stateParams, AdvertDetails) {

    //$scope.Advert = AdvertDetails.Advert;
    $scope.Advertlink=AdvertDetails;
    $scope.returnState=$stateParams.returnState;
  
    $rootScope.title =  "Advert | Brimav";
    $rootScope.PageHeading = " Advert ";
    $rootScope.SubPageHeading = "Links" ;

     // confirm advert as complete 

    $scope.confirmlink = function (link_id,publisher_id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var confirmationDetails = {
            headerContent: "Confirm Link " ,
            bodyMessage: " Are you sure you want to confirm this link ?",
            buttonText: "",
            buttonClass: "btn-adgold",
            returnView:'advertlinks({id:' +$stateParams.id +'})'
        };

        var confirmrequirements=
        {
            link_id:linkid,
            publisher_id:publisher_id
        }

        var modalInstance = $uibModal.open({
            templateUrl: 'advertiseradvert/shared/multimodal',
            controller: 'ConfirmAdvertLinkController',
            resolve: {
                ConfirmationDetails: function () {
                    return confirmationDetails;
                },
                 ConfirmRequirements: function () {
                    return confirmrequirements;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            
        });
    };


    

}]);
 

/*--==== Confirm Advert  Controller ====--*/

advertiserAdvertApp.controller('ConfirmAdvertController', ["$scope", "$cookies", "$uibModalInstance", "AdvertiserAdvertService", "$state", "ConfirmationDetails", "SharedMethods", function ($scope,$cookies, $uibModalInstance, AdvertiserAdvertService, $state, ConfirmationDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ConfirmationDetails.id;
    $scope.title = ConfirmationDetails.headerContent;
    $scope.body = ConfirmationDetails.bodyMessage;
    $scope.action = ConfirmationDetails.buttonText;
    $scope.class = ConfirmationDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdvertiserAdvertService.confirmadvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advert.unconfirmed', {}, { reload: true });
            $cookies.remove('Publisher_profile');
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was confirmed successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Advert links yet to complete. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);





/*--==== Confirm Advert Link  Controller ====--*/

advertiserAdvertApp.controller('ConfirmAdvertLinkController', ["$scope", "ConfirmRequirements", "$uibModalInstance", "AdvertiserAdvertService", "$state", "ConfirmationDetails", "SharedMethods", function ($scope, ConfirmRequirements, $uibModalInstance, AdvertiserAdvertService, $state, ConfirmationDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.title = ConfirmationDetails.headerContent;
    $scope.body = ConfirmationDetails.bodyMessage;
    $scope.action = ConfirmationDetails.buttonText;
    $scope.class = ConfirmationDetails.buttonClass;
    $scope.ConfirmRequirements=ConfirmRequirements;
    $scope.returnView=ConfirmationDetails.returnView;
    

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdvertiserAdvertService.confirmadvertlink($scope.ConfirmRequirements);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
        $state.go('advertlinks', {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Link </strong>  was confirmed successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast(' Problem confirming <strong> Advert</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);




/*--==== NEW ADVERT CONTROLLERS AND FUNCTIONS ====--*/


/*--==== Song Upload Controller ====--*/

advertiserAdvertApp.controller('SongUploadController', ["$scope", "$window", "$state", "$stateParams", "SharedMethods", "AdvertiserAdvertService", "$rootScope", "$stateParams", function ($scope, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {

     $rootScope.activeView = "upload";
     $rootScope.advertview = true;
    /*Song Metadata View/Edit*/

    $scope.songPreview=function (event)
    {
        $rootScope.Albumart  = "";
        var song=event.target.files[0]
        $rootScope.song=song;

        var success_callback = function (metadata) 
        {
        
            $state.go('addadvert.metadata');
            $rootScope.Metadata=metadata;
            
            var reader = new FileReader();

            reader.onload = function(e) 
            {
                $rootScope.Albumart  = reader.result; 
                 
            }

            if($rootScope.Metadata.picture!="" && $rootScope.Metadata.picture!=undefined)
            {
               
                reader.readAsDataURL($rootScope.Metadata.picture);
            }
            else
            {
                $rootScope.Albumart="/uploads/images/theme/revolution1-slider-img-bg.jpg";
            }

        };
        var error_callback = function (e) {

        };
        parse_audio_metadata(song, success_callback, error_callback);

    }

    

    /*---- Song Upload Function ----*/
    $scope.songUpload = function()
    {
        $scope.uploading=true;
        var song= $rootScope.song;
        if( song!=undefined && song.type.match ("audio/mp3") || song.type.match ("audio/mpeg") && song.size <= 20000000 )
        {

           $scope.song=song;
           var postSong=  AdvertiserAdvertService.postSong($scope.song);
           postSong.then(function(response)
           {
             $scope.uploadedsonglink=response.data;
             $state.go('addadvert.music',{songurl:$scope.uploadedsonglink,type:'music'},{reload:false});
             $rootScope.progress="";
           },
           function(error)
           {
             $scope.error=true;
             SharedMethods.showValidationErrors($scope,error);
           })
           .finally(function()
           {
            $scope.uploadsong=false;
           });
        } 
         else
        {
            $scope.uploadsong=false;
            $state.go('addadvert.uploadsong');
            SharedMethods.createErrorToast('Select a valid mp3 file less than 15mb');
            $scope.label="Upload";
            
        } 
        
    }


    /*Verify Song Url*/
    $scope.verifySongrl=function()
    {
        $scope.editing=true;
        $scope.url=
        {
            url:$scope.Songurl
        };

        var verifyUrl=AdvertiserAdvertService.verifySongUrl($scope.url);
        verifyUrl.then(function(pl)
        {
            $state.go('addadvert.music',{songurl:pl.data,type:'music'});
        },
        function(error)
        {
            SharedMethods.showValidationErrors($scope, error)
        })
        .finally(function()
        {
            $scope.editing=false;
        })
    };

}]);

/*--==== Modal Terminator Controller ====--*/

advertiserAdvertApp.controller('ModalTerminator', ["$state", "$window", "$uibModalInstance", "$scope", function ($state, $window,$uibModalInstance,$scope) {

    $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    };

     /*---Advert Type Dictator---*/

    $scope.typeSelection=function()
    {
      if($scope.Type=="music")
        {
            $scope.close();
            $state.go('addadvert.uploadsong',{},{});
        }
        else if($scope.Type=="banner")
        {

            $scope.close();
            $state.go('addadvert.banner',{type:'banner'},{reload:false});
        }
        else
        {
            $scope.close();
            $state.go('addadvert.others',{type:'others'},{reload:false});
        }
        
   };

    $scope.print=function()
    {
        
        var url=$state.href('printreceipt',{printdetails:$scope.printdetails});
        $window.open(url,'blank');
      
    }
      

}]);




/*--==== Create Add Controller ====--*/

advertiserAdvertApp.controller('CreateAdvertController', ["$scope", "$cookies", "$state", "$window", "$state", "$stateParams", "SharedMethods", "AdvertiserAdvertService", "$rootScope", "$stateParams", function ($scope,$cookies, $state, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {


    $rootScope.title='Create Advert | Brimav';
    $rootScope.PageHeading="Create"
    $rootScope.SubPageHeading=null
    $scope.Defaultpics = "/uploads/images/theme/uploadicon.jpg";
    $scope.songurl=$stateParams.songurl;
    $rootScope.activeView = "basic";
    $rootScope.advertview = true;
    $scope.type=$stateParams.type;
    $scope.Genres=Genres;
    $scope.SelectedBannerType=[]

    if($scope.type=='banner')
    {
        loadBannerTypes();
   
    }

     function loadBannerTypes()
    {
        var getBannerTypes=AdvertiserAdvertService.getBannerTypes();
        getBannerTypes.then(function(pl)
        {
            $scope.BannerTypes=pl.data;

        },
        function(error)
        {
            //SharedMethods.createErrorToast('Problem getting Banner Types');
        })
    }
   

/*---- Image  preview & upload ----*/

    $scope.stepsModel = [];

    $scope.imageUpload = function(event)
    {
        $scope.stepsModel=[];
          var file = event.target.files[0]; //FileList object

        if(file.type.match ("image/jpeg") || file.type.match ("image/png") || file.type.match ("image/gif") )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
             $scope.featuredImage=file;
        } 
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
        });
    };




    /*---- Attachment  upload(pdf & doc x) ----*/


    $scope.fileUpload = function(event)
    {
      var file = event.target.files[0]; //FileList object
      
        if(file.type.match ("application/pdf") || file.type.match ("application/vnd.openxmlformats-officedocument.wordprocessingml.document") || file.type.match ("application/msword") && file.size <=5000000  )
        {
            $scope.Attachment=file;
        }
        
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

      
    $scope.create=function()
    {
        $scope.creating=true;
        
        $scope.newAdvert=
        {
            title:$scope.Title,
            description:$scope.Description,
            genre:$scope.Genre,
            song:$scope.songurl,
            type:$scope.type,
            attachment:$scope.Attachment,
            image:$scope.featuredImage,
            youtube:$scope.Youtube,
            destination:$scope.Destination,
            itunes:$scope.Itunes 
        };

        if($scope.SelectedBannerType=="" && $scope.type=='banner')
        {
             SharedMethods.createErrorToast('Select atleast one banner type ');
              $scope.continue=false;
              return false;
        }
         
        $cookies.putObject('selected_banner_type', $scope.SelectedBannerType);
         var postAdvert = AdvertiserAdvertService.postAdvert($scope.newAdvert);

        postAdvert.then(function (response) { 

        $state.go('addadvert.selectpublishers', {id:response.data,returnState:$scope.type,type:$scope.type,status:'new'});
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function()
        {
            $scope.creating = false;
        });
    }; 

     $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    }     

}]);


/*--==== Edit Advert Controller ====--*/

advertiserAdvertApp.controller('EditAdvertController', ["Advert", "$scope", "$cookies", "$state", "$window", "$state", "$stateParams", "SharedMethods", "AdvertiserAdvertService", "$rootScope", "$stateParams", function (Advert,$scope,$cookies, $state, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {


    $rootScope.title='Create Advert | Brimav';
    $rootScope.PageHeading="Create"
    $rootScope.SubPageHeading=null
    $rootScope.activeView = "basic";
    $rootScope.advertview = true;

    $scope.Genres=Genres;
    $scope.Advert=Advert;
    $scope.SelectedBannerType=[]; //availlable only when type=banner;


    if($scope.Advert.type=="music")
    {
        $scope.returnView="editbasicinfo";
    }

    else if($scope.Advert.type=="others")
    {
        $scope.returnView="editothers";
    }
    else if($scope.Advert.type=="banner")
    {
        $scope.returnView="editbanner";
        loadBannerTypes();

    }

   

    function loadBannerTypes()
    {
        var getBannerTypes=AdvertiserAdvertService.getBannerTypes();
        getBannerTypes.then(function(pl)
        {
            $scope.BannerTypes=pl.data;

        },
        function(error)
        {
            //SharedMethods.createErrorToast('Problem getting Banner Types');
        })
    }


    

/*---- Image  preview & upload ----*/
  $scope.featuredImage=Advert.imageurl;
    $scope.stepsModel = [];

    $scope.imageUpload = function(event)
    {

        $scope.stepsModel=[];
          var file = event.target.files[0]; //FileList object

        if(file.type.match ("image/jpeg") || file.type.match ("image/png") || file.type.match ("image/gif") && file.size>0 )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
             $scope.featuredImage=file;
        } 
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
        });
    };




    /*---- Attachment  upload(pdf & doc x) ----*/

    $scope.Attachment=$scope.Advert.attachment;
    $scope.fileUpload = function(event)
    {
      var file = event.target.files[0]; //FileList object
      
        if(file.type.match ("application/pdf") || file.type.match ("application/vnd.openxmlformats-officedocument.wordprocessingml.document") || file.type.match ("application/msword") && file.size <=5000000 && file.size>0  )
        {
            $scope.Attachment=file;
        }
        
        else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

      
    $scope.create=function()
    {
        $scope.creating=true;
        $scope.newAdvert=
        {
            id:$stateParams.id,
            title:$scope.Advert.title,
            description:$scope.Advert.description,
            genre:$scope.Advert.genre,
            song:$scope.Advert.songurl,
            type:$scope.Advert.type,
            attachment:$scope.Attachment,
            image:$scope.featuredImage,
            youtube:$scope.Advert.youtube,
            destination:$scope.Advert.destination,
            itunes:$scope.Advert.itunes  
        };

          
        $cookies.putObject('selected_banner_type', $scope.SelectedBannerType);
         if($scope.SelectedBannerType=="" && $scope.Advert.type=='banner')
        {
             SharedMethods.createErrorToast('Select atleast one banner type ');
              $scope.continue=false;
              return false;
        }

         var putAdvert = AdvertiserAdvertService.putAdvert($scope.newAdvert);


        putAdvert.then(function (response) {  

        $state.go('addadvert.selectpublishers', {id:response.data,returnState:$scope.returnView,type:$scope.Advert.type,status:'edit'});
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function()
        {
            $scope.creating = false;
        });
    }; 

     $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    }     

   
}]);




/*--==== Select Advert Publishers Controller ====--*/

advertiserAdvertApp.controller('SelectAdvertPublishersController', ["$scope", "$cookies", "$window", "$state", "$stateParams", "SharedMethods", "AdvertiserAdvertService", "$rootScope", "$stateParams", function ($scope,$cookies, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {
 $rootScope.activeView = "selectpublisher";
 $scope.returnState=$stateParams.returnState;
 $scope.id=$stateParams.id;
 $scope.Type=$stateParams.type;
 var usd_naira="";
 var usd_ghs="";
 var bannertype=$cookies.getObject('selected_banner_type');
 $scope.Currency='NGN';
var offset=0;
 $scope.rate="1";


    if($scope.Type=='banner')
    {
        loadBannerPublishers(bannertype);
      
    }
    else
    {
         loadallPublishers();
        
    }
   

    function loadallPublishers()
    {
        var getPublishers=AdvertiserAdvertService.getallPublishers(offset);
        getPublishers.then(function(response)
        {
            $scope.Websites=response.Websites;
            $scope.Tvs=response.Tvs;
            $scope.Socials=response.Socials;
            usd_naira=response.Rates.usd_naira;
            usd_ghs=response.Rates.usd_ghs;
            offset+=30;
        })
    };

     function loadBannerPublishers(type)
    {

    
        var getPublishers=AdvertiserAdvertService.getBannerPublishers(type,offset);
        getPublishers.then(function(response)
        {
            $scope.Banners=response.Banners;
            offset+=30;
            usd_naira=response.Rates.usd_naira;
            usd_ghs=response.Rates.usd_ghs;
        })
    };



    $scope.$watch('Currency',function(n_currency)
    {
        $scope.Currency=n_currency;
        if(n_currency=="USD")
        {
             $scope.rate=1/usd_naira;
        }
        else
        {
             $scope.rate=1
        }

    });

        /*---get exchange rate in background--*/
         /*var promise=AdvertiserAdvertService.getExchangerate();
       promise.then(function(pl)
        {
            $scope.Rates=pl.rates;
            rate=1/parseInt($scope.Rates.NGN);
        })*/


    $scope.loadMorePublisher=function()
    {
        var loadmore=AdvertiserAdvertService.getallPublishers(offset);
        getPublishers.then(function(response)
        {
            if($scope.Type=='banner')
            {
                $scope.Banners=response;
            }
            else
            {
                $scope.Websites=response.Websites;
                $scope.Tvs=response.Tvs;
                $scope.Socials=response.Socials;
            }
            
            offset+=30;
        })
    }


    $scope.SelectedPublishers = {};
     $scope.noofwebsite=0;
    $scope.Total=0;

/*---- Total price calculation ----*/
    $rootScope.addTotalprize=function(publisherprize)
    {
        $scope.noofwebsite= $scope.noofwebsite +1;
        $scope.Total=$scope.Total + publisherprize;
    };
    
    $rootScope.substractTotalprize=function(publisherprize)
    {
        $scope.noofwebsite= $scope.noofwebsite - 1;
        $scope.Total=$scope.Total - publisherprize;     
    };

    $scope.save=function(){
        
        $scope.continue=true;
        if($scope.SelectedPublishers=="")
        {
             SharedMethods.createErrorToast('Select websites');
             $scope.continue=false;
        }
        else
        {
           
            var publishersplusamount=
            {
                id:$stateParams.id,
                publishers:$scope.SelectedPublishers,
                totalamount:$scope.Total,
                status:$stateParams.status
            };

            
            var putPublisherAndTotalamount=AdvertiserAdvertService.putPublisherAndTotalamount(publishersplusamount);
            putPublisherAndTotalamount.then(function(response)
            {
                
                $state.go('advertpayment', {id:response.data,no: $scope.noofwebsite,rate:1/usd_naira});
            },

            function(error)
            {
                SharedMethods.showValidationErrors($scope,error);
            })
            .finally(function()
            {
                $scope.continue=false;
            });
        }

    };

}]);


/*--==== Advert Payment Controller ====--*/

advertiserAdvertApp.controller('AdvertPaymentController', ["$scope", "$cookies", "$uibModal", "$base64", "Advert", "$stateParams", "$window", "$state", "$stateParams", "SharedMethods", "AdvertiserAdvertService", "$rootScope", "$stateParams", function ($scope, $cookies,$uibModal,$base64,Advert,$stateParams, $window, $state, $stateParams, SharedMethods,AdvertiserAdvertService, $rootScope, $stateParams) {

$rootScope.title='Payment | Brimav';
$rootScope.PageHeading="Create";
$rootScope.SubPageHeading="Payment";
$scope.Advert=Advert
$scope.WebsitesNo=$stateParams.no;
$scope.newprize=$scope.Advert.totalamount;
$scope.discount=0;
$scope.rate=$stateParams.rate;
getAdvertiser();

function getAdvertiser()
{
    var getAdvertiserdetails=AdvertiserAdvertService.getAdvertiserdetails();
    getAdvertiserdetails.then(function(response)
    {
        $scope.Advertiser=response.data;
        
    })
    .finally(function()
    {
        $scope.loading=false; 
    })
};


 /* Verify Coupon Code*/

    $scope.verifyCoupon=function()
    {
        $scope.creating=true;
        $scope.Details=
        {
            couponcode:$scope.Coupon,
            totalamount:$scope.Advert.totalamount,
            advertid:$scope.Advert.id
        };
     
        var verifycoupon=AdvertiserAdvertService.verifycoupon($scope.Details);
        verifycoupon.then(function(pl)
        {
            $scope.newDetails=pl.data;
            $scope.Advert.totalamount=$scope.newDetails.newprize;
            $scope.newprize=$scope.newDetails.newprize;
            $scope.discount=$scope.newDetails.newprize
        },
        function(error)
        {
           
            SharedMethods.createErrorToast('Invalid/Expired Coupon');
        })
        .finally(function()
        {
            $scope.creating=false;
        })

    }


 /* Payment Functions and Configuration*/
    try{
              var handler = SimplePay.configure({
        token: paymentConfirmation, 
        key: 'test_pu_805b105d76274ebda0fdb78b1e4d83c6',  
        image: 'https://brimav.com/uploads/images/logo/brimavcolor.png' 
    });
    }catch(exception)
    {
        
    }
 


    $scope.lunchpayment =function()
        {
          
           
           try{
                 handler.open(SimplePay.CHECKOUT, 
            {
                email: $scope.Advertiser.user.email,
                description: 'Payment for Digital Promotion',
                country: $scope.Advertiser.user.country,
                amount: $scope.newprize+'00',
                currency: $scope.Advertiser.user.currency,
                phone:$scope.Advertiser.user.phonenumber
            });
          }catch(exception)
           {
                alert("Connection error Reload");
           }
            
        };



    function paymentConfirmation(token)
    {
        $scope.loadingView=true;
        //$scope.username=$base64.encode('test_pr_5996c4f02397440fb3821199048827c6'+':');
        $scope.verificationdetails=
        {

            token : token,
            amount: $scope.newprize+'00',
            amount_currency :$scope.Advertiser.user.currency,
            id:$scope.Advert.id,
            websiteno:$scope.WebsitesNo,
            advertiser:$scope.Advertiser.id
            
        };
        
        
       var verifypayment=AdvertiserAdvertService.verifypayment($scope.verificationdetails);

        verifypayment.then(function(response)
        {
            $scope.receipt=response.data;
           
            
            $scope.printdetails=
            {   
                username: $scope.Advertiser.user.username,
                city: $scope.Advertiser.user.city,
                country: $scope.Advertiser.user.country,
                amount: $scope.newprize,
                totalamount: $scope.Advert.totalamount,
                discount:$scope.discount,
                currency:$scope.Advertiser.user.currency,
                cardtype:$scope.receipt.cardtype,
                cardnumber:$scope.receipt.cardnumber,
                phone: $scope.Advertiser.user.phonenumber,
                receiptnumber:'BRM'+$scope.receipt.receipt_number,
                date:$scope.receipt.created_at,
                websiteno:$scope.receipt.websitenumber,
                receiptid:$scope.receipt.id,
            };
            $scope.success($scope.printdetails);
            },
            function(error)
            {
                SharedMethods.createErrorToast('Promotion failed, Try again');
            })
        
        .finally(function()
        {
            $scope.loadingView=false;
        });
    }

    $scope.success=function(PrintDetails)
    {
          $cookies.remove('Publisher_profile');
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        var modalInstance=$uibModal.open({

            templateUrl: 'advertiseradvert/create/advertsuccess',
            controller: 'ReceiptController',
            resolve:
            {
               printdetails:function()
               {
                return PrintDetails;
               }
            }

        });
        modalInstance.opened.then(function()
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        },function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        })
    }


}]);




/*--==== Receipt preview/print Controller ====--*/

advertiserAdvertApp.controller('ReceiptController', ["$state", "printdetails", "AdvertiserAdvertService", "$window", "$uibModalInstance", "$scope", function ($state,printdetails,AdvertiserAdvertService, $window,$uibModalInstance,$scope) {

   
    $scope.close = function () 
    {
        $uibModalInstance.dismiss('cancel');
    };

    

    $scope.PrintDetails=printdetails;
   

    $scope.print=function()
    {
        //$state.go('printreceipt',{id:$scope.PrintDetails.receiptid},{});
       // var url=$state.href('printreceipt',{id:$scope.PrintDetails.receiptid},{});
        $window.open("/api/receipt/"+$scope.PrintDetails.receiptid,'blank');
        //window.location.href="/api/receipt/"+$scope.PrintDetails.receiptid;
      
    }


    $scope.download=function()
    {
        
        var downloadPDF=AdvertiserAdvertService.downloadPDF(
    $scope.PrintDetails.receiptid);       
    }
      

}]);


/*--==== Delete Advert  Controller ====--*/

advertiserAdvertApp.controller('DeleteAdvertController', ["$scope", "$uibModalInstance", "AdvertiserAdvertService", "$state", "DeleteDetails", "SharedMethods", function ($scope, $uibModalInstance, AdvertiserAdvertService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class = DeleteDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdvertiserAdvertService.deleteAdvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go(DeleteDetails.returnState, {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was added to Archive successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem adding <strong> Advert</strong> to Archive. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);


/*--==== Page Print Controller ====--*/

advertiserAdvertApp.controller('PagePrintController', ["$state", "$rootScope", "$stateParams", "Receipt", "$window", "$scope", function ($state,$rootScope,$stateParams, Receipt, $window,$scope) {

    $scope.PrintDetails=Receipt;
    $rootScope.title="Receipt | Brimav";
    
}])




/*GENRES LIST */

var Genres=["Blues", "Hip-Hop","Rap", "Fuji","Jazz","Gospel","Rap/Hip-Hop","R&B","Reggae","Rock","Country","Others"];

/*-----====== ADMIN PROFILE CONTROLLER =====----*/


/*---==== Profile TemplateController =====--*/

advertiserProfileApp.controller('ProfileTemplateController', ["$rootScope", "$cacheFactory", "$scope", "AdvertiserProfileService", "$scope", "ShareData", "Advertiser", "SharedMethods", function ($rootScope,$cacheFactory,$scope,AdvertiserProfileService, $scope, ShareData, Advertiser,SharedMethods) {
    $scope.Advertiser = Advertiser;
    $rootScope.headerClass = "normal-page-title";

    //Get advertiser Picture
    if ($scope.Advertiser.user.picture == null || $scope.Advertiser.user.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";
    else {
        $scope.Picture =  $scope.Advertiser.user.picture;
    }

/*---- Avatar Upload &  preview ----*/

   $scope.stepsModel=[];
    $scope.imageUpload = function(event)
    {
        
          var avatar = event.target.files[0]; //FileList object

        if(avatar.type.match ("image/jpeg") || avatar.type.match ("image/png") || avatar.type.match ("image/gif") )
        {
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(avatar);
             $scope.avatar=avatar;
             var uploadAvatar=AdvertiserProfileService.putUploadavatar($scope.avatar);
             uploadAvatar.then(function(response)
             {
                //$rootScope.Picture=response.data;
                $rootScope.Avatar=response.data;
                var httpCache=$cacheFactory.get('$http');
            httpCache.remove('api/advertiserprofileAPI/shared');
             },
             function(error)
             {
                SharedMethods.createErrorToast('file too large (max:1.4mb)');
             })
        } 
         else
        {
            SharedMethods.createErrorToast('File not supported');
        }
        
    };

    $scope.imageIsLoaded = function(e)
    {
        $scope.$apply(function() 
        {
            $scope.stepsModel=e.target.result;
            $rootScope.Avatar=e.target.result;
        });
    };

}]);


/*---==== Basic Details Controller =====--*/

advertiserProfileApp.controller('BasicDetailsController', ["$scope", "$rootScope", "ShareData", function ($scope, $rootScope, ShareData) {
    
    $rootScope.title = $scope.Advertiser.user.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Basic Details";
    $rootScope.activeView = "basic";
}]);


/*---====Edit Basic Details Controller =====--*/

advertiserProfileApp.controller("EditBasicProfileController", ["$scope", "$cookies", "AdvertiserProfileService", "SharedMethods", "$rootScope", "$state", function ($scope,$cookies, AdvertiserProfileService, SharedMethods, $rootScope, $state) {
    
    $scope.editing = false;

    $rootScope.title = "Edit Profile | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit Basic Details";
    $rootScope.activeView = "basic";
    $scope.Advertiser2={};
    for(var key in $scope.Advertiser)
    {
        $scope.Advertiser2[key]=$scope.Advertiser[key];
    }
   

    $scope.save = function () {
        $scope.editing = true;
        $scope.newAdvertiserdetails = {
            username: $scope.Advertiser2.user.username,
            email: $scope.Advertiser2.user.email,
            facebook:$scope.Advertiser2.facebook,
            twitter:$scope.Advertiser2.twitter,
            phonenumber:$scope.Advertiser2.user.phonenumber,
         
        };

         $scope.toBroadcastdetails = {
            user:
            {
                username: $scope.Advertiser2.user.username,
                email: $scope.Advertiser2.user.email,
                phonenumber:$scope.Advertiser2.user.phonenumber,
            },
            facebook:$scope.Advertiser2.facebook,
            twitter:$scope.Advertiser2.twitter,
            
            country:$scope.Country,
        };

        var PutAdvertiser = AdvertiserProfileService.putEditadvertiserBasicDetails($scope.newAdvertiserdetails);
        PutAdvertiser.then(function (pl) {
             $cookies.remove('Advertiser_profile');
             $scope.$parent.Advertiser = $scope.toBroadcastdetails;
            //$rootScope.$broadcast("updateShareuserAdvertiser", $scope.newAdvertiserdetails);

            $state.go('profile.basic', {}, { reload: false });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong>  updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);


 /*---====Change Password Controller =====--*/   

advertiserProfileApp.controller('ChangePasswordController', ["$scope", "AdvertiserProfileService", "ShareData", "$rootScope", "$state", "SharedMethods", function ($scope, AdvertiserProfileService, ShareData, $rootScope, $state, SharedMethods) {
    $rootScope.title = "Change Password | Brimav";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.headerClass = "normal-page-title";
    $rootScope.InnerHeading = "Change Password";
    $rootScope.activeView = "password";

    $scope.editing = false;

    $scope.change = function () {
        $scope.editing = true;
    
        $scope.passwordetails = {
            current_password: $scope.Currentpassword,
            password: $scope.Password,
            password_confirmation: $scope.Confirmpassword
        };
        console.log($scope.passwordetails);
    
        var putpassword =AdvertiserProfileService.changepassword($scope.passwordetails)
        putpassword.then(function (pl) {
            $scope.Currentpassword = "";
            $scope.Password = "";
            $scope.Confirmpassword = "";
            $scope.fieldErrors = null;
            SharedMethods.createSuccessToast('<strong>Password</strong>  changed successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);