
 /*----====== advertiser ADVERTS SERVICE ======----*/

advertiserAdvertApp.service("AdvertiserAdvertService", function ($http,$rootScope,SharedMethods) {

    var advertBase = "api/advertiseradvertAPI";
    var profileBase = "api/advertiserprofileAPI";
    
  
    /*--===== Get completed Adverts ====--*/
    
    this.getCompletedAdverts = function () {
        return $http.get(advertBase+'/completedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get Uncompleted Adverts ====--*/
    
    this.getUncompletedAdverts = function () {
        return $http.get(advertBase+'/uncompletedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };


    /*--===== Get  Advertlinks by ID ====--*/

    this.getAdvertlinksbyid = function (id) {
        return $http.get(advertBase +'/advertlinksbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Advert details");
            var advert = {};
            return advert;
        });
    };


      /*--===== Get  Advert plus unique reference no ====--*/

    this. getAdvertplusrefence = function (id) {
        return $http.get(advertBase +'/advertplusreference/'+ id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var advert = {};
            return advert;
        });
    };



   

    /*----Get currency exchange rate--*/

    this.getExchangerate=function()
    {
        return $http.get('https://openexchangerates.org/api/latest.json?app_id=18a6af291e844b3882059aab994567f7').then(function(response)
        {
            return response.data;
        });
    };


      /*--===== Get  Advertiser details ====--*/

    this.getAdvertiserdetails = function () {
        return $http.get(profileBase);
    };

     /*--===== Get  Advertbyid ====--*/

    this.getAdvertbyid = function (id) {
        return $http.get(advertBase+"/advertbyid/"+id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var advert = {};
            return advert;
        });
    };



    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherbyid = function (id) {
        return $http.get(advertBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            
            var pub = {};
            return pub;
        });
    };
    
   
   /*--===== Confirm Advert by ID ====--*/

    this.confirmadvert = function (id) {
        var request = $http({
            method: "get",
            url: advertBase + "/confirmadvert/" + id
        });
        return request;
    };


     /*--===== Confirm Advert Link ====--*/

    this.confirmadvertlink = function (confirmdetails) {
        var request = $http({
            method: "put",
            url: advertBase + "/confirmadvertlink",
            data:confirmdetails
        });
        return request;
    };


       /*--===== Get All Publishers ====--*/

    this.getallPublishers = function (offset) {
       return $http.get(advertBase+'/allpublishers/'+offset).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
   };


     /*--===== Get Banner Publishers ====--*/

    this.getBannerPublishers = function (type,offset) {
       return $http.get(advertBase+'/bannerpublishers/'+type+'/'+offset).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
   };


     /*--===== Verify Payment from Gateway ====--*/

   this.verifypayment = function (verificationdetails) {
      
        var request = $http({
            method: "post",
            /*headers: {'Authorization': 'Basic ' + credential},
            url: "https://api.simplepay.ng/v1/payments/verify",*/
            url:advertBase+'/verifypayment',
            data:verificationdetails,
        });

        return request;
    };



        /*--===== Verify Coupon ====--*/

   this.verifycoupon = function (verificationdetails) {
      
        var request = $http({
            method: "post",
            url: advertBase+'/verifycoupon',
            data:verificationdetails,
        });
        return request;
    };



     /*--===== Confirm Payment====--*/

    this.confirmpayment = function (confirmationdetails) {
        var request = $http({
            method: "put",
            url: advertBase + "/confirmpayment",
            data:confirmationdetails,
        });
        return request;
    };



    /*--===== Verify song url====--*/

    this.verifySongUrl = function (url) {
        var request = $http({
            method: "put",
            url: advertBase + "/verifysongurl",
            data:url,
        });
        return request;
    };
   
   /*--===== Post Song ====--*/

    this.postSong = function (song) {
        var request = $http({
            method: "post",
            url: advertBase + "/newsong",
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            formData.append("song" ,song);             
            return formData;            
            },
            eventHandlers:{ 
                progress:function(pl)
                { 
                    /* Upload Progress Bar Determinant*/
                    $rootScope.progress=pl;
                    $rootScope.percent=100*parseInt($rootScope.progress.loaded)/parseInt($rootScope.progress.total);
                    if($rootScope.total=0)
                    {
                        $rootScope.percent=0;
                    }

                    if ($rootScope.percent<25)
                    {
                        $rootScope.type="warning";
                    }

                    else if ($rootScope.percent >25 && $rootScope.percent <50)
                    {
                        $rootScope.type="info";
                    }
                    else if ($rootScope.percent>75 && $rootScope.percent <=100)
                    {
                        $rootScope.type="success";
                    }
                    else
                    {
                        $rootScope.type="danger";
                    }
                }
            },
           
        });
        return request;
    };






    /*--===== Put new Advert ====--*/

    this.postAdvert = function (advertdetails) {
        
        
       var request=$http({
            url:advertBase+'/newadvert',
            method:'post',
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            if(angular.isObject(advertdetails))
            {
                
                for(var key in advertdetails)
                {
                    var value=advertdetails[key];
                    formData.append(key,value);
                }
            }
          
                                                                    
            return formData;            
            },
        });
       return request;
   };


      /*--===== Edit  Advert ====--*/

   this.putAdvert = function (advertdetails) {
    
       var request=$http({
            url:advertBase+'/editadvert',
            method:'post',
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            if(angular.isObject(advertdetails))
            {
                
                for(var key in advertdetails)
                {
                    var value=advertdetails[key];
                    formData.append(key,value);
                }
            }
                                                        
                                     
            return formData;            
            },
        });
       return request;
   };

    /*--===== put Publisher And Totalamount====--*/

    this.putPublisherAndTotalamount = function (details) {
       
        var request = $http({
            method: "put",
            url: advertBase + "/totalamount",
            data:details,
        });
        return request;
    };

        /*--===== Download pdf receipt ====--*/

    this.downloadPDF = function (id) {
       
       return $http.get(advertBase +'/downloadpdf/' + id)
    };


      /*--===== Delete Advert ====--*/

    this.deleteAdvert = function (id) {
        var request=$http(
        {
            url:advertBase+'/removeadvertbyid/'+id,
            method:'delete'
        });
        return request;
     
   };


     /*--===== Get  Receipts ====--*/

    this.getReceipt = function (id) {
        return $http.get(advertBase+'receipt/'+id).then(function(pl)
            {
                return pl.data;
            },
            function(error)
            {
                var Receipt={}
                return Receipt;
            });
    };


     /*--get all  banner typess---*/
    this.getBannerTypes=function()
    {
        return $http.get(advertBase+'/getbannertypes');
    };


});