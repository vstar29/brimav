﻿/*-------========== ADVERTISER SHARED SERVICE =======-------*/

/*--==== Get  Logged Inuser Details ====--*/

advertiserShareApp.service('ShareService', function ($http) {
    //Get current advertiser info
    this.getLoggedInAdvertiser = function () {

         var request=$http({
            url:'api/advertiserprofileAPI/shared',
            method:'get',
        });

        return request;


    };

    /*--===== Mark notification as read ====--*/

    this.markRead = function () {
        var request=$http({
            url:'api/markread',
            method:'get',
        });

        return request;
       
    };

    
})