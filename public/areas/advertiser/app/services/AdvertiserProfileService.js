
/*----====== ADMIN PROFILE SERVICE ======----*/

 
advertiserProfileApp.service('AdvertiserProfileService', function ($http, SharedMethods) {

    var profileBase = "api/advertiserprofileAPI";
    

    /*--===== Get  Basic details ====--*/
   
    this.getAdvertiserBasicDetails = function () {
        return $http.get(profileBase).then(function (response) {
            return response.data;
            
        },
       function (error) {
           SharedMethods.createErrorToast("Problem retreiving your details, kindly refresh");
           var advertiser = {};
           return advertiser;
       });
    };
    

     /*--===== Change Avatar ====--*/
  
  this.putUploadavatar=function(avatar)
  {

        var request = $http({
            method: "post",
            url:profileBase +"/uploadavatar",
            headers : { 'Content-Type' : undefined},
            transformRequest: function () {                
            var formData = new FormData(); 
            formData.append("avatar" ,avatar);              
            return formData;            
            },
        });
        return request;
    };
    

    /*--===== Edit Basic details ====--*/
  
  this.putEditadvertiserBasicDetails=function(AdvertiserDetails)
  {
   
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editadvertiserbasicdetails",
        data:AdvertiserDetails
    });
    return request;
  };


    /*--===== Change Password ====--*/
    
    this.changepassword = function (Passworddetails) {
      
        var request = $http({
            method: 'put',
            url: profileBase + "/changepassword",
            data: Passworddetails
        });
        return request;
    };

})