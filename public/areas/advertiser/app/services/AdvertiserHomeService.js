	/*---====== Advertiser Home Service===-----*/

advertiserHomeApp.service('HomeService',function($http)
{
	 var advertBase = "api/advertiseradvertAPI";

	/*--===== Get unpaid Advert  ====--*/

	this.getUnpaidadvert=function()
	{
		return $http.get(advertBase +'/unpaidadverts').then(function(response)
		{
			return response.data
		},
		function(error)
		{
			var advert={};
			return advert;
		})
	}

	this.deleteAdvert=function(id)
	{
		var request=$http({
			method:'delete',
			url:advertBase +'/removedvertbyid/'+id
		});
		return request;
		
	}
})