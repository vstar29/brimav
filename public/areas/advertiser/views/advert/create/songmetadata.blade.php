<div class="wow fadeIn col-md-12 margin-four" data-wow-duration="300ms">
        <form name="songmetadata" id="songmetadata" novalidate >
    
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>
            	 
            
                <div class="form-group col-md-12 sm-no-padding margin-one-bottom">
                    <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Name</label>

                    <input type="text" readonly id="title" name="title" class="form-control width-120 input-round medium-input margin-one" ng-model="Metadata.title" />     
                </div>

               

                <div class="form-group sm-no-padding margin-one-bottom">
                	<div class="col-md-8 pull-left">
	                    <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Artist</label>
	                    <input type="text" readonly id="artist" name="artist" class="form-control width-50 input-round medium-input margin-one" ng-model="Metadata.artist" /> 

	                    <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Year</label>
	                    <input type="text" readonly id="year" name="year" class="form-control width-50 input-round medium-input margin-one" ng-model="Metadata.year" /> 

	                    <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Genres</label>

                        <select id="genres" disabled name="genres" class="form-control width-50 input-round medium-input margin-one" ng-class="{'input-validation-error': fieldErrors.genres }" >
                        <option value="Metadata.genre" selected>[[Metadata.genre]] </option>
                        </select>

	                
	                </div>

	                <div class="col-md-4">
             			 <div  class="form-group margin-four">
	                     <img class="advertpicture" ng-src="[[Albumart=='' && Unknown ||Albumart]]" />
                    	</div>
        			</div>
                </div>

                 <div class="form-group col-md-12 sm-no-padding margin-one-bottom">
                    <label class = " control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Album</label>

                    <input type="text" id="album" readonly name="album" class="form-control width-120 input-round medium-input margin-one" ng-model="Metadata.album" />     
                </div>
                
                <div class="form-group col-md-8 pull-right   margin-one-bottom">
	                <button class="btn no-pad btn-default btn-medium" ui-sref="addadvert.uploadsong" ng-disabled="editing"><i class="fa  fa-arrow-left"></i></button>
	                <button type="submit" form="songmetadata" ng-click="songUpload()" class="btn no-pad btn-medium btn-adgold no-margin-bottom">
	                  Upload <img ng-show="uploading" class="loader loader-small display-inline-block ng-hide">
	                </button>
           		</div>

        </form>
</div>
