<div class="col-md-12 center-col" id="publisher_list">

	<div class="tab-style2 ">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- tab navigation -->
                <ul  class="nav nav-tabs nav-tabs-light text-left">
                    <li  class="active"><a href="#websites" data-toggle="tab">Websites <i class="fa fa-globe"></i></a></li>
                    <li><a href="#social" data-toggle="tab">Social <i class="fa fa-twitter"></i> </a></li>
                    <li><a href="#tv" data-toggle="tab">Tv-Radio <i class="fa  fa-desktop"></i></a></li>
                </ul>
                <!-- end tab navigation -->
            </div>
        </div>
        <!-- tab content section -->
        <div class="tab-content ">
            <!--websites-->
        	<div class="tab-pane med-text fade in active" id="websites">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <ul>
                    	<li><input type='text' class='width-100 input-round medium-input' style='height: 35px;border-radius: 20px !important;' ng-model='websitesFilter' placeholder='&#xf002;' /></li> 

                        <li style='font-size: 14px;margin-bottom: 10px;border-bottom: solid 1px rgba(204, 204, 204, 0.49);margin-left: 20px;' class='pointer' data-ng-repeat='website in websites| filter: websitesFilter'><a data-ng-click='toggleSelectItem(website,"websites")'><span data-ng-class='getClassName(website)' aria-hidden='true'></span> <span >[[website.websiteurl]] - [[(+website.prize)*rate]] [[currency]]</span> </a></li>

                    </ul>

                    </div>
                </div>
            </div>

            <!--social-->
            <div class="tab-pane med-text fade in" id="social">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <ul>
                        <li><input type='text' class='width-100 input-round medium-input' style='height: 35px;border-radius: 20px !important;' ng-model='socialFilter' placeholder='&#xf002;' /></li> 

                        <li style='font-size: 14px;margin-bottom: 10px;border-bottom: solid 1px rgba(204, 204, 204, 0.49);margin-left: 20px;' class='pointer' data-ng-repeat='social in socials| filter: socialFilter'><a data-ng-click='toggleSelectItem(social,"socials")'><span data-ng-class='getClassName(social)' aria-hidden='true'></span> <span>[[social.handler]] ([[social.followers]] followers) - [[(+social.prize)*rate]] [[currency]]</span> </a></li>

                    </ul>

                    </div>
                </div>
            </div>

            <!--Tvs-->
            <div class="tab-pane med-text fade in" id="tv">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <ul>
                        <li><input type='text' class='width-100 input-round medium-input' style='height: 35px;border-radius: 20px !important;' ng-model='tvFilter' placeholder='&#xf002;' /></li> 

                        <li style='font-size: 14px;margin-bottom: 10px;border-bottom: solid 1px rgba(204, 204, 204, 0.49);margin-left: 20px;' class='pointer' data-ng-repeat='tv in tvs| filter: tvFilter'><a data-ng-click='toggleSelectItem(tv,"tvs")'><span data-ng-class='getClassName(tv)' aria-hidden='true'></span>[[tv.channel_name]] ([[tv.channel_frequency]]) <span>  - [[(+tv.prize)*rate]] [[currency]]</span> </a></li>

                    </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
	
</div>