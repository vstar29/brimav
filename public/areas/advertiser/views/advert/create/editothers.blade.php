<div class="" data-wow-duration="300ms">
        <form name="advertcreate" id="advertcreate" novalidate ng-submit="create()" >
    
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>

            <div class="form-group">
                <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Title<span class="red">*</span></label>

                <input type="text" id="title" name="title" class="form-control width-100 input-round medium-input margin-one" ng-model="Advert.title"
                           ng-class="{'input-validation-error': fieldErrors.title }" /> 
                <span ng-show="fieldErrors.title" class=" field-validation-error">[[fieldErrors.title]]</span>    
            </div>
           

            <div class="form-group sm-no-padding">
               
                <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Content</label>

                
                
                <textarea  id="description" rows="15" name="description" class="form-control width-100 input-round medium-input margin-one" ng-model="Advert.description" placeholder=""  ng-class="{'input-validation-error': fieldErrors.description }"></textarea>
            </div>
             <span ng-show="fieldErrors.description" class="field-validation-error remain2 white-space-pre">[[fieldErrors.description]]</span>


            <div class="form-group sm-no-padding">
               <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1" style="    margin-top: 19px;">Featured Image<span class="red">*</span></label><br>
               <img ng-if="stepsModel !='' || Advert.imageurl !=''" class=" advertpicture"  ng-src="[[stepsModel=='' && Advert.imageurl ||stepsModel]]" /><br>

                 <label class="text-decoration-underline " for="image" uib-tooltip="select image" style="cursor: pointer;" tooltip-placement="bottom" tooltip-trigger="mouseenter"> 
                <input type="file" name="image" id="image" accept="image/*" model= ng-model-instant onchange="angular.element(this).scope().imageUpload(event)"  class="form-control width-40 margin-one hiddeninput"
                   ng-class="{'input-validation-error': fieldErrors.image }" />[[Advert.imageurl==''&& 'Set featured image' || 'Change featured image']]
                
                </label> 
                        
                <span ng-show="fieldErrors.image" class="field-validation-error remain2 white-space-pre">[[fieldErrors.image]]</span>
            </div>


            <div class="form-group sm-no-padding">
                <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Attachment(pdf and doc [max-5mb])</label>
                
                   <input type="file" name="attachment" id="attachment"  model= ng-model-instant onchange="angular.element(this).scope().fileUpload(event)"  class="form-control input-round width-40 margin-one "
                   ng-class="{'input-validation-error': fieldErrors.attachment }" />
                    <span ng-show="fieldErrors.attachment" class="field-validation-error remain2 white-space-pre">[[fieldErrors.attachment]]</span>

            </div>

             <div class="form-group">
                    <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Youtube/Vimeo Link</label>

                    <input type="text" id="youtube" name="youtube" class="form-control width-100 input-round medium-input margin-one" ng-model="Youtube"
                               ng-class="{'input-validation-error': fieldErrors.title }" /> 
                    <span ng-show="fieldErrors.title" class=" field-validation-error">[[fieldErrors.youtube]]</span>    
            </div>
            

             <div class="form-group col-md-12 pull-right">
        
                <button type="submit" form="advertcreate"  class="pull-right no-pad btn btn-medium  btn-adgold no-margin-bottom">
                    Continue
                     <img ng-show="creating" class="display-inline-block remain remain2 loading"  src="/uploads/images/theme/ajax_loader.gif">
                </button>
                <button class="btn no-pad pull-right btn-default btn-medium" ui-sref="home" ng-disabled="editing"><i class="fa  fa-arrow-left"></i></button>
            </div>
        </form>
    
</div>
