<div class="col-md-7 center-col" data-wow-duration="300ms">
        <form name="advertcreate" id="advertcreate" novalidate ng-submit="create()" >
    
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>

            <div class="form-group">
                <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Title<small class="red">*</small></label>

                <input type="text" id="title" name="title" class="form-control width-100 input-round medium-input margin-one" ng-model="Title"
                           ng-class="{'input-validation-error': fieldErrors.title }" /> 
                <span ng-show="fieldErrors.title" class=" field-validation-error">[[fieldErrors.title]]</span>    
            </div>
           

           
            <div class="form-group ">
               <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1" style="    margin-top: 19px;"> Banner Image<small class="red">*</small></label>
              
                <input type="file" name="image" id="image" accept="image/*" model= ng-model-instant onchange="angular.element(this).scope().imageUpload(event)"  class="form-control width-100 margin-one "
                   ng-class="{'input-validation-error': fieldErrors.image }" />
                   
                     <img ng-if="stepsModel !=''" class="advertpicture" ng-show="stepsModel"  ng-src="[[stepsModel]]" /><br>

                <span ng-show="fieldErrors.image" class="field-validation-error remain2 white-space-pre">[[fieldErrors.image]]</span>
            </div>


            <div class="form-group sm-no-padding">
                <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Destination Url Address <small>(optional)</small></label>
                
                <input value="" placeholder="http://" type="text" id="destination" name="destination" class="form-control width-100 input-round medium-input margin-one" ng-model="Destination"
                       ng-class="{'input-validation-error': fieldErrors.title }" /> 
            <span ng-show="fieldErrors.title" class=" field-validation-error">[[fieldErrors.destination]]</span>    
            </div>


            <div class="form-group">

                 <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Select Banner Size  <small class="red">*</small></label>
                <bannertype-multiselect model="SelectedBannerType" options="BannerTypes"></bannertype-multiselect>
            </div>

          
            

             <div class="form-group col-md-12 pull-right">
        
                <button type="submit" form="advertcreate"  class="pull-right no-pad btn btn-medium  btn-adgold no-margin-bottom">
                    Continue
                   <img ng-show="creating" class="display-inline-block remain remain2 loading"  src="/uploads/images/theme/ajax_loader.gif">
                </button>
                <button class="btn no-pad pull-right btn-default btn-medium" ui-sref="advert.unconfirmed" ng-disabled="editing"><i class="fa  fa-arrow-left"></i></button>
            </div>
        </form>
    
</div>
