<!Doctype html>
<html class="no-js" data-ng-app="AdvertiserAdvertModule" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Ad-GOLD | Bridging The Gap </title>
        <meta name="description" content="AdGold - Designed by VGD">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="vstar" content="VGD">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
        
        <!--styles and scripts>-->

      

        <link rel="stylesheet" type="text/css" href="{{elixir('css/templatestyle.css')}}">
        
    </head>
    <body class="ng-cloak" ng-controller="PagePrintController">
        <!-- navigation panel -->
        
        
       <div class="wow ng-cloak fadeIn col-md-6 center-col content-top-margin margin-four" data-wow-duration="300ms">

            <div class="col-md-12 margin-four-bottom row ">
                <div class="col-md-5 pull-left" >
                    <img title="adgold logo" src="/uploads/images/logo/brimavcolor.png" style="max-width:30%" />
                    
                </div>

                <div class="pull-right">
                    <span class="display-block black-text ">14b Williams</span>
                    <span class="display-block black-text">Lagos, Nigeria</span>
                    <span class="display-block black-text">+2347031840158</span>
                </div>
            </div>
            <div class="row" style="display:block !important; width: 100% ">
                <span class="black-text" style="font-size:15px ;font-weight:400">Receipt # : BRM{{$no}}</span> 
                <span class="black-text pull-right" style="font-weight: 400;font-size:15px">Date: {{$date}}</span>
            </div>
            <hr class="bg-adgold" />
            <div class="col-md-10 margin-four row" >
                <div class="col-md-5 ">
                    <label class="adgold" style="font-weight:450;font-size:15px">BILL TO:</label>
                    <span class="display-block black-text ">{{$username}}</span>
                    <span class="display-block black-text ">{{$city}}, {{$country}}</span>
                    <span class="display-block black-text ">{{$email}}</span>
                    <span class="display-block black-text ">{{$phonenumber}}</span>

                </div>

                <div class="pull-right col-md-5 ">
                    <label class="adgold" style="font-weight :450;font-size:15px">PAYMENT DETAILS:</label>
                    <span class="display-block black-text ">{{$cardtype}} ##########{{$cardnumber}}</span>
                    <span class="display-block black-text green ">Paid: {{$currency}} {{$amount}}</span>
                </div>
                
            </div>

            <hr class="bg-adgold col-md-12 " />
            <div class="col-md-10 margin-four row" style="margin-left: 18px">

                <label class="adgold" style="font-weight :450;font-size:15px">ITEM:</label>
                

            
            <label  class="pull-right adgold" style="font-weight: 450;font-size:15px">Digital Advert</label>
                <ul style="list-style-type:none">
                    <li><span>No of sites</span><span class="pull-right">{{$websiteno}}</span></li>
                    <li><span>Amount</span><span class="pull-right">{{$currency}} {{$amount}}</span></li>
                     <li><span>Discount</span><span class="pull-right">{{$currency}} {{$discount}}</span></li>
                    <li><span>Total</span><span class="pull-right green">{{$currency}} {{$totalamount}}</span></li>
                </ul>
                    
            </div>

        </div>


    </body>
     <script type="text/javascript" src="/scripts/brimav_script.js"></script>
        <script src="/scripts/angularui.js"></script>
        <script type="text/javascript" src="/areas/advertiser/app/advertiserscripts.js"></script>

         <script type="text/javascript" src="/areas/advertiser/app/advertiser_templates.js"></script>
         <script type="text/javascript">
                jQuery(function()
                {
                setTimeout(
                  function() 
                  {
                      window.print();
                  }, 1000);
                  
                });
               
             
        </script>
</html>