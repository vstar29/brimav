<div class="wow fadeIn col-md-12 margin-four" data-wow-duration="300ms">
        <form name="advertcreate" id="advertcreate" novalidate ng-submit="create()" >
    
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>


            <div class="pull-right col-md-8">
                <div class="form-group">
                    <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Title<span class="red">*</span></label>

                    <input type="text" id="title" name="title" class="form-control width-100 input-round medium-input margin-one" ng-model="Advert.title"
                               ng-class="{'input-validation-error': fieldErrors.title }" /> 
                    <span ng-show="fieldErrors.title" class=" field-validation-error">[[fieldErrors.title]]</span>    
                </div>
               

                <div class="form-group sm-no-padding">
                    <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Description<span class="red">*</span></label>
                    
                         <textarea id="description" rows="14" name="description"    class=" form-control width-100 input-round medium-input margin-one" ng-model="Advert.description" placeholder="Describe your song and the artist"  ng-class="{'input-validation-error': fieldErrors.description }"></div>
                        </textarea>
                        <span ng-show="fieldErrors.description" class="field-validation-error remain2 white-space-pre">[[fieldErrors.description]]</span>
                </div>
        


                <div class="form-group sm-no-padding">
                   <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1">Genres<span class="red">*</span></label>

                        <select id="genres" name="genres" ng-model="Advert.genre" class="form-control width-100 input-round medium-input margin-one" ng-class="{'input-validation-error': fieldErrors.genres }" ng-options="genre for genre in Genres" >
                        <option value="">....Select Genre..... </option>
                        </select>
                        <span ng-show="fieldErrors.genre" class="field-validation-error remain2 white-space-pre">[[fieldErrors.genre]]</span>
                       
                </div>

                 <div class="form-group">
                    <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Itunes/Soundcloud Link</label>

                    <input type="text" id="itunes" name="itunes" class="form-control width-100 input-round medium-input margin-one" ng-model="Advert.itunes"
                               ng-class="{'input-validation-error': fieldErrors.title }" /> 
                    <span ng-show="fieldErrors.title" class=" field-validation-error">[[fieldErrors.itune]]</span>    
                </div>
                
        </div>

        <div class="pull-left col-md-4">
              <div  class="form-group">
                        
                         <label class = "control-label  font-14 black-text font-weight-500 letter-spacing-1" style="    margin-top: 5px;">Artwork<span class="red">*</span></label>
                        <label for="image" uib-tooltip="select image" style="cursor: pointer;" tooltip-placement="bottom" tooltip-trigger="mouseenter"> 
                        <input type="file" name="image" id="image" accept="image/*" model= ng-model-instant onchange="angular.element(this).scope().imageUpload(event)"  class="form-control width-40 margin-one hiddeninput"
                           ng-class="{'input-validation-error': fieldErrors.image }" /> 
                                <img style="height: 180px;" class=" advertpicture" ng-src="[[stepsModel=='' && Advert.imageurl ||stepsModel]]" />
                        </label>
                        <span ng-show="fieldErrors.image" class="field-validation-error remain2 white-space-pre">[[fieldErrors.image]]</span>
                    </div>
        </div>

        <div class="form-group col-md-12 pull-right">
            
            <button type="submit" form="advertcreate"  class="pull-right no-pad btn btn-medium  btn-adgold no-margin-bottom">
                Continue
                 <img ng-show="creating" class="display-inline-block remain remain2 loading"  src="/uploads/images/theme/ajax_loader.gif">
               
            </button>
            <button class="btn no-pad pull-right btn-default btn-medium" ui-sref="home" ng-disabled="editing"><i class="fa  fa-arrow-left"></i></button>
        </div>
    </form>
    
</div>
