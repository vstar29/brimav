 <div class="wow fadeIn col-md-12 margin-four" data-wow-duration="300ms">
    
    <div class="form-group center-col col-md-10 margin-one-bottom ">
         
        <div class="col-md-5 pull-left ">
            <label for="song" class="pointer mobileupload-button btn btn-medium btn-adgold ">
                <input type="file" onchange="angular.element(this).scope().songPreview(event)" name="song" id="song" accept="audio/*" class="hiddeninput  form-control width-40 margin-one"  ng-class="{'input-validation-error': fieldErrors.song }" />
                Upload
            </label>

            <span ng-show="fieldErrors.song" class="field-validation-error remain2 white-space-pre">[[fieldErrors.song]]</span>
                 
        </div>
        <h3 class="or-adjust col-md-1">OR</h3>
        <div class="col-md-5 pull-right">
            <form id="songurl", name="songurl" novalidate ng-submit="verifySongrl()">
            <div class="form-group margin-one-bottom">
                <input type="text" name="songurl" id="songurl" placeholder=" Enter song url" ng-required="true" class="form-control input-round medium-input margin-one width-100 margin-one" ng-model="Songurl"  ng-class="{'input-validation-error': fieldErrors.url }" />  

                 <span ng-show="fieldErrors.url" class=" field-validation-error remain2 white-space-pre">[[fieldErrors.url]]</span>
            </div>
            <button type="submit" form="songurl"  class="btn btn-small pull-right  no-margin-bottom" style="    margin-top: -3px;">
               <i class="fa  fa-arrow-right"></i>
                <img ng-show="editing" class="loader loader-small display-inline-block ng-hide">
            </button>
            </form>
        </div>


        
    </div>

</div>


    <span>Maximum uploadable size is 20mb</span>