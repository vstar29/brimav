<div id="generalContent" class="ng-hide center-col-md col-md-12 ng-cloak  margin-top-three" ng-show="!loadingView">

<div class="col-md-10 col-sm-10 center-col">
    <div class="tab-style5">
        <div class="row">
            <div class="tabs-left col-md-12 col-sm-12">
                <!-- tab navigation -->
                <ul class="nav nav-tabs nav-tabs-custom pull-left extradetails">

                <li class="pointer" ng-class="{'active': activeView == 'upload'}"><a ui-sref="addadvert.uploadsong">Upload</a></li>
                    <li class="pointer" ng-class="{'active': activeView == 'basic'}"><a>Basic info</a></li>
                    <li class="pointer" ng-class="{'active': activeView == 'selectpublisher'}"><a >Platforms</a></li>
                       
                </ul>
                <!-- end tab navigation -->
                <!-- tab content section -->
                <div class="tab-content position-relative overflow-hidden" style=" border: solid 1px #65432a;border-radius: 1%;">
                    <!-- tab content -->
                    <div class="tab-panel med-text fade in active">
                        <div ui-view class="ng-cloak center-col">
	
						</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>