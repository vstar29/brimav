
<div class="col-md-6 bg-gray regmargin" >

	<div  class="modal-header">
	    <span class="modal-title " style="font-size: 11px !important;"> SELECT ADVERT TYPE</span>
	</div>
 	<div class="modal-body">
    
    
	    <form name="recoverform"  ng-submit="typeSelection()">
	    	<div class="form-group">
	    		<select id="type" ng-required="true" name="type" ng-model="Type" class="form-control  input-round medium-input " ng-class="{'input-validation-error': fieldErrors.Type }" >
	    			<option value="">....Select Advert..... </option>
	                <option value="music"  name="music">Music Promotion</option>
	                 <option value="banner" name="others">Banners</option>
	                <option value="others" name="others">Videos/Others</option>
	               
                </select>
			   
			</div>
		    <div class="form-group pull-right">
                <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" ng-disabled="recoverform.$pristine"   type="submit">Go
                </button>
             </div>	
	    </form> 
	    
    </div>
    <button title="Close (Esc)" type="button" ng-click="close()" class=" mfp-close">×</button>
</div>
