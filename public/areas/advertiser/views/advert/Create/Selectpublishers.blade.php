<div class="wow fadeIn col-md-12 " data-wow-duration="300ms">         
<div class="form-group">
    <select id="Currency" name="Currency" ng-model="Currency" class="form-control">
        <option value="USD">USD</option>
         <option value="NGN">NGN</option>
    </select>
</div>
    <div class="form-group col-md-7 center-col  margin-four-bottom" style="margin-top: -15px;">
        <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1"><span>Total([[noofwebsite]])</span></label>
        <div class="col-md-9 ">
            <label class="col-md-9  green"> [[Currency]] [[Total*rate|currency:"":2]] </label>
        </div>
    </div>
   

    <div ng-if="Type!='banner'" class="form-group col-md-12 center-col " style="  margin-top: 70px">
        <dropdown-multiselect infinite-scroll="myPagingFunction()" model="SelectedPublishers" websites="Websites" tvs="Tvs" socials="Socials" currency="Currency" rate="rate"></dropdown-multiselect> 
         <span ng-show="fieldErrors.publishers" class=" field-validation-error remain2 white-space-pre">[[fieldErrors.publishers]]</span>  
    </div> 

     <div ng-if="Type=='banner'" class="form-group col-md-12 center-col " style="  margin-top: 70px">
        <publisher-select infinite-scroll="myPagingFunction()" model="SelectedPublishers" banners="Banners"  currency="Currency" rate="rate"></publisher-select> 
         <span ng-show="fieldErrors.publishers" class=" field-validation-error remain2 white-space-pre">[[fieldErrors.publishers]]</span>  
    </div> 


    <div class="form-group col-md-12 pull-right  margin-four-bottom" style="margin-top:155px">
     
    
        <button type="submit" ng-click="save(SelectedPublishers.length)"  class="btn btn-medium no-pad pull-right btn-adgold no-margin-bottom">
            CONTINUE
            <img ng-show="continue"  class="display-inline-block remain remain2 loading" src="/uploads/images/theme/ajax_loader.gif">
        </button>

        <button class="btn no-pad pull-right btn-default btn-medium" ui-sref=" addadvert.[[returnState]]({id:id})
        " ng-disabled="editing"><i class="fa  fa-arrow-left"></i></button>

    </div>
   
</div>