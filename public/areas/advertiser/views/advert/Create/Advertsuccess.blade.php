
<div class="regmargin col-md-12 receipt"  style="margin-right: 0% !important;     padding-bottom: 10px !important">
	<div  class="modal-header ">
	    <span class="modal-title"  style="font-size: 13px !important; height:30px; text-align:center !important; font-weight:360"> Payment Receipt </span>
	</div>
	
 	<div>
    
	    <div class="wow fadeIn col-md-10 center-col margin-four" data-wow-duration="300ms">

		<div class="col-md-12 margin-four-bottom ">
			<div class="col-md-5 pull-left" >
				<img title="brimav logo" src="/uploads/images/logo/brimavcolor.png" style="max-width:60% ; margin-bottom: 10px" />
				
			</div>

			<div class="pull-right extradetails">
				<span class="display-block black-text ">14b Williams</span>
				<span class="display-block black-text">Lagos, Nigeria</span>
				<span class="display-block black-text">+2347031840158</span>
			</div>
		</div>

		<span class="black-text" style="font-size:13px ;font-weight:400">Receipt # :[[PrintDetails.receiptnumber]]</span> 
		<span class="black-text pull-right" style="font-weight: 400;font-size:12-px">Date: [[PrintDetails.date | amDateFormat:'MMM Do YYYY']]</span>
		<hr class="bg-adgold" />
		<div class="col-md-12 margin-four" >
			<div class="col-md-5 form-group">
				<label class="adgold" style="font-weight:450;font-size:13px">BILL TO:</label>
				<span class="display-block black-text ">[[PrintDetails.username]]</span>
				<span class="display-block black-text ">[[PrintDetails.city]], [[PrintDetails.country]]</span>
				<span class="display-block black-text ">[[PrintDetails.phone]]</span>

			</div>

			<div class="pull-right">
				<label class="adgold" style="font-weight :450;font-size:13px">PAYMENT DETAILS:</label>
				<span class="display-block black-text ">[[PrintDetails.cardtype]] ##########[[PrintDetails.cardnumber]]</span>
				<span class="display-block  green ">Paid: [[PrintDetails.amount|currency:"":0]] [[PrintDetails.currency]]</span>
			</div>
			
		</div>

		<hr class="bg-adgold col-md-12 " />
		<div class="col-md-10 margin-four" style="margin-left: 18px">

			<label class="adgold" style="font-weight :450;font-size:13px">ITEM:</label>
			

		<label  class="pull-right adgold" style="font-weight: 450;font-size:13px">Digital Advert</label>
			<ul style="list-style-type:none">
				<li><span>No of platforms</span><span class="pull-right">[[PrintDetails.websiteno]]</span></li>
				<li><span>Amount</span><span class="pull-right">[[PrintDetails.totalamount|currency:"":0]] [[PrintDetails.currency]]</span></li>
				<li><span>Discount</span><span class="pull-right">[[PrintDetails.discount|currency:"":0]]  [[PrintDetails.currency]]</span></li>
				<li><span>Total</span><span class="pull-right green">[[PrintDetails.amount|currency:"":0]] [[PrintDetails.currency]]</span></li>
			</ul>
				
		</div>

	</div>

	</div>

	<div class="modal-footer col-md-12 display-block" style="border:0px !important">
	    <button  style="margin-right:30px" class="pointer" ng-click="print()"><i class="fa  fa-print small-icon "></i></button>
	  
	</div>
	    
    <button title="Close (Esc)" type="button" ng-click="close()" class=" mfp-close" style="background-color:transparent !important; color:black !important">×</button>

</div>
    