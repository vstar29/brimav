<div class="col-md-12">
  <div class="center-col col-md-6 ng-cloak" id="payment">
    <button  class="btn no-pad   btn-medium" ui-sref="addadvert.selectpublishers({id:Advert.id})" ng-disabled="editing"><i class="fa  fa-arrow-left"></i></button>

    <h3 class="margin-two-bottom pull-right " style="margin-right: 150px;
      margin-top: 35px">Checkout</h3>
                        
        <li class="list-group-item">
          <b>Title</b> <a class="pull-right">[[Advert.title|limitTo:40]]</a>
        </li>
        <li ng-if="Advert.songurl!=null && Advert.songurl!='' && Advert.songurl!='undefined'" class="list-group-item">
          <b>Song</b> <audio ng-if="Advert.songurl!=null && Advert.songurl!='' && Advert.songurl!='undefined'"  controls  class="pull-right"  ng-src="[[Advert.songurl]]" style="width:160px" ></audio>
        </li>
        <li class="list-group-item">
          <b>Medium No</b> <a class="pull-right" >[[WebsitesNo]]</a>
        </li>
        <li class="list-group-item">
          <b>Total</b> <a class="pull-right green">[[Advert.totalamount|currency:"N":0]] ~ [[(+Advert.totalamount)*rate]] USD</a>
        </li>
        <li class="list-group-item pull-right">

          <button type="submit" ng-click="lunchpayment()"  class="btn btn-medium  no-margin-bottom" style="background-color: #d4e622; border:solid 1px #333">
                      <img src="/uploads/images/theme/logo-checkout.png" class="paybutton"><img ng-show="loading" class="loader loader-small display-inline-block ng-hide">
                        </button>
        </li>
        <!--ng-if="0"-->
        <div  class="form-group" style="margin-top: 20px; margin-left: -15px">
          <form nnovalidate ng-submit="verifyCoupon()"  class="col-md-6">
            <input type="text" name="coupon" id="coupon" ng-required="true" placeholder="Coupon code" ng-model="Coupon" class=" form-control coupon-input input-round medium-input">
             
             <button type="submit" class="btn coupon-button pull-right btn-round btn-small no-pad btn-adgold">Use <img ng-show="creating" class="loader loader-small display-inline-block ng-hide"></button>
          </form>
         </div>
        </ul>
  </div>
</div>
