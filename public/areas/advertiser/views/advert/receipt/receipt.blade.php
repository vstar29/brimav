<head>
	<style type="text/css">

	.pull-left{text-align: left !important;}
	.pull-right{width:40%;float: right !important;}
	.black-text{text-color:black;}
	.display-block{display:block !important;}
	.green{color:green;}
	.col-md-8{width: 86.66666667%;}
	.col-md-12{width: 100%;}
	.bg-adgold{background-color:#65432a;}
	.adgold{color: #65432a}
	.content-top-margin{margin-top: 100px;}
	.center-col{float: none!important;
    			margin-left: auto!important;
    			margin-right: auto!important;
    		}
    .margin-four:{    margin-top: 4%!important;
    margin-bottom: 4%!important;}
</style>
</head>



<div  class="col-md-8 center-col content-top-margin margin-four" >

        <div class="col-md-12 margin-four-bottom ">
            <div class="col-md-5 pull-left"  >
                <img title="Brimav" src="/uploads/images/logo/brimavcolor.png" style="max-width:50%" />
                
            </div>

            <div class="pull-right">
                <span class="col-md-12 black-text ">Brimav</span><br/>
                <span class="col-md-12 black-text">Lagos, Nigeria</span><br/>
                <span class="col-md-12 black-text">+2347031840158</span><br/>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px; margin-bottom:30px">
        	<div  class="black-text" style="font-size:15px ; font-weight:400">Receipt # : {{$no}}</div> 
			<div  class="black-text pull-right" style="text-align:right !important; margin-top:-34px  font-weight: 400;font-size:15px">{{$date}}</div></div>
		<hr class="bg-adgold" />

		<div class="col-md-12 " style="margin-top:20px margin-bottom:30px" >
			<div class="col-md-5 form-group">
				<label class="adgold" style="font-weight:450;font-size:15px">BILL TO:</label><br/>
				<span class="display-block black-text ">{{$username}}</span><br/>
				<span class="display-block black-text ">{{$city}}, {{$country}}</span><br/>
				<span class="display-block black-text">{{$phonenumber}}</span>

			</div>

            <div class="pull-right" style="margin-top:-126px">
				<label class="adgold" style="font-weight :450;font-size:15px">PAYMENT DETAILS:</label><br/>
				<span class="display-block black-text ">{{$cardtype}} ##########{{$cardnumber}}</span><br/>
				<span class="display-block black-text green ">Paid: {{$totalamount}}  {{$currency}}</span>
			</div>
            
        </div>

        <hr class="bg-adgold col-md-12 " style="margin-top:40px" />
        <div class="col-md-10 margin-four" style="margin-left: 18px">

           <label class="col-md-12">
	           <label class="adgold" style="font-weight :450;font-size:15px">ITEM:</label>
			   <label  class="pull-right adgold" style="font-weight: 450;font-size:15px">Digital Advert</label>
			</label>
			<ul class="pull-right" style="list-style-type:none">
				<span class=""><span>No of sites: </span><span class="pull-right">	{{$websiteno}}</span></span>
				<li class="col-md-12"><span>Amount:		</span><span class="pull-right">				{{$totalamount}}  {{$currency}}</span></li>
				<li class="col-md-12"><span>Discount: {{$discount}}  {{$currency}}	</span><span class="pull-right"> </span></li>
				<li class="col-md-12"><span>Total:	</span><span class="pull-right green"> 		{{$amount}} {{$currency}}</span></li>
			</ul>
                
        </div>

</div>