<!-- Create Form-->
<div class="col-md-5">
    <form name="formCreate" novalidate ng-submit="create()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>

            <div class="form-group ">
                <label class = "control-label big-label" >Title</label>
                <input type="text" id="title" name="title" class="form-control" ng-model="Title"
                        ng-required="true" ng-class="{'input-validation-error': fieldErrors.title }" />
                <span ng-show="fieldErrors.title" class="field-validation-error white-space-pre">[[fieldErrors.title]]</span>
            </div>


            <div class="form-group">
                <label class = "control-label big-label" >Body
                </label>
                <textarea id="body" name="Body" class="form-control" ng-required="true" rows="4" ng-model="Body" ng-class="{'input-validation-error': fieldErrors.body }"></textarea>
                <span ng-show="fieldErrors.body" class="field-validation-error white-space-pre">[[fieldErrors.body]]</span>
            </div>

            <div class="form-group">
                <label class = "control-label big-label" >Body
                </label>
               <input type="file" id="song" name="song" class="form-control" ng-model="Song"
                        ng-required="true" ng-class="{'input-validation-error': fieldErrors.music }" />

                <a ng-click="addmusicurl()" ng-show="showaddinput"><i class="fa  fa-link i-extra-small-box i-rounded i-bordered" uib-tooltip="paste song url" tooltip-placement="top" tooltip-trigger="mouseenter"></i></a>

                        <span ng-show="fieldErrors.music" class="field-validation-error white-space-pre">[[fieldErrors.music]]</span>
            </div>

            <div class="form-group">
                <label class = "control-label big-label" >Body
                </label>
               <input type="file" id="image1" name="image2" class="form-control" ng-model="Image1"
                        ng-required="true" ng-class="{'input-validation-error': fieldErrors.image1 }" />

                <a ng-click="addextraimage()" ng-show="showaddextraimage"><i class="fa  fa-plus-square i-extra-small-box i-rounded i-bordered" uib-tooltip="paste song url" tooltip-placement="top" tooltip-trigger="mouseenter"></i></a>
                
                        <span ng-show="fieldErrors.image1" class="field-validation-error white-space-pre">[[fieldErrors.image1]]</span>
            </div>

            <div class="form-group">
                <label class = "control-label big-label" >Publisher
                </label>
                    <select id="gender" name="gender" ng-model="Admin2.gender" class="form-control width-80">
                    <option value="">--- Select Publishers ---</option>
                    <option value="Male" ><input type="checkbox" name=""></option>
                    <option value="FeMale" ><input type="checkbox" name=""></option>
                    </select>  
            </div>



             <div class="form-group margin-four-bottom text-right margin-four-bottom">
                    <button class="btn btn-default btn-medium" ui-sref="profile.basic" ng-disabled="creating">Cancel</button>
                    <button type="submit" class="btn btn-medium btn-black no-margin-bottom" ">
                        Create
                        <img ng-show="creating" class="display-inline-block remain remain2 loading loader" src="/uploads/images/theme/ajax_loader.gif">
                    </button>
            </div>
        </div>
    </form>
</div>
<!--Return Section-->
<div class="col-md-12">
    <hr />
    <button class="btn btn-default btn-medium"  ui-sref="postcategory"><i class="fa fa-angle-left"></i>Back to List</button>
</div>
