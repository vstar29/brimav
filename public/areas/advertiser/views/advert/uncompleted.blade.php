
<!-- List All UnCompleted Adverts -->
<div class="col-md-12">
<div class=" ">
    <button class="btn btn-medium btn-create" ng-click="selectType()"> Create Advert<i class="fa fa-plus"></i></button>
</div>


    <div class="table-responsive">
        <table class="table table-hover margin-three">
            <thead>
                <tr class="sortable ">
                
                    <th ng-click="sort('date')">
                        <label>Date</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='date'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='date'"></span>
                    </th>
                    <th ng-click="sort('title')">
                       <label>Title</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='title'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey != 'title'"></span>
                    </th>


                    <th ng-click="sort('amount')">
                          <label>Total Amount(N)</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='amount'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='amount'"></span>
                    </th>
                    <th ng-click="sort('status')">
                          <label>Status</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='status'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='status'"></span>
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="UnconfirmedAdvert in filteredItems = (UnconfirmedAdvert | orderBy:sortKey:!reverse |filter:SearchText |itemsPerPage:10)">
               
                    <td>[[UnconfirmedAdvert.created_at | amDateFormat:'MMM Do YYYY']]</td>
                    <td>[[UnconfirmedAdvert.title]]</td>
                    <td>[[UnconfirmedAdvert.totalamount]]</td>
                    <td>[[UnconfirmedAdvert.status]]</td>
                    <td class="text-right">
                    
                        <a ui-sref="advertlinks({id:UnconfirmedAdvert.id})">
                            <i class="fa fa-chain i-extra-small-box i-rounded i-bordered" uib-tooltip="Get Links" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                        
                        <a ng-if="UnconfirmedAdvert.status == 'UNCONFIRMED'" ng-click="confirmadvert(UnconfirmedAdvert.id)">
                            <i class="fa fa-check i-extra-small-box i-rounded i-bordered" uib-tooltip="Confirm Advert" tooltip-placement="bottom" tooltip-trigger="mouseenter"></i>
                        </a>

                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Unconfirmed advert found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>



