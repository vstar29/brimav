<div class="wow fadeIn col-md-12" data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="save()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>



            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Username</label>
                <div class="col-md-9">
               
                    <input type="text" id="username" maxlength="15" name="username" class="form-control width-80 margin-one" ng-model="Advertiser2.user.username"
                           ng-class="{'input-validation-error': fieldErrors.username }" />
                    <span ng-show="fieldErrors.username" class="field-validation-error remain2 white-space-pre">[[fieldErrors.username]]</span>
                    
                </div>
            </div>
           

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Email</label>
                <div class="col-md-9">
                    <input type="email" id="email" name="email" class="form-control width-80" ng-model="Advertiser2.user.email"
                           ng-class="{'input-validation-error': fieldErrors.email }" />
                    <span ng-show="fieldErrors.email" class="field-validation-error remain2 white-space-pre">[[fieldErrors.email]]</span>
                </div>
            </div>


            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Phone Number</label>
                <div class="col-md-9">
                    <input type="tel" id="phonenumber" name="phonenumber" class="form-control width-80" ng-model="Advertiser2.user.phonenumber"
                           ng-class="{'input-validation-error': fieldErrors.phonenumber }" />
                    <span ng-show="fieldErrors.phonenumber" class="field-validation-error remain2 white-space-pre">[[fieldErrors.phonenumber]]</span>
                </div>
            </div>

            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1"><i class="fa fa-facebook small-icon "></i></label>
                <div class="col-md-9">
                    <input type="text" id="fb" maxlength="18" name="fb" placeholder="facebook username" class="form-control width-80" ng-model="Advertiser2.facebook"
                           ng-class="{'input-validation-error': fieldErrors.facebook }" />
                    <span ng-show="fieldErrors.facebook" class="field-validation-error remain2 white-space-pre">[[fieldErrors.facebook]]</span>
                </div>
            </div>

            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1"><i class="fa fa-twitter small-icon "></i></label>
                <div class="col-md-9">
                    <input type="text" maxlength="18" placeholder="twitter username" id="twitter" name="twitter" class="form-control width-80" ng-model="Advertiser2.twitter"
                           ng-class="{'input-validation-error': fieldErrors.twitter }" />
                    <span ng-show="fieldErrors.twitter" class="field-validation-error remain2 white-space-pre">[[fieldErrors.twitter]]</span>
                </div>
            </div>


            <div class="form-group text-right margin-four-bottom">
                <button class="btn btn-default btn-medium" ui-sref="profile.basic" ng-disabled="editing">Cancel</button>
                <button type="submit" class="btn btn-medium btn-adgold no-margin-bottom">
                    Save
                     <img ng-show="editing" class="display-inline-block remain remain2 loading"  src="/uploads/images/theme/ajax_loader.gif">
                    
                </button>
            </div>
        </div>
    </form>
</div>


