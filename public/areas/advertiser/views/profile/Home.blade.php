
<div id="generalContent" class="ng-hide ng-cloak  margin-top-three" ng-show="!loadingView">
    <div id="generalAvertiserInfo" class="wow fadeIn col-md-9 pull-right div-inbetween margin-one-bottom" data-wow-duration="300ms">  
    </div>


    <!-- My Tab-->
     <h3 class=" col-md-10 margin-two center-col">[[InnerHeading]]</h3>
    <div class="col-md-12 my-tab ng-cloak">
        <div class="col-md-3  ">
            <div class="box center-adjust">
                <div class="advertiserprofile-user-img center-col hidden-sm-down">
    
                   <label uib-tooltip="change" style="cursor: pointer;" tooltip-placement="left" tooltip-trigger="mouseenter"   for="avatar"><img class="advertiserprofile-user-img  center-adjust " ng-src="[[stepsModel=='' && Picture ||stepsModel]]"/>
                     </label>
                    <input type="file" accept="image/*" onchange="angular.element(this).scope().imageUpload(event)" name="avatar" id="avatar" class="hiddeninput"> 
                     
                </div>
               
               
                <ul class="list-group list-group-unbordered extradetails">
                    
                    <li class="list-group-item">
                      <b><i class="fa  fa-mobile small-icon "></i></b> <a class="pull-right">[[Advertiser.user.phonenumber]]</a>
                    </li>
                    <li class="list-group-item">
                      <b><i class="fa fa-facebook small-icon "></i></b> <a class="pull-right" href="http://facebook.com/[[Advertiser.facebook]]">[[Advertiser.facebook]]</a>
                    </li>
                    <li class="list-group-item">
                      <b><i class="fa fa-twitter small-icon "></i></b> <a class="pull-right" href="http://twitter.com/[[Advertiser.twitter]]">[[Advertiser.twitter]]</a>
                    </li>
              </ul>

             </div>
        </div>

        <div class="tab-style3">
        <div class="col-md-8 col-sm-12">
            <!-- tab navigation -->
            <ul class="nav nav-tabs nav-tabs-light text-left ">
                <li ng-class="{'active': activeView == 'basic'}"  ><a ui-sref="profile.basic"><i class="fa fa-home"></i></a></li>
                
                <li ng-class="{'active': activeView == 'password'}"><a ui-sref="profile.changepassword">Change Password</a></li>
            </ul>
        </div>
        <div class=" col-md-9 tab-content padding-three ">
            <!-- Nested View Loading Spinner -->
            <div class="nestedLoadingHolder centerAbsolute ng-hide" ng-show="loadingNestedView">
                <div class="spinner spinner2">
                     <img src="/uploads/images/theme/ajax_loader.gif" style="width: 70px; height: 70px;" />
                </div>
            </div>
            <!-- End Loading Spinner-->
            <div class="row">
                <div ui-view class="tab-pane ng-cloak center-col fade in active col-md-9 col-sm-12"></div>
            </div>
                    
        </div>
    </div>
    </div>
</div>
    