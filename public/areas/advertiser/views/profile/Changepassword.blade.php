<div class="wow fadeIn col-md-12" data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="change()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>

            <div class="form-group margin-four-bottom">
                <label class="control-label col-md-4 text-left font-14 gray-text font-weight-500 letter-spacing-1">Old password</label>
                <div class="col-md-8">
                    <input type="password" id="Currentpassword" name="Currentpassword" class="form-control width-80 input-round" ng-model="Currentpassword" ng-class="{'input-validation-error': fieldErrors.current_password }"
                           >
                    <span ng-show="fieldErrors.current_password" class="field-validation-error remain2 white-space-pre">[[fieldErrors.current_password]]</span>
                </div>
            </div>

            <div class="form-group margin-four-bottom">
                <label class="control-label col-md-4 text-left font-14 gray-text font-weight-500 letter-spacing-1">New password</label>
                <div class="col-md-8">
                    <input type="password" id="password" name="password" class="input-round form-control width-80" ng-model="Password"
                           ng-class="{'input-validation-error': fieldErrors.password_confirmation }" />
                    <span ng-show="fieldErrors.password" class="field-validation-error remain2 white-space-pre">[[fieldErrors.password]]</span>
                </div>
            </div>

            <div class="form-group margin-four-bottom">
               <label class="control-label col-md-4 text-left font-14 gray-text font-weight-500 letter-spacing-1">Confirm password </label>

                <div class="col-md-8">
                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control width-80 input-round" ng-model="Confirmpassword"
                           ng-class="{'input-validation-error': fieldErrors.password_confirmation }" />
                    <span ng-show="fieldErrors.password_confirmation" class="field-validation-error remain2 white-space-pre">[[fieldErrors.password_confirmation]]</span>

                </div>
            </div>
            <div class="form-group margin-four-bottom text-right margin-four-bottom">
                    <button class="btn btn-default btn-medium" ui-sref="profile.basic" ng-disabled="editing">Cancel</button>
                    <button type="submit" class="btn btn-medium btn-adgold no-margin-bottom">
                        Save
                        <img ng-show="editing" class="display-inline-block remain remain2 loading"  src="/uploads/images/theme/ajax_loader.gif">
                    </button>
            </div>
        </div>
    </form>
</div>

