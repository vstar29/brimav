
<div class="wow fadeIn ng-cloak col-md-12" data-wow-duration="300ms" style="overflow:auto;">
    <div class="col-md-12 no-padding margin-one border-bottom-light padding-bottom-05">
        <span class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
           <label>Username :</label>
        </span>
        <span class="col-md-9 no-padding-left letter-spacing-1 font-14 black-text">
            [[Advertiser.user.username]]
        </span>
    </div>



    <div class="col-md-12 no-padding margin-one border-bottom-light padding-bottom-05">
        <span class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
           <label>Email :</label>
        </span>
        <span class="col-md-9 no-padding-left letter-spacing-1 font-14 black-text">
            [[Advertiser.user.email]]
        </span>
    </div>


    <div class="col-md-12 no-padding margin-one border-bottom-light padding-bottom-05">
        <span class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
            <label>Phone number :</label>
        </span>
        <span class="col-md-9 no-padding-left letter-spacing-1 font-14 black-text">
            [[Advertiser.user.phonenumber]]
        </span>
    </div>

</div>
<div class="col-md-12 text-right">
    <button class="btn btn-default btn-medium" ui-sref="profile.editbasic"><i class="fa fa-pencil"></i>Modify</button>
</div>

