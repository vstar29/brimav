<div id="generalContent" class="ng-hide ng-cloak col-md-12 dashboard-home  margin-top-three" ng-show="!loadingView">

	<div class="col-md-8 margin-top-three" style="margin-top: 30px;">

		<h5 ng-if="Adverts.length > 1">Hello <span ng-controller="AdvertiserShareController"> [[Advertiser.user.username]]</span>, you have [[Adverts.length]] unpaid adverts</h5>


		<h5 ng-if="Adverts.length == 1">Hello<span ng-controller="AdvertiserShareController"> [[Advertiser.user.username]]</span>, you have [[Adverts.length]] unpaid advert</h5>
		
		<div ng-repeat="Advert in Adverts"  class="col-md-4 gray-round-border  margin-three no-padding" style="margin-top: 30px !important;">
		
			
			<div class="col-md-12 margin-fifteenneg">
				<label class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
		           <label>Date :</label>
		        </label>
		        <label class=" no-padding-left letter-spacing-1 font-14 black-text">
		            [[Advert.created_at | amDateFormat:'MMM D']]
		        </label>
	        </div>
	       
	        <div class="col-md-12 margin-fifteenneg">
		        <label class="col-md-3 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
		           <label>Title :</label>
		        </label>
		        <label class=" no-padding-left letter-spacing-1 font-14 black-text">
		            [[Advert.title |limitTo:22]]
		        </label>
	        </div>

	         <div class="col-md-12 margin-fifteenneg">
		        <label class="col-md-4 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
		           <label>Amount:</label>
		        </label>
		        <label class=" green no-padding-left letter-spacing-1 font-14 ">
		            [[Advert.totalamount  |currency: " ":0]] N
		        </label>
	        </div>

	        <div class="col-md-12 margin-fifteenneg">
		        <label class="col-md-4 no-padding-left-right font-weight-500 font-14 gray-text letter-spacing-1">
		           <label>Status:</label>
		        </label>
		        <label class=" no-padding-left letter-spacing-1 font-14 black-text">
		            Not paid
		        </label>
	        </div>
	        <div class=" col-md-12 fade-header-icon-bg" style="height: 22px;">
				<a title="Delete" ng-click="delete(Advert.id)" class="pointer pull-left"><i class="fa fa-times "></i></a>
				<a title="Continue" ng-click="go(Advert.id,Advert.type)" class=" pointer pull-right"><i class="fa fa-arrow-right"></i></a>
			</div>

		</div>

		<div ng-if="Adverts.length == 0" class="col-md-8 welcome-guide">
	
		<h4 class="text-center" > Brimav Guide </h4>
			
			<p> Brimav digital advertising and promotion  is simple and automated, simply create an advert, advert can be of type banner, music promotion, post and others, select websites on which you wish to place your adverts and make payment.</p>
			<p> Publisher place your advert on their websites and return the link to your promoted content </p>

			<p>You get mailed when a publisher return link to your advert, login to your dashboard, click on adverts, and view the links of the coresponding adverts </p>

			<p>Verify the link and click on confirm so that the publisher can be paid</p>


			<p> If a Publisher failed to publish your adverts, you get refunded within 24hrs.</p>

			<p>Advert links that are not confirm by Advertiser is automatically confirm within 48hrs.</p>

			<p>Live chart is avallable Mon - Friday (8:00 AM -8:00PM). Support mail: <span class='green'>support@brimav.com</span></p>

			

		</div>
	

	</div>

	<div class="col-md-4  margin-top-ten">
		<button ng-click="selectType()" class="btn-default pull-right btn btn-large button btn-round">CREATE ADVERT</button>

	</div>


</div>