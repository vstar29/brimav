<!Doctype html>
<html class="no-js" data-ng-app="@yield('module')" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Brimav - Designed by Vacvi">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="vstar" content="Vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
        
        <!--styles and scripts>-->

      

        <link rel="stylesheet" type="text/css" href="{{elixir('css/templatestyle.css')}}">
        
       <script type="text/javascript" src="{{ elixir('scripts/templatescripts.js')}}"></script>

        <script type="text/javascript" src="{{ elixir('scripts/angularjs.js')}}"></script>

        <script src="{{ elixir('scripts/angularui.js')}}"></script>

        <script type="text/javascript" src="{{ elixir('scripts/toastscript.js')}}"></script>

        <script type="text/javascript" src="areas/advertiser/app/App.js"></script>
        <script type="text/javascript" src="areas/advertiser/app/controllers/ShareController.js"></script>
        <script type="text/javascript" src="areas/advertiser/app/services/AdvertiserShareService.js"></script>
        <script type="text/javascript" src="areas/advertiser/app/controllers/AdvertController.js"></script>
        <script type="text/javascript" src="areas/advertiser/app/services/AdvertiserAdvertService.js"></script>


   
    </head>
    <body class="ng-cloak">
        <!-- navigation panel -->
        
        
       <div id="printpdf" class="wow ng-cloak fadeIn col-md-8 center-col content-top-margin margin-four" data-wow-duration="300ms" ng-class="{'hide':!printon}">

            <div class="col-md-12 margin-four-bottom ">
                <div class="col-md-5 pull-left" >
                    <img title="adgold logo" src="uploads/images/logo/adgoldcolorl.png" style="max-width:30%" />
                    
                </div>

                <div class="pull-right">
                    <span class="display-block black-text "></span>
                    <span class="display-block black-text">Osogbo, Nigeria</span>
                    <span class="display-block black-text">+2347031840158</span>
                </div>
            </div>

            <span class="black-text" style="font-size:15px ;font-weight:400">Receipt # :[[PrintDetails.receiptnumber]]</span> 
            <span class="black-text pull-right" style="font-weight: 400;font-size:15px">Date: [[PrintDetails.date]]</span>
            <hr class="bg-adgold" />
            <div class="col-md-12 margin-four" >
                <div class="col-md-5 form-group">
                    <label class="adgold" style="font-weight:450;font-size:15px">BILL TO:</label>
                    <span class="display-block black-text ">[[PrintDetails.usernmae]]</span>
                    <span class="display-block black-text ">[[PrintDetails.city]], [[PrintDetails.country]]</span>
                    <span class="display-block black-text ">P[[PrintDetails.phone]]</span>

                </div>

                <div class="pull-right">
                    <label class="adgold" style="font-weight :450;font-size:15px">PAYMENT DETAILS:</label>
                    <span class="display-block black-text ">[[PrintDetails.cardtype]] ##########[[PrintDetails.cardnumber]]</span>
                    <span class="display-block black-text green ">Paid: [[PrintDetails.amount|currency:"":0]] [[PrintDetails.currency]]</span>
                </div>
                
            </div>

            <hr class="bg-adgold col-md-12 " />
            <div class="col-md-10 margin-four" style="margin-left: 18px">

                <label class="adgold" style="font-weight :450;font-size:15px">ITEM:</label>
                

            
            <label  class="pull-right adgold" style="font-weight: 450;font-size:15px">Digital Advert</label>
                <ul style="list-style-type:none">
                    <li><span>No of sites</span><span class="pull-right">[[PrintDetails.websiteno]]</span></li>
                    <li><span>Amount</span><span class="pull-right">[[PrintDetails.amount|currency:"":0]] [[PrintDetails.currency]]</span></li>
                    <li><span>Discount</span><span class="pull-right">[[PrintDetails.discount|currency:"":0]] [[PrintDetails.currency]]</span></li>
                    <li><span>Total</span><span class="pull-right green">[[PrintDetails.amount|currency:"":0]] [[PrintDetails.currency]]</span></li>
                </ul>
                    
            </div>

        </div>


    </body>
</html>