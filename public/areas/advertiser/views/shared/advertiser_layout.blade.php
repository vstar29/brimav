<!Doctype html>
<html class="no-js" data-ng-app="@yield('module')" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
       <meta name="theme-color" content="#1e313e"  />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="72x72" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="114x114" href="/uploads/images/logo/favicon.ico">
        <!--styles and scripts>-->
         <style>
            .ng-cloak
            {
                display: none !important;
            }

        </style>

        
         
        <link rel="stylesheet" type="text/css" href="/css/templatestyle.css">
      

        

    <!--End styles and scripts>-->


        
       
    </div>

        

    </head>
    <body class="ng-cloak bg-body">
        <!-- navigation panel -->
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-white nav-dark-transparent bg-adgold " role="navigation">
            <div class="container">
                <div class="row">

                    
                    <!-- end user  -->
                    <!-- logo -->
                    <div class="col-md-2 pull-left logo-responsive"><a target="_self" class="logo-light" href="https://brimav.com"><img alt="" src="/uploads/images/logo/brimavwhite.png" class="logo" /></a><a class="logo-dark" target="_self" href="https://brimav.com"><img alt="" src="/uploads/images/logo/brimavcolor.png" class="logo" /></a></div>
                    <!-- end logo -->
                    <!--  user menu  -->
                    <div class="col-md-2 hidden-sm-down fade-header-icon-bg no-padding-left search-cart-header pull-right">
                        
                        <div class="top-cart">
                            <!-- user menu -->
                            <a class="shopping-cart">
                                <i class="fa  fa-user"></i>
                                <div class="subtitle"></div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- user menu content -->
                            <div ng-controller="AdvertiserShareController" class="ng-cloak cart-content ng-cloak">
                                <ul class="cart-list">
                                    <li>
                                        
                                        <div class="pull-right">
                                            <span class="username text-uppercase">H!  [[Advertiser.user.username]]</span><br/>
                                            <span class="role">ADVERTISER</span>
                                        </div>
                                            <img style=" width:70px;height:70px"  class="pull-left responsive photo-wrapper"  alt="pics" ng-src="[[Avatar]]">
   
                                    </li>
                                </ul>
                                
                                <p class="buttons">
                                    <a href="advertiser/profile" target="_self" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr" ng-class="{'active':profilePage}">Profile</a>
                                    <a href="api/auth/logout" target="_self" class="btn-danger btn btn-very-small  no-margin-bottom margin-seven no-margin-right pull-right">Signout</a>

                            
                                </p>
                            </div>
                        </div>
                            <!-- end user menu-->

                    </div>
                    <!-- end user  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right ">
                        <button id="toggleside" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#side"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right menu-mobile">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="hidden-sm-down fade-header-icon-bg nav navbar-nav navbar-right panel-group">
                               


                                <li class="dropdown" ng-controller="AdvertiserShareController"><a ng-click="markRead()" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell "></i><span ng-if="Notifications.length>0" class="notify_count">[[Notifications.length]] </span></a>
                                     
                                    
                                         <ul style="padding: 10px;background: rgb(0, 0, 0);"  class="dropdown-menu">
                                         <li style="text-transform: none;color: white;text-align: center;" ng-if="Notifications.length==0">No new notification</li>
                                             
                                            <li style="border-bottom: solid 1px rgba(185, 185, 185, 0.11);" ng-repeat="notification in Notifications">
                                                <a target="_self" style="text-transform: capitalize;" href ="[[notification.advert_link]]"  class="dropdown-item"> [[notification.content]] <span style="color: #3c763d;" class="pull-right">[[notification.created_at|amTimeAgo]]</span>
                                                </a>

                                            </li>
                                        
                                         </ul>
                                     
                                </li>
                            


                                <!-- end menu item -->
                               
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>
        <!--end navigation panel --> 

        <!--side bar nav-->

            <nav class="sidebar hidden-sm-down navbar-fixed-top  " role="navigation">
            <div class="container">
                <div class="row">

                    
                    
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse" id="side">
                            <ul style="    margin-top: 90px;" id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->
                               
                                <li ><a href="advertiser/home" ng-class="{'active':homePage}" target="_self"><i class="fa  fa-dedent"></i> Dashboard </a></li>

    
                                <li><a href="advertiser/advert" ng-class="{'active':advertPage && advertview}" target="_self"><i class="fa fa-bullhorn"></i> Adverts </a></li>

                                 <li class=""><a href="advertiser/profile" ng-class="{'active':profilePage}" target="_self"><i class="fa fa-user"></i> Profile </a></li>

                                  <li class=""><a href="api/auth/logout"  target="_self"><i class="fa fa-sign-out"></i> Logout </a></li>


                            


                                <!-- end menu item -->
                               
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
            <div class="position-absolute" style="bottom: 10px;left:12px">
                 <span class="text-lower"> Copyright © Vacvi  <?php echo date('Y'); ?></span>          
            </div>
        </nav>

        <!--End side bar nav-->

<!-- head section -->
    <section  ng-hide="loadingView" style="padding-top: 0px !important;">
       

        <!-- content section -->
        <section class="wow  margin-two slideInRight fadeIn padding-one" style="margin-top: 7% !important;">
            <div class="container">
                <div class="row">
                    <!-- content  -->
                    <div class=" content col-md-11 ng-cloak ">
                        @yield('content')
                    </div>
                </div>
           </div> 
        </section>
        <!-- end content section -->
        
        
    </section>


    

    <!-- footer -->
    <footer ng-hide="loadingView"  class=" bg-gray wow fadeIn border-fade">
             
        <div  class="container-fluid  footer-bottom">
                <div class="container">
                    <div class="10">
                       

                        <div class="col-md-5 col-sm-4 col-xs-12 hidden-sm-down copyright text-center letter-spacing-1 xs-text-center xs-margin-bottom-one">
                            <a href="https://www.simplepay.ng"><img style="    width: 140px;height: 40px;" src="/uploads/images/theme/logo-checkout.png"></a>
                        </div>
                        
                        <!-- end copyright -->
                        <!-- logo -->
                        <div class="col-md-5 pull-right  col-sm-4 col-xs-12 copyright text-right xs-text-center">

                            <a href="https://vacvi.com" target="_blank"><img style="width: 40%;" src="/uploads/images/logo/vacvi-logo.png" alt="vacvi" /></a>
                        </div>
                        <!-- end logo -->
                    </div>
                </div>
            </div>
          
   
        </footer>
        <!-- end content section -->

        <!-- footer -->
       
         <!-- end footer -->

        <!-- notification-->
    <toast></toast>
    <!-- end notification-->


     <!-- View Loading Spinner -->
    <div ng-show="loadingView" class="spinner centerAbsolute">
        <img src="/uploads/images/theme/loadingcolor.gif">
    </div>
    <!-- End Loading Spinner-->



    <!-- Modal Loading Spinner -->
    <div id="loadingModalHolder" class="ng-hide">
        <div class="loadingModal modal-backdrop in">
            <div class="spinner">
                 <img src="/uploads/images/theme/loadingwhite.gif">
            </div>
        </div>
    </div>
    <!-- End Modal Loading Spinner--> 

        <!--SCRIPTS-->
          <script src="https://checkout.simplepay.ng/simplepay.js"></script>
        
         <script type="text/javascript" src="/scripts/brimav_script.js"></script>
        <script src="/scripts/angularui.js"></script>
        <script type="text/javascript" src="/areas/advertiser/app/advertiserscripts.js"></script>

         <script type="text/javascript" src="/areas/advertiser/app/advertiser_templates.js"></script>
        
          <!--SCRIPTS-->

          <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/588b53a3bab87e66427d107b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

jQuery('#toggleside').click(function()
{
  
    jQuery('.sidebar').toggleClass('hidden-sm-down');
});
</script>
<!--End of Tawk.to Script-->
   


    </body>
</html>

