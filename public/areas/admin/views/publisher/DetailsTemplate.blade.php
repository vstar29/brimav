
<!-- Publisher Details -->
<div class="col-md-12 no-float center-block">
    <!-- Basic Details -->
    <div class="col-md-12 wow slideInDown pull-left" data-wow-duration="300ms" style="overflow:auto;">
        <div class="dl-horizontal col-md-3 pull-right">
            <img  src="[[Picture]]" style="width:200px; height:160px; border:solid 1px #65432A;"  />
            
            <ul class="nav profilepublisher margin">
                <li ng-class="{'active': detailsView }"  ><a ui-sref="publisherdetails"><i class="fa fa-angle-left"></i>BASIC DETAILS</a></li>

                <li ng-class="{'active': activeView=='account'}"><a ui-sref="publisherdetails.account"><i class="fa fa-angle-left"></i>ACCOUNT DETAILS</a></li>

                <li ng-class="{'active': activeView == 'payments'}"><a ui-sref="publisherdetails.payments"><i class="fa fa-angle-left"></i>PAYMENTS DETAILS</a></li>
            </ul>
        </div>
       
        <div class="col-md-9">
            <div ui-view> 
<div class="col-md-8">
    <span class="text-uppercase margin-bottom-seven black-text letter-spacing-2 display-block font-weight-600 text-myExtra-large padding-one">
                   BASIC DETAILS
                </span>

                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                         <label>Username</label>
                    </span>
                    <span class="col-md-8 no-padding-left letter-spacing-1">
                        [[Publisherbasicdetails.username]]
                    </span>
                </div>
                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                         <label>Email</label>
                    </span>
                    <span class="col-md-8 no-padding-left letter-spacing-1">
                        [[Publisherbasicdetails.email]]
                    </span>
                </div>

                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                         <label>Prize</label>
                    </span>
                    <span class="col-md-8 no-padding-left letter-spacing-1">
                        [[Publisheraccountdetails.prize |currency:'N':0]]
                    </span>
                </div>

                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                        <label>Phone Number</label>
                    </span>
                    <span class="col-md-8 no-padding-left letter-spacing-1">
                        [[Publisherbasicdetails.phonenumber]]
                    </span>
                </div>

              
                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                        <label>Website Url</label>
                    </span>
                    <a href="http://[[Publisheraccountdetails.websiteurl]]" class="col-md-8 no-padding-left letter-spacing-1">
                        [[Publisheraccountdetails.websitename]]
                    </a>
                </div>

                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                        <label>Status</label>
                    </span>
                    <span class="col-md-8 no-padding-left letter-spacing-1">
                        <span ng-class="{'green':Publisherbasicdetails.status == 'ACTIVE', 'blue':Publisherbasicdetails.status == 'PENDING', 'red':Publisherbasicdetails.status == 'INACTIVE'}">
                            [[Publisherbasicdetails.status]]
                        </span>
                    </span>
                </div>

                <div class="col-md-12 no-padding">
                    <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                       <label>Join On</label>
                    </span>
                    <span class="col-md-8 no-padding-left letter-spacing-1">
                        [[Publisherbasicdetails.created_at | amDateFormat:'MMMM Do YYYY']]
                    </span>
                </div>

            </div>

</div>
        </div>
    </div>
</div>
           

<!--Return Section-->
<div class="col-md-12">
    <hr />
    <button class="btn btn-default btn-medium" ui-sref="[[returnState]]"><i class="fa fa-angle-left"></i>[[returnStatelabel]]</button>
</div>


