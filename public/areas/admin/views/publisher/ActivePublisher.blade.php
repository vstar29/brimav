
<!-- Loading Image -->
<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>

<!-- List of All Publishers -->
<div class="col-md-12 ">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('username')">
                        <label>UserName</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='username'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='title'"></span>
                    </th>
                   

                    <th ng-click="sort('email')">
                          <label>Email</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='email'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='email'"></span>
                    </th>

                    <th ng-click="sort('status')">
                          <label>Status</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='status'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='status'"></span>
                    </th>

                    <th ng-click="sort('phonenumber')">
                          <label>Phone Number</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='phonenumber' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='phonenumber' && sortKey != ''"></span>
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="ActivePublisher in filteredItems = (ActivePublishers | orderBy:sortKey:reverse |filter:searchText |itemsPerPage:10)">
                    <td ng-class="{'inactive':ActivePublisher.status == 'INACTIVE'}">[[ActivePublisher.username]]</td>
                    <td ng-class="{'inactive':ActivePublisher.status == 'INACTIVE'}">[[ActivePublisher.email]]</td>
                    <td ng-class="{'inactive':ActivePublisher.status == 'INACTIVE'}">[[ActivePublisher.status]]</td>
                    <td ng-class="{'inactive':ActivePublisher.status == 'INACTIVE'}">[[ActivePublisher.phonenumber]]</td>

                    <td class="text-right">
                        <a ui-sref="publisherdetails({id:ActivePublisher.userable_id})">
                            <i class="fa fa-eye i-extra-small-box i-rounded i-bordered" uib-tooltip="View" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="ActivePublisher.status == 'ACTIVE'" ng-click="deactivatePublisher(ActivePublisher.id, ActivePublisher.username,'active')">
                            <i class="fa fa-ban i-extra-small-box i-rounded i-bordered" uib-tooltip="Deactivate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="ActivePublisher.status == 'INACTIVE'" ng-click="activatePublisher(ActivePublisher.id, ActivePublisher.username,'active')">
                            <i class="fa fa-check i-extra-small-box i-rounded i-bordered" uib-tooltip="Activate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                        <a  ng-click="deletePublisher(ActivePublisher.id, ActivePublisher.username,'active')">
                                <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Delete" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                            </a>
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Active Publishers found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>



