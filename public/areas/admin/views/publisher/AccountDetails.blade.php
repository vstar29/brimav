 
    <div class="col-md-8">
    <span class="text-uppercase margin-bottom-seven black-text letter-spacing-2 display-block font-weight-600 text-myExtra-large padding-one">
                   ACCOUNT DETAILS
                </span>

        <div class="col-md-12 no-padding">
            <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                <label>Banker</label>
            </span>
            <span class="col-md-8 no-padding-left letter-spacing-1">
                [[Publisheraccountdetails.banker]]
            </span>
        </div>

        <div class="col-md-12 no-padding">
            <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                <label>Account Name</label>
            </span>
            <span class="col-md-8 no-padding-left letter-spacing-1">
                [[Publisheraccountdetails.accountname]]
            </span>
        </div>

        <div class="col-md-12 no-padding">
            <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                <label>Account Number</label>
            </span>
            <span class="col-md-8 no-padding-left letter-spacing-1">
                [[Publisheraccountdetails.accountnumber]]
            </span>
        </div>

        <div class="col-md-12 no-padding">
            <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                <label>Balance</label>
            </span>  

        <div class="panel-group toggles">

                <div role="tablist" id="type1-headingOne" class="panel-heading">
                    <span class="col-md-8 no-padding-left letter-spacing-1"> [[Publisherbalance.balance |currency:"N":0]]<i ng-click="toggle()" class="pull-right col-md-9" ng-class="{'fa fa-plus':!reverse, 'fa fa-minus':reverse}"></i></span>
                </div>
                <div ui-view>
                    
                </div>
        </div>
    </div>
    </div>

