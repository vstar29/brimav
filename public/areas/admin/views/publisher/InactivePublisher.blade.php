
<!-- Loading Image -->
<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>

<!-- List of All Publishers -->
<div class="col-md-12 ng-cloak">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('username')">
                        <label>UserName</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='username'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='title'"></span>
                    </th>
                   

                    <th ng-click="sort('email')">
                          <label>Email</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='email'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='email'"></span>
                    </th>

                    <th ng-click="sort('status')">
                          <label>Status</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='status'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='status'"></span>
                    </th>

                    <th ng-click="sort('phonenumber')">
                          <label>Phone Number</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='phonenumber' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='phonenumber' && sortKey != ''"></span>
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="InActivePublisher in filteredItems = (InactivePublisher | orderBy:sortKey:reverse |filter:searchText |itemsPerPage:10)">
                    <td ng-class="{'inactive':InActivePublisher.status == 'INACTIVE'}">[[InActivePublisher.username]]</td>
                    <td ng-class="{'inactive':InActivePublisher.status == 'INACTIVE'}">[[InActivePublisher.email]]</td>
                    <td ng-class="{'inactive':InActivePublisher.status == 'INACTIVE'}">[[InActivePublisher.status]]</td>
                    <td ng-class="{'inactive':InActivePublisher.status == 'INACTIVE'}">[[InActivePublisher.phonenumber]]</td>

                    <td class="text-right">
                        <a ui-sref="publisherdetails({id:InActivePublisher.userable_id})">
                            <i class="fa fa-eye i-extra-small-box i-rounded i-bordered" uib-tooltip="View" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="InActivePublisher.status == 'ACTIVE'" ng-click="deactivatePublisher(InActivePublisher.id, InActivePublisher.username, 'inactive')">
                            <i class="fa fa-ban i-extra-small-box i-rounded i-bordered" uib-tooltip="Deactivate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="InActivePublisher.status == 'INACTIVE'" ng-click="activatePublisher(InActivePublisher.id, InActivePublisher.username,'inactive')">
                            <i class="fa fa-check i-extra-small-box i-rounded i-bordered" uib-tooltip="Activate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                        <a  ng-click="deletePublisher(InPublisher.id, InPublisher.username,'inactive')">
                                <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Delete" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                            </a>
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Active Publishers found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>



