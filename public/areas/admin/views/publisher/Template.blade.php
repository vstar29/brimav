<div id="generalContent" class="ng-hide ng-cloak  margin-top-three" ng-show="!loadingView">
<!-- Search -->

<!-- Search -->
<div class="form-group col-md-4 pull-right" style="margin-right: 16px;">
    <input type="text" ng-model="SearchText" class=" form-control input-round tablesearch pull-right" placeholder="&#xf002;">
</div>

<div class="col-md-4 pull-left " style="margin-left: 16px;">
    <select ng-model="Filterview"  class="input-round tablesearch">
    <option value="all" name="publisher.all" >ALL</option>
     <option value="active" name="publisher.active" >ACTIVE</option>  
     <option value="inactive" name="publisher.inactive" >INACTIVE</option> 
    </select>
</div>


<!-- Loading Image -->

<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" style="width: 70px; height: 70px;" />
</div>

<!-- View Completed/ Uncompleted Publisher -->


<div class="col-md-12 ng-cloak">
<div class="nestedLoadingHolder centerAbsolute ng-hide" ng-show="loadingNestedView">
	<div class="spinner spinner2">
    	<img src="/uploads/images/theme/loadingwhite.gif" style="width: 70px; height: 70px;">
    </div>
</div>
    <div ui-view></div>
</div>

</div>