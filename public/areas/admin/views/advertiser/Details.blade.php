
<!-- Advertiser Details -->
<div class="col-md-10 no-float center-block">
    <!-- Basic Details and Address -->
    <div class="col-md-12 wow slideInDown" data-wow-duration="300ms" style="overflow:auto;">
        <div class="dl-horizontal col-md-4">
            <img class="roundedimage border" style="width: 200px;
height: 200px" ng-src="[[Picture]]" />
        </div>
        <div class="col-md-8">
            <span class="text-uppercase margin-bottom-seven black-text letter-spacing-2 display-block font-weight-600 text-myExtra-large padding-one">
                [[Advertiser.username]]
            </span>


            <div class="col-md-12 no-padding">
                <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                     <label>Email</label>
                </span>
                <span class="col-md-8 no-padding-left letter-spacing-1">
                    [[Advertiser.email]]
                </span>
            </div>


            <div class="col-md-12 no-padding">
                <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                   <label>Join On</label>
                </span>
                <span class="col-md-8 no-padding-left letter-spacing-1">
                    [[Advertiser.created_at | amDateFormat:'MMMM Do YYYY']]
                </span>
            </div>


            <div class="col-md-12 no-padding">
                <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                    <label>Phone Number</label>
                </span>
                <span class="col-md-8 no-padding-left letter-spacing-1">
                    [[Advertiser.phonenumber]]
                </span>
            </div>

            <div class="col-md-12 no-padding">
                <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                    <label>Number of Adverts</label>
                </span>
                <span class="col-md-8 no-padding-left letter-spacing-1">
                    [[Advertcount]]
                </span>
            </div>

            <div class="col-md-12 no-padding">
                <span class="col-md-4 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                    <label>Status</label>
                </span>
                <span class="col-md-8 no-padding-left letter-spacing-1">
                    <span ng-class="{'green':Advertiser.status == 'ACTIVE', 'blue':Advertiser.status == 'PENDING', 'red':Advertiser.status == 'INACTIVE'}">
                        [[Advertiser.status]]
                    </span>
                </span>
            </div>


        </div>
    </div>



</div>

<!--Return Section-->
<div class="col-md-12">
    <hr />
    <button class="btn btn-default btn-medium" ui-sref="[[returnState]]"><i class="fa fa-angle-left"></i>[[returnStatelabel]]</button>
</div>


