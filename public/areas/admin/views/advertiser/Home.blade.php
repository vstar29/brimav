
<!-- Search -->
<div class="form-group pull-right col-md-4 ">
    <input type="text" ng-model="SearchText" class=" pull-right form-control input-round tablesearch" placeholder="&#xf002;">
</div>
<!-- Loading Image -->
<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>

<!-- List of All Advertisers -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('username')">
                        <label>UserName</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='username'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='username'"></span>
                    </th>
                   

                    <th ng-click="sort('email')">
                          <label>Email</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='email'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='email'"></span>
                    </th>

                    <th ng-click="sort('status')">
                          <label>Status</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='status'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='status'"></span>
                    </th>

                    <th ng-click="sort('phonenumber')">
                          <label>Phone Number</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='phonenumber' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='phonenumber' && sortKey != ''"></span>
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>

                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="Advt in filteredItems = (Advertisers | orderBy:sortKey:reverse |filter:searchAdvertiser |itemsPerPage:10)">
                    <td ng-class="{'inactive':Advt.status == 'INACTIVE'}">[[Advt.username]]</td>
                    <td ng-class="{'inactive':Advt.status == 'INACTIVE'}">[[Advt.email]]</td>
                    <td ng-class="{'inactive':Advt.status == 'INACTIVE'}">[[Advt.status]]</td>
                    <td ng-class="{'inactive':Advt.status == 'INACTIVE'}">[[Advt.phonenumber]]</td>

                    <td class="text-right">
                        <a ui-sref="viewadvertiser({username:Advt.username})">
                            <i class="fa fa-eye i-extra-small-box i-rounded i-bordered" uib-tooltip="View" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="Advt.status == 'ACTIVE'" ng-click="deactivateAdvertiser(Advt.id, Advt.username)">
                            <i class="fa fa-ban i-extra-small-box i-rounded i-bordered" uib-tooltip="Deactivate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="Advt.status == 'INACTIVE'" ng-click="activateAdvertiser(Advt.id, Advt.username)">
                            <i class="fa fa-check i-extra-small-box i-rounded i-bordered" uib-tooltip="Activate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                        <a ng-click="deleteAdvertiser(Advt.id, Advt.username)">
                            <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Activate" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Advertisers found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>