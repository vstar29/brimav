
<!-- Search -->
<div class="form-group pull-right col-md-4 ">
    <input type="text" ng-model="SearchText" class=" pull-right form-control input-round tablesearch" placeholder="&#xf002;">
</div>

<div class=" " style="margin-left: 15px;">
    <a ui-sref="create"> <button class="btn btn-medium " > Add Coupon<i class="fa fa-plus"></i></button></a>
</div>
<!-- Loading Image -->
<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>

<!-- List of All Coupon -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('name')">
                        <label>Name</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='name'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='name'"></span>
                    </th>
                   

                    <th>
                          <label>Code</label>
                        
                    </th>


                    <th ng-click="sort('percent')">
                          <label>Percent</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='percent'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='percent'"></span>
                    </th>

                    <th ng-click="sort('start_date')">
                          <label>Start Date</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='start_date' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='start_date' && sortKey != ''"></span>
                    </th>

                     <th ng-click="sort('stop_date')">
                          <label>Stop Date</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='stop_date' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='stop_date' && sortKey != ''"></span>
                    </th>

                     <th ng-click="sort('adverttype')">
                          <label>Type</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='adverttype' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='adverttype' && sortKey != ''"></span>
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>

                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="Coup in filteredItems = (Coupons | orderBy:sortKey:reverse |filter:SearchText |itemsPerPage:10)">
              
                    <td>[[Coup.name]]</td>
                    <td>[[Coup.code]]</td>
                    <td>[[Coup.percent]]</td>
                    <td>[[Coup.start 
                    | amDateFormat:'MMMM Do YYYY']]</td>
                    <td>[[Coup.stop 
                    | amDateFormat:'MMMM Do YYYY']]</td>
                    <td>[[Coup.adverttype]]</td>

                    <td class="text-right">
                        <a ui-sref="edit({id:Coup.id})">
                            <i class="fa fa-pencil i-extra-small-box i-rounded i-bordered" uib-tooltip="Edit" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                       
                        <a ng-click="deletecoupon(Coup.id)">
                            <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Delete" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Coupon found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>