<div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="edit()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Name</label>
                <div class="col-md-9 leftfifteen">
               
                    <input type="text" id="name" name="name" class="form-control width-70 margin-one" ng-model="Coupon2.name"
                           ng-class="{'input-validation-error': fieldErrors.name }" />
                    <span ng-show="fieldErrors.name" class="field-validation-error remain2 white-space-pre">[[fieldErrors.name]]</span>
                    
                </div>
            </div>
           

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Code</label>
                <div class="col-md-9 leftfifteen">
                    <input type="text" id="code" name="code" class="form-control width-70" ng-model="Coupon2.code"
                           ng-class="{'input-validation-error': fieldErrors.code }" />
                    <span ng-show="fieldErrors.code" class="field-validation-error remain2 white-space-pre">[[fieldErrors.code]]</span>
                </div>
            </div>


            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Percent</label>
                <div class="col-md-9 leftfifteen">
                    <input type="text" id="percent" name="percent" class="form-control width-70" ng-model="Coupon2.percent"
                           ng-class="{'input-validation-error': fieldErrors.percent }" />
                    <span ng-show="fieldErrors.percent" class="field-validation-error remain2 white-space-pre">[[fieldErrors.percent]]</span>
                </div>
            </div>

             <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Type</label>
                <div class="col-md-9 leftfifteen">

                    <select id="type" ng-required="true" name="type" ng-model="Coupon2.adverttype" class="form-control width-70  input-round medium-input " ng-class="{'input-validation-error': fieldErrors.adverttype }" >
                    <option value="">....Select Type..... </option>
                    <option value="music"  name="music">Music </option>
                    <option value="others" name="others">Others</option>
                </select>
                   
                </div>
            </div>

             <div class="form-group  margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Start Date</label>
                
                <div class="col-md-6 input-group">
                    <input type="date" class="form-control"  ng-model="Coupon2.start"  style="height: 40px" ng-class="{'input-validation-error': fieldErrors.start }" />
                    <span  class="input-group-addon pointer"><i class="fa fa-calendar"></i></span>   
                </div>
            </div>

            <div class="form-group  margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Stop Date</label>
                
                <div class="col-md-6 input-group">
                     <input style="height: 40px" type="date" class="form-control" ng-model="Coupon2.stop"  ng-class="{'input-validation-error': fieldErrors.stop }" />
                    <span  class="input-group-addon pointer"><i class="fa fa-calendar"></i></span>   
                </div>
            </div>


            <div class="form-group pull-right ">
                <button class="btn btn-default btn-round btn-medium" ui-sref="profile.basic" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                    Save
                   
                    <img ng-show="creating" class="loader loader-small display-inline-block ng-hide">
                </button>
            </div>
        </div>
    </form>
</div>


