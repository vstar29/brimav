@extends('areas/admin/views/shared.admin_layout')

@section('content')

<!--<script type="text/javascript" src="areas/admin/app/App.js"></script>
<script type="text/javascript" src="areas/admin/app/services/AdminShareService.js"></script>
<script type="text/javascript" src="areas/admin/app/services/AdminProfileService.js"></script>
<script type="text/javascript" src="areas/admin/app/controllers/ShareController.js"></script>
<script type="text/javascript" src="areas/admin/app/controllers/ProfileController.js"></script>-->

@section('module', 'AdminProfileModule')
<div ui-view></div>

@endsection