
<div id="generalContent" class="ng-hide ng-cloak  margin-top-three" ng-show="!loadingView">
   


    <!-- My Tab-->
    <div class="col-md-12 my-tab ng-cloak">
        
        <div class="tab-style3">
        <div class="col-md-12 col-sm-12">
            <!-- tab navigation -->
            <ul class="nav nav-tabs nav-tabs-light text-left profileslink">
                <li ng-class="{'active': activeView == 'basic'}"  ><a ui-sref="profile.basic"><i class="fa fa-home"></i></a></li>
                <li ng-class="{'active': activeView == 'password'}"><a ui-sref="profile.changepassword">Change Password</a></li>
            </ul>
        </div>
        <div class=" col-md-9 tab-content padding-three ">
            <!-- Nested View Loading Spinner -->
            <div class="nestedLoadingHolder centerAbsolute ng-hide" ng-show="loadingNestedView">
                <div class="spinner spinner2">
                     <img src="/uploads/images/theme/ajax_loader.gif" style="width: 70px; height: 70px;" />
                </div>
            </div>
            <!-- End Loading Spinner-->
            <div class="row">
                <div ui-view class="tab-pane center-col fade in active col-md-9 col-sm-12"></div>
            </div>
                    
        </div>
    </div>
    </div>
</div>
    