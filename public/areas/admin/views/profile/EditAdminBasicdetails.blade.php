<div class="wow fadeIn col-md-12" data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="save()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>



            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">UserName</label>
                <div class="col-md-9">
               
                    <input type="text" id="username" name="username" class="form-control width-80 margin-one" ng-model="Admin2.username"
                           ng-class="{'input-validation-error': fieldErrors.username }" />
                    <span ng-show="fieldErrors.username" class="field-validation-error remain2 white-space-pre">[[fieldErrors.username]]</span>
                    
                </div>
            </div>
           

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Email</label>
                <div class="col-md-9">
                    <input type="email" id="email" name="email" class="form-control width-80" ng-model="Admin2.email"
                           ng-class="{'input-validation-error': fieldErrors.email }" />
                    <span ng-show="fieldErrors.email" class="field-validation-error remain2 white-space-pre">[[fieldErrors.email]]</span>
                </div>
            </div>


            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 gray-text font-weight-500 letter-spacing-1">Phone Number</label>
                <div class="col-md-9">
                    <input type="tel" id="phonenumber" name="phonenumber" class="form-control width-80" ng-model="Admin2.phonenumber"
                           ng-class="{'input-validation-error': fieldErrors.phonenumber }" />
                    <span ng-show="fieldErrors.phonenumber" class="field-validation-error remain2 white-space-pre">[[fieldErrors.phonenumber]]</span>
                </div>
            </div>


            <div class="form-group pull-right ">
                <button class="btn btn-default btn-round btn-medium" ui-sref="profile.basic" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                    Save
                   
                    <img ng-show="editing" class="loader loader-small display-inline-block ng-hide">
                </button>
            </div>
        </div>
    </form>
</div>


