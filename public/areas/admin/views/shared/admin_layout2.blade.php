<!doctype html>
<html class="no-js" data-ng-app="AdminHomeModule"  lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <meta name="theme-color" content="#65432a" />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="72x72" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="114x114" href="/uploads/images/logo/favicon.ico">


    <!--styles and scripts>-->

        <link rel="stylesheet" type="text/css" href="{{elixir('css/templatestyle.css')}}">

        <script type="text/javascript" src="{{ elixir('scripts/brimav_script.js')}}"></script>

    
        <script type="text/javascript" src="{{ elixir('scripts/revolution.js')}}"></script>

      

        <script src="{{ elixir('scripts/angularui.js')}}"></script>


        <script type="text/javascript" src="{{ elixir('areas/admin/app/adminscripts.js')}}"></script>




    <!--End styles and scripts>-->

        

    </head>
    <body  class="ng-cloak" ng-controller="HomeController">
      
        <!-- navigation panel -->
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-white nav-border-bottom" role="navigation">
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a class="logo-light" href="/" target="_self"><img alt="" src="/uploads/images/logo/brimavwhite.png" class="logo" /></a><a class="logo-dark" href="/" target="_self"><img alt="" src="/uploads/images/logo/brimavcolor.png" class="logo" /></a></div>
                    <!-- end logo -->
                     <!-- search and user menu  -->
                    <div class="col-md-2 no-padding-left search-cart-header pull-right">
                        

                        <div class="top-cart">
                            <!-- user menu -->
                            <a class="shopping-cart">
                                <i class="fa  fa-user"></i>
                                <div class="subtitle"> Profile</div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- user menu content -->
                            <div ng-controller="AdminShareController" class="ng-cloak cart-content">
                                <ul class="cart-list">
                                    <li>
                                        
                                        <div class="pull-right">
                                            <span class="username text-uppercase">H!  [[Admin.username]]</span><br/>
                                            <span class="role">ADMIN</span>
                                        </div>
                                            <img style="width:80px;height:80px" class="pull-left"  alt="pics" src="/uploads/images/theme/unknown.jpg">
   
                                    </li>
                                </ul>
                                
                                <p class="buttons">
                                    <a href="adminprofile" target="_self" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr">Profile</a>
                                    <a href="api/auth/logout" target="_self" class="btn btn-very-small-danger no-margin-bottom margin-seven no-margin-right pull-right">Signout</a>
                                </p>
                            </div>
                        </div>
                            <!-- end user menu-->

                    </div>
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">

                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                               <!-- menu item -->
                                <li ><a href="admin/home" ng-class="{'active':homePage}" target="_self">Home </a></li>

                                <li><a href="adminpublisher" ng-class="{'active':publisherPage}" target="_self">Publishers </a></li>

                                <li><a href="adminadvert" ng-class="{'active':advertPage}" target="_self">Advert </a></li>

                                <li><a href="adminadvertiser" ng-class="{'active':adrtistPage}" target="_self">Advertisers </a></li>

                                <li><a href="adminpayment" ng-class="{'active':paymentPage}" target="_self">Payment </a></li>

                                <li><a href="admincoupon" ng-class="{'active':couponPage}" target="_self">Coupon </a></li>

                                  <li><a href="adminbannertype" ng-class="{'active':bannerTypePage}" target="_self">BannerTypes </a></li>
                                <!-- end menu item -->
                              
                                
  
                                <!-- end menu item -->
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>

           <!-- end navigation panel -->
        <!-- slider -->
         <section id="myCarousel"> 
            <div class="tp-banner-container">
                <div class="revolution-slider-full" >
                    <ul>    <!-- SLIDE 1 -->
                      
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="900" data-thumb=""  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                            <img src="/uploads/images/homeslider/mi-phyno.jpg"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" style="background-color:#6D6969">
                            <div class="slider-overlay bg-slider" style=""></div>
                            <!-- LAYERS 1 -->
                            <div class="tp-caption light_medium_30_shadowed lfb ltt tp-resizeme"
                                 data-x="center" data-hoffset="0"
                                 data-y="center" data-voffset="-50"
                                 data-speed="300"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            </div>
                            <!-- LAYERS 2 -->
                            <div class="tp-caption homeslidertextleft light_heavy_70_shadowed lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="0"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Make your Way To<br><br>Limelight With Ease
                            </div>
                            <!-- LAYERS 3 -->
                            <div class="tp-caption bg-green homeslidertextleft customin tp-resizeme rs-parallaxlevel-0"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="80"
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="yes"
                                 data-splitout="none"
                                 data-elementdelay="0.1"
                                 data-endelementdelay="0.1"
                                 data-linktoslide="next"
                                 style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                 <a href="/register"  class='largeredbtn inner-link' target="_self">Try Now</a>
                            </div>
                        </li>
                        <!-- SLIDE 2 -->
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="900" data-thumb=""  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                             <img src="/uploads/images/homeslider/signup-bg.jpg" style="background-color:#6D6969"    data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                           <div class="slider-overlay bg-slider" ></div>
                            <!-- LAYERS 1 -->
                            <div class="tp-caption light_medium_30_shadowed lfb ltt tp-resizeme"
                                 data-x="center" data-hoffset="0"
                                 data-y="center" data-voffset="-50"
                                 data-speed="600"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            </div>
                            <!-- LAYERS 2 -->
                            <div class="tp-caption homeslidertextleft light_heavy_70_shadowed lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="0"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Earn big from <br> <br> your websites 
                            </div>
                            <!-- LAYERS 3 -->
                            <div class="tp-caption homeslidertextleft customin  tp-resizeme rs-parallaxlevel-0"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="80"
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.1"
                                 data-endelementdelay="0.1"
                                 data-linktoslide="next"
                                 style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="/register"  class='largeredbtn inner-link' target="_self">Start</a>
                            </div>
                        </li>

                          <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="900" data-thumb=""  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                            <img src="/uploads/images/homeslider/mreazi.jpg"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="slider-overlay bg-slider" ></div>
                            <!-- LAYERS 1 -->
                            <div class="tp-caption light_medium_30_shadowed lfb ltt tp-resizeme"
                                 data-x="center" data-hoffset="0"
                                 data-y="center" data-voffset="-50"
                                 data-speed="400"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            </div>
                            <!-- LAYERS 2 -->
                            <div class="tp-caption light_heavy_70_shadowed lfb homeslidertextleft ltt tp-resizeme"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="0"
                                 data-speed="400"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Promote Your Songs <br><br>On Top Blogs
                            </div>
                            <!-- LAYERS 3 -->
                            <div id="about" class="tp-caption homeslidertextleft bg-dark-blue customin tp-resizeme rs-parallaxlevel-0"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="80"
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.1"
                                 data-endelementdelay="0.1"
                                 data-linktoslide="next"
                                 style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="/register"  class='largeredbtn inner-link' target="_self">Promote Now!</a>

                            </div>
                        </li>
                    </ul> 
                </div>
            </div>      
                 
        </section>
        <!-- end slider -->
        <!-- about section -->
        <section  class=" no-margin padding-three wow fadeIn bg-gray row">
                  <div class="col-md-12"> 
                        
                        <div class="col-md-5 hometextpad no-padding text-center center-col">
                           
                            <span id="how" class="text-med black_bold_32 black-text width-90 center-col  no-margin-bottom"> Brimav is a digital platform that is bridging the gap between creativity and the world by putting your creative works (Music, Contents, Videos and Others) on numerous of Nigeria, Ghana, SouthAfrica and Tanzania based websites.
                                There by helping talented individual reach their desired audience and also help bloggers to earn a living from their websites/blogs.
                            </span> 
                        </div>

                        
                    </div>
               
        </section>
        
        
        <!-- end about section -->

        <div style=" background-color: #6c4c34 !important;">


            <section class="wow fadeIn bg-gray  margin-one-bottom no-margin no-padding  border-top" style="border-top: solid 2px #65432a;">
                <div class="row col-md-8 center-col">



                    <div class="col-md-4 col-sm-6">

                        <div class="card">
                            <img class="card-img-top img-no-dither" src=" /uploads/images/homeslider/olamide.jpg" alt="reliable">
                            <div class="card-block">
                              <h3>Top Spot Guaranted</h3>
                              <span>Your Top spot is Guaranted with Smaf digital promotion</span>

                           </div>
                        </div>
                    </div>

                      <div class="col-md-4 col-sm-6">
                        <div class="card">
                            <img class="card-img-top img-no-dither" src=" /uploads/images/homeslider/reliable.jpg" alt="reliable">
                            <div class="card-block">
                              <h3>Reliable And Secured</h3>
                              <span>Secured transaction as Publisher is not paid until Advert completion</span>

                            </div>
                         </div>
                    </div>

                      <div class="col-md-4 col-sm-6">

                        <div class="card">
                            <img class="card-img-top img-no-dither" src=" /uploads/images/homeslider/mobilecompat-small.jpg" alt="responsiveness">
                            <div class="card-block">
                              <h3>Accessible Anywhere</h3>
                              <span>Our Mobile Friendliness Platform is accesible anywhere</span>

                           </div>
                        </div>
                    </div>

                  
                </div>

            </section>
            <!-- features section -->
            <section  class="wow fadeIn bg-gray  margin-one-bottom no-margin no-padding  border-top">
                <div class=" col-md-12 col-sm-12 bg-adgold padding-one text-center">
                <span class="section-title white-text">How It Works</span>
                </div>
               
                
                    <div class="col-md-11 col-sm-12 center-col">
                        <!-- tab -->
                        <div class="tab-style2">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <!-- tab navigation -->
                                    <ul class="nav nav-tabs nav-tabs-light text-left">
                                        <li class="active"><a href="#tab2_sec1" data-toggle="tab">As Artist / Advertiser</a></li>
                                        <li><a href="#tab2_sec2" data-toggle="tab">As Blogger / Publisher</a></li>
                                       
                                    </ul>
                                    <!-- end tab navigation -->
                                </div>
                            </div>
                            <!-- tab content section -->
                            <div class="tab-content" style="border:0px!important">
                                <!-- tab content -->
                                <div class="tab-pane med-text fade in active" id="tab2_sec1">
                                    <div class="row">
                                        <div class="hometextpad col-md-12 col-sm-12">
                                            <p class="black-text">With just four steps, you get your music/advert on world platform.</p>
                                            <ul style="list-style-type:disc">
                                                <li><a href="https://brimav.com/register" target="_self">Sign up</a> as Advertiser</li> 
                                                <li>Uploads your song/advert</li> 
                                                <li>Select websites you wish to promote your song on.</li>
                                                <li>Make payment</li>
                                                <li>Links to your promoted content is returned by publishers</li>
                                                <li>Confirm the link</li>
                                            </ul>
                                            <br/>
                                             <p class="black-text">No Publisher is credited until you confirm the link.</p>

                                             <p class="black-text">If a Publisher fails to promote your content , your money will be refunded.</p>
                                        </div>
                                         
                                    </div>
                                </div>
                                <!-- end tab content -->
                                <!-- tab content -->
                                <div class="tab-pane fade in" id="tab2_sec2">
                                    <div class="row">
                                        <div class=" hometextpad col-md-12 col-sm-12 ">
                                            
        
                                            <ul style="list-style-type:disc">
                                                <li><a href="https://brimav.com/register" target="_self">Sign up</a> as Publisher</li> 
                                                <li>Put content on your blog</li> 
                                                <li>Return link to the content</li>
                                                <li>You account get credited after advertiser confirm the link</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <!-- end tab content section -->
                        </div>
                        <!-- end tab --> 
                         <hr/>  
                </div>
            </section>

            

        </div>

    
        <section class="no-margin no-padding">
           
                            <!-- tab content -->
                            @yield('nothing')
                            <!-- end tab content -->
                            <!-- tab content -->
                            
        </section>
        
       
        <!-- footer -->
        <footer>
            <div id="contact" class=" bg-gray footer-top">
                <div class="container">
                    <div class="padding-three">
                        <!-- phone -->
                        <div class="col-md-4 col-sm-4 text-center"><i class="icon-phone small-icon black-text"></i><h6 class="black-text margin-two no-margin-bottom">+2347031840158</h6></div>
                        <!-- end phone -->
                        <!-- address -->
                        <div class="col-md-4 col-sm-4 text-center"><i class="icon-map-pin small-icon black-text"></i><h6 class="black-text margin-two no-margin-bottom">Lagos - Nigeria</h6></div>
                        <!-- end address -->
                        <!-- email -->
                        <div class="col-md-4 col-sm-4 text-center"><i class="icon-envelope small-icon black-text"></i><h6 class="margin-two "><a href="mailto:no-" class="black-text">info@adgold.com</a></h6></div>
                        <!-- end email -->
                    </div>
                </div>
            </div>
           
                      
        
            
            <div class="container-fluid bg-adgold footer-bottom">
                <div class="container">
                    <div class="padding-two">
                        <!-- copyright -->
                        <div class="col-md-6 col-sm-6 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one">
                           &copy; <?php echo date('Y'); ?> powered by <a class="display-inline"  href="https://vacvi.com"><img style=" margin-left: -70px" src="/uploads/images/logo/vacvi-logo-white.png/"></a> 
                        </div>
                        <!-- end copyright -->
                        <!-- logo -->
                        <div class="col-md-6 col-sm-6 col-xs-12 footer-logo text-right xs-text-center">
                            <a href="https://brimav.com"><img src="/uploads/images/logo/brimavwhite.png" alt="" /></a>
                        </div>
                        <!-- end logo -->
                    </div>
                </div>
            </div>
            <!-- scroll to top -->
            <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
            <!-- scroll to top End... -->
        </footer>
        <!-- end footer -->


<!-- notification-->
    <toast></toast>
    <!-- end notification-->

    <!-- View Loading Spinner --> 
    <div ng-show="loadingView" class="spinner centerAbsolute">
        <img src="/uploads/images/theme/loadingcolor.gif">
    </div>
    <!-- End Loading Spinner-->

    <!-- Modal Loading Spinner -->
    <div id="loadingModalHolder" class="ng-hide">
        <div class="loadingModal modal-backdrop in">
            <div class="spinner">
                 <img src="/uploads/images/theme/loadingwhite.gif">
            </div>
        </div>
    </div>
    <!-- End Modal Loading Spinner--> 

    </body>
</html>
