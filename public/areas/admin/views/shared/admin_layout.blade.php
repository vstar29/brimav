<!Doctype html>
<html class="no-js" data-ng-app="@yield('module')" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <meta name="theme-color" content="#65432a" />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="72x72" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="114x114" href="/uploads/images/logo/favicon.ico">
       <style>
            .ng-cloak
            {
            display:none !important
            }
            
       </style>

        <!--styles and scripts>-->
        
        <link rel="stylesheet" type="text/css" href="{{elixir('css/templatestyle.css')}}">

         <script type="text/javascript" src="{{ elixir('scripts/brimav_script.js')}}"></script>
        
        <script src="{{ elixir('scripts/angularui.js')}}"></script>

        <script type="text/javascript" src="{{ elixir('areas/admin/app/adminscripts.js')}}"></script>


    <!--End styles and scripts>-->

    </head>
    <body class="ng-cloak">
        <!-- navigation panel -->
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-white nav-dark-transparent " role="navigation">
            <div class="container">
            
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a class="logo-light" href="/"><img alt="" src="/uploads/images/logo/brimavwhite.png" class="logo" /></a><a target="_self" class="logo-dark" href="/"><img alt=""  src="/uploads/images/logo/brimavcolor.png" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and user menu  -->
                    <div class="col-md-2 no-padding-left search-cart-header pull-right">
                        

                        <div class="top-cart">
                            <!-- user menu -->
                            <a class="shopping-cart">
                                <i class="fa  fa-user"></i>
                                <div class="subtitle"> Profile</div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- user menu content -->
                            <div ng-controller="AdminShareController" class="ng-cloak cart-content">
                                <ul class="cart-list">
                                    <li>
                                        
                                        <div class="pull-right">
                                            <span class="username text-uppercase">H!  [[Admin.username]]</span><br/>
                                            <span class="role">ADMIN</span>
                                        </div>
                                            <img style="width:80px;height:80px" class="pull-left"  alt="pics" src="/uploads/images/theme/unknown.jpg">
   
                                    </li>
                                </ul>
                                
                                <p class="buttons">
                                    <a href="adminprofile" target="_self" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr">Profile</a>
                                    <a href="api/auth/logout" target="_self" class="btn btn-very-small-danger no-margin-bottom margin-seven no-margin-right pull-right">Signout</a>
                                </p>
                            </div>
                        </div>
                            <!-- end user menu-->

                    </div>
                    <!-- end search  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->
                                <li ><a href="admin/home" ng-class="{'active':homePage}" target="_self">Home </a></li>


                                <li><a href="adminpublisher"  ng-class="{'active':publisherPage}"  target="_self" >Publishers </a></li>

                                <li><a href="adminadvert" ng-class="{'active':advertPage}" target="_self">Advert </a></li>

                                <li><a href="adminadvertiser" ng-class="{'active':adrtistPage}" target="_self">Advertisers </a></li>
                                <li><a href="adminpayment" ng-class="{'active':paymentPage}" target="_self">Payment </a></li>

                                 <li><a href="admincoupon" ng-class="{'active':couponPage}" target="_self">Coupon </a></li>

                                  <li><a href="adminbannertype" ng-class="{'active':bannerTypePage}" target="_self">BannerTypes </a></li>
                                <!-- end menu item -->
                               
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>
        <!--end navigation panel --> 

<!-- head section -->
    <section ng-hide="loadingView" class="">
    
         

        <!-- content section -->
    
        <section class="wow fadeIn padding-one">
            <div class="container">
                <div class="row">
                    <!-- content  -->
                    <div class="col-md-12 ng-cloak" id="content">
                         @yield('content')
                    </div>
                </div>
           </div> 

        </section>
        <!-- end content section -->


    </section>

        
 
    <!-- footer -->
    <footer class="wow fadeIn">
             
        <div  class="container-fluid  footer-bottom">
                <div class="container">
                    <div class="row">
                     <!-- copyright -->
                        <div class="col-md-6 col-sm-6 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one">
                           &copy; <?php echo date('Y'); ?> powered by <a class="display-inline"  href="http://vacvi.com"><img style=" margin-left: -70px" src="/uploads/images/logo/vacvi-logo-white.png/"></a> 
                        </div>
                        <!-- end copyright -->
                        <!-- logo -->
                        <div class="col-md-6 col-sm-6 col-xs-12 footer-logo text-right xs-text-center">
                            <a href="index.html"><img src="/uploads/images/logo/brimavcolor.png" alt=""/></a>
                        </div>
                        <!-- end logo -->
                    </div>
                </div>
            </div>

            <!-- scroll to top --> 
            <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a> 
            <!-- scroll to top End... --> 

            
   
        </footer>
         <!-- end footer -->

        <!-- notification-->
    <toast></toast>
    <!-- end notification-->


     <!-- View Loading Spinner -->
    <div ng-show="loadingView" class="spinner centerAbsolute">
        <img src="/uploads/images/theme/loadingcolor.gif">
    </div>
    <!-- End Loading Spinner-->



    <!-- Modal Loading Spinner -->
    <div id="loadingModalHolder" class="ng-hide">
        <div class="loadingModal modal-backdrop in">
            <div class="spinner">
                 <img src="/uploads/images/theme/loadingwhite.gif">
            </div>
        </div>
    </div>
    <!-- End Modal Loading Spinner--> 

           

    </body>


</html>