
<!-- List of All paid Payments -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('date')">
                        <label>Date</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='date'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='date'"></span>
                    </th>

                      <th >
                        <label>Website Name</label>
                        
                    </th>
                   
                    <th >
                        <label>Account No</label>
                        
                    </th>

                
                     <th ng-click="sort('amount')">
                          <label>Amount(N)</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='amount' || sortkey == ''"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='amount' && sortKey != ''"></span>
                       
                    </th>

                    <th class="text-right">
                         <label>Actions</label>
                    </th>
                </tr>
            </thead>
            <tbody>

                <tr dir-paginate="Payment in filteredItems = (Payments | orderBy:sortKey:reverse |filter:SearchText |itemsPerPage:10)">

                    <td >[[Payment.created_at | amDateFormat:'MMM Do, YYYY']] </td>
                 
                    
                    <td ><a href="http://[[Payment.websitename]]">[[Payment.publisher.websiteurl]]</a></td>

                    <td ><a href="">[[Payment.publisher.accountnumber]]</a></td>

                    <td>[[Payment.amount |currency:"N":0]]</td>
                   
                    <td class="text-right">
                        <a ui-sref="[[viewState]]({id:Payment.publisher_id})">
                            <i class="fa fa-eye i-extra-small-box i-rounded i-bordered" uib-tooltip="View Publisher" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                        

                        <a ng-if="Payment.status == 'PAID'" ng-click="disconfirmPayment(Payment.id)">
                            <i class="fa fa-ban i-extra-small-box i-rounded i-bordered" uib-tooltip="Disconfirm" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                        <a ng-if="Payment.status == 'NOT PAID'" ng-click="confirmPayment(Payment.id)">
                            <i class="fa fa-check i-extra-small-box i-rounded i-bordered" uib-tooltip="Confirm" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                    </td>
                </tr>


                <tr ng-if="filteredItems.length == 0"><td colspan="8">No Payments found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>



