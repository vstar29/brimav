@extends('areas/admin/views/shared.admin_layout')

@section('content')

<!--<script type="text/javascript" src="areas/admin/app/App.js"></script>
<script type="text/javascript" src="areas/admin/app/services/AdminShareService.js"></script>
<script type="text/javascript" src="areas/admin/app/services/AdminPaymentService.js"></script>
<script type="text/javascript" src="areas/admin/app/controllers/ShareController.js"></script>
<script type="text/javascript" src="areas/admin/app/controllers/PaymentController.js"></script>-->

@section('module', 'AdminPaymentModule')
<div ui-view></div>

@endsection