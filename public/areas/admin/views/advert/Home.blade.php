
<!-- List of All Adverts -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th>
                    </th>

                    <th ng-click="sort('date')">
                        <label>Date</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='date'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='date'"></span>
                    </th>
                    <th ng-click="sort('title')">
                       <label>Title</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='title'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey != 'title'"></span>
                    </th>

                    <th ng-click="sort('amount')">
                          <label>Total Amount(N)</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='amount'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='amount'"></span>
                    </th>
                    <th ng-click="sort('status')">
                          <label>Status</label>
                        <span class="no-animate sort-icon" ng-show="sortKey=='status'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='status'"></span>
                    </th>


                   <th class="text-right">
                         <label>Actions</label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="Advert in filteredItems = (Adverts | orderBy:sortKey:reverse |filter:SearchText |itemsPerPage:10)">
                <td><input type="checkbox" id="multiselect" name="multiselect" ng-click="multiSelect(Advert.id)"></td>
                    <td ng-class="{'inactive':Advert.status == 'UNCOMPLETED'}">[[Advert.created_at | amDateFormat:'MMMM Do YYYY']]</td>

                    <td ng-class="{'inactive':Advert.status == 'UNCOMPLETED'}">[[Advert.title]]</td>
                    <td ng-class="{'inactive':Advert.status == 'UNCOMPLETED'}">[[Advert.totalamount]]</td>
                    <td ng-class="{'inactive':Advert.status == 'UNCOMPLETED'}">[[Advert.status]]</td>
                    <td class="text-right">
                        <a ui-sref="advertdetails({id:Advert.id})">
                            <i class="fa fa-eye i-extra-small-box i-rounded i-bordered" uib-tooltip="View Advert" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>

                        
                        <a ng-if="Advert.status == 'UNCOMPLETED'" ng-click="confirmadvert(Advert.id)">
                            <i class="fa fa-check i-extra-small-box i-rounded i-bordered" uib-tooltip="Confirm Advert" tooltip-placement="bottom" tooltip-trigger="mouseenter"></i>
                        </a>

                        <a  ng-click="deleteadvert(Advert.id)">
                            <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Delete Advert" tooltip-placement="bottom" tooltip-trigger="mouseenter"></i>
                        </a>

                         
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No advert found</td></tr>
            </tbody>
        </table>
        
    </div>

<!-- Bulk Actions -->

        <form  name="bulkadvert" id="bulkadvert"  novalidate ng-submit="bulkActions()">
        <div  class="col-md-8">
        <Select  name="Bulk" id="Bulk" ng-model="Bulk" class="input-round width-adjust no-margin">
            <option value="">Bulk Actions</option>
            <option value="delete" name="delete">Delete</option>
        </Select>
        <button type="submit" class=" input-round text-lowercase" form="bulkadvert">Apply</button>
        
        </div>
        
        
        
        </form> 
 
    
<!-- Pagination Control -->
    <dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>
    </div>




