<div class="col-md-12">
	<div id="generalContent" class="ng-hide ng-cloak  margin-top-three" ng-show="!loadingView">
	
		<!-- Search -->
		<div class="form-group col-md-4 pull-right" style="margin-right: 16px;">
		    <input type="text" ng-model="SearchText" class=" form-control input-round tablesearch pull-right" placeholder="&#xf002;">
		</div>
		<!-- View Completed/ Uncompleted Advert -->
		<div class=" col-md-4 pull-left" style="margin-left: 16px;">
		    <select ng-model="Filterview" class="input-round tablesearch">
		    <option value="all" name="advert.all" >ALL</option>
		     <option value="completed" name="advert.completed" >CONFIRMED</option>  
		     <option value="uncompleted" name="advert.uncompleted" >UNCONFIRMED</option> 
		    </select>
		</div>
		
		<!-- Loading Image -->
		<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
		    <img src="/uploads/images/theme/ajax_loader.gif" />
		</div>

		

		<div class="col-md-12 ng-cloak">
		<div class="nestedLoadingHolder centerAbsolute ng-hide" ng-show="loadingNestedView">
		    <img src="/uploads/images/theme/loadingwhite.gif">
		</div>
		    <div ui-view></div>
		</div>

	</div>
</div>