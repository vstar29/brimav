
<!-- Advert Details -->
<div class="col-md-10 no-float center-block">
    <!-- Basic Details>-->
    <div class="col-md-12 wow slideInDown" data-wow-duration="300ms" style="overflow:auto;">

        <div class="col-md-8">
            <span class="text-uppercase black-text letter-spacing-2 margin-bottom-seven display-block font-weight-600 text-myExtra-large padding-one ">
                [[AdvertDetails.title]]
            </span>

            <div class="col-md-12 no-padding">
                <span class="col-md-3 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                    <label>Date</label>
                </span>
                <span class="col-md-9 no-padding-left letter-spacing-1">
                    [[AdvertDetails.created_at| amDateFormat:'MMMM Do YYYY']]
                </span>
            </div>

             <div class="col-md-12 no-padding">
                <span class="col-md-3 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                     <label>Type</label>
                </span>
                <a ui-sref="viewadvertiser({id:AdvertDetails.advertiser_id})" class="col-md-9 no-padding-left letter-spacing-1" uib-tooltip="View" tooltip-placement="top" tooltip-trigger="mouseenter">
                    [[AdvertDetails.type]]
                </a>
            </div>


            <div class="col-md-12 no-padding">
                <span class="col-md-3 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                     <label>Advertiser</label>
                </span>
                <a ui-sref="viewadvertiser({id:AdvertDetails.advertiser_id})" class="col-md-9 no-padding-left letter-spacing-1" uib-tooltip="View" tooltip-placement="top" tooltip-trigger="mouseenter">
                    [[Advertiser]]
                </a>
            </div>

            <div class="col-md-12 no-padding">
                <span class="col-md-3 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                     <label>Total Amount (N)</label>
                </span>
                <span class="col-md-9 green no-padding-left letter-spacing-1">
                    [[AdvertDetails.totalamount |currency:"N":0]]
                </span>
            </div>


            <div class="col-md-12 no-padding">
                <span class="col-md-3 no-padding-left-right font-weight-600 dark-gray-text letter-spacing-1">
                    <label>Status</label>
                </span>
                <span class="col-md-9 no-padding-left letter-spacing-1">
                    <span ng-class="{'green':AdvertDetails.status == 'CONFIRMED', 'red':AdvertDetails.status == 'UNCONFIRMED'}">
                        [[AdvertDetails.status]]
                    </span>
                </span>
            </div>


        </div>
    </div>

    <hr class="col-md-12">

    <!-- List of Publishers  -->
    <div class="col-md-10 wow slideInUp " data-wow-duration="300ms" style="overflow:auto;">
      <h3 class="margin-bottom-three left-text">PUBLISHERS</h3>
        <div class="col-md-6  pull-right">
            <ul style="list-style-type:disc">
        <li ng-repeat="publisher in Publishers" class="text-uppercase"><a ui-sref="publisherdetails({id:publisher.id})" uib-tooltip="View" tooltip-placement="top" tooltip-trigger="mouseenter">[[publisher.websitename]]</a></li>
            </ul>
        </div>
    </div>


</div>

<!--Return Section-->
<div class="col-md-12">
    <hr />
    <button class="btn btn-default btn-medium" ui-sref="advert.all"><i class="fa fa-angle-left"></i>Back to Adverts </button>
</div>
</div>


