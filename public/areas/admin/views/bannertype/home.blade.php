
<!-- Search -->
<div class="form-group pull-right col-md-4 ">
    <input type="text" ng-model="SearchText" class=" pull-right form-control input-round tablesearch" placeholder="&#xf002;">
</div>

<div class=" " style="margin-left: 15px;">
    <a ui-sref="create"> <button class="btn btn-medium " > Add Banner Type<i class="fa fa-plus"></i></button></a>
</div>
<!-- Loading Image -->
<div ng-show="contentLoading" class="col-md-1 no-padding-left margin-half-top remain">
    <img src="/uploads/images/theme/ajax_loader.gif" />
</div>

<!-- List of All Coupon -->
<div class="col-md-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="sortable">
                    <th ng-click="sort('name')">
                        <label>Name</label>
                        <span class="sort-icon no-animate" ng-show="sortKey=='name'"
                              ng-class="{'fa fa-angle-up':reverse,'fa fa-angle-down':!reverse}"></span>
                        <span class="space no-animate" ng-show="sortKey!='name'"></span>
                    </th>
                   

                    <th>
                          <label>Description</label>
                        
                    </th>

                       <th>
                          <label>Size</label>
                        
                    </th>



                    <th class="text-right">
                         <label>Actions</label>
                    </th>

                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="BannerType in filteredItems = (BannerTypes | orderBy:sortKey:reverse |filter:SearchText |itemsPerPage:10)">
              
                    <td>[[BannerType.bannertype_name]]</td>
                    <td>[[BannerType.description]]</td>
                    <td>[[BannerType.size]]</td>
            
                    <td class="text-right">
                        <a ui-sref="edit({id:BannerType.id})">
                            <i class="fa fa-pencil i-extra-small-box i-rounded i-bordered" uib-tooltip="Edit" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                       
                        <a ng-click="deletebannertype(BannerType.id)">
                            <i class="fa fa-times i-extra-small-box i-rounded i-bordered" uib-tooltip="Delete" tooltip-placement="top" tooltip-trigger="mouseenter"></i>
                        </a>
                    </td>
                </tr>
                <tr ng-if="filteredItems.length == 0"><td colspan="8">No BannerType found</td></tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Pagination Control -->
<dir-pagination-controls class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>