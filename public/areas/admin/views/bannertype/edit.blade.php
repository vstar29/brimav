<div class="wow fadeIn col-md-8 center-col" data-wow-duration="300ms">
    <form name="formEdit" novalidate ng-submit="edit()" class="form-horizontal">
        <div class="form-horizontal">
            <div class="validation-summary-errors" ng-show="validationErrors">
                <ul>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Name</label>
                <div class="col-md-9 leftfifteen">
               
                    <input type="text" id="name" name="name" class="form-control width-70 margin-one" ng-model="BannerType2.bannertype_name"
                           ng-class="{'input-validation-error': fieldErrors.name }" />
                    <span ng-show="fieldErrors.name" class="field-validation-error remain2 white-space-pre">[[fieldErrors.name]]</span>
                    
                </div>
            </div>
           

            <div class="form-group margin-four-bottom">
                <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Description</label>
                <div class="col-md-9 leftfifteen">
                    <input type="text" id="code" name="Description" class="form-control width-70" ng-model="BannerType2.description"
                           ng-class="{'input-validation-error': fieldErrors.description }" />
                    <span ng-show="fieldErrors.description" class="field-validation-error remain2 white-space-pre">[[fieldErrors.description]]</span>
                </div>
            </div>


            <div class="form-group margin-four-bottom">
               <label class = "control-label col-md-3 text-left font-14 black-text font-weight-500 letter-spacing-1">Size</label>
                <div class="col-md-9 leftfifteen">
                    <input type="text" id="size" name="size" class="form-control width-70" ng-model="BannerType2.size"
                           ng-class="{'input-validation-error': fieldErrors.size }" />
                    <span ng-show="fieldErrors.size" class="field-validation-error remain2 white-space-pre">[[fieldErrors.size]]</span>
                </div>
            </div>

            

            <div class="form-group pull-right ">
                <button class="btn btn-default btn-round btn-medium" ui-sref="admintype" ng-disabled="editing"><i class="fa fa-arrow-left"></i></button>
                <button type="submit" class="btn btn-round btn-medium btn-adgold no-margin-bottom">
                    Save
                   
                    <img ng-show="creating" class="loader loader-small display-inline-block ng-hide">
                </button>
            </div>
        </div>
    </form>
</div>