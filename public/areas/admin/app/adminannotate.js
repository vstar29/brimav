/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var adminShareApp = angular.module('AdminShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','angularMoment','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', ["ngToast", "$uibModalStack", function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {

            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }
            /*for (var key in $scope.fieldErrors) {
                alert(key + ": " + $scope.fieldErrors[key])
            }*/
        } 
        else if($scope.validationErrors.count==1)
        {
           
            SharedMethods.createErrorToast($scope.validationErrors);
        }
        else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }

    /* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
    
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="fa fa-thumbs-up alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning alert-danger margin-right-04"></i>' + cont
        });
    }
    
    /* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }
}])
.config(["$urlRouterProvider", "$locationProvider", "$stateProvider", "$breadcrumbProvider", "ngToastProvider", "$uibModalProvider", "$urlMatcherFactoryProvider", "$interpolateProvider", function ($urlRouterProvider, $locationProvider,  $stateProvider,$breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider) {
    //debugger; //break point

    //modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'top',
        horizontalPosition: 'center',
        timeout: 4000,
        maxNumber: 3,
        animation: 'fade',
        dismissButton: true,
    });

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    $breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/admin/home" target="_self">Home&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });




    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
    
    //make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    //$urlRouterProvider.otherwise('/404');

    $locationProvider.html5Mode(true);
}])
.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})




.run(["$rootScope", "SharedMethods", function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
}]);




/*===================== HOME SPA ====================*/
var adminHomeApp = angular.module('AdminHomeModule', ['AdminShareModule'])
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$interpolateProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
    

    $urlRouterProvider.otherwise('/admin/home');
   
    $stateProvider
    .state('home', {
        url: '/admin/home',
        templateUrl: 'adminhome/home',
        controller: 'HomeController'
    });
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.homePage = true;
}]);







/*===================== ADVERT SPA ========================*/
var adminAdvertApp = angular.module('AdminAdvertModule',['AdminShareModule'])
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
  
    $urlRouterProvider.otherwise('/admin/advert');

    $stateProvider
     // Get List of adverts
    .state('advert', {
        url: '/admin/advert',
        abstract:true,
        templateUrl: 'adminadvert/template',
        controller: 'AdvertTemplateController',
        resolve:{
            Adverts:["AdminAdvertService", function(AdminAdvertService)
            {
                return AdminAdvertService.getAllAdverts();
            }],
            UncompletedAdverts:["AdminAdvertService", function(AdminAdvertService)
            {
                return AdminAdvertService.getUncompletedAdverts();
            }],

            CompletedAdverts:["AdminAdvertService", function(AdminAdvertService)
            {
                return AdminAdvertService.getCompletedAdverts();
            }],
        }
        
    })
       

    // Get List of adverts
    .state('advert.all', {
        url: '',
        templateUrl: 'adminadvert/home',
        controller: 'AdvertHomeController',
        
        ncyBreadcrumb: {
            label: 'Adverts'
        },
        resolve:{
            Adverts:["Adverts", function(Adverts)
            {
                return Adverts;
            }]
        }
    })

     .state('advert.completed', {
        url: '',
        params: { returnState: "advert" },
        templateUrl: 'adminadvert/completed',
        controller: 'CompletedAdvertController',
        ncyBreadcrumb: {
            label: 'Confirmed',
            parent: 'advert.all'
        },
        resolve:
        {
            CompletedAdverts:["CompletedAdverts", function(CompletedAdverts)
            {
                return CompletedAdverts;
            }]
        }
        
    })

    .state('advert.uncompleted', {
        url: '/uncompleted',
        params: { returnState: "advert" },
        templateUrl: 'adminadvert/uncompleted',
        controller: 'UncompletedAdvertController',
        ncyBreadcrumb: {
            label: 'Unconfirmed',
            parent: 'advert.all'
        },
        resolve:
        {
            UncompletedAdverts:["UncompletedAdverts", function(UncompletedAdverts)
            {
                return UncompletedAdverts;
            }]
        }
    })
       
    // Get Advert Details
    .state('advertdetails' , {
        url: '/admin/advert/details/:id',
        params: { returnState: "advert" },
        templateUrl: 'adminadvert/details',
        controller: 'AdvertDetailsController',
        ncyBreadcrumb: {
            label: 'Details',
            parent: 'advert.all'
        },
        resolve:
        {
            AdvertDetails:["AdminAdvertService", "$stateParams", function(AdminAdvertService, $stateParams)
            {
                return AdminAdvertService.getAdvertDetails($stateParams.id);
            }]

        }
    })

 
    //Publisher Details Template
    .state('publisherdetails' , {
        url: '/admin/advert/details/publisher/:id?=:username',
        params: {
            returnState: ["$stateParams", function ($stateParams) {
                return 'advertdetails({id:' + $stateParams.id + '})';
            }]
        },
        templateUrl: 'adminpublisher/details',
        controller: 'PublisherDetailsController',
        resolve: {
            PublisherDetails: ["$stateParams", "AdminAdvertService", function ($stateParams, AdminAdvertService) {
                return AdminAdvertService.getPublisherDetails($stateParams.id);
            }]
        },
        ncyBreadcrumb: {
            label: ' Publisher',
            parent: 'advertdetails'
        }
    })  


     //Get account Details
    .state('publisherdetails.account' , {
        url: '/account',
        templateUrl: 'adminpublisher/accountdetails',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Publisher',
            parent: 'publisherdetails'
        }
    }) 

     //Get payments Details
    .state('publisherdetails.payments' , {
        url: '/payment',
        params:{viewState:'publisherdetails'},
        templateUrl: 'adminpayment/home',
        controller: 'PaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Publisher',
            parent: 'publisherdetails'
        }
    }) 

    .state('publisherdetails.account.editbalance' , {
        url: '',
        params:{returnState:'publisherdetails.account'},
        templateUrl: 'adminpublisher/editbalance',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Publisher',
            parent: 'publisherdetails.account'
        }
    }) 

    .state('viewadvertiser', {
        url: '/admin/advert/details/advertiser/:id?=:username',
        params: {
            returnState: ["$stateParams", function ($stateParams) {
                return 'advertdetails({id:' + $stateParams.id + '})';
            }]
        },
        templateUrl: 'adminadvertiser/details',
        controller: 'AdvertiserDetailsController',
        resolve: {
            Advertiser: ["$stateParams", "AdminAdvertService", function ($stateParams, AdminAdvertService) {
                return AdminAdvertService.getAdvertiserDetails($stateParams.id);
            }]
        },
        ncyBreadcrumb: {
            label: ' Advertiser',
            parent: 'advertdetails'
        }
    })
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.advertPage = true;
}]);


/*===================== ADVERTISER SPA ====================*/
var adminAdvertiserApp = angular.module('AdminAdvertiserModule', ['AdminShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/admin/advertiser');

    $stateProvider
    //Member's Homepage (Active List)
    .state('advertiser', {
        url: '/admin/advertiser',
        templateUrl: 'adminadvertiser/home',
        controller: 'AdvertisersHomeController',
        ncyBreadcrumb: {
            label: 'Advertiser'
        }
    })

    // Get Advertiser Details
    .state('viewadvertiser', {
        url: '/admin/advertiser/details/{username}',
        params:{returnState:"advertiser"},
        templateUrl: 'adminadvertiser/details',
        controller: 'AdvertiserDetailsController',
       
        resolve: {
            Advertiser: ["$stateParams", "AdminAdvertiserService", function ($stateParams, AdminAdvertiserService) {
                return AdminAdvertiserService.getAdvertiser($stateParams.username);
            }]
        },
        ncyBreadcrumb: {
            label: ' Details',
            parent: 'advertiser'
        }
    });
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.advertiserPage = true;
}]);


/*===================== PUBLISHER SPA ====================*/
var adminPublisherApp = angular.module('AdminPublisherModule', ['AdminShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/admin/publisher');
    


$stateProvider

     // Publisher Template
    .state('publisher', {
        url: '/admin/publisher',
        abstract:true,
        templateUrl: 'adminpublisher/template',
        controller: 'PublisherTemplateController',
        resolve:{
            Publishers:["AdminPublisherService", function(AdminPublisherService)
            {
                return AdminPublisherService.getAllPublishers();
            }],
            InactivePublishers:["AdminPublisherService", function(AdminPublisherService)
            {
                return AdminPublisherService.getInactivePublishers();
            }],

            ActivePublishers:["AdminPublisherService", function(AdminPublisherService)
            {
                return AdminPublisherService.getActivePublishers();
            }]
        }
        
    })
       
    // Get List of Publishers
    .state('publisher.all', {
        url: '',
        templateUrl: 'adminpublisher/home',
        controller: 'PublisherHomeController',
        
        ncyBreadcrumb: {
            label: 'Publishers'
        },
        resolve:{
            Publishers:["Publishers", function(Publishers)
            {
                return Publishers;
            }]
        }
    })

     .state('publisher.active', {
        url: '/active',
        templateUrl: 'adminpublisher/active',
        controller: 'ActivePublisherController',
        ncyBreadcrumb: {
            label: 'Active',
            parent: 'publisher.all'
        },
        resolve:
        {
            ActivePublishers:["ActivePublishers", function(ActivePublishers)
            {
                return ActivePublishers;
            }]
        }
        
    })

    .state('publisher.inactive', {
        url: '/inactive',
        templateUrl: 'adminpublisher/inactive',
        controller: 'InactivePublisherController',
        ncyBreadcrumb: {
            label: 'Inactive',
            parent: 'publisher.all'
        },
        resolve:
        {
            InactivePublishers:["InactivePublishers", function(InactivePublishers)
            {
                return InactivePublishers;
            }]
        }
    })
       
   //Publisher Details Template
    .state('publisherdetails' , {
        url: '/admin/publisher/details/:id',
        params:{returnState:"publisher.all"},
        templateUrl: 'adminpublisher/details',
        controller: 'PublisherDetailsController',
        resolve:
        {
            PublisherDetails:["AdminPublisherService", "$stateParams", function(AdminPublisherService, $stateParams)
            {
                return AdminPublisherService.getPublisherDetails($stateParams.id);
            }]

        },
        ncyBreadcrumb: {
            label: 'Details',

        }
    })  


     //Get account Details
    .state('publisherdetails.account' , {
        url: '',
        templateUrl: 'adminpublisher/accountdetails',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Account',
            parent: 'publisherdetails'
        }
    }) 

     //Get payments Details
    .state('publisherdetails.payments' , {
        url: '',
        params:{viewState:'publisherdetails'},
        templateUrl: 'adminpublisher/paymentdetails',
        controller: 'PaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Payments',
            parent: 'publisherdetails'
        }
    }) 

    .state('publisherdetails.account.editbalance' , {
        url: '',
        params:{returnState:'publisherdetails.account'},
        templateUrl: 'adminpublisher/editbalance',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Account',
            parent: 'publisherdetails'
        }
    }) 
}])
.run(["$rootScope", function ($rootScope) {
    $rootScope.publisherPage = true;
}]);


    /*===================== PROFILE SPA ====================*/
   var adminProfileApp = angular.module('AdminProfileModule', ['AdminShareModule'])
    .config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/admin/profile');

        
        $stateProvider
        // Template for the Profile Pages
        .state('profile', {
            abstract: true,
            url: '/admin/profile',
            templateUrl: 'adminprofile/home',
            controller: 'ProfileTemplateController',
            resolve: {
                Admin: ["AdminProfileService", function (AdminProfileService) {
                    return AdminProfileService.getAdminBasicDetails(); 
                }]
            }
        })

        //Profile (Display Admin Profile)
        .state('profile.basic', {
            url: '',
            templateUrl: 'adminprofile/basicdetails',
            controller: 'BasicDetailsController',
            resolve: {
                Admin: ["Admin", function (Admin) {
                    return Admin;
                }],

            },
            ncyBreadcrumb: {
            label: 'Profile'
        }
            
        })
       
        // Edit mode for Admin Basic Details
        .state('profile.editbasic', {
            url: '/edit',
            templateUrl: 'adminprofile/editadminbasicdetails',
            controller: 'EditBasicProfileController',
            resolve: {
                Admin: ["Admin", function (Admin) {
                    return Admin;
                }]
            },
            ncyBreadcrumb: {
                label: 'Edit Basic Details',
                parent: 'profile.basic'
            }
                
        })

         // Change Password
        .state('profile.changepassword', {
            url: '/security',
            templateUrl: 'adminprofile/changepassword',
            controller: 'ChangePasswordController',
            resolve: {
                Admin: ["Admin", function (Admin) {
                    return Admin;
                }]
            },
            ncyBreadcrumb: {
                label: 'Change Password',
                parent: 'profile.basic'
            }
        })  

    }])
    .run(["$rootScope", function ($rootScope) {
    $rootScope.profilePage = true;
}]);

        /*===================== PAYMENTS SPA ====================*/
    var adminPaymentApp = angular.module('AdminPaymentModule', ['AdminShareModule'])
    .config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/admin/payment');

        $stateProvider
        //Payment's Homepage 
        .state('payment', {
            url: '/admin/payment',
            params:{viewState:'publisherdetails'},
            abstract:true,
            templateUrl: 'adminpayment/home',
            controller: 'PaymentsHomeController',
            ncyBreadcrumb: {
                label: 'Payment'
            },
            resolve:
            {
                AllPayments:["AdminPaymentService", function(AdminPaymentService)
                {
                    return AdminPaymentService.getAllPayments();
        
                }]
            }
        })

          // Get all payment request
    .state('payment.request', {
        url: '',
        templateUrl: 'adminpayment/request',
        controller: 'PaymentRequestController',
        
        ncyBreadcrumb: {
            label: 'Request'
        },
        resolve:
        {
            Requests:["AllPayments", function(AllPayments)
            {
                return AllPayments.Requests;
            }]
        }
        
    })

         // Get all payment request
    .state('payment.paid', {
        url: '/paid',
        templateUrl: 'adminpayment/paid',
        controller: 'PaidPaymentController',
        
        ncyBreadcrumb: {
            label: 'Paid'
        },
        resolve:
        {
            Paid:["AllPayments", function(AllPayments)
            {
                return AllPayments.Payments
            }]
        }
        
    })

        
        //Publisher Details Template
    .state('publisherdetails' , {
        url: '/admin/payment/publisher/:id',
        params:{returnState:'payment.request'},
        templateUrl: 'adminpublisher/details',
        controller: 'PublisherDetailsController',
        resolve: {
            PublisherDetails: ["$stateParams", "AdminPaymentService", function ($stateParams, AdminPaymentService) {
                return AdminPaymentService.getPublisherDetails($stateParams.id);
            }]
        },
        ncyBreadcrumb: {
            label: ' Publisher',
            parent: 'payment'
        }
    })  


     //Get account Details
    .state('publisherdetails.account' , {
        url: '',
        templateUrl: 'adminpublisher/accountdetails',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Account',
            parent: 'publisherdetails'
        }
    }) 

     //Get payments Details
    .state('publisherdetails.payments' , {
        url: '',
        params:{viewState:'publisherdetails'},
        templateUrl: 'adminpayment/publisherpaymount',
        controller: 'PublisherPaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Payment',
            parent: 'publisherdetails'
        }
    }) 

    .state('publisherdetails.account.editbalance' , {
        url: '',
        params:{returnState:'publisherdetails.account'},
        templateUrl: 'adminpublisher/editbalance',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Edit ',
            parent: 'publisherdetails.account'
        }
    }) 

}])
.run(["$rootScope", function($rootScope)
{
$rootScope.paymentPage=true;
}]);


/*===================== COUPON SPA ====================*/


var adminCouponApp=angular.module('AdminCouponModule',['AdminShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/admin/coupon');

    $stateProvider.

    state('coupon',
    {
        url:'/admin/coupon',
        templateUrl:'admincoupon/home',
        controller:'CouponHomeController',
        
         ncyBreadcrumb:
        {
            label:'coupon'
        }
    })

    .state('create',{

        url:'/admin/coupon/create',
        templateUrl:'admincoupon/create',
        controller:'CouponCreatecontroller',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'coupon'
        }
    })

    .state('edit',
    {
        url:'/admin/coupon/edit',
        params:{id:null},
        templateUrl:'admincoupon/edit',
        controller:'CouponEditcontroller',
        resolve:
        {
            Coupon:["AdminCouponService", "$stateParams", function(AdminCouponService,$stateParams)
            {
                
                return AdminCouponService.getCouponbyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'coupon'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.couponPage=true;
}]);




/*===================== BANNER TYPE SPA ====================*/


var adminBannerTypeApp=angular.module('AdminBannerTypeModule',['AdminShareModule'])
.config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/admin/bannertype');

    $stateProvider.

    state('bannertype',
    {
        url:'/admin/bannertype',
        templateUrl:'adminbannertype/home',
        controller:'AdminBannerTypeHomeController',
        
         ncyBreadcrumb:
        {
            label:'bannertype'
        }
    })

    .state('create',{

        url:'/admin/bannertype/create',
        templateUrl:'adminbannertype/create',
        controller:'BannerTypeCreatecontroller',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'bannertype'
        }
    })

    .state('edit',
    {
        url:'/admin/bannertype/edit',
        params:{id:null},
        templateUrl:'adminbannertype/edit',
        controller:'BannerTypeEditcontroller',
        resolve:
        {
            BannerType:["AdminBannerTypeService", "$stateParams", function(AdminBannerTypeService,$stateParams)
            {
                
                return AdminBannerTypeService.getBannerTypebyid($stateParams.id);
            }]
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'bannertype'
        }
    })

}])

.run(["$rootScope", function($rootScope)
{
    $rootScope.bannerTypePage=true;
}]);
/*-------========== ADMIN SHARED SERVICE =======-------*/

/*--==== Get  Logged Inuser Details ====--*/

adminShareApp.service('ShareService', ["$http", function ($http) {
    //Get current admin info
    this.getLoggedInAdmin = function () {
        return $http.get("/api/adminprofileAPI");

    };
}])
adminAdvertiserApp.service("AdminAdvertiserService", ["$http", "SharedMethods", function ($http, SharedMethods) {

    var advertiserBase = "api/adminadvertiserAPI";
    

    /*=============== AdvertiserS ===============*/
    //get all Active Advertisers
    this.getAllAdvertisers = function () {
        return $http.get(advertiserBase);
    };

    //Get Advertiser by name 
    this.getAdvertiser = function (name) {
        return $http.get(advertiserBase +'/advertiserbyusername/' +name).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem loading Advertiser information");
            var Advertiser = {};
            return Advertiser;
        });
    };

    //Deactivate Advertiser by ID 

    this.deactivateAdvertiser = function (id) {
        var request = $http({
            method: "get",
            url: advertiserBase + "/deactivateadvertiser/" + id
        });
        return request;
    };

    //Activate Advertiser by ID

    this.activateAdvertiser = function (id) {
        var request = $http({
            method: "get",
            url: advertiserBase + "/activateadvertiser/" + id
        });
        return request;
    };

     //Delete Advertiser by ID 
    this.deleteAdvertiser = function (id) {
        var request = $http({
            method: "get",
            url: advertiserBase + "/deleteadvertiser/" + id
        });
        return request;
    };

}]);

 /*----====== ADMIN ADVERTS SERVICE ======----*/

adminAdvertApp.service("AdminAdvertService", ["$http", "SharedMethods", function ($http, SharedMethods) {

    var advertBase = "api/adminadvertAPI";
    var pubBase = "api/adminpublisherAPI";
    var advertiserBase = "api/adminadvertiserAPI";



    /*--===== Get all Adverts ====--*/
    
    this.getAllAdverts = function () {
        return $http.get(advertBase).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get completed Adverts ====--*/
    
    this.getCompletedAdverts = function () {
        return $http.get(advertBase+'/completedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get Uncompleted Adverts ====--*/
    
    this.getUncompletedAdverts = function () {
        return $http.get(advertBase+'/uncompletedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };


    /*--===== Get  Advert by ID ====--*/

    this.getAdvertDetails = function (id) {
        return $http.get(advertBase +'/advertbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Advert details");
            var advert = {};
            return advert;
        });
    };
    

    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherDetails = function (id) {
        return $http.get(pubBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },

        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var advert = {};
            return advert;
        });
    };

    /*--===== Edit  Publisher Balance ====--*/

    this.putBalance = function (newBalance) {

        var request=$http(
        {   url:pubBase +'/editpublisherbalance/',
            method:'put',
            data: newBalance
        });
            return request;
    };

/*--===== Get  Advertiser by ID ====--*/

    this.getAdvertiserDetails = function (id) {
        return $http.get(advertiserBase +'/advertiserbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Advertiser details");
            var advert = {};
            return advert;
        });
    };
   
   /*--===== Confirm Advert by ID ====--*/

    this.confirmadvert = function (id) {
        var request = $http({
            method: "put",
            url: advertBase + "/confirmadvert/" + id
        });
        return request;
    };


    /*--===== Delete Advert by ID ====--*/

    this.deleteadvert = function (id) {
        var request = $http({
            method: 'get',
            url: advertBase + "/removeadvert/" + id
        });
        return request;
    };



/*--===== Delete Bulk Advert  ====--*/

    this.deletebulkadvert = function (Bulkids) {
        var request = $http({
            method: 'get',
            url: advertBase + "/removeadvertbulk/"+Bulkids,
           
        });
        return request;
    };
}]);
			/*====---ADMIN COUPON SERVICE--=====*/


adminCouponApp.service('AdminCouponService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var couponBase="api/admincouponAPI";
	  var advertiserBase = "api/adminadvertiserAPI";


	/*--get all  coupon---*/
	this.getAll=function()
	{
		return $http.get(couponBase);
	};


	/*--get  coupon by id---*/
	this.getCouponbyid=function(id)
	{
		
		return $http.get(couponBase +'/couponbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting coupon details");
			var COUPON={};
			return COUPON;
		})
	};


	/*--create coupon---*/
	this.postCoupon=function(data)
	{
		var request=$http(
			{
				url:couponBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit coupon---*/
	this.putCoupon=function(data)
	{
		var request=$http(
			{
				url:couponBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete coupon---*/

    this.deleteCoupon = function (id) {
        var request=$http(
        {
            url:couponBase+'/couponremove/'+id,
            method:'get'
        });
        return request;
     
   };
}])
			/*====---ADMIN BANNER TYPE SERVICE--=====*/


 adminBannerTypeApp.service('AdminBannerTypeService',["$http", "SharedMethods", function($http,SharedMethods)
{
	var bannertypeBase="api/adminbannertypeAPI";
	  //var advertiserBase = "api/adminadvertiserAPI";


	/*--get all  banner types---*/
	this.getAll=function()
	{
		return $http.get(bannertypeBase);
	};


	/*--get  banner type by id---*/
	this.getBannerTypebyid=function(id)
	{
		
		return $http.get(bannertypeBase +'/bannertypebyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting banner type details");
			var BANNERTYPE={};
			return BANNERTYPE;
		})
	};


	/*--create banner type---*/
	this.postBannerType=function(data)
	{
		var request=$http(
			{
				url:bannertypeBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit bannertype---*/
	this.putBannerType=function(data)
	{
		var request=$http(
			{
				url:bannertypeBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete bannetype---*/

    this.deleteBannerType = function (id) {
        var request=$http(
        {
            url:bannertypeBase+'/bannertyperemove/'+id,
            method:'get'
        });
        return request;
     
   };
}])
adminPaymentApp.service('AdminPaymentService', ["$http", "SharedMethods", function($http,SharedMethods)
{
var publisherBase="api/adminpublisherAPI";
var payBase="api/adminpaymentAPI";



/*--===== Get  All  Payments ====--*/

    this.getAllPayments = function () {
        return $http.get(payBase).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var Payments = {};
            return Payments;
        });
    };

    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherDetails = function (id) {
        return $http.get(publisherBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var publisher = {};
            return publisher;
        });
    };

    
    /*--===== Edit  Publisher Balance ====--*/

    this.putBalance = function (newBalance) {

        var request=$http(
        {   url:publisherBase +'/editpublisherbalance/',
            method:'put',
            data: newBalance
        });
            return request;
    };


    /*---Confirm request as paid--*/

    this.confirmPayment=function(id)
    {
        return $http.get(payBase+'/confirmpayment/'+id).then(function(response)
        {
            return response.data;
        });
    }

    /*---Disconfirm request as paid--*/

    this.disconfirmPayment=function(id)
    {
        return $http.get(payBase+'/disconfirmpayment/'+id).then(function(response)
        {
            return response.data;
        });
    }
	
}]);

/*----====== ADMIN PROFILE SERVICE ======----*/

 
adminProfileApp.service('AdminProfileService', ["$http", "SharedMethods", function ($http, SharedMethods) {

    var profileBase = "api/adminprofileAPI";
    

    /*--===== Get  Basic details ====--*/
   
    this.getAdminBasicDetails = function () {
        return $http.get(profileBase).then(function (response) {
          console.log(response.data);
            return response.data;
            
        },
       function (error) {
           SharedMethods.createErrorToast("Problem retreiving your details, kindly try again");
           var admin = {};
           return admin;
       });
    };
    

    /*--===== Edit Basic details ====--*/
  
  this.putEditadminBasicDetails=function(AdminDetails)
  {
    
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editadminbasicdetails",
        data:AdminDetails
    });
    return request;
  };


    /*--===== Change Password ====--*/
    
    this.changepassword = function (Passworddetails) {
      
        var request = $http({
            method: 'put',
            url: profileBase + "/changepassword",
            data: Passworddetails
        });
        return request;
    };

}])
/*----====== ADMIN PUBLISHERS SERVICE ======----*/

adminPublisherApp.service("AdminPublisherService", ["$http", "SharedMethods", function ($http, SharedMethods) {

    var publisherBase = "api/adminpublisherAPI";


    /*--===== Get all Publishers ====--*/
    
    this.getAllPublishers = function () {
       

        return $http.get(publisherBase).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting publishers");
            var publisher = {};
            return publisher;
            });
    };

    /*--===== Get active Publishers ====--*/
    
    this.getActivePublishers = function () {
        return $http.get(publisherBase+'/activepublishers').then(function(response)
            {

                return response.data;

            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting publishers");
            var publisher = {};
            return publisher;
            });
    };

    /*--===== Get Inactive Publishers ====--*/
    
    this.getInactivePublishers = function () {
        return $http.get(publisherBase+'/inactivepublishers').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting publishers");
            var publisher = {};
            return publisher;
            });
    };


    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherDetails = function (id) {
        return $http.get(publisherBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var publisher = {};
            return publisher;
        });
    };

    /*--===== Edit  Publisher Balance ====--*/

    this.putBalance = function (newBalance) {

        var request=$http(
        {   url:publisherBase +'/editpublisherbalance/',
            method:'put',
            data: newBalance
        });
            return request;
    };

    /*--===Deactivate Publisher by ID--==*/ 
    this.deactivatePublisher = function (id) {
        var request = $http({
            method: "get",
            url: publisherBase + "/deactivatepublisher/" + id
        });
        return request;
    };

    /*---===Activate Publisher by ID--===*/

    this.activatePublisher = function (id) {
        var request = $http({
            method: "get",
            url: publisherBase + "/activatepublisher/" + id
        });
        return request;
    };


     /*---===Delete Publisher by ID--===*/
     
    this.deletePublisher = function (id) {
        var request = $http({
            method: "get",
            url: publisherBase + "/deletepublisher/" + id
        });
        return request;
    };
    
}]);
/*-------========== ADMIN SHARED CONTROLLER =======-------*/

/*--==== Get  Logged In-User Details ====--*/
adminShareApp.controller('AdminShareController', ["$scope", "$state", "ShareService", "$cookies", function ($scope, $state, ShareService,$cookies) {

      $scope.Admin=$cookies.getObject('Admin_profile');
    if ($scope.Admin == null || $scope.Admin === undefined)
    {
      loadAdminInfo();
    }
      


    function loadAdminInfo() {
        var promiseGetAdminInfo = ShareService.getLoggedInAdmin();

        promiseGetAdminInfo.then(function (pl) {
            $scope.Admin = pl.data;
            $cookies.putObject('Admin_profile', $scope.Admin);

          
        },
        function (errorPl) {
            $scope.error = errorPl;
            
        });
    }

   

}]);

// HomeController
adminHomeApp.controller('HomeController', ["$scope", "$rootScope", function ($scope, $rootScope) {

    $rootScope.title = "Home | Brimav";
}]);
/*-------========== ADMIN ADVERTS CONTROLLER =======-------*/

/*--==== Adert Template Controller ====--*/

adminAdvertApp.controller('AdvertTemplateController', ["SharedMethods", "AdminAdvertService", "$rootScope", "$uibModal", "$state", "$scope", "Adverts", function ( SharedMethods, AdminAdvertService,$rootScope, $uibModal, $state, $scope, Adverts) {
    $scope.Adverts = Adverts;
   
    $rootScope.headerClass = "normal-page-title";

    //Set default filter, sort parameters
    $scope.Filterview = "all";
    $scope.sortKey = "date";
    $scope.reverse=false;
    $scope.bulkIds=[]; // Array to hold bulk actions key.


   
     $scope.$watch('Filterview', function (filterkey) {
        $state.go('advert.'+filterkey); // Go to the state selected
        $scope.Filterview=filterkey; //set the Filter to the param passed
    });

    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.multiSelect=function(id)
    {
       
        var indexint=-1;
        angular.forEach($scope.bulkIds,function(advertid,index)
        {
            if(id==advertid)
            {
                indexint=index;
            }
        });



        if(indexint>=0)
        {
            $scope.bulkIds.splice(indexint,1);
        }
        else
        {
            $scope.bulkIds.push(id);
        }
        
    
    }
   



    // confirm advert as complete 

    $scope.confirmadvert = function (id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var confirmationDetails = {
            id : id,
            headerContent: "Confirm Advert " ,
            bodyMessage: "Publishers will be credited. Are you sure you want to confirm this member?",
            buttonText: "",
            buttonClass: "btn-adgold",
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvert/dialogbox',
            controller: 'ConfirmAdvertController',
            resolve: {
                ConfirmationDetails: function () {
                    return confirmationDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };
   

// Delete Advert 

    $scope.deleteadvert = function (id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            id : id,
            headerContent: "Delete Advert " ,
            bodyMessage: "Are you sure you want to delete this Advert?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvert/dialogbox',
            controller: 'DeleteAdvertController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };
   
}]);


/*--==== Advert Home Controller ====--*/

adminAdvertApp.controller('AdvertHomeController', ["$scope", "$state", "SharedMethods", "AdminAdvertService", "$rootScope", "$uibModal", function ($scope,$state, SharedMethods, AdminAdvertService, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon

    
    $scope.searchAdverts = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };


    $scope.bulkActions=function()
    {
        if($scope.Bulk=="delete")
        {
            
            var deleteadverts=AdminAdvertService.deletebulkadvert($scope.bulkIds);
            deleteadverts.then(function(pl)
            {
               
                $scope.bulkActions={};
                $state.go('advert.all', {}, { reload: true });
                SharedMethods.createSuccessToast('Adverts deleted successfully');
            },
            function(error)
            {
                SharedMethods.createErrorToast('Problem deleting adverts');
            })
        }
    }
    
}])


/*--==== Complete Advert Controller ====--*/

adminAdvertApp.controller('CompletedAdvertController', ["$scope", "CompletedAdverts", "AdminAdvertService", "$rootScope", "$uibModal", function ($scope, CompletedAdverts, AdminAdvertService, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Confirmed Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon
    $scope.ConfirmedAdvert=CompletedAdverts;
    
    
}]);


/*--==== UnComplete Advert Controller ====--*/

adminAdvertApp.controller('UncompletedAdvertController', ["$scope", "UncompletedAdverts", "AdminAdvertService", "$rootScope", "$uibModal", function ($scope, UncompletedAdverts, AdminAdvertService, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Unconfirmed Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon
    $scope.UnconfirmedAdvert=UncompletedAdverts;
     
    }]);

    


/*--==== Advert Details Controller ====--*/

adminAdvertApp.controller('AdvertDetailsController', ["$scope", "$rootScope", "$stateParams", "AdvertDetails", function ($scope, $rootScope, $stateParams, AdvertDetails) {

    $scope.AdvertDetails = AdvertDetails.AdvertDetails;
    $scope.Publishers=AdvertDetails.Publishers;
    $scope.Advertiser=AdvertDetails.Advertiser;

    $rootScope.title =  "Advert | Brimav";
    $rootScope.PageHeading = " Advert ";
    $rootScope.SubPageHeading = "Details" ;

}]);
 

/*--==== Confirm Advert  Controller ====--*/

adminAdvertApp.controller('ConfirmAdvertController', ["$scope", "$uibModalInstance", "AdminAdvertService", "$state", "ConfirmationDetails", "SharedMethods", function ($scope, $uibModalInstance, AdminAdvertService, $state, ConfirmationDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ConfirmationDetails.id;
    $scope.title = ConfirmationDetails.headerContent;
    $scope.body = ConfirmationDetails.bodyMessage;
    $scope.action = ConfirmationDetails.buttonText;
    $scope.class = ConfirmationDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertService.confirmadvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advert.all', {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was confirmed successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem confirming <strong> Advert</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);


/*--==== Delete Advert  Controller ====--*/

adminAdvertApp.controller('DeleteAdvertController', ["$scope", "$uibModalInstance", "AdminAdvertService", "$state", "DeleteDetails", "SharedMethods", function ($scope, $uibModalInstance, AdminAdvertService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class = DeleteDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertService.deleteadvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advert.all', {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was deleted successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem deleting <strong> Advert</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);

/*--==== Advertiser Details Controller ====--*/

adminAdvertApp.controller('AdvertiserDetailsController', ["$scope", "AdminAdvertService", "$rootScope", "$stateParams", "Advertiser", function ($scope, AdminAdvertService, $rootScope, $stateParams, Advertiser) {

    $scope.Advertiser = Advertiser.Advertiser;
    $scope.Advertcount = Advertiser.Advertcount;
    $scope.returnState= $stateParams.returnState;
     $scope.returnStatelabel="Back to Advertisers";

    if ($scope.Advertiser.picture == null || $scope.Advertiser.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Advertiser.picture;
        }

    $rootScope.title = $scope.Advertiser.username + " Details | Brimav";
    $rootScope.PageHeading = "Details";
    $rootScope.SubPageHeading = $scope.Advertiser.username ;

}]);


/*--==== Publisher Details Controller ====--*/

adminAdvertApp.controller('PublisherDetailsController', ["$scope", "$state", "$stateParams", "SharedMethods", "AdminAdvertService", "$rootScope", "$stateParams", "PublisherDetails", function ($scope, $state, $stateParams, SharedMethods,AdminAdvertService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Advert";
    $rootScope.activeView="publisherdetails";

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Advert"
    $rootScope.SubPageHeading="Publisher"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisherbasicdetails.picture;
        }

        $scope.reverse=false; // for fa plus and minus control
        $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

    $scope.save=function()
    {
        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };

         var putbalance = AdminAdvertService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            
            $state.go('publisherdetails.account');
            $scope.reverse=false;
            $scope.collapse=true;
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };

}]);


/*--==== Publisher Account Details Controller ====--*/
adminAdvertApp.controller('AccountDetailsController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title=' Publisher Details | Brimav';
    $rootScope.PageHeading="Advert";
    $rootScope.SubPageHeading="Publisher";
    $rootScope.activeView="account";
   
   
}]);


/*--==== Publisher Payments Details Controller ====--*/
adminAdvertApp.controller('PaymentsHomeController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title=' Publisher Details | Brimav';
    $rootScope.PageHeading="Advert";
    $rootScope.SubPageHeading="Publisher";
    $rootScope.activeView="payments";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
}]);

 
/*==================== Advertiser Controller ====================*/
// AdvertiserListController
adminAdvertiserApp.controller('AdvertisersHomeController', ["$scope", "AdminAdvertiserService", "$rootScope", "$uibModal", function ($scope, AdminAdvertiserService, $rootScope, $uibModal) {

    $rootScope.title = "Advertisers | Brimav"
    $rootScope.PageHeading = "Advertisers";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "username";
    $scope.reverse = false;

    loadAllAdvertisers();

    // load all Advertisers
    function loadAllAdvertisers() {
     
        var promiseGetAdvertisers = AdminAdvertiserService.getAllAdvertisers();

        promiseGetAdvertisers.then(function (pl) {
            $scope.Advertisers = pl.data;
        },
        function (errorPl) {
            $scope.error = errorPl;
        })
        .finally(function () {
            $scope.contentLoading = false;
        });
    }
    $scope.searchAdvertiser = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }


    // dectivate a Advertiser
    $scope.deactivateAdvertiser = function (id, username) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deactivateDetails = {
            id : id,
            headerContent: "Deactivate " + username ,
            bodyMessage: "This Advertiser will be deactivated and unable to perform any activities. Are you sure you want to deactivate this Advertiser?",
            buttonText: "Deactivate",
            buttonClass: "btn-danger",
            username:username
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvertiser/dialogbox',
            controller: 'DeactivateAdvertiserController',
            resolve: {
                DeactivateDetails: function () {
                    return deactivateDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };

    // activate a Advertiser
    $scope.activateAdvertiser = function (id, username) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var activateDetails = {
            id: id,
            headerContent: "Activate " + username,
            bodyMessage: 'This Advertiser will be activated and will be regain access to Brimav. Are you sure you want to activate this Advertiser?',
            buttonText: "Activate",
            buttonClass: "btn-adgold", //blue
            username:username
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvertiser/dialogbox',
            controller: 'ActivateAdvertiserController',
            resolve: {
                ActivateDetails: function () {
                    return activateDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });

    };


    /*---delete advertiser---*/
    $scope.deleteAdvertiser=function(id,username)
    {
         jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            id: id,
            headerContent: "Delete " + username,
            bodyMessage: 'This Advertiser will be deleted permanetly from Brimav. Are you sure you want to activate this Advertiser?',
            buttonText: "Delete",
            buttonClass: "btn-danger", 
            username:username
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvertiser/dialogbox',
            controller: 'DeleteAdvertiserController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            
        }, function () {
            
        });

    };

}]);

// AdvertisersDetailsController
adminAdvertiserApp.controller('AdvertiserDetailsController', ["$scope", "AdminAdvertiserService", "$rootScope", "$stateParams", "Advertiser", function ($scope, AdminAdvertiserService, $rootScope, $stateParams, Advertiser) {

    $scope.Advertiser = Advertiser.Advertiser;
    $scope.Advertcount = Advertiser.Advertcount;
    $scope.returnState= $stateParams.returnState;
     $scope.returnStatelabel="Back to Advertisers";

    if ($scope.Advertiser.picture == null || $scope.Advertiser.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Advertiser.picture;
        }

    $rootScope.title = $scope.Advertiser.username + " Details | Brimav";
    $rootScope.PageHeading = "Details";
    $rootScope.SubPageHeading = $scope.Advertiser.username ;


}]);
 



/*--Deactivate Advertiser Controller--*/

adminAdvertiserApp.controller('DeactivateAdvertiserController', ["$scope", "$uibModalInstance", "AdminAdvertiserService", "$state", "DeactivateDetails", "SharedMethods", function ($scope, $uibModalInstance, AdminAdvertiserService, $state, DeactivateDetails, SharedMethods) {

    $scope.deleting = false; //hide loading gif
    $scope.id = DeactivateDetails.id;
    $scope.title = DeactivateDetails.headerContent;
    $scope.body = DeactivateDetails.bodyMessage;
    $scope.action = DeactivateDetails.buttonText;
    $scope.class = DeactivateDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;
        var promiseDeactivate = AdminAdvertiserService.deactivateAdvertiser($scope.id);

        promiseDeactivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advertiser', {}, { reload: true });
            SharedMethods.createSuccessToast('Advertiser <strong>' + DeactivateDetails.username +' </strong>  was deactivated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
            SharedMethods.createErrorToast('Problem deactivating Advertiser <strong>' + DeactivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    }
}]);




/*--Activate Advertiser Controller--*/

adminAdvertiserApp.controller('ActivateAdvertiserController', ["$scope", "$uibModalInstance", "AdminAdvertiserService", "$state", "ActivateDetails", "SharedMethods", function ($scope, $uibModalInstance, AdminAdvertiserService, $state, ActivateDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ActivateDetails.id;
    $scope.title = ActivateDetails.headerContent;
    $scope.body = ActivateDetails.bodyMessage;
    $scope.action = ActivateDetails.buttonText;
    $scope.class = ActivateDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertiserService.activateAdvertiser($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advertiser', {}, { reload: true });
           SharedMethods.createSuccessToast('Advertiser <strong>' + ActivateDetails.username +' </strong>  was activated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem activating Advertiser <strong>' + ActivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);




/*--Delete Advertiser Controller--*/

adminAdvertiserApp.controller('DeleteAdvertiserController', ["$scope", "$uibModalInstance", "AdminAdvertiserService", "$state", "DeleteDetails", "SharedMethods", function ($scope, $uibModalInstance, AdminAdvertiserService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class =DeleteDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertiserService.deleteAdvertiser($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advertiser', {}, { reload: true });
           SharedMethods.createSuccessToast('Advertiser <strong>' + DeleteDetails.username +' </strong>  was deleted successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem deleting Advertiser <strong>' + DeleteDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);

 						/*====---ADMIN COUPON controller===---*/

/*---coupon  home controller---*/

 adminCouponApp.controller('CouponHomeController',["AdminCouponService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(AdminCouponService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$scope.deleting=false;


 	loadCoupon();
 	function loadCoupon()
 	{
 		var getCoupon=AdminCouponService.getAll();
 		getCoupon.then(function(pl)
 		{
 			$scope.Coupons=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting coupon');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletecoupon=function(id)
 	{
        
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Coupon " ,
            bodyMessage: "Are you sure you want to delete this Coupon?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'adminadvert/dialogbox',
 		 	controller:'CouponDeletecontroller',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---coupon  create controller---*/
 adminCouponApp.controller('CouponCreatecontroller',["AdminCouponService", "SharedMethods", "$scope", "$state", "$rootScope", function(AdminCouponService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$rootScope.SubPageHeading='Create';
 	
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newcoupon={
 		'code':$scope.Code,
 		'percent':$scope.Percent,
 		'start':$scope.start.date,
 		'stop':$scope.stop.date,
 		'adverttype':$scope.Type,
 		'name':$scope.Name,
 		'adverttype':$scope.Type
 		}

	 	var postCoupon=AdminCouponService.postCoupon($scope.newcoupon);
	 	postCoupon.then(function(pl)
	 	{
	 		$state.go('coupon',{},{reload:true});
	 		SharedMethods.createSuccessToast('Coupon created succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---coupon  edit controller---*/
 adminCouponApp.controller('CouponEditcontroller',["AdminCouponService", "Coupon", "SharedMethods", "$scope", "$state", "$rootScope", function(AdminCouponService,Coupon, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Coupon=Coupon;
 	$scope.Coupon2={};
	for(var key in Coupon)
 	{
 		$scope.Coupon2[key]=$scope.Coupon[key];
 	}

 

 	

 	$scope.edit=function()
 	{
 		$scope.editing=true

 		$scope.newcoupon={
        'id':$scope.Coupon2.id,
 		'code':$scope.Coupon2.code,
 		'percent':$scope.Coupon2.percent,
 		'start':$scope.Coupon2.start,
 		'stop':$scope.Coupon2.stop,
 		'adverttype':$scope.Coupon2.adverttype,
 		'name':$scope.Coupon2.name
 		}

       
	 	var putCoupon=AdminCouponService.putCoupon($scope.newcoupon);
	 	putCoupon.then(function(pl)
	 	{
            $state.go('coupon',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Coupon edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---coupon  delete controller---*/
 adminCouponApp.controller('CouponDeletecontroller',["AdminCouponService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(AdminCouponService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$rootScope.SubPageHeading='Edit';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteCoupon=AdminCouponService.deleteCoupon($scope.DeleteDetails.id);
 		deleteCoupon.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('coupon', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Coupon deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting coupon');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 }])
 						/*====---ADMIN BANNER TYPES controller===---*/

/*---banner types  home controller---*/

 adminBannerTypeApp.controller('AdminBannerTypeHomeController',["AdminBannerTypeService", "SharedMethods", "$uibModal", "$scope", "$state", "$rootScope", function(AdminBannerTypeService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner types | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$scope.deleting=false;


 	loadBannerTypes();
 	function loadBannerTypes()
 	{
 		var getBannerTypes=AdminBannerTypeService.getAll();
 		getBannerTypes.then(function(pl)
 		{
 			$scope.BannerTypes=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Banner Types');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletebannertype=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete BannerTypes " ,
            bodyMessage: "Are you sure you want to delete this BannerTypes?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'adminadvert/dialogbox',
 		 	controller:'BannerTypeDeletecontroller',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 }])



 /*---Banner type  create controller---*/
  adminBannerTypeApp.controller('BannerTypeCreatecontroller',["AdminBannerTypeService", "SharedMethods", "$scope", "$state", "$rootScope", function(AdminBannerTypeService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner Types | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$rootScope.SubPageHeading='Create';
 	
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newbannertype={
 		'name':$scope.Name,
 		'description':$scope.Description,
 		'size':$scope.Size,
 		};

	 	var postBannerType=AdminBannerTypeService.postBannerType($scope.newbannertype);
	 	postBannerType.then(function(pl)
	 	{
	 		$state.go('bannertype',{},{reload:true});
	 		SharedMethods.createSuccessToast('BannerType created succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 }])


  /*---Banner type  edit controller---*/
 adminBannerTypeApp.controller('BannerTypeEditcontroller',["AdminBannerTypeService", "BannerType", "SharedMethods", "$scope", "$state", "$rootScope", function(AdminBannerTypeService,BannerType, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='BannerType | Brimav';
 	$rootScope.PageHeading='BannerType';
 	$rootScope.SubPageHeading='Edit';
 	$scope.BannerType=BannerType;
 	$scope.BannerType2={};
	for(var key in BannerType)
 	{
 		$scope.BannerType2[key]=$scope.BannerType[key];
 	}

 

 	

 	$scope.edit=function()
 	{

 		$scope.editing=true

 		$scope.newbannertype={
	    'id':$scope.BannerType2.id,
 		'name':$scope.BannerType2.bannertype_name,
 		'description':$scope.BannerType2.description,
 		'size':$scope.BannerType2.size,
 		};



       
	 	var putBannerType=AdminBannerTypeService.putBannerType($scope.newbannertype);
	 	putBannerType.then(function(pl)
	 	{
            $state.go('bannertype',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Banner Type edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 }])


   /*---Banner type  delete controller---*/
 adminBannerTypeApp.controller('BannerTypeDeletecontroller',["AdminBannerTypeService", "$uibModalInstance", "DeleteDetails", "SharedMethods", "$scope", "$state", "$rootScope", function(AdminBannerTypeService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner Types | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteBannerType=AdminBannerTypeService.deleteBannerType($scope.DeleteDetails.id);
 		deleteBannerType.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('bannertype', {}, { reload: true });
	 		SharedMethods.createSuccessToast('BannerType deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Banner Type');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 }])
/*==================== Payment Controller ====================*/
// PaymetListController
adminPaymentApp.controller('PaymentsHomeController', ["$scope", "SharedMethods", "$cookies", "$state", "$stateParams", "AdminPaymentService", "$rootScope", function ($scope,SharedMethods,$cookies, $state, $stateParams, AdminPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "";
    $rootScope.SubPageHeading = Request;
    $scope.viewState=$stateParams.viewState;
    //Set default filter, sort parameters
    

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.Filterview = "request";
    $scope.sortKey = "date";
    $scope.reverse = false;

     $scope.$watch('Filterview', function (filterkey) {
        $state.go('payment.'+filterkey); // Go to the state selected
        $scope.Filterview=filterkey; //set the Filter to the param passed
    });

  

  
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };

    /*--Confirm request as paid--*/
    $scope.confirmPayment=function(id)
    {
        var confirmPayment=AdminPaymentService.confirmPayment(id);
        confirmPayment.then(function()
        {
             $cookies.remove('Publisher_profile');
            $state.go('payment.request',{},{reload:true});
            SharedMethods.createSuccessToast('Successfully');
        },
        function(error)
        {
            SharedMethods.createErrorToast('Error occured');
        })
    };

    /*--Disconfirm request as paid--*/
    $scope.disconfirmPayment=function(id)
    {
        var disconfirmPayment=AdminPaymentService.disconfirmPayment(id);
        disconfirmPayment.then(function()
        {
             $state.go('payment.request',{},{reload:true})
            SharedMethods.createSuccessToast('Succesfully');
        },
        function(error)
        {
            SharedMethods.createErrorToast('Error occured');
        })
    }
}]);


/*--Paid Controller---*/

adminPaymentApp.controller('PaidPaymentController', ["Paid", "$scope", "$state", "$stateParams", "AdminPaymentService", "$rootScope", function (Paid,$scope, $state, $stateParams, AdminPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "Payments";
    $rootScope.SubPageHeading = 'Paid';
    $scope.Payments=Paid;

   
       

}]);



/*--Request Controller---*/

adminPaymentApp.controller('PaymentRequestController', ["Requests", "$scope", "$stateParams", "AdminPaymentService", "$rootScope", function (Requests,$scope, $stateParams, AdminPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "Payments";
    $rootScope.SubPageHeading = 'Request';
    $scope.Requests=Requests;

   
       

}]);





/*--==== Publisher Details Controller ====--*/

adminPaymentApp.controller('PublisherDetailsController', ["$scope", "$state", "$stateParams", "SharedMethods", "AdminPaymentService", "$rootScope", "$stateParams", "PublisherDetails", function ($scope, $state, $stateParams, SharedMethods,AdminPaymentService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Payments";
    $rootScope.activeView="publisherdetails";

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisherbasicdetails.picture;
        }

        $scope.reverse=false; // for fa plus and minus control
        $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

    $scope.save=function()
    {
        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };

         var putbalance = AdminPaymentService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            
            $state.go('publisherdetails.account');
            $scope.reverse=false;
            $scope.collapse=true;
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };

}]);


/*--==== Publisher Account Details Controller ====--*/
adminPaymentApp.controller('AccountDetailsController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
    $rootScope.activeView="account";
   
   
}]);



/*--==== Publisher Payments Details Controller ====--*/
adminPaymentApp.controller('PublisherPaymentsHomeController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
}]);

 
/*-------========== ADMIN ADVERTS CONTROLLER =======-------*/

/*--==== Adert Template Controller ====--*/

adminPublisherApp.controller('PublisherTemplateController', ["$rootScope", "$uibModal", "$state", "$scope", "Publishers", function ($rootScope, $uibModal, $state, $scope, Publishers) {

    $scope.Publishers = Publishers;
    $scope.contentLoading=false;
   
    $rootScope.headerClass = "normal-page-title";

    //Set default filter, sort parameters
    $scope.Filterview = "all";
    $scope.sortKey = "date";
    $scope.reverse=false;

   
     $scope.$watch('Filterview', function (filterkey) {
        $state.go('publisher.'+filterkey); // Go to the state selected
        $scope.Filterview=filterkey; //set the Filter to the param passed
    });

    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }


    /*-- dectivate a Publisher--*/
    $scope.deactivatePublisher = function (id, username, returnstate) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deactivateDetails = {
            id : id,
            headerContent: "Deactivate " + username ,
            bodyMessage: "This Publisher will be deactivated and unable to perform any activities. Are you sure you want to deactivate this Publisher?",
            buttonText: "Deactivate",
            buttonClass: "btn-danger",
            username:username,
            returnstate:returnstate

        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminpublisher/dialogbox',
            controller: 'DeactivatePublisherController',
            resolve: {
                DeactivateDetails: function () {
                    return deactivateDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };

    /*-- activate a Publisher--*/
    $scope.activatePublisher = function (id, username,returnstate) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var activateDetails = {
            id: id,
            headerContent: "Activate " + username,
            bodyMessage: 'This Publisher will be activated and will be regain access to Brimav. Are you sure you want to activate this Publisher?',
            buttonText: "Activate",
            buttonClass: "btn-adgold", //blue
            username:username,
            returnstate:returnstate
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminpublisher/dialogbox',
            controller: 'ActivatePublisherController',
            resolve: {
                ActivateDetails: function () {
                    return activateDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });

    };


     /*-- delete a Publisher--*/
    $scope.deletePublisher = function (id, username,returnstate) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            id: id,
            headerContent: "Activate " + username,
            bodyMessage: 'This Publisher will be deletd permanetly from Brimav. Are you sure you want to activate this Publisher?',
            buttonText: "Activate",
            buttonClass: "btn-danger",
            username:username,
            returnstate:returnstate
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminpublisher/dialogbox',
            controller: 'DeletePublisherController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });

    };

}]);


    

/*--==== Publisher Home Controller ====--*/

adminPublisherApp.controller('PublisherHomeController', ["$scope", "AdminPublisherService", "$rootScope", "$uibModal", function ($scope, AdminPublisherService, $rootScope, $uibModal) {

    $rootScope.title = "Publishers | Brimav"
    $rootScope.PageHeading = "Publishers";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon  
}]);


/*--==== Active Publisher Controller ====--*/

adminPublisherApp.controller('ActivePublisherController', ["$scope", "ActivePublishers", "AdminPublisherService", "$rootScope", "$uibModal", function ($scope, ActivePublishers, AdminPublisherService, $rootScope, $uibModal) {

    $rootScope.title = "Publishers | Brimav"
    $rootScope.PageHeading = "Active Publishers";
    $rootScope.SubPageHeading = null;

    $rootScope.contentLoading = false; // show loading icon
    $scope.ActivePublishers=ActivePublishers;
    
    
}]);


/*--==== InActive Publisher Controller ====--*/

adminPublisherApp.controller('InactivePublisherController', ["$scope", "InactivePublishers", "AdminPublisherService", "$rootScope", "$uibModal", function ($scope, InactivePublishers, AdminPublisherService, $rootScope, $uibModal) {

    $rootScope.title = "Publishers | Brimav"
    $rootScope.PageHeading = "Inactive Publishers";
    $rootScope.SubPageHeading = null;

    $scope.InactivePublisher=InactivePublishers; 

    }]);

    


/*--==== Publisher Details Controller ====--*/

adminPublisherApp.controller('PublisherDetailsController', ["$scope", "$state", "$stateParams", "SharedMethods", "AdminPublisherService", "$rootScope", "$stateParams", "PublisherDetails", function ($scope, $state, $stateParams, SharedMethods,AdminPublisherService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Publishers";
    $rootScope.detailsView=true;


    $rootScope.title=$scope.Publisherbasicdetails.username + ' Details | Brimav';
    $rootScope.PageHeading="Details"
    $rootScope.SubPageHeading="Basic"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisherbasicdetails.picture;
        }

        

}]);


/*--==== Publisher Account Details Controller ====--*/
adminPublisherApp.controller('AccountDetailsController', ["$scope", "SharedMethods", "$state", "AdminPublisherService", "$stateParams", "$rootScope", function ($scope,SharedMethods, $state,AdminPublisherService, $stateParams, $rootScope) {

    $rootScope.title=$scope.Publisherbasicdetails.username + ' Details | Brimav';
    $rootScope.PageHeading="Details";
    $rootScope.SubPageHeading="Account";
    $rootScope.activeView="account";

    $scope.reverse=false; // for fa plus and minus control
    $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

     $scope.save=function()
    {

        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };
        

         var putbalance = AdminPublisherService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            $scope.reverse=false;
            $scope.collapse=true;
            $state.go('publisherdetails.account');
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };
   
   
}]);



/*--==== Publisher Payments Details Controller ====--*/
adminPublisherApp.controller('PaymentsHomeController', ["$scope", "$stateParams", "$rootScope", function ($scope, $stateParams, $rootScope) {

    $rootScope.title=$scope.Publisherbasicdetails.username + ' Details | Brimav';
    $rootScope.PageHeading="Details";
    $rootScope.SubPageHeading="Payments";
    $rootScope.activeView="payments";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments

    function loadAllPayments() {
     $scope.accountnumber=$scope.Publisheraccountdetails.accountnumber;
     $scope.websitename=$scope.Publisheraccountdetails.websitename;
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
}]);

 
/*--==== Publisher DeactivateController ====--*/

adminPublisherApp.controller('DeactivatePublisherController', ["$scope", "$stateParams", "$uibModalInstance", "AdminPublisherService", "$state", "DeactivateDetails", "SharedMethods", function ($scope, $stateParams, $uibModalInstance, AdminPublisherService, $state, DeactivateDetails, SharedMethods) {

    $scope.deleting = false; //hide loading gif
    $scope.id = DeactivateDetails.id;
    $scope.title = DeactivateDetails.headerContent;
    $scope.body = DeactivateDetails.bodyMessage;
    $scope.action = DeactivateDetails.buttonText;
    $scope.class = DeactivateDetails.buttonClass;
    $scope.returnstate=DeactivateDetails.returnstate;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;
        var promiseDeactivate = AdminPublisherService.deactivatePublisher($scope.id);

        promiseDeactivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('publisher.'+DeactivateDetails.returnstate, {}, { reload: true });
            SharedMethods.createSuccessToast('Publisher <strong>' + DeactivateDetails.username +' </strong>  was deactivated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
            SharedMethods.createErrorToast('Problem deactivating Publisher <strong>' + DeactivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    }
}]);


/*--==== Publisher Activate Controller ====--*/


adminPublisherApp.controller('ActivatePublisherController', ["$scope", "$stateParams", "$uibModalInstance", "AdminPublisherService", "$state", "ActivateDetails", "SharedMethods", function ($scope, $stateParams, $uibModalInstance, AdminPublisherService, $state, ActivateDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ActivateDetails.id;
    $scope.title = ActivateDetails.headerContent;
    $scope.body = ActivateDetails.bodyMessage;
    $scope.action = ActivateDetails.buttonText;
    $scope.class = ActivateDetails.buttonClass;
    $scope.returnstate=ActivateDetails.returnstate;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminPublisherService.activatePublisher($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('publisher.'+$scope.returnstate, {}, { reload: true });
           SharedMethods.createSuccessToast('Publisher <strong>' + ActivateDetails.username +' </strong>  was activated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem activating Publisher <strong>' + ActivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);




/*--====Delete Publisher Controller ====--*/


adminPublisherApp.controller('DeletePublisherController', ["$scope", "$stateParams", "$uibModalInstance", "AdminPublisherService", "$state", "DeleteDetails", "SharedMethods", function ($scope, $stateParams, $uibModalInstance, AdminPublisherService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class = DeleteDetails.buttonClass;
    $scope.returnstate=DeleteDetails.returnstate;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminPublisherService.deletePublisher($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('publisher.'+$scope.returnstate, {}, { reload: true });
           SharedMethods.createSuccessToast('Publisher <strong>' + DeleteDetails.username +' </strong>  deleted successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem deleting Publisher <strong>' + DeleteDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
}]);




/*-----====== ADMIN PROFILE CONTROLLER =====----*/


/*---==== Profile TemplateController =====--*/

adminProfileApp.controller('ProfileTemplateController', ["$rootScope", "$scope", "ShareData", "Admin", function ($rootScope, $scope, ShareData, Admin) {
    $scope.Admin = Admin;
    $rootScope.headerClass = "normal-page-title";

    
}]);


/*---==== Basic Details Controller =====--*/

adminProfileApp.controller('BasicDetailsController', ["$scope", "$rootScope", "ShareData", function ($scope, $rootScope, ShareData) {
    
    $rootScope.title = $scope.Admin.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Basic Details";
    $rootScope.activeView = "basic";
}]);


/*---====Edit Basic Details Controller =====--*/

adminProfileApp.controller("EditBasicProfileController", ["$scope", "$cookies", "AdminProfileService", "SharedMethods", "$rootScope", "$state", function ($scope,$cookies, AdminProfileService, SharedMethods, $rootScope, $state) {
    $scope.Admin2 = {};
    for (var key in $scope.Admin)
        $scope.Admin2[key] = $scope.Admin[key];
    $scope.editing = false;

    $rootScope.title = "Edit Profile | 9tunez";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit Basic Details";
    $rootScope.activeView = "basic";
    $rootScope.PageHeading = "Edit Profile";
 
    $scope.save = function () {
        $scope.editing = true;
        $scope.newAdmindetails = {
            username: $scope.Admin2.username,
            email: $scope.Admin2.email,
            phonenumber: $scope.Admin2.phonenumber
        };

        var PutAdmin = AdminProfileService.putEditadminBasicDetails($scope.newAdmindetails);
        PutAdmin.then(function (pl) {
            $scope.$parent.Admin = $scope.newAdmindetails;
            $cookies.remove('Admin_profile');
            $rootScope.$broadcast("updateShareuserAdmin", $scope.newAdmindetails);

            $state.go('profile.basic', {}, { reload: false });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong> was updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);


 /*---====Change Password Controller =====--*/   

adminProfileApp.controller('ChangePasswordController', ["$scope", "AdminProfileService", "ShareData", "$rootScope", "$state", "SharedMethods", function ($scope, AdminProfileService, ShareData, $rootScope, $state, SharedMethods) {
    $rootScope.title = "Change Password | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.headerClass = "normal-page-title";
    $rootScope.InnerHeading = "Change Password";
    $rootScope.activeView = "password";

    $scope.editing = false;

    $scope.change = function () {
        $scope.editing = true;
    
        $scope.passwordetails = {
            current_password: $scope.Currentpassword,
            password: $scope.Password,
            password_confirmation: $scope.Confirmpassword
        };
    
        var putpassword =AdminProfileService.changepassword($scope.passwordetails)
        putpassword.then(function (pl) {
            $scope.Currentpassword = "";
            $scope.Password = "";
            $scope.Confirmpassword = "";
            $scope.fieldErrors = null;
            SharedMethods.createSuccessToast('<strong>Password</strong> was changed successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
}]);