﻿/*----====== ADMIN PUBLISHERS SERVICE ======----*/

adminPublisherApp.service("AdminPublisherService", function ($http, SharedMethods) {

    var publisherBase = "api/adminpublisherAPI";


    /*--===== Get all Publishers ====--*/
    
    this.getAllPublishers = function () {
       

        return $http.get(publisherBase).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting publishers");
            var publisher = {};
            return publisher;
            });
    };

    /*--===== Get active Publishers ====--*/
    
    this.getActivePublishers = function () {
        return $http.get(publisherBase+'/activepublishers').then(function(response)
            {

                return response.data;

            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting publishers");
            var publisher = {};
            return publisher;
            });
    };

    /*--===== Get Inactive Publishers ====--*/
    
    this.getInactivePublishers = function () {
        return $http.get(publisherBase+'/inactivepublishers').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting publishers");
            var publisher = {};
            return publisher;
            });
    };


    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherDetails = function (id) {
        return $http.get(publisherBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var publisher = {};
            return publisher;
        });
    };

    /*--===== Edit  Publisher Balance ====--*/

    this.putBalance = function (newBalance) {

        var request=$http(
        {   url:publisherBase +'/editpublisherbalance/',
            method:'put',
            data: newBalance
        });
            return request;
    };

    /*--===Deactivate Publisher by ID--==*/ 
    this.deactivatePublisher = function (id) {
        var request = $http({
            method: "get",
            url: publisherBase + "/deactivatepublisher/" + id
        });
        return request;
    };

    /*---===Activate Publisher by ID--===*/

    this.activatePublisher = function (id) {
        var request = $http({
            method: "get",
            url: publisherBase + "/activatepublisher/" + id
        });
        return request;
    };


     /*---===Delete Publisher by ID--===*/
     
    this.deletePublisher = function (id) {
        var request = $http({
            method: "get",
            url: publisherBase + "/deletepublisher/" + id
        });
        return request;
    };
    
});