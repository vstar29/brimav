adminPaymentApp.service('AdminPaymentService', function($http,SharedMethods)
{
var publisherBase="api/adminpublisherAPI";
var payBase="api/adminpaymentAPI";



/*--===== Get  All  Payments ====--*/

    this.getAllPayments = function () {
        return $http.get(payBase).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var Payments = {};
            return Payments;
        });
    };

    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherDetails = function (id) {
        return $http.get(publisherBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var publisher = {};
            return publisher;
        });
    };

    
    /*--===== Edit  Publisher Balance ====--*/

    this.putBalance = function (newBalance) {

        var request=$http(
        {   url:publisherBase +'/editpublisherbalance/',
            method:'put',
            data: newBalance
        });
            return request;
    };


    /*---Confirm request as paid--*/

    this.confirmPayment=function(id)
    {
        return $http.get(payBase+'/confirmpayment/'+id).then(function(response)
        {
            return response.data;
        });
    }

    /*---Disconfirm request as paid--*/

    this.disconfirmPayment=function(id)
    {
        return $http.get(payBase+'/disconfirmpayment/'+id).then(function(response)
        {
            return response.data;
        });
    }
	
});