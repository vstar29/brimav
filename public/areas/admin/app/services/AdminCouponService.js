			/*====---ADMIN COUPON SERVICE--=====*/


adminCouponApp.service('AdminCouponService',function($http,SharedMethods)
{
	var couponBase="api/admincouponAPI";
	  var advertiserBase = "api/adminadvertiserAPI";


	/*--get all  coupon---*/
	this.getAll=function()
	{
		return $http.get(couponBase);
	};


	/*--get  coupon by id---*/
	this.getCouponbyid=function(id)
	{
		
		return $http.get(couponBase +'/couponbyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting coupon details");
			var COUPON={};
			return COUPON;
		})
	};


	/*--create coupon---*/
	this.postCoupon=function(data)
	{
		var request=$http(
			{
				url:couponBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit coupon---*/
	this.putCoupon=function(data)
	{
		var request=$http(
			{
				url:couponBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete coupon---*/

    this.deleteCoupon = function (id) {
        var request=$http(
        {
            url:couponBase+'/couponremove/'+id,
            method:'get'
        });
        return request;
     
   };
})