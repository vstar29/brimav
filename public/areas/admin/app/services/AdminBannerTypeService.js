			/*====---ADMIN BANNER TYPE SERVICE--=====*/


 adminBannerTypeApp.service('AdminBannerTypeService',function($http,SharedMethods)
{
	var bannertypeBase="api/adminbannertypeAPI";
	  //var advertiserBase = "api/adminadvertiserAPI";


	/*--get all  banner types---*/
	this.getAll=function()
	{
		return $http.get(bannertypeBase);
	};


	/*--get  banner type by id---*/
	this.getBannerTypebyid=function(id)
	{
		
		return $http.get(bannertypeBase +'/bannertypebyid/'+id).then(function(pl)
		{
			return pl.data
		},
		function(error)
		{
			SharedMethods.createErrorToast("Problem getting banner type details");
			var BANNERTYPE={};
			return BANNERTYPE;
		})
	};


	/*--create banner type---*/
	this.postBannerType=function(data)
	{
		var request=$http(
			{
				url:bannertypeBase+'/create',
				method:'POST',
				data:data
			});
		return request;
	};


	/*--edit bannertype---*/
	this.putBannerType=function(data)
	{
		var request=$http(
			{
				url:bannertypeBase+'/edit',
				method:'PUT',
				data:data
			});
		return request;
	};


	/*--delete bannetype---*/

    this.deleteBannerType = function (id) {
        var request=$http(
        {
            url:bannertypeBase+'/bannertyperemove/'+id,
            method:'get'
        });
        return request;
     
   };
})