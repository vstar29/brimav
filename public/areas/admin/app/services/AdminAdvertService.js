
 /*----====== ADMIN ADVERTS SERVICE ======----*/

adminAdvertApp.service("AdminAdvertService", function ($http, SharedMethods) {

    var advertBase = "api/adminadvertAPI";
    var pubBase = "api/adminpublisherAPI";
    var advertiserBase = "api/adminadvertiserAPI";



    /*--===== Get all Adverts ====--*/
    
    this.getAllAdverts = function () {
        return $http.get(advertBase).then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get completed Adverts ====--*/
    
    this.getCompletedAdverts = function () {
        return $http.get(advertBase+'/completedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            //SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };

    /*--===== Get Uncompleted Adverts ====--*/
    
    this.getUncompletedAdverts = function () {
        return $http.get(advertBase+'/uncompletedadverts').then(function(response)
            {
                return response.data;
            },
            function(error)
            {
            SharedMethods.createErrorToast("Problem getting adverts");
            var advert = {};
            return advert;
            });
    };


    /*--===== Get  Advert by ID ====--*/

    this.getAdvertDetails = function (id) {
        return $http.get(advertBase +'/advertbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Advert details");
            var advert = {};
            return advert;
        });
    };
    

    /*--===== Get  Publisher by ID ====--*/

    this.getPublisherDetails = function (id) {
        return $http.get(pubBase +'/publisherbyid/' + id).then(function (response) {
            return response.data;
        },

        function (error) {
            SharedMethods.createErrorToast("Problem getting Publisher details");
            var advert = {};
            return advert;
        });
    };

    /*--===== Edit  Publisher Balance ====--*/

    this.putBalance = function (newBalance) {

        var request=$http(
        {   url:pubBase +'/editpublisherbalance/',
            method:'put',
            data: newBalance
        });
            return request;
    };

/*--===== Get  Advertiser by ID ====--*/

    this.getAdvertiserDetails = function (id) {
        return $http.get(advertiserBase +'/advertiserbyid/' + id).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem getting Advertiser details");
            var advert = {};
            return advert;
        });
    };
   
   /*--===== Confirm Advert by ID ====--*/

    this.confirmadvert = function (id) {
        var request = $http({
            method: "put",
            url: advertBase + "/confirmadvert/" + id
        });
        return request;
    };


    /*--===== Delete Advert by ID ====--*/

    this.deleteadvert = function (id) {
        var request = $http({
            method: 'get',
            url: advertBase + "/removeadvert/" + id
        });
        return request;
    };



/*--===== Delete Bulk Advert  ====--*/

    this.deletebulkadvert = function (Bulkids) {
        var request = $http({
            method: 'get',
            url: advertBase + "/removeadvertbulk/"+Bulkids,
           
        });
        return request;
    };
});