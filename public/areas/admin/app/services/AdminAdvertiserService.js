﻿adminAdvertiserApp.service("AdminAdvertiserService", function ($http, SharedMethods) {

    var advertiserBase = "api/adminadvertiserAPI";
    

    /*=============== AdvertiserS ===============*/
    //get all Active Advertisers
    this.getAllAdvertisers = function () {
        return $http.get(advertiserBase);
    };

    //Get Advertiser by name 
    this.getAdvertiser = function (name) {
        return $http.get(advertiserBase +'/advertiserbyusername/' +name).then(function (response) {
            return response.data;
        },
        function (error) {
            SharedMethods.createErrorToast("Problem loading Advertiser information");
            var Advertiser = {};
            return Advertiser;
        });
    };

    //Deactivate Advertiser by ID 

    this.deactivateAdvertiser = function (id) {
        var request = $http({
            method: "get",
            url: advertiserBase + "/deactivateadvertiser/" + id
        });
        return request;
    };

    //Activate Advertiser by ID

    this.activateAdvertiser = function (id) {
        var request = $http({
            method: "get",
            url: advertiserBase + "/activateadvertiser/" + id
        });
        return request;
    };

     //Delete Advertiser by ID 
    this.deleteAdvertiser = function (id) {
        var request = $http({
            method: "get",
            url: advertiserBase + "/deleteadvertiser/" + id
        });
        return request;
    };

});