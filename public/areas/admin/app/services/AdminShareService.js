﻿/*-------========== ADMIN SHARED SERVICE =======-------*/

/*--==== Get  Logged Inuser Details ====--*/

adminShareApp.service('ShareService', function ($http) {
    //Get current admin info
    this.getLoggedInAdmin = function () {
        return $http.get("/api/adminprofileAPI");

    };
})