﻿
/*----====== ADMIN PROFILE SERVICE ======----*/

 
adminProfileApp.service('AdminProfileService', function ($http, SharedMethods) {

    var profileBase = "api/adminprofileAPI";
    

    /*--===== Get  Basic details ====--*/
   
    this.getAdminBasicDetails = function () {
        return $http.get(profileBase).then(function (response) {
          console.log(response.data);
            return response.data;
            
        },
       function (error) {
           SharedMethods.createErrorToast("Problem retreiving your details, kindly try again");
           var admin = {};
           return admin;
       });
    };
    

    /*--===== Edit Basic details ====--*/
  
  this.putEditadminBasicDetails=function(AdminDetails)
  {
    
    var request=$http(
    {
        method:"put",
        url:profileBase +"/editadminbasicdetails",
        data:AdminDetails
    });
    return request;
  };


    /*--===== Change Password ====--*/
    
    this.changepassword = function (Passworddetails) {
      
        var request = $http({
            method: 'put',
            url: profileBase + "/changepassword",
            data: Passworddetails
        });
        return request;
    };

})