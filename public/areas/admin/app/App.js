/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var adminShareApp = angular.module('AdminShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','angularMoment','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {

            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }
            /*for (var key in $scope.fieldErrors) {
                alert(key + ": " + $scope.fieldErrors[key])
            }*/
        } 
        else if($scope.validationErrors.count==1)
        {
           
            SharedMethods.createErrorToast($scope.validationErrors);
        }
        else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }

    /* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
    
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="fa fa-thumbs-up alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning alert-danger margin-right-04"></i>' + cont
        });
    }
    
    /* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }
})
.config(function ($urlRouterProvider, $locationProvider,  $stateProvider,$breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider) {
    //debugger; //break point

    //modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'top',
        horizontalPosition: 'center',
        timeout: 4000,
        maxNumber: 3,
        animation: 'fade',
        dismissButton: true,
    });

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    $breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/admin/home" target="_self">Home&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp;&#xf18e &nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });




    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
    
    //make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    //$urlRouterProvider.otherwise('/404');

    $locationProvider.html5Mode(true);
})
.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})




.run(function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
});




/*===================== HOME SPA ====================*/
var adminHomeApp = angular.module('AdminHomeModule', ['AdminShareModule'])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $interpolateProvider) {
    

    $urlRouterProvider.otherwise('/admin/home');
   
    $stateProvider
    .state('home', {
        url: '/admin/home',
        templateUrl: 'adminhome/home',
        controller: 'HomeController'
    });
})
.run(function ($rootScope) {
    $rootScope.homePage = true;
});







/*===================== ADVERT SPA ========================*/
var adminAdvertApp = angular.module('AdminAdvertModule',['AdminShareModule'])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
  
    $urlRouterProvider.otherwise('/admin/advert');

    $stateProvider
     // Get List of adverts
    .state('advert', {
        url: '/admin/advert',
        abstract:true,
        templateUrl: 'adminadvert/template',
        controller: 'AdvertTemplateController',
        resolve:{
            Adverts:function(AdminAdvertService)
            {
                return AdminAdvertService.getAllAdverts();
            },
            UncompletedAdverts:function(AdminAdvertService)
            {
                return AdminAdvertService.getUncompletedAdverts();
            },

            CompletedAdverts:function(AdminAdvertService)
            {
                return AdminAdvertService.getCompletedAdverts();
            },
        }
        
    })
       

    // Get List of adverts
    .state('advert.all', {
        url: '',
        templateUrl: 'adminadvert/home',
        controller: 'AdvertHomeController',
        
        ncyBreadcrumb: {
            label: 'Adverts'
        },
        resolve:{
            Adverts:function(Adverts)
            {
                return Adverts;
            }
        }
    })

     .state('advert.completed', {
        url: '',
        params: { returnState: "advert" },
        templateUrl: 'adminadvert/completed',
        controller: 'CompletedAdvertController',
        ncyBreadcrumb: {
            label: 'Confirmed',
            parent: 'advert.all'
        },
        resolve:
        {
            CompletedAdverts:function(CompletedAdverts)
            {
                return CompletedAdverts;
            }
        }
        
    })

    .state('advert.uncompleted', {
        url: '/uncompleted',
        params: { returnState: "advert" },
        templateUrl: 'adminadvert/uncompleted',
        controller: 'UncompletedAdvertController',
        ncyBreadcrumb: {
            label: 'Unconfirmed',
            parent: 'advert.all'
        },
        resolve:
        {
            UncompletedAdverts:function(UncompletedAdverts)
            {
                return UncompletedAdverts;
            }
        }
    })
       
    // Get Advert Details
    .state('advertdetails' , {
        url: '/admin/advert/details/:id',
        params: { returnState: "advert" },
        templateUrl: 'adminadvert/details',
        controller: 'AdvertDetailsController',
        ncyBreadcrumb: {
            label: 'Details',
            parent: 'advert.all'
        },
        resolve:
        {
            AdvertDetails:function(AdminAdvertService, $stateParams)
            {
                return AdminAdvertService.getAdvertDetails($stateParams.id);
            }

        }
    })

 
    //Publisher Details Template
    .state('publisherdetails' , {
        url: '/admin/advert/details/publisher/:id?=:username',
        params: {
            returnState: function ($stateParams) {
                return 'advertdetails({id:' + $stateParams.id + '})';
            }
        },
        templateUrl: 'adminpublisher/details',
        controller: 'PublisherDetailsController',
        resolve: {
            PublisherDetails: function ($stateParams, AdminAdvertService) {
                return AdminAdvertService.getPublisherDetails($stateParams.id);
            }
        },
        ncyBreadcrumb: {
            label: ' Publisher',
            parent: 'advertdetails'
        }
    })  


     //Get account Details
    .state('publisherdetails.account' , {
        url: '/account',
        templateUrl: 'adminpublisher/accountdetails',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Publisher',
            parent: 'publisherdetails'
        }
    }) 

     //Get payments Details
    .state('publisherdetails.payments' , {
        url: '/payment',
        params:{viewState:'publisherdetails'},
        templateUrl: 'adminpayment/home',
        controller: 'PaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Publisher',
            parent: 'publisherdetails'
        }
    }) 

    .state('publisherdetails.account.editbalance' , {
        url: '',
        params:{returnState:'publisherdetails.account'},
        templateUrl: 'adminpublisher/editbalance',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Publisher',
            parent: 'publisherdetails.account'
        }
    }) 

    .state('viewadvertiser', {
        url: '/admin/advert/details/advertiser/:id?=:username',
        params: {
            returnState: function ($stateParams) {
                return 'advertdetails({id:' + $stateParams.id + '})';
            }
        },
        templateUrl: 'adminadvertiser/details',
        controller: 'AdvertiserDetailsController',
        resolve: {
            Advertiser: function ($stateParams, AdminAdvertService) {
                return AdminAdvertService.getAdvertiserDetails($stateParams.id);
            }
        },
        ncyBreadcrumb: {
            label: ' Advertiser',
            parent: 'advertdetails'
        }
    })
})
.run(function ($rootScope) {
    $rootScope.advertPage = true;
});


/*===================== ADVERTISER SPA ====================*/
var adminAdvertiserApp = angular.module('AdminAdvertiserModule', ['AdminShareModule'])
.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/admin/advertiser');

    $stateProvider
    //Member's Homepage (Active List)
    .state('advertiser', {
        url: '/admin/advertiser',
        templateUrl: 'adminadvertiser/home',
        controller: 'AdvertisersHomeController',
        ncyBreadcrumb: {
            label: 'Advertiser'
        }
    })

    // Get Advertiser Details
    .state('viewadvertiser', {
        url: '/admin/advertiser/details/{username}',
        params:{returnState:"advertiser"},
        templateUrl: 'adminadvertiser/details',
        controller: 'AdvertiserDetailsController',
       
        resolve: {
            Advertiser: function ($stateParams, AdminAdvertiserService) {
                return AdminAdvertiserService.getAdvertiser($stateParams.username);
            }
        },
        ncyBreadcrumb: {
            label: ' Details',
            parent: 'advertiser'
        }
    });
})
.run(function ($rootScope) {
    $rootScope.advertiserPage = true;
});


/*===================== PUBLISHER SPA ====================*/
var adminPublisherApp = angular.module('AdminPublisherModule', ['AdminShareModule'])
.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/admin/publisher');
    


$stateProvider

     // Publisher Template
    .state('publisher', {
        url: '/admin/publisher',
        abstract:true,
        templateUrl: 'adminpublisher/template',
        controller: 'PublisherTemplateController',
        resolve:{
            Publishers:function(AdminPublisherService)
            {
                return AdminPublisherService.getAllPublishers();
            },
            InactivePublishers:function(AdminPublisherService)
            {
                return AdminPublisherService.getInactivePublishers();
            },

            ActivePublishers:function(AdminPublisherService)
            {
                return AdminPublisherService.getActivePublishers();
            }
        }
        
    })
       
    // Get List of Publishers
    .state('publisher.all', {
        url: '',
        templateUrl: 'adminpublisher/home',
        controller: 'PublisherHomeController',
        
        ncyBreadcrumb: {
            label: 'Publishers'
        },
        resolve:{
            Publishers:function(Publishers)
            {
                return Publishers;
            }
        }
    })

     .state('publisher.active', {
        url: '/active',
        templateUrl: 'adminpublisher/active',
        controller: 'ActivePublisherController',
        ncyBreadcrumb: {
            label: 'Active',
            parent: 'publisher.all'
        },
        resolve:
        {
            ActivePublishers:function(ActivePublishers)
            {
                return ActivePublishers;
            }
        }
        
    })

    .state('publisher.inactive', {
        url: '/inactive',
        templateUrl: 'adminpublisher/inactive',
        controller: 'InactivePublisherController',
        ncyBreadcrumb: {
            label: 'Inactive',
            parent: 'publisher.all'
        },
        resolve:
        {
            InactivePublishers:function(InactivePublishers)
            {
                return InactivePublishers;
            }
        }
    })
       
   //Publisher Details Template
    .state('publisherdetails' , {
        url: '/admin/publisher/details/:id',
        params:{returnState:"publisher.all"},
        templateUrl: 'adminpublisher/details',
        controller: 'PublisherDetailsController',
        resolve:
        {
            PublisherDetails:function(AdminPublisherService, $stateParams)
            {
                return AdminPublisherService.getPublisherDetails($stateParams.id);
            }

        },
        ncyBreadcrumb: {
            label: 'Details',

        }
    })  


     //Get account Details
    .state('publisherdetails.account' , {
        url: '',
        templateUrl: 'adminpublisher/accountdetails',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Account',
            parent: 'publisherdetails'
        }
    }) 

     //Get payments Details
    .state('publisherdetails.payments' , {
        url: '',
        params:{viewState:'publisherdetails'},
        templateUrl: 'adminpublisher/paymentdetails',
        controller: 'PaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Payments',
            parent: 'publisherdetails'
        }
    }) 

    .state('publisherdetails.account.editbalance' , {
        url: '',
        params:{returnState:'publisherdetails.account'},
        templateUrl: 'adminpublisher/editbalance',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Account',
            parent: 'publisherdetails'
        }
    }) 
})
.run(function ($rootScope) {
    $rootScope.publisherPage = true;
});


    /*===================== PROFILE SPA ====================*/
   var adminProfileApp = angular.module('AdminProfileModule', ['AdminShareModule'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/admin/profile');

        
        $stateProvider
        // Template for the Profile Pages
        .state('profile', {
            abstract: true,
            url: '/admin/profile',
            templateUrl: 'adminprofile/home',
            controller: 'ProfileTemplateController',
            resolve: {
                Admin: function (AdminProfileService) {
                    return AdminProfileService.getAdminBasicDetails(); 
                }
            }
        })

        //Profile (Display Admin Profile)
        .state('profile.basic', {
            url: '',
            templateUrl: 'adminprofile/basicdetails',
            controller: 'BasicDetailsController',
            resolve: {
                Admin: function (Admin) {
                    return Admin;
                },

            },
            ncyBreadcrumb: {
            label: 'Profile'
        }
            
        })
       
        // Edit mode for Admin Basic Details
        .state('profile.editbasic', {
            url: '/edit',
            templateUrl: 'adminprofile/editadminbasicdetails',
            controller: 'EditBasicProfileController',
            resolve: {
                Admin: function (Admin) {
                    return Admin;
                }
            },
            ncyBreadcrumb: {
                label: 'Edit Basic Details',
                parent: 'profile.basic'
            }
                
        })

         // Change Password
        .state('profile.changepassword', {
            url: '/security',
            templateUrl: 'adminprofile/changepassword',
            controller: 'ChangePasswordController',
            resolve: {
                Admin: function (Admin) {
                    return Admin;
                }
            },
            ncyBreadcrumb: {
                label: 'Change Password',
                parent: 'profile.basic'
            }
        })  

    })
    .run(function ($rootScope) {
    $rootScope.profilePage = true;
});

        /*===================== PAYMENTS SPA ====================*/
    var adminPaymentApp = angular.module('AdminPaymentModule', ['AdminShareModule'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/admin/payment');

        $stateProvider
        //Payment's Homepage 
        .state('payment', {
            url: '/admin/payment',
            params:{viewState:'publisherdetails'},
            abstract:true,
            templateUrl: 'adminpayment/home',
            controller: 'PaymentsHomeController',
            ncyBreadcrumb: {
                label: 'Payment'
            },
            resolve:
            {
                AllPayments:function(AdminPaymentService)
                {
                    return AdminPaymentService.getAllPayments();
        
                }
            }
        })

          // Get all payment request
    .state('payment.request', {
        url: '',
        templateUrl: 'adminpayment/request',
        controller: 'PaymentRequestController',
        
        ncyBreadcrumb: {
            label: 'Request'
        },
        resolve:
        {
            Requests:function(AllPayments)
            {
                return AllPayments.Requests;
            }
        }
        
    })

         // Get all payment request
    .state('payment.paid', {
        url: '/paid',
        templateUrl: 'adminpayment/paid',
        controller: 'PaidPaymentController',
        
        ncyBreadcrumb: {
            label: 'Paid'
        },
        resolve:
        {
            Paid:function(AllPayments)
            {
                return AllPayments.Payments
            }
        }
        
    })

        
        //Publisher Details Template
    .state('publisherdetails' , {
        url: '/admin/payment/publisher/:id',
        params:{returnState:'payment.request'},
        templateUrl: 'adminpublisher/details',
        controller: 'PublisherDetailsController',
        resolve: {
            PublisherDetails: function ($stateParams, AdminPaymentService) {
                return AdminPaymentService.getPublisherDetails($stateParams.id);
            }
        },
        ncyBreadcrumb: {
            label: ' Publisher',
            parent: 'payment'
        }
    })  


     //Get account Details
    .state('publisherdetails.account' , {
        url: '',
        templateUrl: 'adminpublisher/accountdetails',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Account',
            parent: 'publisherdetails'
        }
    }) 

     //Get payments Details
    .state('publisherdetails.payments' , {
        url: '',
        params:{viewState:'publisherdetails'},
        templateUrl: 'adminpayment/publisherpaymount',
        controller: 'PublisherPaymentsHomeController',
        ncyBreadcrumb: {
            label: 'Payment',
            parent: 'publisherdetails'
        }
    }) 

    .state('publisherdetails.account.editbalance' , {
        url: '',
        params:{returnState:'publisherdetails.account'},
        templateUrl: 'adminpublisher/editbalance',
        controller: 'AccountDetailsController',
        ncyBreadcrumb: {
            label: 'Edit ',
            parent: 'publisherdetails.account'
        }
    }) 

})
.run(function($rootScope)
{
$rootScope.paymentPage=true;
});


/*===================== COUPON SPA ====================*/


var adminCouponApp=angular.module('AdminCouponModule',['AdminShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/admin/coupon');

    $stateProvider.

    state('coupon',
    {
        url:'/admin/coupon',
        templateUrl:'admincoupon/home',
        controller:'CouponHomeController',
        
         ncyBreadcrumb:
        {
            label:'coupon'
        }
    })

    .state('create',{

        url:'/admin/coupon/create',
        templateUrl:'admincoupon/create',
        controller:'CouponCreatecontroller',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'coupon'
        }
    })

    .state('edit',
    {
        url:'/admin/coupon/edit',
        params:{id:null},
        templateUrl:'admincoupon/edit',
        controller:'CouponEditcontroller',
        resolve:
        {
            Coupon:function(AdminCouponService,$stateParams)
            {
                
                return AdminCouponService.getCouponbyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'coupon'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.couponPage=true;
});




/*===================== BANNER TYPE SPA ====================*/


var adminBannerTypeApp=angular.module('AdminBannerTypeModule',['AdminShareModule'])
.config(function($stateProvider,$urlRouterProvider)
{
    $urlRouterProvider.otherwise('/admin/bannertype');

    $stateProvider.

    state('bannertype',
    {
        url:'/admin/bannertype',
        templateUrl:'adminbannertype/home',
        controller:'AdminBannerTypeHomeController',
        
         ncyBreadcrumb:
        {
            label:'bannertype'
        }
    })

    .state('create',{

        url:'/admin/bannertype/create',
        templateUrl:'adminbannertype/create',
        controller:'BannerTypeCreatecontroller',
        ncyBreadcrumb:
        {
            label:'Create',
            parent:'bannertype'
        }
    })

    .state('edit',
    {
        url:'/admin/bannertype/edit',
        params:{id:null},
        templateUrl:'adminbannertype/edit',
        controller:'BannerTypeEditcontroller',
        resolve:
        {
            BannerType:function(AdminBannerTypeService,$stateParams)
            {
                
                return AdminBannerTypeService.getBannerTypebyid($stateParams.id);
            }
        },
        ncyBreadcrumb:
        {
            label:'Edit',
            parent:'bannertype'
        }
    })

})

.run(function($rootScope)
{
    $rootScope.bannerTypePage=true;
});