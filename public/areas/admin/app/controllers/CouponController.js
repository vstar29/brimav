 						/*====---ADMIN COUPON controller===---*/

/*---coupon  home controller---*/

 adminCouponApp.controller('CouponHomeController',function(AdminCouponService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$scope.deleting=false;


 	loadCoupon();
 	function loadCoupon()
 	{
 		var getCoupon=AdminCouponService.getAll();
 		getCoupon.then(function(pl)
 		{
 			$scope.Coupons=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting coupon');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletecoupon=function(id)
 	{
        
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete Coupon " ,
            bodyMessage: "Are you sure you want to delete this Coupon?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'adminadvert/dialogbox',
 		 	controller:'CouponDeletecontroller',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---coupon  create controller---*/
 adminCouponApp.controller('CouponCreatecontroller',function(AdminCouponService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$rootScope.SubPageHeading='Create';
 	
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newcoupon={
 		'code':$scope.Code,
 		'percent':$scope.Percent,
 		'start':$scope.start.date,
 		'stop':$scope.stop.date,
 		'adverttype':$scope.Type,
 		'name':$scope.Name,
 		'adverttype':$scope.Type
 		}

	 	var postCoupon=AdminCouponService.postCoupon($scope.newcoupon);
	 	postCoupon.then(function(pl)
	 	{
	 		$state.go('coupon',{},{reload:true});
	 		SharedMethods.createSuccessToast('Coupon created succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---coupon  edit controller---*/
 adminCouponApp.controller('CouponEditcontroller',function(AdminCouponService,Coupon, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$rootScope.SubPageHeading='Edit';
 	$scope.Coupon=Coupon;
 	$scope.Coupon2={};
	for(var key in Coupon)
 	{
 		$scope.Coupon2[key]=$scope.Coupon[key];
 	}

 

 	

 	$scope.edit=function()
 	{
 		$scope.editing=true

 		$scope.newcoupon={
        'id':$scope.Coupon2.id,
 		'code':$scope.Coupon2.code,
 		'percent':$scope.Coupon2.percent,
 		'start':$scope.Coupon2.start,
 		'stop':$scope.Coupon2.stop,
 		'adverttype':$scope.Coupon2.adverttype,
 		'name':$scope.Coupon2.name
 		}

       
	 	var putCoupon=AdminCouponService.putCoupon($scope.newcoupon);
	 	putCoupon.then(function(pl)
	 	{
            $state.go('coupon',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Coupon edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---coupon  delete controller---*/
 adminCouponApp.controller('CouponDeletecontroller',function(AdminCouponService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Coupon | Brimav';
 	$rootScope.PageHeading='Coupon';
 	$rootScope.SubPageHeading='Edit';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteCoupon=AdminCouponService.deleteCoupon($scope.DeleteDetails.id);
 		deleteCoupon.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('coupon', {}, { reload: true });
	 		SharedMethods.createSuccessToast('Coupon deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting coupon');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 })