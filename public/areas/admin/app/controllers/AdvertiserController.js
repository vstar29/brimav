/*==================== Advertiser Controller ====================*/
// AdvertiserListController
adminAdvertiserApp.controller('AdvertisersHomeController', function ($scope, AdminAdvertiserService, $rootScope, $uibModal) {

    $rootScope.title = "Advertisers | Brimav"
    $rootScope.PageHeading = "Advertisers";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "username";
    $scope.reverse = false;

    loadAllAdvertisers();

    // load all Advertisers
    function loadAllAdvertisers() {
     
        var promiseGetAdvertisers = AdminAdvertiserService.getAllAdvertisers();

        promiseGetAdvertisers.then(function (pl) {
            $scope.Advertisers = pl.data;
        },
        function (errorPl) {
            $scope.error = errorPl;
        })
        .finally(function () {
            $scope.contentLoading = false;
        });
    }
    $scope.searchAdvertiser = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }


    // dectivate a Advertiser
    $scope.deactivateAdvertiser = function (id, username) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deactivateDetails = {
            id : id,
            headerContent: "Deactivate " + username ,
            bodyMessage: "This Advertiser will be deactivated and unable to perform any activities. Are you sure you want to deactivate this Advertiser?",
            buttonText: "Deactivate",
            buttonClass: "btn-danger",
            username:username
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvertiser/dialogbox',
            controller: 'DeactivateAdvertiserController',
            resolve: {
                DeactivateDetails: function () {
                    return deactivateDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };

    // activate a Advertiser
    $scope.activateAdvertiser = function (id, username) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var activateDetails = {
            id: id,
            headerContent: "Activate " + username,
            bodyMessage: 'This Advertiser will be activated and will be regain access to Brimav. Are you sure you want to activate this Advertiser?',
            buttonText: "Activate",
            buttonClass: "btn-adgold", //blue
            username:username
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvertiser/dialogbox',
            controller: 'ActivateAdvertiserController',
            resolve: {
                ActivateDetails: function () {
                    return activateDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });

    };


    /*---delete advertiser---*/
    $scope.deleteAdvertiser=function(id,username)
    {
         jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            id: id,
            headerContent: "Delete " + username,
            bodyMessage: 'This Advertiser will be deleted permanetly from Brimav. Are you sure you want to activate this Advertiser?',
            buttonText: "Delete",
            buttonClass: "btn-danger", 
            username:username
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvertiser/dialogbox',
            controller: 'DeleteAdvertiserController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            
        }, function () {
            
        });

    };

});

// AdvertisersDetailsController
adminAdvertiserApp.controller('AdvertiserDetailsController', function ($scope, AdminAdvertiserService, $rootScope, $stateParams, Advertiser) {

    $scope.Advertiser = Advertiser.Advertiser;
    $scope.Advertcount = Advertiser.Advertcount;
    $scope.returnState= $stateParams.returnState;
     $scope.returnStatelabel="Back to Advertisers";

    if ($scope.Advertiser.picture == null || $scope.Advertiser.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Advertiser.picture;
        }

    $rootScope.title = $scope.Advertiser.username + " Details | Brimav";
    $rootScope.PageHeading = "Details";
    $rootScope.SubPageHeading = $scope.Advertiser.username ;


});
 



/*--Deactivate Advertiser Controller--*/

adminAdvertiserApp.controller('DeactivateAdvertiserController', function ($scope, $uibModalInstance, AdminAdvertiserService, $state, DeactivateDetails, SharedMethods) {

    $scope.deleting = false; //hide loading gif
    $scope.id = DeactivateDetails.id;
    $scope.title = DeactivateDetails.headerContent;
    $scope.body = DeactivateDetails.bodyMessage;
    $scope.action = DeactivateDetails.buttonText;
    $scope.class = DeactivateDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;
        var promiseDeactivate = AdminAdvertiserService.deactivateAdvertiser($scope.id);

        promiseDeactivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advertiser', {}, { reload: true });
            SharedMethods.createSuccessToast('Advertiser <strong>' + DeactivateDetails.username +' </strong>  was deactivated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
            SharedMethods.createErrorToast('Problem deactivating Advertiser <strong>' + DeactivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    }
});




/*--Activate Advertiser Controller--*/

adminAdvertiserApp.controller('ActivateAdvertiserController', function ($scope, $uibModalInstance, AdminAdvertiserService, $state, ActivateDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ActivateDetails.id;
    $scope.title = ActivateDetails.headerContent;
    $scope.body = ActivateDetails.bodyMessage;
    $scope.action = ActivateDetails.buttonText;
    $scope.class = ActivateDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertiserService.activateAdvertiser($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advertiser', {}, { reload: true });
           SharedMethods.createSuccessToast('Advertiser <strong>' + ActivateDetails.username +' </strong>  was activated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem activating Advertiser <strong>' + ActivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});




/*--Delete Advertiser Controller--*/

adminAdvertiserApp.controller('DeleteAdvertiserController', function ($scope, $uibModalInstance, AdminAdvertiserService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class =DeleteDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertiserService.deleteAdvertiser($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advertiser', {}, { reload: true });
           SharedMethods.createSuccessToast('Advertiser <strong>' + DeleteDetails.username +' </strong>  was deleted successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem deleting Advertiser <strong>' + DeleteDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});
