 						/*====---ADMIN BANNER TYPES controller===---*/

/*---banner types  home controller---*/

 adminBannerTypeApp.controller('AdminBannerTypeHomeController',function(AdminBannerTypeService,SharedMethods, $uibModal,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner types | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$scope.deleting=false;


 	loadBannerTypes();
 	function loadBannerTypes()
 	{
 		var getBannerTypes=AdminBannerTypeService.getAll();
 		getBannerTypes.then(function(pl)
 		{
 			$scope.BannerTypes=pl.data;
 		},
 		function(error)
 		{
 			SharedMethods.createErrorToast('Problem getting Banner Types');
 		})
 	}
 	


 	$scope.create=function()
 	{
 		$state.go('create',{},{reload:true});
 	}

 	$scope.deletebannertype=function(id)
 	{
 		 jQuery("#loadingModalHolder").removeClass('ng-hide');

 		 var deleteDetails = {
            id : id,
            headerContent: "Delete BannerTypes " ,
            bodyMessage: "Are you sure you want to delete this BannerTypes?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };
 		 var modalInstance=$uibModal.open(
 		 {
 		 	templateUrl:'adminadvert/dialogbox',
 		 	controller:'BannerTypeDeletecontroller',
 		 	resolve:
 		 	{
 		 		DeleteDetails:function()
 		 		{
 		 			return deleteDetails;
 		 		}
 		 	}
 		 });
 		 modalInstance.opened.then(function(){
 		 	jQuery("#loadingModalHolder").addClass('ng-hide')
 		 });
 		 modalInstance.result.then(function () {
            
        }, function () {
            
        });

 	};
 })



 /*---Banner type  create controller---*/
  adminBannerTypeApp.controller('BannerTypeCreatecontroller',function(AdminBannerTypeService,SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner Types | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$rootScope.SubPageHeading='Create';
 	
 	

 	$scope.create=function()
 	{
 		$scope.creating=true;
		$scope.newbannertype={
 		'name':$scope.Name,
 		'description':$scope.Description,
 		'size':$scope.Size,
 		};

	 	var postBannerType=AdminBannerTypeService.postBannerType($scope.newbannertype);
	 	postBannerType.then(function(pl)
	 	{
	 		$state.go('bannertype',{},{reload:true});
	 		SharedMethods.createSuccessToast('BannerType created succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})
	 	.finally(function()
	 	{
	 		$scope.creating=false;
	 	});
 	};
 })


  /*---Banner type  edit controller---*/
 adminBannerTypeApp.controller('BannerTypeEditcontroller',function(AdminBannerTypeService,BannerType, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='BannerType | Brimav';
 	$rootScope.PageHeading='BannerType';
 	$rootScope.SubPageHeading='Edit';
 	$scope.BannerType=BannerType;
 	$scope.BannerType2={};
	for(var key in BannerType)
 	{
 		$scope.BannerType2[key]=$scope.BannerType[key];
 	}

 

 	

 	$scope.edit=function()
 	{

 		$scope.editing=true

 		$scope.newbannertype={
	    'id':$scope.BannerType2.id,
 		'name':$scope.BannerType2.bannertype_name,
 		'description':$scope.BannerType2.description,
 		'size':$scope.BannerType2.size,
 		};



       
	 	var putBannerType=AdminBannerTypeService.putBannerType($scope.newbannertype);
	 	putBannerType.then(function(pl)
	 	{
            $state.go('bannertype',{}, {reload:true});
	 		SharedMethods.createSuccessToast('Banner Type edited succesfully');
	 	},
	 	function(error)
	 	{
	 		SharedMethods.showValidationErrors($scope,error);
	 	})

	 	.finally(function()
	 	{
	 		$scope.editing=false;
	 	});
 	};
 	
 	
 })


   /*---Banner type  delete controller---*/
 adminBannerTypeApp.controller('BannerTypeDeletecontroller',function(AdminBannerTypeService,$uibModalInstance,DeleteDetails, SharedMethods,$scope,$state,$rootScope)
 {
 	$rootScope.title='Banner Types | Brimav';
 	$rootScope.PageHeading='Banner Types';
 	$rootScope.SubPageHeading='Delete';
 	$scope.DeleteDetails=DeleteDetails;

 	$scope.title=$scope.DeleteDetails.headerContent;
 	$scope.body=$scope.DeleteDetails.bodyMessage;
 	$scope.action=$scope.DeleteDetails.buttonText;
 	$scope.class=$scope.DeleteDetails.buttonClass


 	

 	$scope.cancel=function()
 	{
 		$uibModalInstance.dismiss()
 	}

 	$scope.confirm=function()
 	{
 		
 		$scope.deleting=true;
 		var deleteBannerType=AdminBannerTypeService.deleteBannerType($scope.DeleteDetails.id);
 		deleteBannerType.then(function(pl)
	 	{
            $uibModalInstance.close();
            $state.go('bannertype', {}, { reload: true });
	 		SharedMethods.createSuccessToast('BannerType deleted succesfully');
	 	},
	 	function(error)
	 	{
            $uibModalInstance.close();
	 		SharedMethods.createErrorToast('Problem deleting Banner Type');
	 	})

	 	.finally(function()
	 	{
	 		$scope.deleting=false;
	 	});
 	};	
 })