/*-------========== ADMIN ADVERTS CONTROLLER =======-------*/

/*--==== Adert Template Controller ====--*/

adminPublisherApp.controller('PublisherTemplateController', function ($rootScope, $uibModal, $state, $scope, Publishers) {

    $scope.Publishers = Publishers;
    $scope.contentLoading=false;
   
    $rootScope.headerClass = "normal-page-title";

    //Set default filter, sort parameters
    $scope.Filterview = "all";
    $scope.sortKey = "date";
    $scope.reverse=false;

   
     $scope.$watch('Filterview', function (filterkey) {
        $state.go('publisher.'+filterkey); // Go to the state selected
        $scope.Filterview=filterkey; //set the Filter to the param passed
    });

    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }


    /*-- dectivate a Publisher--*/
    $scope.deactivatePublisher = function (id, username, returnstate) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deactivateDetails = {
            id : id,
            headerContent: "Deactivate " + username ,
            bodyMessage: "This Publisher will be deactivated and unable to perform any activities. Are you sure you want to deactivate this Publisher?",
            buttonText: "Deactivate",
            buttonClass: "btn-danger",
            username:username,
            returnstate:returnstate

        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminpublisher/dialogbox',
            controller: 'DeactivatePublisherController',
            resolve: {
                DeactivateDetails: function () {
                    return deactivateDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };

    /*-- activate a Publisher--*/
    $scope.activatePublisher = function (id, username,returnstate) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var activateDetails = {
            id: id,
            headerContent: "Activate " + username,
            bodyMessage: 'This Publisher will be activated and will be regain access to Brimav. Are you sure you want to activate this Publisher?',
            buttonText: "Activate",
            buttonClass: "btn-adgold", //blue
            username:username,
            returnstate:returnstate
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminpublisher/dialogbox',
            controller: 'ActivatePublisherController',
            resolve: {
                ActivateDetails: function () {
                    return activateDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });

    };


     /*-- delete a Publisher--*/
    $scope.deletePublisher = function (id, username,returnstate) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            id: id,
            headerContent: "Activate " + username,
            bodyMessage: 'This Publisher will be deletd permanetly from Brimav. Are you sure you want to activate this Publisher?',
            buttonText: "Activate",
            buttonClass: "btn-danger",
            username:username,
            returnstate:returnstate
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminpublisher/dialogbox',
            controller: 'DeletePublisherController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;
                }
            }
        });

        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });

    };

});


    

/*--==== Publisher Home Controller ====--*/

adminPublisherApp.controller('PublisherHomeController', function ($scope, AdminPublisherService, $rootScope, $uibModal) {

    $rootScope.title = "Publishers | Brimav"
    $rootScope.PageHeading = "Publishers";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon  
});


/*--==== Active Publisher Controller ====--*/

adminPublisherApp.controller('ActivePublisherController', function ($scope, ActivePublishers, AdminPublisherService, $rootScope, $uibModal) {

    $rootScope.title = "Publishers | Brimav"
    $rootScope.PageHeading = "Active Publishers";
    $rootScope.SubPageHeading = null;

    $rootScope.contentLoading = false; // show loading icon
    $scope.ActivePublishers=ActivePublishers;
    
    
});


/*--==== InActive Publisher Controller ====--*/

adminPublisherApp.controller('InactivePublisherController', function ($scope, InactivePublishers, AdminPublisherService, $rootScope, $uibModal) {

    $rootScope.title = "Publishers | Brimav"
    $rootScope.PageHeading = "Inactive Publishers";
    $rootScope.SubPageHeading = null;

    $scope.InactivePublisher=InactivePublishers; 

    });

    


/*--==== Publisher Details Controller ====--*/

adminPublisherApp.controller('PublisherDetailsController', function ($scope, $state, $stateParams, SharedMethods,AdminPublisherService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Publishers";
    $rootScope.detailsView=true;


    $rootScope.title=$scope.Publisherbasicdetails.username + ' Details | Brimav';
    $rootScope.PageHeading="Details"
    $rootScope.SubPageHeading="Basic"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisherbasicdetails.picture;
        }

        

});


/*--==== Publisher Account Details Controller ====--*/
adminPublisherApp.controller('AccountDetailsController', function ($scope,SharedMethods, $state,AdminPublisherService, $stateParams, $rootScope) {

    $rootScope.title=$scope.Publisherbasicdetails.username + ' Details | Brimav';
    $rootScope.PageHeading="Details";
    $rootScope.SubPageHeading="Account";
    $rootScope.activeView="account";

    $scope.reverse=false; // for fa plus and minus control
    $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

     $scope.save=function()
    {

        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };
        

         var putbalance = AdminPublisherService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            $scope.reverse=false;
            $scope.collapse=true;
            $state.go('publisherdetails.account');
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };
   
   
});



/*--==== Publisher Payments Details Controller ====--*/
adminPublisherApp.controller('PaymentsHomeController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title=$scope.Publisherbasicdetails.username + ' Details | Brimav';
    $rootScope.PageHeading="Details";
    $rootScope.SubPageHeading="Payments";
    $rootScope.activeView="payments";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments

    function loadAllPayments() {
     $scope.accountnumber=$scope.Publisheraccountdetails.accountnumber;
     $scope.websitename=$scope.Publisheraccountdetails.websitename;
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
});

 
/*--==== Publisher DeactivateController ====--*/

adminPublisherApp.controller('DeactivatePublisherController', function ($scope, $stateParams, $uibModalInstance, AdminPublisherService, $state, DeactivateDetails, SharedMethods) {

    $scope.deleting = false; //hide loading gif
    $scope.id = DeactivateDetails.id;
    $scope.title = DeactivateDetails.headerContent;
    $scope.body = DeactivateDetails.bodyMessage;
    $scope.action = DeactivateDetails.buttonText;
    $scope.class = DeactivateDetails.buttonClass;
    $scope.returnstate=DeactivateDetails.returnstate;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;
        var promiseDeactivate = AdminPublisherService.deactivatePublisher($scope.id);

        promiseDeactivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('publisher.'+DeactivateDetails.returnstate, {}, { reload: true });
            SharedMethods.createSuccessToast('Publisher <strong>' + DeactivateDetails.username +' </strong>  was deactivated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
            SharedMethods.createErrorToast('Problem deactivating Publisher <strong>' + DeactivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    }
});


/*--==== Publisher Activate Controller ====--*/


adminPublisherApp.controller('ActivatePublisherController', function ($scope, $stateParams, $uibModalInstance, AdminPublisherService, $state, ActivateDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ActivateDetails.id;
    $scope.title = ActivateDetails.headerContent;
    $scope.body = ActivateDetails.bodyMessage;
    $scope.action = ActivateDetails.buttonText;
    $scope.class = ActivateDetails.buttonClass;
    $scope.returnstate=ActivateDetails.returnstate;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminPublisherService.activatePublisher($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('publisher.'+$scope.returnstate, {}, { reload: true });
           SharedMethods.createSuccessToast('Publisher <strong>' + ActivateDetails.username +' </strong>  was activated successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem activating Publisher <strong>' + ActivateDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});




/*--====Delete Publisher Controller ====--*/


adminPublisherApp.controller('DeletePublisherController', function ($scope, $stateParams, $uibModalInstance, AdminPublisherService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class = DeleteDetails.buttonClass;
    $scope.returnstate=DeleteDetails.returnstate;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminPublisherService.deletePublisher($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('publisher.'+$scope.returnstate, {}, { reload: true });
           SharedMethods.createSuccessToast('Publisher <strong>' + DeleteDetails.username +' </strong>  deleted successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem deleting Publisher <strong>' + DeleteDetails.username + '</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});



