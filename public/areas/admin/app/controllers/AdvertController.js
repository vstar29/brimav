/*-------========== ADMIN ADVERTS CONTROLLER =======-------*/

/*--==== Adert Template Controller ====--*/

adminAdvertApp.controller('AdvertTemplateController', function ( SharedMethods, AdminAdvertService,$rootScope, $uibModal, $state, $scope, Adverts) {
    $scope.Adverts = Adverts;
   
    $rootScope.headerClass = "normal-page-title";

    //Set default filter, sort parameters
    $scope.Filterview = "all";
    $scope.sortKey = "date";
    $scope.reverse=false;
    $scope.bulkIds=[]; // Array to hold bulk actions key.


   
     $scope.$watch('Filterview', function (filterkey) {
        $state.go('advert.'+filterkey); // Go to the state selected
        $scope.Filterview=filterkey; //set the Filter to the param passed
    });

    $scope.sort = function (keyname) {

        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.multiSelect=function(id)
    {
       
        var indexint=-1;
        angular.forEach($scope.bulkIds,function(advertid,index)
        {
            if(id==advertid)
            {
                indexint=index;
            }
        });



        if(indexint>=0)
        {
            $scope.bulkIds.splice(indexint,1);
        }
        else
        {
            $scope.bulkIds.push(id);
        }
        
    
    }
   



    // confirm advert as complete 

    $scope.confirmadvert = function (id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var confirmationDetails = {
            id : id,
            headerContent: "Confirm Advert " ,
            bodyMessage: "Publishers will be credited. Are you sure you want to confirm this member?",
            buttonText: "",
            buttonClass: "btn-adgold",
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvert/dialogbox',
            controller: 'ConfirmAdvertController',
            resolve: {
                ConfirmationDetails: function () {
                    return confirmationDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };
   

// Delete Advert 

    $scope.deleteadvert = function (id) {
        jQuery("#loadingModalHolder").removeClass('ng-hide');

        var deleteDetails = {
            id : id,
            headerContent: "Delete Advert " ,
            bodyMessage: "Are you sure you want to delete this Advert?",
            buttonText: "Delete",
            buttonClass: "btn-danger",
        };

        var modalInstance = $uibModal.open({
            templateUrl: 'adminadvert/dialogbox',
            controller: 'DeleteAdvertController',
            resolve: {
                DeleteDetails: function () {
                    return deleteDetails;
                }
            }
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
            //$scope.selected = selectedItem;
        }, function () {
            //alert('Modal dismissed at: ' + new Date());
        });
    };
   
});


/*--==== Advert Home Controller ====--*/

adminAdvertApp.controller('AdvertHomeController', function ($scope,$state, SharedMethods, AdminAdvertService, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon

    
    $scope.searchAdverts = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };


    $scope.bulkActions=function()
    {
        if($scope.Bulk=="delete")
        {
            
            var deleteadverts=AdminAdvertService.deletebulkadvert($scope.bulkIds);
            deleteadverts.then(function(pl)
            {
               
                $scope.bulkActions={};
                $state.go('advert.all', {}, { reload: true });
                SharedMethods.createSuccessToast('Adverts deleted successfully');
            },
            function(error)
            {
                SharedMethods.createErrorToast('Problem deleting adverts');
            })
        }
    }
    
})


/*--==== Complete Advert Controller ====--*/

adminAdvertApp.controller('CompletedAdvertController', function ($scope, CompletedAdverts, AdminAdvertService, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Confirmed Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon
    $scope.ConfirmedAdvert=CompletedAdverts;
    
    
});


/*--==== UnComplete Advert Controller ====--*/

adminAdvertApp.controller('UncompletedAdvertController', function ($scope, UncompletedAdverts, AdminAdvertService, $rootScope, $uibModal) {

    $rootScope.title = "Adverts | Brimav"
    $rootScope.PageHeading = "Unconfirmed Adverts";
    $rootScope.SubPageHeading = null;

    $scope.contentLoading = true; // show loading icon
    $scope.UnconfirmedAdvert=UncompletedAdverts;
     
    });

    


/*--==== Advert Details Controller ====--*/

adminAdvertApp.controller('AdvertDetailsController', function ($scope, $rootScope, $stateParams, AdvertDetails) {

    $scope.AdvertDetails = AdvertDetails.AdvertDetails;
    $scope.Publishers=AdvertDetails.Publishers;
    $scope.Advertiser=AdvertDetails.Advertiser;

    $rootScope.title =  "Advert | Brimav";
    $rootScope.PageHeading = " Advert ";
    $rootScope.SubPageHeading = "Details" ;

});
 

/*--==== Confirm Advert  Controller ====--*/

adminAdvertApp.controller('ConfirmAdvertController', function ($scope, $uibModalInstance, AdminAdvertService, $state, ConfirmationDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = ConfirmationDetails.id;
    $scope.title = ConfirmationDetails.headerContent;
    $scope.body = ConfirmationDetails.bodyMessage;
    $scope.action = ConfirmationDetails.buttonText;
    $scope.class = ConfirmationDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertService.confirmadvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advert.all', {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was confirmed successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem confirming <strong> Advert</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});


/*--==== Delete Advert  Controller ====--*/

adminAdvertApp.controller('DeleteAdvertController', function ($scope, $uibModalInstance, AdminAdvertService, $state, DeleteDetails, SharedMethods) {

    $scope.deleting = false;
    $scope.id = DeleteDetails.id;
    $scope.title = DeleteDetails.headerContent;
    $scope.body = DeleteDetails.bodyMessage;
    $scope.action = DeleteDetails.buttonText;
    $scope.class = DeleteDetails.buttonClass;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.confirm = function () {
        SharedMethods.keepModalOpen($scope);
        $scope.deleting = true;

        var promiseActivate = AdminAdvertService.deleteadvert($scope.id);

        promiseActivate.then(function (pl) {
            $uibModalInstance.close();
            $state.go('advert.all', {}, { reload: true });
           SharedMethods.createSuccessToast(' <strong> Advert </strong>  was deleted successfully!');
        },
        function (error) {
            $uibModalInstance.close();
           SharedMethods.createErrorToast('Problem deleting <strong> Advert</strong>. Try again!');
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    };
});

/*--==== Advertiser Details Controller ====--*/

adminAdvertApp.controller('AdvertiserDetailsController', function ($scope, AdminAdvertService, $rootScope, $stateParams, Advertiser) {

    $scope.Advertiser = Advertiser.Advertiser;
    $scope.Advertcount = Advertiser.Advertcount;
    $scope.returnState= $stateParams.returnState;
     $scope.returnStatelabel="Back to Advertisers";

    if ($scope.Advertiser.picture == null || $scope.Advertiser.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Advertiser.picture;
        }

    $rootScope.title = $scope.Advertiser.username + " Details | Brimav";
    $rootScope.PageHeading = "Details";
    $rootScope.SubPageHeading = $scope.Advertiser.username ;

});


/*--==== Publisher Details Controller ====--*/

adminAdvertApp.controller('PublisherDetailsController', function ($scope, $state, $stateParams, SharedMethods,AdminAdvertService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Advert";
    $rootScope.activeView="publisherdetails";

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Advert"
    $rootScope.SubPageHeading="Publisher"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisherbasicdetails.picture;
        }

        $scope.reverse=false; // for fa plus and minus control
        $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

    $scope.save=function()
    {
        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };

         var putbalance = AdminAdvertService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            
            $state.go('publisherdetails.account');
            $scope.reverse=false;
            $scope.collapse=true;
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };

});


/*--==== Publisher Account Details Controller ====--*/
adminAdvertApp.controller('AccountDetailsController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title=' Publisher Details | Brimav';
    $rootScope.PageHeading="Advert";
    $rootScope.SubPageHeading="Publisher";
    $rootScope.activeView="account";
   
   
});


/*--==== Publisher Payments Details Controller ====--*/
adminAdvertApp.controller('PaymentsHomeController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title=' Publisher Details | Brimav';
    $rootScope.PageHeading="Advert";
    $rootScope.SubPageHeading="Publisher";
    $rootScope.activeView="payments";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
});

 