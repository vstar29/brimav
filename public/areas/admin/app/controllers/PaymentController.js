/*==================== Payment Controller ====================*/
// PaymetListController
adminPaymentApp.controller('PaymentsHomeController', function ($scope,SharedMethods,$cookies, $state, $stateParams, AdminPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "";
    $rootScope.SubPageHeading = Request;
    $scope.viewState=$stateParams.viewState;
    //Set default filter, sort parameters
    

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.Filterview = "request";
    $scope.sortKey = "date";
    $scope.reverse = false;

     $scope.$watch('Filterview', function (filterkey) {
        $state.go('payment.'+filterkey); // Go to the state selected
        $scope.Filterview=filterkey; //set the Filter to the param passed
    });

  

  
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };

    /*--Confirm request as paid--*/
    $scope.confirmPayment=function(id)
    {
        var confirmPayment=AdminPaymentService.confirmPayment(id);
        confirmPayment.then(function()
        {
             $cookies.remove('Publisher_profile');
            $state.go('payment.request',{},{reload:true});
            SharedMethods.createSuccessToast('Successfully');
        },
        function(error)
        {
            SharedMethods.createErrorToast('Error occured');
        })
    };

    /*--Disconfirm request as paid--*/
    $scope.disconfirmPayment=function(id)
    {
        var disconfirmPayment=AdminPaymentService.disconfirmPayment(id);
        disconfirmPayment.then(function()
        {
             $state.go('payment.request',{},{reload:true})
            SharedMethods.createSuccessToast('Succesfully');
        },
        function(error)
        {
            SharedMethods.createErrorToast('Error occured');
        })
    }
});


/*--Paid Controller---*/

adminPaymentApp.controller('PaidPaymentController', function (Paid,$scope, $state, $stateParams, AdminPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "Payments";
    $rootScope.SubPageHeading = 'Paid';
    $scope.Payments=Paid;

   
       

});



/*--Request Controller---*/

adminPaymentApp.controller('PaymentRequestController', function (Requests,$scope, $stateParams, AdminPaymentService, $rootScope) {

    $rootScope.title = "Payments | Brimav"
    $rootScope.PageHeading = "Payments";
    $rootScope.SubPageHeading = 'Request';
    $scope.Requests=Requests;

   
       

});





/*--==== Publisher Details Controller ====--*/

adminPaymentApp.controller('PublisherDetailsController', function ($scope, $state, $stateParams, SharedMethods,AdminPaymentService, $rootScope, $stateParams, PublisherDetails) {

    $scope.Publisheraccountdetails = PublisherDetails.Publisheraccountdetails;
    $scope.Publisherbasicdetails = PublisherDetails.Publisherbasicdetails;
    $scope.Publisherbalance = PublisherDetails.Publisherbalance;
    $scope.Publisherpaymentsdetails=PublisherDetails.Publisherpaymentsdetails;
    $scope.returnState=$stateParams.returnState;
    $scope.returnStatelabel="Back to Payments";
    $rootScope.activeView="publisherdetails";

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
   

     if ($scope.Publisherbasicdetails.picture == null || $scope.Publisherbasicdetails.picture === undefined)
        $scope.Picture = "/uploads/images/theme/unknown.jpg";

    else {
        $scope.Picture = $scope.Publisherbasicdetails.picture;
        }

        $scope.reverse=false; // for fa plus and minus control
        $scope.collapse=true; // for edit balance toggle

    $scope.toggle=function()
    {
        
        if($scope.collapse==true)
        {
            $state.go('publisherdetails.account.editbalance');
            $scope.reverse=!$scope.reverse;
            $scope.collapse=!$scope.collapse;
        }
        else
        {
            $state.go('publisherdetails.account');
            $scope.collapse=!$scope.collapse;
            $scope.reverse=!$scope.reverse;
        }
        
        
    };

    $scope.save=function()
    {
        $scope.newBalance=
        {
            id:$scope.Publisherbalance.id,
            publisherid:$scope.Publisheraccountdetails.id,
            balance:$scope.Publisherbalance.balance
        };

         var putbalance = AdminPaymentService.putBalance($scope.newBalance);

         putbalance.then(function (pl) {
            
            $state.go('publisherdetails.account');
            $scope.reverse=false;
            $scope.collapse=true;
            $scope.Publisherbalance=$scope.newBalance;
            SharedMethods.createSuccessToast('successfully!');
        },
        function (error) { 
           $scope.reverse=true;
           $scope.collapse=false;
           SharedMethods.createErrorToast(error.data.balance);
        })
        .finally(function()
        {
            $scope.deleting = false;
        });
    

    };

});


/*--==== Publisher Account Details Controller ====--*/
adminPaymentApp.controller('AccountDetailsController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher"
    $rootScope.activeView="account";
   
   
});



/*--==== Publisher Payments Details Controller ====--*/
adminPaymentApp.controller('PublisherPaymentsHomeController', function ($scope, $stateParams, $rootScope) {

    $rootScope.title='Publisher Details | Brimav';
    $rootScope.PageHeading="Payment"
    $rootScope.SubPageHeading="Publisher";
    $scope.viewState=$stateParams.viewState;
   
   

    $scope.contentLoading = true; // show loading icon

    //Set default sort parameters
    $scope.sortKey = "date";
    $scope.reverse = false;

    loadAllPayments();

    // load allPayments
    function loadAllPayments() {
     
     $scope.Payments=$scope.Publisherpaymentsdetails;
     $scope.contentLoading = false; // show loading icon
    }
    $scope.searchPayment = function (rows) {
        
        return (angular.lowercase(rows.username).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.fullname).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.email).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.gender).indexOf($scope.SearchText || '') !== -1 || angular.lowercase(rows.phoneNumber).indexOf($scope.SearchText || '') !== -1) ;
    };
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
});

 