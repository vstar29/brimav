﻿/*-----====== ADMIN PROFILE CONTROLLER =====----*/


/*---==== Profile TemplateController =====--*/

adminProfileApp.controller('ProfileTemplateController', function ($rootScope, $scope, ShareData, Admin) {
    $scope.Admin = Admin;
    $rootScope.headerClass = "normal-page-title";

    
});


/*---==== Basic Details Controller =====--*/

adminProfileApp.controller('BasicDetailsController', function ($scope, $rootScope, ShareData) {
    
    $rootScope.title = $scope.Admin.username + " | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Basic Details";
    $rootScope.activeView = "basic";
});


/*---====Edit Basic Details Controller =====--*/

adminProfileApp.controller("EditBasicProfileController", function ($scope,$cookies, AdminProfileService, SharedMethods, $rootScope, $state) {
    $scope.Admin2 = {};
    for (var key in $scope.Admin)
        $scope.Admin2[key] = $scope.Admin[key];
    $scope.editing = false;

    $rootScope.title = "Edit Profile | 9tunez";
    $rootScope.PageHeading = null;
    $rootScope.SubPageHeading = null;
    $rootScope.InnerHeading = "Edit Basic Details";
    $rootScope.activeView = "basic";
    $rootScope.PageHeading = "Edit Profile";
 
    $scope.save = function () {
        $scope.editing = true;
        $scope.newAdmindetails = {
            username: $scope.Admin2.username,
            email: $scope.Admin2.email,
            phonenumber: $scope.Admin2.phonenumber
        };

        var PutAdmin = AdminProfileService.putEditadminBasicDetails($scope.newAdmindetails);
        PutAdmin.then(function (pl) {
            $scope.$parent.Admin = $scope.newAdmindetails;
            $cookies.remove('Admin_profile');
            $rootScope.$broadcast("updateShareuserAdmin", $scope.newAdmindetails);

            $state.go('profile.basic', {}, { reload: false });

            //show success message
            SharedMethods.createSuccessToast('<strong>Profile</strong> was updated successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});


 /*---====Change Password Controller =====--*/   

adminProfileApp.controller('ChangePasswordController', function ($scope, AdminProfileService, ShareData, $rootScope, $state, SharedMethods) {
    $rootScope.title = "Change Password | Brimav";
    $rootScope.PageHeading = "Profile";
    $rootScope.SubPageHeading = null;
    $rootScope.headerClass = "normal-page-title";
    $rootScope.InnerHeading = "Change Password";
    $rootScope.activeView = "password";

    $scope.editing = false;

    $scope.change = function () {
        $scope.editing = true;
    
        $scope.passwordetails = {
            current_password: $scope.Currentpassword,
            password: $scope.Password,
            password_confirmation: $scope.Confirmpassword
        };
    
        var putpassword =AdminProfileService.changepassword($scope.passwordetails)
        putpassword.then(function (pl) {
            $scope.Currentpassword = "";
            $scope.Password = "";
            $scope.Confirmpassword = "";
            $scope.fieldErrors = null;
            SharedMethods.createSuccessToast('<strong>Password</strong> was changed successfully!');
        },
        function (error) {
            SharedMethods.showValidationErrors($scope, error);
        })
        .finally(function () {
            $scope.editing = false;
        });
    };
});