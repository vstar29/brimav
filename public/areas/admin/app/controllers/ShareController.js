﻿/*-------========== ADMIN SHARED CONTROLLER =======-------*/

/*--==== Get  Logged In-User Details ====--*/
adminShareApp.controller('AdminShareController', function ($scope, $state, ShareService,$cookies) {

      $scope.Admin=$cookies.getObject('Admin_profile');
    if ($scope.Admin == null || $scope.Admin === undefined)
    {
      loadAdminInfo();
    }
      


    function loadAdminInfo() {
        var promiseGetAdminInfo = ShareService.getLoggedInAdmin();

        promiseGetAdminInfo.then(function (pl) {
            $scope.Admin = pl.data;
            $cookies.putObject('Admin_profile', $scope.Admin);

          
        },
        function (errorPl) {
            $scope.error = errorPl;
            
        });
    }

   

});
