
/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var homeShareApp = angular.module('HomeShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {

            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }
            /*for (var key in $scope.fieldErrors) {
                alert(key + ": " + $scope.fieldErrors[key])
            }*/
        } else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }

	/* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
	
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="fa fa-thumbs-up alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning alert-danger margin-right-04"></i>' + cont
        });
    }

    this.notify = function (cont) {
        var myToastMsg = ngToast.success({
            content:  cont
        });
    }

	
	/* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }
})
.config(function ($urlRouterProvider, $locationProvider, $breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider,$stateProvider) {
    //debugger; //break point

	//modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'top',
        horizontalPosition: 'right',
        timeout: 10000,
        maxNumber: 1,
        animation: 'fade',
        dismissButton: true,
        className: 'homenotify',
    });

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    $breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/" target="_self">Home&nbsp;&nbsp;|&nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp;|&nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });

    
    

    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
	
	//make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    //$urlRouterProvider.otherwise('/404');

    $locationProvider.html5Mode(true);
})

.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})




   

/*====-------INTERNATIONAL TELL PHONE DIRECTIVE*/

    .directive('phonenumber',function($rootScope,$filter)
    {
        return{
            restrict: 'E',
            templateUrl:'home/registration/telltemplate',
            scope:{
                country:'=',
                number:'=',
            },
            controller:function($scope)
            {
                $scope.country ="NG";
                var tell=jQuery('#phonenumber');
                tell.intlTelInput({
                    initialCountry: "ng",
                    onlyCountries: ['ng', 'gh', 'za','tz']
                });   
              jQuery('.country-list').on('click',function()
              {
                var country =tell.intlTelInput("getSelectedCountryData").iso2;
                $scope.country=$filter('uppercase')(country);
                $rootScope.Country=$scope.country;
               });     
            }
        };
    })

.run(function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
});


 /*=============HOME SPA==============*/

var homeApp=angular.module('HomeModule',['HomeShareModule'])

    .config(function($urlRouterProvider, $stateProvider)
    {
      
        $urlRouterProvider.otherwise('/');
        //Home Page
        $stateProvider.state('/',
        { 
            url:'/',
            templateUrl:'home/index',
            controller:'HomeController',

        })
    })

     .run (function($rootScope)
     {
        $rootScope.homePage=true;
     });


     /*SIGN UP MODULE*/

     var regApp=angular.module('RegistrationModule',['HomeShareModule'])

    .config(function($urlRouterProvider, $stateProvider)
    {
       
       
        
        $urlRouterProvider.otherwise('/register');
        $stateProvider.state('signup',

        { 
            url:'/register',
            templateUrl:'home/registration/registration',
            controller:'RegistrationController',
        })

        .state('publisherreg',
        {
            url:'/register/publisher',
            templateUrl:'home/registration/publisher',
            controller:'RegistrationController',
        })

    })

     .run (function($rootScope)
     {
        $rootScope.signupPage=true;
     });


    


    
 