 /*=====----HOME SHARE CONTROLLER----=====*/
homeShareApp.service('HomeService', function($http)
{   
	
	this.putlogin=function(logincredentials)
	{
		var request=$http(
		{
			method:"post",
			url:"api/auth/login",
			headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
			data: $.param(logincredentials)
			
		});
		return request;
	};

	this.getlogout=function()
	{
		return $http.get('api/auth/logout/');
	
	}


	this.requesttoken=function(email)
	{
		var request=$http(
		{
			method:"post",
			url:"api/register/emailverificationtoken",
			data: email
			
		});
		return request;
	}

	this.changepassword=function(details)
	{
		var request=$http(
		{
			method:"put",
			url:"api/register/changepassword",
			data: details
			
		});
		return request;
	}
});