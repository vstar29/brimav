// HOME CONTROLLER
homeShareApp.controller('HomeController',function($scope,$cookies, ShareData, $document, $window, $anchorScroll, $location,$uibModal,$rootScope, SharedMethods, HomeService,$state)
{
    
	$rootScope.title="Brimav | Bridging The Gap";
    $scope.login=false;
    $scope.tokenrequestactive=false;
   
     SharedMethods.notify('<i class="fa fa-credit-card alert-default margin-right-04"></i> Enjoy 10% discount with Coupon <br/> <strong><span class="text-center">ADG-10AW</span></strong>');
	$scope.signin=function()
	{
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        $rootScope.title="Brimav | Bridging The Gap";
        $rootScope.signmode=true;
        $rootScope.signupPage=false;
        $rootScope.loginmode=true;
		


        var modalInstance = $uibModal.open({
            keyboard:false,
            backdrop:'static',
            templateUrl: 'home/logins/login',
            controller: 'LoginController',
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
           
        },
        function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
         
        }, function () {
            
        });
    };

    $scope.contact=function(event)
    {
       
        var old= $location.hash();
        $location.hash('contact');
        $anchorScroll();
        $location.hash(old);
    };

    $scope.about=function()
    {
        var old= $location.hash();
        $location.hash('about');
        $anchorScroll();
        $location.hash(old);
    };

     $scope.how=function()
    {
        var old= $location.hash();
        $location.hash('how');
        $anchorScroll();
        $location.hash(old);
    };



     $scope.normallogin=function()
    {
        $scope.login=true;
       
        $scope.logincredentials={
            username:$scope.username,
            password:$scope.password,
            remember:$scope.remember
            
        };
        
        var putlogincredentials=HomeService.putlogin($scope.logincredentials);
        putlogincredentials.then(function(response)
        {   
            $scope.info=response.data;
            $scope.Avatar=$scope.info.picture;
             if ($scope.info.picture == null || $scope.info.picture === undefined || $scope.info.picture == "")
             {
              $scope.Avatar = "/uploads/images/theme/unknown.jpg";
             }
            SharedMethods.notify(' <img class=" welcome-avatar responsive photo-wrapper"  alt="pics" src="'+$scope.Avatar+'">Welcome '+$scope.info.username);
            
            if($scope.info.userable_type=='Admin')
            {
                $window.location.href="admin/home";
            }

            else if($scope.info.userable_type=='Advertiser')
            {
                $window.location.href="advertiser/home";
            }

             else if($scope.info.userable_type=='Publisher')
            {
                $window.location.href="publisher/home";
            }
            else
            {
                $window.location.href="/";
            }
 
        },

         function (error) {

           
            $scope.signing = false;
            $scope.password="";
            SharedMethods.showValidationErrors($scope, error);
        })
        
        .finally(function () {
            $scope.login = false;
        });

    };

      $scope.recover=function(){
        jQuery('#loginpop').modal('hide');
        $rootScope.signmode=true;
        
        var modalInstance = $uibModal.open({
            templateUrl: 'home/logins/passwordrecovery',
            controller: 'ExtraController',
        });
        modalInstance.opened.then(function () {
           
        });
        modalInstance.result.then(function () {
         
        }, function () {
            
        });

    };


    /*---Initial Sign up Initialization---*/

    $scope.continue=function()
    {
       $scope.initialReg=
       {
        'init_email':$scope.email_init,
        'init_password':$scope.password_init
       }
       
       $cookies.put('init_email', $scope.email_init);
       $cookies.put('init_password', $scope.password_init);
       $cookies.put('init_username', $scope.username_init);
        $window.location.href='/register';

    };

    

    
	
});

   

homeShareApp.controller('LoginController',function($scope,$uibModal,$uibModalInstance, $rootScope,HomeService,SharedMethods, $window )
{
	

	$scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $rootScope.signmode=false;
    };


    

     $scope.confirmlogin=function()
    {
        $scope.login=true;
    	SharedMethods.keepModalOpen($scope);
        
    	$scope.logincredentials={
    		username:$scope.username,
    		password:$scope.password,
    		remember:$scope.remember
    	    
    	};
    	
    	var putlogincredentials=HomeService.putlogin($scope.logincredentials);
    	putlogincredentials.then(function(response)
    	{	
            $scope.info=response.data;
            $uibModalInstance.close();
            if($scope.info.userable_type=='Admin')
            {
                $window.location.href="admin/home";
            }

            else if($scope.info.userable_type=='Advertiser')
            {
                $window.location.href="advertiser/home";
            }

             else if($scope.info.userable_type=='Publisher')
            {
                $window.location.href="publisher/home";
            }
            else
            {
                $window.location.href="/";
            }
 
    	},

    	 function (error) {

    	 	SharedMethods.keepModalOpen($scope);
    	 	$scope.signing = false;
            $scope.password="";
            SharedMethods.showValidationErrors($scope, error);
        })
        
        .finally(function () {
            $scope.login = false;
        });

    };

      $scope.recover=function(){
        $scope.cancel();
        $rootScope.signmode=true;
        
        var modalInstance = $uibModal.open({
            templateUrl: 'home/logins/passwordrecovery',
            controller: 'ExtraController',
        });
        modalInstance.opened.then(function () {
           
        });
        modalInstance.result.then(function () {
         
        }, function () {
            
        });

    };

  

});

homeShareApp.controller('ChangePasswordController',function($scope,SharedMethods,$window,HomeService,$stateParams,$rootScope,HomeService)
{
    $scope.changing=false;
    $rootScope.changepassword=true;
    
    $scope.changepassword=function()
    {
        $scope.changing=true;

        $scope.details={
            'email':$scope.email,
            'password':$scope.password,
            'password_confirmation':$scope.confirmpassword
        }
       

        var changepassword=HomeService.changepassword($scope.details);
        changepassword.then(function()
        {
            SharedMethods.createSuccessToast('Password change successful');
            $window.location.href="/";

        },
        function(error)

        {
           $scope.changing=false;
           SharedMethods.showValidationErrors($scope,error); 
        })
        

        .finally(function()
        {
            $scope.changing=false;
        })


    };

    $scope.cancel=function()
    {
        $window.location.href="/";
    }

    

})



homeShareApp.controller('ExtraController',function($scope, SharedMethods,$uibModal,HomeService,$stateParams,$rootScope,HomeService,$uibModalInstance)
{


    $scope.recoverpassword=function()
    {

        $scope.requesting=true;
       $scope.details={
        'email':$scope.recovermail,
        'type':'password'
       };

        var displayDetails=
            {
                'title':'PASSWORD RECOVERY',
                'body':'Check your email for recovery link'
            };
      
        
        var recover=HomeService.requesttoken($scope.details)
        recover.then(function()
        {

            $scope.close();
            $rootScope.signmode=true;
           var modalInstance = $uibModal.open({
                    templateUrl: 'home/registration/regsuccesfull',
                    controller: 'DisplayController',
                    resolve:
                    {
                       displayDetails: function()

                        {
                            return displayDetails;
                        }
                    },
                });
                modalInstance.opened.then(function () {
                   
                });
                modalInstance.result.then(function () {
                 
                }, function () {
                    
                });

        },
        function(error)
        {

            SharedMethods.showValidationErrors($scope,error);
        })
        .finally(function()
        {
            $scope.requesting=false
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
        $rootScope.signmode=false;
    }

});

homeShareApp.controller('CallBackController', function($scope,$uibModal, $rootScope,HomeService,SharedMethods, $window )
{

     $scope.tokenrequest=function()
    {
        $scope.tokenrequestactive=true;

    };

     $scope.requesttoken=function(type)
    {
        $scope.requesting=true;
        $scope.email={
            'email':$scope.tokenemail,
            'type':type
        };
        var request=HomeService.requesttoken($scope.email);
        request.then(function()
        {
            var displayDetails=
            {
                'title':'PASSWORD RECOVERY',
                'body':'Check your email for recovery link.'
            };
             $scope.requesting = false;
             $rootScope.loginmode=true;
            
            var modalInstance = $uibModal.open({
                backdrop:'static',
                keyboard:false,
                templateUrl: 'home/registration/regsuccesfull',
                controller: 'DisplayController',
                resolve:
                        {
                           displayDetails: function()

                            {
                                return displayDetails;
                            }
                        },
            });
            modalInstance.opened.then(function () {
                
            });
            modalInstance.result.then(function () {
             
            }, function () {
    
            });
        },
        function(error)
        {
            $scope.requesting = false;
            $scope.tokenemail="";
            SharedMethods.showValidationErrors($scope,error);
        })
        .finally(function()
        {
            $scope.requesting=false;
        });
    };


});


homeShareApp.controller('DisplayController',function($scope,$window, displayDetails, SharedMethods,$uibModal,HomeService,$stateParams,$rootScope,HomeService,$uibModalInstance)
{

    $scope.displayDetails=displayDetails;


       $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
        $window.location.href="/";
     }
 });



