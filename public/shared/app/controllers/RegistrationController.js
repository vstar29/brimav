// REG CONTROLLER
regApp.controller('RegistrationController', function($scope,$stateParams,$cookies,ShareData, $document, $filter,$location,SharedMethods,$anchorScroll, $filter,$rootScope, RegistrationService, $state,$uibModal)
{
    $rootScope.changepassword=false;
	$rootScope.title="Brimav | Join";
    $rootScope.signmode=false;
    $scope.tokenrequestactive=false;
    $scope.publisher=false;
    $scope.joining=false;
    $scope.Banks=Banks ;
    $scope.Citys="";
    $scope.Country="NG";
     $scope.City="";
    $scope.Currency="NGN";
 
   

    /*---get init email and password from cookies--*/
    
    
    $scope.Email=$cookies.get('init_email');
    $scope.Password=$cookies.get('init_password');
    $scope.Confirmpassword=$cookies.get('init_password');
    $scope.Username=$cookies.get('init_username');
    
    $cookies.remove('init_email');
    $cookies.remove('init_password');
    $cookies.remove('init_username');
   
  $scope.$watch('Country',function(newcountry)
   {
    if(newcountry=="TZ" || newcountry=="GH" || newcountry=="NG" || newcountry=="ZA")
    {
       
       $scope.Citys=CountryCities[newcountry];
       //$scope.Currency=Countrycurrency[newcountry]; 
    }
    
      
   })
   
   $scope.synccity=function()
   {

   }
   

	$scope.join=function()
	{
        if($scope.Type=="")
        {
            SharedMethods.createErrorToast('Select membership type');
        }
        else
        {

            //$scope.phonenumber=jQuery("#phonenumber").intlTelInput("getNumber");
            $scope.phonenumber=jQuery("#phonenumber").val();
            $scope.joining=true;
            $scope.regDetails={
            username:$scope.Username,
            email:$scope.Email,
            password:$scope.Password,
            password_confirmation:$scope.Confirmpassword,
            city:$scope.City,
            type:$scope.Type,
            phonenumber: $scope.phonenumber,
            country:$rootScope.Country,
            currency:$scope.Currency,
            };
            var putregdentials=RegistrationService.register($scope.regDetails);
            putregdentials.then(function(response)
            {   
                var displayDetails=
            {
                'title':'Successful',
                'body':'Thanks for joining Brimav, check your email for confirmation code.'
            };
                 $scope.joining = false;
                $rootScope.signmode=true;
                var modalInstance = $uibModal.open({
                    backdrop:'static',
                    keyboard:false,
                    templateUrl: 'home/registration/regsuccesfull',
                    controller: 'SuccessController',
                    resolve:
                    {
                       displayDetails: function()

                        {
                            return displayDetails;
                        }
                    },
                });
                modalInstance.opened.then(function () {
                    
                });
                modalInstance.result.then(function () {
                 
                }, function () {
                    
                });
        
            },

             function (error) {

                $scope.validationErrors=error;
                $scope.joining = false;
                $scope.Password="";
                $scope.Confirmpassword="";
                SharedMethods.showValidationErrors($scope, error);
            })
            
            .finally(function () {
                 $scope.joining = false;
            });}
    }

    $scope.tokenrequest=function()
    {
        $scope.tokenrequestactive=true;

    }

    $scope.requesttoken=function(type)
    {
        $scope.requesting=true;
        $scope.email={
            'email':$scope.tokenemail,
            'type':type
        };
        var request=HomeService.requesttoken($scope.email);
        request.then(function()
        {
            var displayDetails=
            {
                'title':'Successful',
                'body':'Check your email for confirmation code.'
            };
             $scope.requesting = false;
             $rootScope.loginmode=true;
            
            var modalInstance = $uibModal.open({
                backdrop:'static',
                keyboard:false,
                templateUrl: 'home/registration/regsuccesfull',
                controller: 'SuccessController',
                resolve:
                        {
                           displayDetails: function()

                            {
                                return displayDetails;
                            }
                        },
            });
            modalInstance.opened.then(function () {
                
            });
            modalInstance.result.then(function () {
             
            }, function () {
    
            });
        },
        function(error)
        {
            $scope.requesting = false;
            $scope.tokenemail="";
            SharedMethods.showValidationErrors($scope,error);
        })
        .finally(function()
        {
            $scope.requesting=false;
        });
    };


});
        
      
regApp.controller('SuccessController', function($scope,$window,displayDetails, $rootScope, $window,$uibModalInstance )
{
    $scope.displayDetails=displayDetails;
    
    $scope.close = function () {

        $uibModalInstance.close();
        $window.location.href="/";
    }; 

});




   
/*========== CITYs ==========*/

    var CountryCities={};
    var Countrycurrency={};
   var Banks=["Eco Bank","First Bank","GT Bank", "UBA","Zenith Bank"];

    CountryCities["TZ"]=["Dares Salaam ", "Mwanza",  "Zanzibar City","Arusha ", "Mbeya", "Morogoro","Tanga" ,  "Dodoma ", "Kigoma" , "Moshi", "Kilimanjaro"];
    CountryCities["NG"]= ["Awka","Abuja", "Asaba",  " Abakaliki","Abeokuta","Ado-Ekiti","Akure", "Bauchi", "Benin City", "Calabar","Damaturu" ,"Ejigbo","Enugu","Gusau", "Gombe", "Owerri", "Dutse",
   "Kaduna","Kano","Katsina","Kebbi", "Lokoja","Ibadan","Zamfara", "Ilorin","Lagos","Lafia","Makurdi", "Maiduguri","Minna","Oshogbo", "Jos","Port Harcourt","Sokoto","Jalingo","Umuahia", "Uyo","Yenagoa","Yola"];

    CountryCities["GH"]= [" Akosombo","Accra","Adenta", "Aflao",  "Agogo","Agona Swedru","Akim Oda",    "Anloga","Asamankese","Ashiaman","Bawku","Berekum","Bolgatanga","Cape Coast", "Dome",
    "Effiakuma","Ejura","Gbawe","Ho","Hohoe","Kintampo","Koforidua","Konongo","Kumasi","Lashibi"," Madina", " Mampong"," Nkawkaw", "Nsawam","Nungua", "Obuasi","Oduponkpehe", "Prestea" ,"Savelugu" ,"Suhum" ,"Sunyani" ,
    "Taifa", "Takoradi","Tamale","Tarkwa", "Techiman","Tema","Teshie", "Wa", "Wenchi", " Winneba"," Yendi"];
    
    CountryCities["ZA"]=["Alice","Butterworth","East London","Graaff-Reinet","Grahamstown","King William’s Town","Mthatha","Port Elizabeth","Queenstown","Uitenhage","Zwelitsha","Bethlehem","Jagersfontein","Kroonstad",
    "Odendaalsrus","Parys","Phuthaditjhaba","Sasolburg","Virginia","Welkom","Benoni","Boksburg","Brakpan","Carletonville","Germiston","Johannesburg","Krugersdorp","Durban","Empangeni","Ladysmith","Newcastle","Pietermaritzburg",
    "Pinetown","Giyani","Lebowakgomo","Musina","Phalaborwa","Emalahleni","Nelspruit","Secunda","Bellville","Cape Town","Constantia","George","Hopefield","Oudtshoorn"];

    Countrycurrency["TZ"]=["USD"];
    Countrycurrency["NG"]=["NGN"];
    Countrycurrency["ZA"]=["USD"];
    Countrycurrency["GH"]=["USD"];




