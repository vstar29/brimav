
/*===================== SHARED SPA ====================*/
// This will contain controllers and services that will be shared amongst the other SPAs
var homeShareApp = angular.module('HomeShareModule', ['ncy-angular-breadcrumb', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ui.router', 'ngToast',
    'ngAnimate', 'ngSanitize','ngCookies'])
.factory("ShareData", function () {
    return { value: 0 }
})
.service('SharedMethods', ["ngToast", "$uibModalStack", function (ngToast, $uibModalStack) {
    /* Utility Functions */
    this.showValidationErrors = function ($scope, error, holder) {
        $scope.validationErrors = [];
        $scope.fieldErrors = {};
        if (error.data && angular.isObject(error.data)) {

            modelErrors = error.data;
            var count = 0;
            for (var key in modelErrors) {
                value = modelErrors[key].toString();
                key = key.toString();
                if (key == "") {
                    $scope.validationErrors.push(value);
                }
                else if (key.indexOf(".ModelValue") != -1) {
                    if (holder != null && holder !== undefined)
                        $scope[holder][key.substr(0, key.indexOf(".ModelValue"))] = value;
                    else $scope[capitalize1(key.substr(0, key.indexOf(".ModelValue")))] = value;
                }
                else {
                    if (key.indexOf(".") != -1) //is in the form "object.property"
                        key = key.substr(key.indexOf(".") + 1);
                    if (value.indexOf(".,") == -1)
                        $scope.fieldErrors[key] = value;
                    else {
                        var arr = value.split(".,");
                        value = "";
                        for (var x in arr) value += arr[x] + "\n";
                        $scope.fieldErrors[key] = value;
                    }
                }
            }
            /*for (var key in $scope.fieldErrors) {
                alert(key + ": " + $scope.fieldErrors[key])
            }*/
        } else {
            $scope.validationErrors.push('Problem occurred.');
        };

    }

	/* Prevent Modal Pop-up from being closed when an operation is active*/
    this.keepModalOpen = function ($scope) {
        $scope.processing = true; // show loading gif
        $uibModalStack.getTop().value.keyboard = false;
        $uibModalStack.getTop().value.backdrop = 'static';
        jQuery('#modal-cancel').attr('disabled', true);
    }
	
    /*Notifications*/
    this.createSuccessToast = function (cont) {
        var myToastMsg = ngToast.success({
            content: '<i class="fa fa-thumbs-up alert-success margin-right-04"></i>'+ cont
        });
    }

    this.createErrorToast = function (cont) {
        var myToastMsg = ngToast.danger({
            content: '<i class="fa fa-warning alert-danger margin-right-04"></i>' + cont
        });
    }

    this.notify = function (cont) {
        var myToastMsg = ngToast.success({
            content:  cont
        });
    }

	
	/* Dismiss all notifications */
    this.dismissToasts = function (){
        ngToast.dismiss();
    }

    function capitalize1(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }
}])
.config(["$urlRouterProvider", "$locationProvider", "$breadcrumbProvider", "ngToastProvider", "$uibModalProvider", "$urlMatcherFactoryProvider", "$interpolateProvider", "$stateProvider", function ($urlRouterProvider, $locationProvider, $breadcrumbProvider, ngToastProvider, $uibModalProvider, $urlMatcherFactoryProvider,$interpolateProvider,$stateProvider) {
    //debugger; //break point

	//modal pop-up
    $uibModalProvider.options = {
        animation: true,
        backdrop: true,
        keyboard: true
    };

    //toast notifications
    ngToastProvider.configure({
        verticalPosition: 'top',
        horizontalPosition: 'right',
        timeout: 10000,
        maxNumber: 1,
        animation: 'fade',
        dismissButton: true,
        className: 'homenotify',
    });

    //change of angular js tag setup

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

   //breadcrumb setup
    $breadcrumbProvider.setOptions({
        template: '<div class="breadcrumb">'
                    + '<span><a href="/" target="_self">Home&nbsp;&nbsp;|&nbsp;&nbsp;</a></span>'
                    + '<span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">'
                        + '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}&nbsp;&nbsp;|&nbsp;&nbsp;</a>'
                        + '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>'
                    + '</span>'
                  + '</div>'
    });

    
    

    // force lowercase links
    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            $location.replace().path(normalized);
        }
    });
	
	//make links case insensitive
    /*$urlMatcherFactoryProvider.caseInsensitive(true);
    $urlMatcherFactoryProvider.strictMode(false);*/

    //Redirect to 404 page if route is not found
    //$urlRouterProvider.otherwise('/404');

    $locationProvider.html5Mode(true);
}])

.directive('autoComplete', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            // elem is a jquery lite object if jquery is not present,
            // but with jquery and jquery ui, it will be a full jquery object.
            elem.autocomplete({
                valueKey: 'name',
                titleKey: 'name',
                source: [scope.AutoCompleteSource], //from your service
                openOnFocus: true
            }).on('selected.xdsoft', function (e, datum) {
                scope.AutoCompleteSelect = datum;
            });
        }
    };
})




   

/*====-------INTERNATIONAL TELL PHONE DIRECTIVE*/

    .directive('phonenumber',["$rootScope", "$filter", function($rootScope,$filter)
    {
        return{
            restrict: 'E',
            templateUrl:'home/registration/telltemplate',
            scope:{
                country:'=',
                number:'=',
            },
            controller:["$scope", function($scope)
            {
                $scope.country ="NG";
                var tell=jQuery('#phonenumber');
                tell.intlTelInput({
                    initialCountry: "ng",
                    onlyCountries: ['ng', 'gh', 'za','tz']
                });   
              jQuery('.country-list').on('click',function()
              {
                var country =tell.intlTelInput("getSelectedCountryData").iso2;
                $scope.country=$filter('uppercase')(country);
                $rootScope.Country=$scope.country;
               });     
            }]
        };
    }])

.run(["$rootScope", "SharedMethods", function ($rootScope, SharedMethods) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Show a loading message until promises aren't resolved
        if (toState.resolve) {
            if (toState["name"].indexOf(".") != -1 && fromState["name"].indexOf(".") != -1) {
                $rootScope.loadingNestedView = true;
            }

            else $rootScope.loadingView = true;
        }

        //Remove all showing notifications
        SharedMethods.dismissToasts();
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState, fromStateParams) {
        // Hide loading message
        if (toState.resolve) {
            $rootScope.loadingView = false;
            $rootScope.loadingNestedView = false;
        }

        if (toState["name"].indexOf(".") == -1 || fromState["name"].indexOf(".") == -1) {
            jQuery(document).scrollTop(0);
        }
    });
}]);


 /*=============HOME SPA==============*/

var homeApp=angular.module('HomeModule',['HomeShareModule'])

    .config(["$urlRouterProvider", "$stateProvider", function($urlRouterProvider, $stateProvider)
    {
      
        $urlRouterProvider.otherwise('/');
        //Home Page
        $stateProvider.state('/',
        { 
            url:'/',
            templateUrl:'home/index',
            controller:'HomeController',

        })
    }])

     .run (["$rootScope", function($rootScope)
     {
        $rootScope.homePage=true;
     }]);


     /*SIGN UP MODULE*/

     var regApp=angular.module('RegistrationModule',['HomeShareModule'])

    .config(["$urlRouterProvider", "$stateProvider", function($urlRouterProvider, $stateProvider)
    {
       
       
        
        $urlRouterProvider.otherwise('/register');
        $stateProvider.state('signup',

        { 
            url:'/register',
            templateUrl:'home/registration/registration',
            controller:'RegistrationController',
        })

        .state('publisherreg',
        {
            url:'/register/publisher',
            templateUrl:'home/registration/publisher',
            controller:'RegistrationController',
        })

    }])

     .run (["$rootScope", function($rootScope)
     {
        $rootScope.signupPage=true;
     }]);


    


    
 
 /*=====----HOME SHARE CONTROLLER----=====*/
homeShareApp.service('HomeService', ["$http", function($http)
{   
	
	this.putlogin=function(logincredentials)
	{
		var request=$http(
		{
			method:"post",
			url:"api/auth/login",
			headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
			data: $.param(logincredentials)
			
		});
		return request;
	};

	this.getlogout=function()
	{
		return $http.get('api/auth/logout/');
	
	}


	this.requesttoken=function(email)
	{
		var request=$http(
		{
			method:"post",
			url:"api/register/emailverificationtoken",
			data: email
			
		});
		return request;
	}

	this.changepassword=function(details)
	{
		var request=$http(
		{
			method:"put",
			url:"api/register/changepassword",
			data: details
			
		});
		return request;
	}
}]);
// HOME CONTROLLER
homeShareApp.controller('HomeController',["$scope", "$cookies", "ShareData", "$document", "$window", "$anchorScroll", "$location", "$uibModal", "$rootScope", "SharedMethods", "HomeService", "$state", function($scope,$cookies, ShareData, $document, $window, $anchorScroll, $location,$uibModal,$rootScope, SharedMethods, HomeService,$state)
{
    
	$rootScope.title="Brimav | Bridging The Gap";
    $scope.login=false;
    $scope.tokenrequestactive=false;
   
     SharedMethods.notify('<i class="fa fa-credit-card alert-default margin-right-04"></i> Enjoy 10% discount with Coupon <br/> <strong><span class="text-center">ADG-10AW</span></strong>');
	$scope.signin=function()
	{
        jQuery("#loadingModalHolder").removeClass('ng-hide');
        $rootScope.title="Brimav | Bridging The Gap";
        $rootScope.signmode=true;
        $rootScope.signupPage=false;
        $rootScope.loginmode=true;
		


        var modalInstance = $uibModal.open({
            keyboard:false,
            backdrop:'static',
            templateUrl: 'home/logins/login',
            controller: 'LoginController',
        });
        modalInstance.opened.then(function () {
            jQuery("#loadingModalHolder").addClass('ng-hide');
           
        },
        function(error)
        {
            jQuery("#loadingModalHolder").addClass('ng-hide');
        });
        modalInstance.result.then(function () {
         
        }, function () {
            
        });
    };

    $scope.contact=function(event)
    {
       
        var old= $location.hash();
        $location.hash('contact');
        $anchorScroll();
        $location.hash(old);
    };

    $scope.about=function()
    {
        var old= $location.hash();
        $location.hash('about');
        $anchorScroll();
        $location.hash(old);
    };

     $scope.how=function()
    {
        var old= $location.hash();
        $location.hash('how');
        $anchorScroll();
        $location.hash(old);
    };



     $scope.normallogin=function()
    {
        $scope.login=true;
       
        $scope.logincredentials={
            username:$scope.username,
            password:$scope.password,
            remember:$scope.remember
            
        };
        
        var putlogincredentials=HomeService.putlogin($scope.logincredentials);
        putlogincredentials.then(function(response)
        {   
            $scope.info=response.data;
            $scope.Avatar=$scope.info.picture;
             if ($scope.info.picture == null || $scope.info.picture === undefined || $scope.info.picture == "")
             {
              $scope.Avatar = "/uploads/images/theme/unknown.jpg";
             }
            SharedMethods.notify(' <img class=" welcome-avatar responsive photo-wrapper"  alt="pics" src="'+$scope.Avatar+'">Welcome '+$scope.info.username);
            
            if($scope.info.userable_type=='Admin')
            {
                $window.location.href="admin/home";
            }

            else if($scope.info.userable_type=='Advertiser')
            {
                $window.location.href="advertiser/home";
            }

             else if($scope.info.userable_type=='Publisher')
            {
                $window.location.href="publisher/home";
            }
            else
            {
                $window.location.href="/";
            }
 
        },

         function (error) {

           
            $scope.signing = false;
            $scope.password="";
            SharedMethods.showValidationErrors($scope, error);
        })
        
        .finally(function () {
            $scope.login = false;
        });

    };

      $scope.recover=function(){
        jQuery('#loginpop').modal('hide');
        $rootScope.signmode=true;
        
        var modalInstance = $uibModal.open({
            templateUrl: 'home/logins/passwordrecovery',
            controller: 'ExtraController',
        });
        modalInstance.opened.then(function () {
           
        });
        modalInstance.result.then(function () {
         
        }, function () {
            
        });

    };


    /*---Initial Sign up Initialization---*/

    $scope.continue=function()
    {
       $scope.initialReg=
       {
        'init_email':$scope.email_init,
        'init_password':$scope.password_init
       }
       
       $cookies.put('init_email', $scope.email_init);
       $cookies.put('init_password', $scope.password_init);
       $cookies.put('init_username', $scope.username_init);
        $window.location.href='/register';

    };

    

    
	
}]);

   

homeShareApp.controller('LoginController',["$scope", "$uibModal", "$uibModalInstance", "$rootScope", "HomeService", "SharedMethods", "$window", function($scope,$uibModal,$uibModalInstance, $rootScope,HomeService,SharedMethods, $window )
{
	

	$scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $rootScope.signmode=false;
    };


    

     $scope.confirmlogin=function()
    {
        $scope.login=true;
    	SharedMethods.keepModalOpen($scope);
        
    	$scope.logincredentials={
    		username:$scope.username,
    		password:$scope.password,
    		remember:$scope.remember
    	    
    	};
    	
    	var putlogincredentials=HomeService.putlogin($scope.logincredentials);
    	putlogincredentials.then(function(response)
    	{	
            $scope.info=response.data;
            $uibModalInstance.close();
            if($scope.info.userable_type=='Admin')
            {
                $window.location.href="admin/home";
            }

            else if($scope.info.userable_type=='Advertiser')
            {
                $window.location.href="advertiser/home";
            }

             else if($scope.info.userable_type=='Publisher')
            {
                $window.location.href="publisher/home";
            }
            else
            {
                $window.location.href="/";
            }
 
    	},

    	 function (error) {

    	 	SharedMethods.keepModalOpen($scope);
    	 	$scope.signing = false;
            $scope.password="";
            SharedMethods.showValidationErrors($scope, error);
        })
        
        .finally(function () {
            $scope.login = false;
        });

    };

      $scope.recover=function(){
        $scope.cancel();
        $rootScope.signmode=true;
        
        var modalInstance = $uibModal.open({
            templateUrl: 'home/logins/passwordrecovery',
            controller: 'ExtraController',
        });
        modalInstance.opened.then(function () {
           
        });
        modalInstance.result.then(function () {
         
        }, function () {
            
        });

    };

  

}]);

homeShareApp.controller('ChangePasswordController',["$scope", "SharedMethods", "$window", "HomeService", "$stateParams", "$rootScope", "HomeService", function($scope,SharedMethods,$window,HomeService,$stateParams,$rootScope,HomeService)
{
    $scope.changing=false;
    $rootScope.changepassword=true;
    
    $scope.changepassword=function()
    {
        $scope.changing=true;

        $scope.details={
            'email':$scope.email,
            'password':$scope.password,
            'password_confirmation':$scope.confirmpassword
        }
       

        var changepassword=HomeService.changepassword($scope.details);
        changepassword.then(function()
        {
            SharedMethods.createSuccessToast('Password change successful');
            $window.location.href="/";

        },
        function(error)

        {
           $scope.changing=false;
           SharedMethods.showValidationErrors($scope,error); 
        })
        

        .finally(function()
        {
            $scope.changing=false;
        })


    };

    $scope.cancel=function()
    {
        $window.location.href="/";
    }

    

}])



homeShareApp.controller('ExtraController',["$scope", "SharedMethods", "$uibModal", "HomeService", "$stateParams", "$rootScope", "HomeService", "$uibModalInstance", function($scope, SharedMethods,$uibModal,HomeService,$stateParams,$rootScope,HomeService,$uibModalInstance)
{


    $scope.recoverpassword=function()
    {

        $scope.requesting=true;
       $scope.details={
        'email':$scope.recovermail,
        'type':'password'
       };

        var displayDetails=
            {
                'title':'PASSWORD RECOVERY',
                'body':'Check your email for recovery link'
            };
      
        
        var recover=HomeService.requesttoken($scope.details)
        recover.then(function()
        {

            $scope.close();
            $rootScope.signmode=true;
           var modalInstance = $uibModal.open({
                    templateUrl: 'home/registration/regsuccesfull',
                    controller: 'DisplayController',
                    resolve:
                    {
                       displayDetails: function()

                        {
                            return displayDetails;
                        }
                    },
                });
                modalInstance.opened.then(function () {
                   
                });
                modalInstance.result.then(function () {
                 
                }, function () {
                    
                });

        },
        function(error)
        {

            SharedMethods.showValidationErrors($scope,error);
        })
        .finally(function()
        {
            $scope.requesting=false
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
        $rootScope.signmode=false;
    }

}]);

homeShareApp.controller('CallBackController', ["$scope", "$uibModal", "$rootScope", "HomeService", "SharedMethods", "$window", function($scope,$uibModal, $rootScope,HomeService,SharedMethods, $window )
{

     $scope.tokenrequest=function()
    {
        $scope.tokenrequestactive=true;

    };

     $scope.requesttoken=function(type)
    {
        $scope.requesting=true;
        $scope.email={
            'email':$scope.tokenemail,
            'type':type
        };
        var request=HomeService.requesttoken($scope.email);
        request.then(function()
        {
            var displayDetails=
            {
                'title':'PASSWORD RECOVERY',
                'body':'Check your email for recovery link.'
            };
             $scope.requesting = false;
             $rootScope.loginmode=true;
            
            var modalInstance = $uibModal.open({
                backdrop:'static',
                keyboard:false,
                templateUrl: 'home/registration/regsuccesfull',
                controller: 'DisplayController',
                resolve:
                        {
                           displayDetails: function()

                            {
                                return displayDetails;
                            }
                        },
            });
            modalInstance.opened.then(function () {
                
            });
            modalInstance.result.then(function () {
             
            }, function () {
    
            });
        },
        function(error)
        {
            $scope.requesting = false;
            $scope.tokenemail="";
            SharedMethods.showValidationErrors($scope,error);
        })
        .finally(function()
        {
            $scope.requesting=false;
        });
    };


}]);


homeShareApp.controller('DisplayController',["$scope", "$window", "displayDetails", "SharedMethods", "$uibModal", "HomeService", "$stateParams", "$rootScope", "HomeService", "$uibModalInstance", function($scope,$window, displayDetails, SharedMethods,$uibModal,HomeService,$stateParams,$rootScope,HomeService,$uibModalInstance)
{

    $scope.displayDetails=displayDetails;


       $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
        $window.location.href="/";
     }
 }]);





regApp.service('RegistrationService', ["$http", function($http)
{   
	
	this.register=function(details)
	{
		var request=$http(
		{
			method:"post",
			url:"api/register/register",
			data: details
			
		});
		return request;
	};

	

	/*---for publisher registration--*/

	this.completereg=function(detials)
	{
		var request=$http(
		{
			method:'post',
			url:'api/register/completereg',
			data:detials
		});
		return request;
	};

	
}]);
// REG CONTROLLER
regApp.controller('RegistrationController', ["$scope", "$stateParams", "$cookies", "ShareData", "$document", "$filter", "$location", "SharedMethods", "$anchorScroll", "$filter", "$rootScope", "RegistrationService", "$state", "$uibModal", function($scope,$stateParams,$cookies,ShareData, $document, $filter,$location,SharedMethods,$anchorScroll, $filter,$rootScope, RegistrationService, $state,$uibModal)
{
    $rootScope.changepassword=false;
	$rootScope.title="Brimav | Join";
    $rootScope.signmode=false;
    $scope.tokenrequestactive=false;
    $scope.publisher=false;
    $scope.joining=false;
    $scope.Banks=Banks ;
    $scope.Citys="";
    $scope.Country="NG";
     $scope.City="";
    $scope.Currency="NGN";
 
   

    /*---get init email and password from cookies--*/
    
    
    $scope.Email=$cookies.get('init_email');
    $scope.Password=$cookies.get('init_password');
    $scope.Confirmpassword=$cookies.get('init_password');
    $scope.Username=$cookies.get('init_username');
    
    $cookies.remove('init_email');
    $cookies.remove('init_password');
    $cookies.remove('init_username');
   
  $scope.$watch('Country',function(newcountry)
   {
    if(newcountry=="TZ" || newcountry=="GH" || newcountry=="NG" || newcountry=="ZA")
    {
       
       $scope.Citys=CountryCities[newcountry];
       //$scope.Currency=Countrycurrency[newcountry]; 
    }
    
      
   })
   
   $scope.synccity=function()
   {

   }
   

	$scope.join=function()
	{
        if($scope.Type=="")
        {
            SharedMethods.createErrorToast('Select membership type');
        }
        else
        {

            //$scope.phonenumber=jQuery("#phonenumber").intlTelInput("getNumber");
            $scope.phonenumber=jQuery("#phonenumber").val();
            $scope.joining=true;
            $scope.regDetails={
            username:$scope.Username,
            email:$scope.Email,
            password:$scope.Password,
            password_confirmation:$scope.Confirmpassword,
            city:$scope.City,
            type:$scope.Type,
            phonenumber: $scope.phonenumber,
            country:$rootScope.Country,
            currency:$scope.Currency,
            };
            var putregdentials=RegistrationService.register($scope.regDetails);
            putregdentials.then(function(response)
            {   
                var displayDetails=
            {
                'title':'Successful',
                'body':'Thanks for joining Brimav, check your email for confirmation code.'
            };
                 $scope.joining = false;
                $rootScope.signmode=true;
                var modalInstance = $uibModal.open({
                    backdrop:'static',
                    keyboard:false,
                    templateUrl: 'home/registration/regsuccesfull',
                    controller: 'SuccessController',
                    resolve:
                    {
                       displayDetails: function()

                        {
                            return displayDetails;
                        }
                    },
                });
                modalInstance.opened.then(function () {
                    
                });
                modalInstance.result.then(function () {
                 
                }, function () {
                    
                });
        
            },

             function (error) {

                $scope.validationErrors=error;
                $scope.joining = false;
                $scope.Password="";
                $scope.Confirmpassword="";
                SharedMethods.showValidationErrors($scope, error);
            })
            
            .finally(function () {
                 $scope.joining = false;
            });}
    }

    $scope.tokenrequest=function()
    {
        $scope.tokenrequestactive=true;

    }

    $scope.requesttoken=function(type)
    {
        $scope.requesting=true;
        $scope.email={
            'email':$scope.tokenemail,
            'type':type
        };
        var request=HomeService.requesttoken($scope.email);
        request.then(function()
        {
            var displayDetails=
            {
                'title':'Successful',
                'body':'Check your email for confirmation code.'
            };
             $scope.requesting = false;
             $rootScope.loginmode=true;
            
            var modalInstance = $uibModal.open({
                backdrop:'static',
                keyboard:false,
                templateUrl: 'home/registration/regsuccesfull',
                controller: 'SuccessController',
                resolve:
                        {
                           displayDetails: function()

                            {
                                return displayDetails;
                            }
                        },
            });
            modalInstance.opened.then(function () {
                
            });
            modalInstance.result.then(function () {
             
            }, function () {
    
            });
        },
        function(error)
        {
            $scope.requesting = false;
            $scope.tokenemail="";
            SharedMethods.showValidationErrors($scope,error);
        })
        .finally(function()
        {
            $scope.requesting=false;
        });
    };


}]);
        
      
regApp.controller('SuccessController', ["$scope", "$window", "displayDetails", "$rootScope", "$window", "$uibModalInstance", function($scope,$window,displayDetails, $rootScope, $window,$uibModalInstance )
{
    $scope.displayDetails=displayDetails;
    
    $scope.close = function () {

        $uibModalInstance.close();
        $window.location.href="/";
    }; 

}]);




   
/*========== CITYs ==========*/

    var CountryCities={};
    var Countrycurrency={};
   var Banks=["Eco Bank","First Bank","GT Bank", "UBA","Zenith Bank"];

    CountryCities["TZ"]=["Dares Salaam ", "Mwanza",  "Zanzibar City","Arusha ", "Mbeya", "Morogoro","Tanga" ,  "Dodoma ", "Kigoma" , "Moshi", "Kilimanjaro"];
    CountryCities["NG"]= ["Awka","Abuja", "Asaba",  " Abakaliki","Abeokuta","Ado-Ekiti","Akure", "Bauchi", "Benin City", "Calabar","Damaturu" ,"Ejigbo","Enugu","Gusau", "Gombe", "Owerri", "Dutse",
   "Kaduna","Kano","Katsina","Kebbi", "Lokoja","Ibadan","Zamfara", "Ilorin","Lagos","Lafia","Makurdi", "Maiduguri","Minna","Oshogbo", "Jos","Port Harcourt","Sokoto","Jalingo","Umuahia", "Uyo","Yenagoa","Yola"];

    CountryCities["GH"]= [" Akosombo","Accra","Adenta", "Aflao",  "Agogo","Agona Swedru","Akim Oda",    "Anloga","Asamankese","Ashiaman","Bawku","Berekum","Bolgatanga","Cape Coast", "Dome",
    "Effiakuma","Ejura","Gbawe","Ho","Hohoe","Kintampo","Koforidua","Konongo","Kumasi","Lashibi"," Madina", " Mampong"," Nkawkaw", "Nsawam","Nungua", "Obuasi","Oduponkpehe", "Prestea" ,"Savelugu" ,"Suhum" ,"Sunyani" ,
    "Taifa", "Takoradi","Tamale","Tarkwa", "Techiman","Tema","Teshie", "Wa", "Wenchi", " Winneba"," Yendi"];
    
    CountryCities["ZA"]=["Alice","Butterworth","East London","Graaff-Reinet","Grahamstown","King William’s Town","Mthatha","Port Elizabeth","Queenstown","Uitenhage","Zwelitsha","Bethlehem","Jagersfontein","Kroonstad",
    "Odendaalsrus","Parys","Phuthaditjhaba","Sasolburg","Virginia","Welkom","Benoni","Boksburg","Brakpan","Carletonville","Germiston","Johannesburg","Krugersdorp","Durban","Empangeni","Ladysmith","Newcastle","Pietermaritzburg",
    "Pinetown","Giyani","Lebowakgomo","Musina","Phalaborwa","Emalahleni","Nelspruit","Secunda","Bellville","Cape Town","Constantia","George","Hopefield","Oudtshoorn"];

    Countrycurrency["TZ"]=["USD"];
    Countrycurrency["NG"]=["NGN"];
    Countrycurrency["ZA"]=["USD"];
    Countrycurrency["GH"]=["USD"];




