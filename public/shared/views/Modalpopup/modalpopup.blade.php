<div style="background-color:white !important">
	<div class="modal-header">
	    <h4 class="modal-title">[[title]]</h4>
	</div>
	 <div class="modal-body">
	    [[body]]
	</div>
	<div class="modal-footer">
	    <button class="btn [[class]] btn-round btn-small" ng-click='confirm()'>Confirm [[action]]
	    <img ng-show="deleting" class="loader loader-small display-inline-block ng-hide" src="/uploads/images/theme/ajax_loader.gif">
	    </button>
	</div>
	    <button title="Close (Esc)" type="button" ng-click="cancel()" class=" mfp-close">×</button>
    </div>

	