<!doctype html>
<html class="no-js" data-ng-app="@yield('module')" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <meta name="theme-color" content="#1e313e "  />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="72x72" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="114x114" href="/uploads/images/logo/favicon.ico">
        <style>
            .ng-cloak
            {
                display: none !important;
            }

        </style>

         <!--styles and scripts>-->

      <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>

      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>-->
        <link rel="stylesheet" type="text/css" href="/css/templatestyle.css">
        <link rel="stylesheet" type="text/css" href="css/theme/intlTelInput.css">
        
        <script type="text/javascript" src="/scripts/jangular.js"></script>
       <script type="text/javascript" src="/scripts/templatescripts.js"></script>

        <script type="text/javascript" src="/scripts/angularjs.js"></script>

        <script src="/scripts/angularui.js"></script>

        <script src="/shared/app/registrationscripts.js"></script>



    <!--End styles and scripts>-->
    </head>
    <body class="bg-center ng-cloak bg-reg"style="background-image:url('/uploads/images/homeslider/signup.jpg' ); background-size:cover ;background-position:top;     background-repeat: no-repeat;background-color: #888;" id="reg">




        <!-- navigation panel -->
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-white nav-border-bottom" role="navigation">
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a target="_self" class="logo-light" href="https://brimav.com"><img alt=""   src="/uploads/images/logo/brimavwhite.png" class="logo" /></a><a target="_self" class="logo-dark" href="/"><img alt="" src="/uploads/images/logo/brimavcolor.png" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and cart  -->
                    <h4 ng-controller="HomeController" ><a style="    background-color: ##1e313e !important;padding: 5px 12px !important;border-radius: 5px !important;border-color: #1e313e !important;
    margin-top: 7px;" ng-click="signin()"  class="mobile-login   radius-30 button btn-round navbar-toggle font-weight-400 font-10 " >SIGN IN</a></h4>
                  
                    <div class="col-md-8 no-padding-right accordion-menu text-right hidden-sm-down">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group pull-right">
                                <!-- menu item -->
                                

                                <li style="background-color: #1e313e !important;padding:2px 12px;border-radius: 5px !important;" ng-controller="HomeController" class="btn-primary btn btn-small radius-30 button btn-round" >
                                    <a ng-click="signin()"  uib-tooltip="login" tooltip-placement="bottom" tooltip-trigger="mouseenter" ng-class="{active:aboutPage}" target="_self">SIGN IN</a>
                                </li>

                                    
                                <!-- end menu item -->
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>

        <section style="padding-top: 0px !important;" >
            
        </section>
        <!-- end navigation panel -->
        <!-- content section -->
        <section class=" fadeIn regres " style="margin-bottom: 0;
    padding-bottom: 0;">
            <div class="container">
                <div class="row">
                    <!-- content  -->
                    <div class="col-md-3 reg_container ng-cloak center-col medium-opacity">
                        @yield('content')
                    </div>
                </div>
           </div> 
        </section>
        <!-- footer -->
        <footer class="fadeIn  border-top-light">
            
            
            <div class="container-fluid bg-home footer-bottom">
                <div class="container">
                    <div class="padding-two">
                        <!-- copyright -->
                        <div class="col-md-4 col-sm-4 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one">
                          <span class="text-lower"> Copyright © Vacvi  <?php echo date('Y'); ?>.</span> 
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 copyright text-center letter-spacing-1 xs-text-center xs-margin-bottom-one">
                            <!--<a href="https://www.simplepay.ng"><img style="    width: 140px;height: 40px;" src="/uploads/images/theme/logo-checkout.png"></a>-->
                        </div>
                        
                        <!-- end copyright -->
                        <!-- logo -->
                        <div class="col-md-4  col-sm-4 col-xs-12 copyright text-right xs-text-center">

                           powered by  <a href="https://vacvi.com" target="_blank"><img src="/uploads/images/logo/vacvi-logo-white.png/" alt="vacvi" /></a>
                        </div>
                        <!-- end logo -->
                    </div>
                </div>
            </div>
            <!-- scroll to top -->
            <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
            <!-- scroll to top End... -->
        </footer>
        <!-- end footer -->


<!-- notification-->
    <toast></toast>
    <!-- end notification-->

    <!-- View Loading Spinner --> 
    <div ng-show="loadingView" class="spinner centerAbsolute">
        <img src="/uploads/images/theme/loadingcolor.gif">
    </div>
    <!-- End Loading Spinner-->

    <!-- Modal Loading Spinner -->
    <div id="loadingModalHolder" class="ng-hide">
        <div class="loadingModal modal-backdrop in">
            <div class="spinner">
                 <img src="/uploads/images/theme/loadingwhite.gif">
            </div>
        </div>
    </div>
    <!-- End Modal Loading Spinner--> 

     <script type="text/javascript" src="/shared/app/shared_templates.js"></script>

    <!--Agreement modal-->


<!--login pop up-->
    <div   class="modal zoom-anim-dialog fade " id="agreement" role="dialog" >
        <div class="modal-dialog" style="padding:0">

            <!-- Modal content-->
            <div    class="modal-content col-md-6  center-col col-sm-10 " style=" background-color: white !important" >
                <div class="modal-header">
                 <h6> BRIMAV Agreement</h6>
                </div>
                <div class="modal-body">
                    
                <ul style="list-style-type: disc">
                    <li> All adverts are to be completed within 24 hours.</li>
                    <li> Never use audio url from BRIMAV directly on your websites  (Download audio file and upload to your websites) as we clear our disk periodically.</li>
                    <li>BRIMAV charge 9.7% as commission per Advert.</li>

                </ul>
            
                 
                
                </div>
                 <button  title="Close (Esc)" type="button" onclick="closepop()" class=" mfp-close">×</button>
            </div>


        </div>
    </div>
<!--/Agreement modal-->

    </body>
</html>


<script type="text/javascript">
    
    function closepop()
    {

        jQuery('#agreement').modal('hide');
    }
</script>