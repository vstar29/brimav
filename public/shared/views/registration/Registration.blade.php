
    <div class="center-col regadj text-center "  ng-class="{'hide':signmode} ">

        
    <form style="padding: 10px;" name="regform" ng-submit="join()" >

               
            <div class="form-group">

                <input type="text" ng-required="true" id="username" name="username" placeholder="USERNAME" class="form-control width-100" ng-model="Username"
                       ng-class="{'input-validation-error': fieldErrors.username }" />
                <span ng-show="fieldErrors.username" class="field-validation-error">[[fieldErrors.username]]</span>

            
            </div>

            <div class="form-group">
               
                <input type="email" id="email" name="email" ng-required="true"  placeholder="EMAIL" class="form-control width-100" ng-model="Email"
                       ng-class="{'input-validation-error': fieldErrors.email }" />
                 <span ng-show="fieldErrors.email" class="field-validation-error ">[[fieldErrors.email]]</span>

              
                
            </div>

            <div class="form-group ">
               
               
                <input type="password" ng-required="true"  placeholder="PASSWORD" pattern=".{6,}" title="Min of 6 characters" class="form-control width-100" ng-model="Password"
                       ng-class="{'input-validation-error': fieldErrors.password }" />
                <span ng-show="fieldErrors.password" class="field-validation-error text-center ">[[fieldErrors.password]]</span>

               
            </div>

      
             <div class="form-group" >
                
                <phonenumber ng-click=synccity() country="Country" number="phonenumber" ng-class="{'input-validation-error': fieldErrors.phonenumber }"></phonenumber >
                 <span ng-show="fieldErrors.phonenumber" class="field-validation-error text-center ">[[fieldErrors.phonenumber]]</span>      
            </div>

            <div  class="form-group">
                <select id="city " class="form-control" name="city" ng-model="City" ng-options="city for city in Citys | orderBy">
                    <option value="">Select City</option>
                </select>

            </div>


            <div class="form-group ">
                
                <div>
                    <select id="Type" ng-required="true" name="Type"  class="form-control width-100" ng-model="Type"
                            ng-class="{'input-validation-error': fieldErrors.type}">
                        <option value="">------MEMBERSHIP TYPE ----------</option>
                         <option  value="Advertiser">Advertiser / Artist</option>
                         <option value="Publisher">Publisher / Blogger</option>
                    </select>

                </div>
            </div>

          
            <div ng-show="joining" class="  modal-backdrop in bg-adgold">
                <div class="actionspinner">
                     <img  src="/uploads/images/theme/loadingcolor.gif">
                </div>
            </div>
            
                
        <button style="border: 2px solid white;
    background-color: #000000;" type="submit" ng-click="update()" class="btn btn-medium btn-adgold no-margin-bottom form-control width-100" >
                        JOIN
        </button>
             
    </form>
      
</div>

