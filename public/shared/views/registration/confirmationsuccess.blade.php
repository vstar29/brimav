 <!doctype html>
<html class="no-js" lang="en">
    <head>
	    <base href='/'>
	        <title>Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <meta name="theme-color" content="#1e313e" />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">


 		<link rel="stylesheet" type="text/css" href="/css/theme/bootstrap.css">
 		<link rel="stylesheet" type="text/css" href="/css/regextra.css">

 	
 	</head>

 		<body>
 			<div class="nav">
 				<div class="container">
 					<a target="_self" class="logo-dark " href="https://brimav.com"><img alt=""  src="/uploads/images/logo/brimavwhite.png" class="logo" /></a>
 				</div>
 			</div>
	 		<div class="container content">
	 				
	 			<div class="col-md-4 center-col">
	 				<span class="red">{{$message}}</span>
	 				<form action="api/login" method="post">

	 					<div class="form-group">
		 					<img src="/uploads/images/theme/unknown.jpg" class="avatar">
		 				</div>
		 				<div class="form-group">
		 					<input class="form-control" type="text" readonly="true" value="{{$email}}" name="email" placeholder="Email">
		 				</div>

		 				<div class="form-group">
		 					<input class="form-control" type="password" name="password" placeholder="Password">
		 				</div>

		 				<div class="form-group">
		 					<button class="btn btn-small" type="submit">Login</button>
		 				</div>
		 			</form>
		 			<a class="white-text" href="/register" target="self" style="margin-left: 27%; position: absolute;margin-top: 40px !important; ">Have no account?</a>

	 			</div>

	 			
	 		</div>


	 		</div>

 		</body>

 </html>