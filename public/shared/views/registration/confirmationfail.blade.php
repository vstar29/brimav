@extends('shared.views.registration.registrationtemplate')

@section('content')


@section('module','RegistrationModule')

<script type="text/javascript" src="shared/app/registrationscripts.js"></script>

  <div ng-controller="RegistrationController" class="  center-col regadj text-center " style="padding-top: 4%;padding-bottom: 0% !important" ng-class="{'hide':loginmode}">
    <div ng-class="{'hide':tokenrequestactive}" class="modal-header">
        <span class="modal-title " style="font-size: 15px !important;color: red!important;">ERROR</span>
    </div>
     <div class="modal-body">
        <span ng-class="{'hide':tokenrequestactive}">Email confirmation fail. <h4><a style="color:green !important;" class="pointer" ng-click="tokenrequest('email')">Request</a></h4>for another confirmation code.</span>
	    <div id="tokenform"  ng-class="{'hide':!tokenrequestactive}">
		    <form name="tokenform" id="tokenform" ng-submit="requesttoken()">
		    	<div class="form-group">
				    <input type="text" required="true" class="form-control width-120 input-round medium-input " name="username" placeholder="Email" id="tokenemail" ng-model="tokenemail" ng-class="{'input-validation-error': fieldErrors.email }">
				    <span ng-show="fieldErrors.email" class="field-validation-error text-center ">[[fieldErrors.email]]</span>
				</div>
			    <div class="form-group">
	                <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" ng-disabled="tokenform.$invalid" type="submit">Request <img ng-show="requesting" class="display-inline-block"  src="/uploads/images/theme/ajax_loader.gif">
	                </button>
	             </div>	
		    </form>
	    </div>
    </div>
    
</div>

@endsection

