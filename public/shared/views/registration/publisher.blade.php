<div class="completereg" ng-class="{'hidden':succesful}">


  <h3 class="compeletereg-head">Fill the form below to Complete your registration</h3>

  <form  ng-submit="completereg()">

    <div class="form-group">
      <select  ng-model="Medium">
          <option value="website">Website</option>
          <option value="social">Social Platform</option>
          <option value="tv">TV/Radio</option>
      </select>
    </div>
    <div ng-show="website">
      <div class="form-group" >
                        
          <input type="text" ng-required="true" id="Websitename" name="Websitename" placeholder="WEBSITE NAME"  class="form-control width-100" ng-model="Websitename"
                 ng-class="{'input-validation-error': fieldErrors.websitename }" />
          <span ng-show="fieldErrors.websitename" class="field-validation-error ">[[fieldErrors.websitename]]</span>
                                   
      </div>
      <div class="form-group" >

        <input type="url"  id="Websiteurl" name="Websiteurl" placeholder="WEBSITE URL (http://)"  class="form-control width-100" ng-model="Websiteurl"
               ng-class="{'input-validation-error': fieldErrors.websiteurl }" />
               <span ng-show="fieldErrors.websiteurl" class="field-validation-error ">[[fieldErrors.websiteurl]]</span>
                               
      </div>
    </div>

    <div ng-show="social">
      <div class="form-group">
      <select ng-model="socialMedium">
          <option value="">Select</option>
          <option value="facebook">Facebook</option>
          <option value="twitter">Twitter</option>
          <option value="google+">Google+</option>
          <option value="instagram">Instagram</option>
      </select>

        <input type="text" ng-model="socialType" name="facebook" placeholder=" &#xf09a; &#xf0d5; &#xf099; username">
        <input type="number" ng-model="Followers" name="facebook_followers" placeholder="  followers numbers">
      </div>
    </div>

    <div ng-show="tv">
      <div class="form-group">
          <input type="text" name="channelName" ng-model="channelName" placeholder="Channel Name">
          <input type="text" name="frequency" ng-model="Frequency" placeholder="Channel frequency">
      </div>

    </div>

    
    <div class="form-group" >
      <span class="pull-right"><i class="fa fa-info-circle" uib-tooltip="Charge for advert in NGN" tooltip-placement="top" tooltip-trigger="mouseenter"></i></span>
        <input type="text" ng-required="true" id="Prize" name="Prize" placeholder="CHARGE/ADVERT (NGN)"  class="form-control width-100" ng-model="Prize"
               ng-class="{'input-validation-error': fieldErrors.prize }" />
               <span ng-show="fieldErrors.prize" class="field-validation-error ">[[fieldErrors.prize]]</span>
                               
    </div>
    <div class="form-group ">
      <input type="text" ng-required="true" id="Banker" name="Banker" placeholder="Banker"  class="form-control width-100" ng-model="Banker"
               ng-class="{'input-validation-error': fieldErrors.banker }" />
               <span ng-show="fieldErrors.prize" class="field-validation-error ">[[fieldErrors.banker]]</span>
    </div>

    <div   class="form-group" >
      <input type="text" ng-required="true"   id="Accountame" name="Accountname" placeholder="ACCOUNT NAME"  class="form-control width-100" ng-model="Accountname"
             ng-class="{'input-validation-error': fieldErrors.accountname }" />
             <span ng-show="fieldErrors.accountname" class="field-validation-error ">[[fieldErrors.accountname]]</span>
               
    </div>

    <div class="form-group" >
  
      <input type="text" ng-required="true"  id="Accountnumber" name="Accountnumber" placeholder="ACCOUNT NO"  class="form-control width-100" ng-model="Accountnumber"
             ng-class="{'input-validation-error': fieldErrors.accountnumber }" />
             <span ng-show="fieldErrors.accountnumber" class="field-validation-error ">[[fieldErrors.accountnumber]]</span>          
    </div>

    <div class="checkbox form-groupdefault ">
        <label><input type="checkbox" ng-required="true" ng-model="agree"> I agree</label> <a href="" data-toggle="modal" data-target="#agreement" style="margin-left: 60px; color:red">Read</a>
    </div>

    <div class="">
        <button class="btn  no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" type="submit">Submit  <img ng-show="submitting" class="loader loader-small  ng-hide">
        </button>
    </div>
  </form>
</div>