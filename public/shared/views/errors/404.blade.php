<!doctype html>
<html class="no-js" data-ng-app="HomeModule" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Page Not Found </title>
        <meta name="description" content="Digital Content Distributor">
        <meta name="keywords" content="Digital Content Distribution and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <meta name="theme-color" content="#65432a" />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
        <link rel="stylesheet" type="text/css" href="{{elixir('css/templatestyle.css')}}">

   	

    </head>
    <body>
    	<div class="container error-page">
      		<div class="col-md-6">
      			<img src="/uploads/images/homeslider/404_man.jpg">
      		</div>

      		<div class="col-md-6">
      			<h4 class="error-h">Woopsie !-- PAGE NOT FOUND</h4>
      				<a class="btn-primary  pointer btn btn-medium button btn-round" href="https://brimav.com">Go Home <i class="fa fa-home"></i></a>

      				<a class="btn-success  pointer btn btn-medium button btn-round" href="https://brimav.com/register">Register</a>
      		</div>
      	</div>
       
    

    </body>

   
</html>
