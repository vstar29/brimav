
<div class=" col-md-5 round-square col-sm-10 bg-gray medium-opacity regmargin " style="width:270px !important;height:310px !important;" >

            <div style="margin-top:5px !important;">
                <h3>Login</h3>
                
            </div>
                       
            <div class="separator-line bg-adgold no-margin-lr margin-ten"></div>
            <div class="validation-summary-errors margin-four-bottom" ng-show="validationErrors">
                <ul>
                     <li ng-show="fieldErrors.verify">Please confirm your email <a style="color:green"  onclick="verification_resend()" href=""> Resend</a></li>
                    <li ng-repeat="error in validationErrors">[[error]]</li>
                </ul>
            </div>
                <form   ng-submit="confirmlogin()">
           
                    <div class="form-groupdefault ">
                        
                        <label for="username" class="font-weight-500" >Username/Email</label>
                        <input type="text" required="true" class="form-control width-120 input-round medium-input " name="username" placeholder="&#xf007;" id="username" ng-model="username" ng-class="{'input-validation-error': fieldErrors.username }">
                        <span ng-show="fieldErrors.username" class="field-validation-error-login text-center ">[[fieldErrors.username]]</span>
                      
                    </div>


                    <div ng-show="login" class="modal-backdrop in bg-adgold">
                        <div class="spinner">
                             <img  src="/uploads/images/theme/loadingcolor.gif">
                        </div>
                    </div>

                    <div class="form-groupdefault ">
                       
                        <label for="password" class="font-weight-500" >Password</label>
                        <input type="password" required="true" name="password" class="form-control width-120 input-round medium-input " placeholder='&#xf023;' id="password" ng-model="password" ng-class="{'input-validation-error': fieldErrors.password }"> 
                        <span ng-show="fieldErrors.password" class="field-validation-error-login text-center ">[[fieldErrors.password]]</span>
                    </div>

                    <div class="checkbox form-groupdefault ">
                        <label><input type="checkbox" ng-model="remember"> Remember me</label>
                    </div>


                    <div class="form-groupdefault pull-left ">
                   <a ng-click="recover()" class="dark-text font-weight-400 font-10 pointer" >Recover password</a>
                
                    </div>


                    <div class=" pull-right  ">
                        <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" type="submit">Log in
                        </button>
                    </div>

            
                 </form>
                 <button title="Close (Esc)" type="button" ng-click="cancel()" class=" mfp-close">×</button>
                  <a class="white-text" href="/home/registration" target="self" style=" position: absolute;margin-top: 60px !important; margin-left: 50px;">Have no account?</a>
            </div>


            <script type="text/javascript">
                


    function verification_resend()
    {
       
        var data=
        {
            email:jQuery('#username').val(),
            type:'email'
        };
      

        jQuery.ajax(
        {
            url:'/api/register/emailverificationtoken',
            type:'post',
            data:data,
            success:function(data)
            {
                alert('Check your email for confirmation token');
            },
            error:function()
            {
                 alert('User not found');
            }
        });
    }


</script>
            
           