
@extends('shared.views.registration.registrationtemplate')

@section('content')

@section('module','RegistrationModule')

<script type="text/javascript" src="shared/app/App.js"></script>
<script type="text/javascript" src="shared/app/controllers/HomeController.js"></script>
<script type="text/javascript" src="shared/app/services/HomeService.js"></script>

		<div class="center-col regadj text-center" ng-controller="ChangePasswordController" data-ng-init="email='{{$Email}}';" style="padding-top: 4%;padding-bottom: 0% !important" >

	<div  class="modal-header">
	    <span class="modal-title " style="font-size: 13px !important;">CHANGE PASSWORD</span>
	</div>
 	<div class="modal-body">
    
    
	    <form name="changepasswordform" id="changepasswordform" ng-submit="changepassword()">
	    	<div class="form-group">

	    	 <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">New password</label>
			    <input type="password" pattern=".{6,}" title="Min of 6 characters" required="true" class="form-control width-120 input-round medium-input " name="password"  id="password" ng-model="password" ng-class="{'input-validation-error': fieldErrors.password }">
			    <span ng-show="fieldErrors.password" class="field-validation-error-login text-center ">[[fieldErrors.password]]</span>
			</div>

			<div class="form-group">

	    	 <label class = "control-label margin-one-bottom font-14 black-text font-weight-500 letter-spacing-1">Confirm password</label>
	    	 

	    	 
			    <input type="password" pattern=".{6,}" title="Min of 6 characters" required="true" class="form-control width-120 input-round medium-input " name="password"  id="password" ng-model="confirmpassword" ng-class="{'input-validation-error': fieldErrors.confirmpassword }">
			    <span ng-show="fieldErrors.confirmpassword" class="field-validation-error-login text-center ">[[fieldErrors.confirmpassword]]</span>
			</div>
		    <div class="form-group">
                <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" ng-disabled="recoverform.$pristine" type="submit">CHANGE <img ng-show="changing" class="display-inline-block"  src="/uploads/images/theme/ajax_loader.gif">
                </button>
             </div>	
	    </form> 
	    
    </div>
    <button title="Close (Esc)" type="button" ng-click="cancel()" class=" mfp-close">×</button>
</div>


@endsection
