
<div class="col-md-6 bg-gray regmargin" >

	<div  class="modal-header">
	    <span class="modal-title " style="font-size: 13px !important;">PASSWORD RECOVERY</span>
	</div>
 	<div class="modal-body">
    
    
	    <form name="recoverform"  novalidate ng-submit="recoverpassword()">
	    	<div class="form-group">
			    <input type="text"   name="email" placeholder="Enter your email" id="email" ng-model="recovermail" class="form-control width-120 input-round medium-input" ng-class="{'input-validation-error': fieldErrors.email }">
			    <span ng-show="fieldErrors.email" class="field-validation-error-login text-center ">[[fieldErrors.email]]</span>
			</div>
		    <div class="form-group">
                <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input"    type="submit">Recover <img ng-show="requesting" class="display-inline-block remain remain2 loading"  src="/uploads/images/theme/ajax_loader.gif">
                </button>
             </div>	
	    </form> 
	    
    </div>
    <button title="Close (Esc)" type="button" ng-click="close()" class=" mfp-close">×</button>
</div>
