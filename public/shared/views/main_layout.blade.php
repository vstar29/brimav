<!doctype html>
<html class="no-js" data-ng-app="HomeModule" lang="en">
    <head>
    <base href='/'>
        <title ng-bind="title">Brimav | Bridging The Gap </title>
        <meta name="description" content="Digital Advertising">
        <meta name="keywords" content="Digital Advertising and Music Promotion">
        <meta charset="utf-8">
        <meta name="vstar" content="vacvi">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <meta name="theme-color" content="#1e313e " />
        <!-- favicon -->
        <link rel="shortcut icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="72x72" href="/uploads/images/logo/favicon.ico">
        <link rel="apple-touch-icon" sizes="114x114" href="/uploads/images/logo/favicon.ico">
        <style>
            .ng-cloak
            {
                display: none !important;
            }

        </style>

    <!--styles and scripts>-->

      <script type="text/javascript" src="scripts/brimav_script.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/templatestyle.css">
      <script type="text/javascript" src="/scripts/revolution.js"></script>
      <script src="/scripts/angularui.js"></script>

    <!--End styles and scripts>-->

        

    </head>
    <body  class="ng-cloak" style="background-color: #fbfbfb;">
      
        <!-- navigation panel -->
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-white nav-border-bottom" role="navigation" style="opacity: 0.97;">
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a target="_self" class="logo-light" href="/" ><img alt="" src="/uploads/images/logo/brimavwhite.png" class="logo" /></a><a class="logo-dark" href="https://brimav.com" target="_self"><img alt="" src="/uploads/images/logo/brimavcolor.png" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and cart  -->
                    
                    <!-- end search and cart  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                    
                        <button type="button" style="margin-bottom: -9px !important;" class="navbar-toggle color-de" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar bg-de"></span> <span class="icon-bar bg-de"></span> <span class="icon-bar bg-de"></span> </button>
                       
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">

                        <div ng-controller="HomeController" class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav  navbar-right panel-group">
                                <!-- menu item -->
                                <li class="dropdown panel">
                                    <a href="/" ng-class="{active:homePage}" target="_self">Home </a>
                                </li>

                                <li class="dropdown panel pointer" ng-class="{active:howPage}"><a  href="" ng-click="how()"  >How It Works</a>
                                </li>

                                  <li class="dropdown panel pointer"  ng-class="{active:aboutPage}">  <a ng-click="about()">About</a>
                                </li>


                                  <li class="dropdown panel pointer"  ng-class="{active:contactPage}">  <a ng-click="contact()">Contact</a>
                                </li>
                              
                                <li class="btn-primary radius-30 nav-btn btn-nav pointer btn btn-small button btn-round">
                                    <a  data-toggle='modal' data-target='#loginpop' class="modal-popup popup-with-move-anim"    uib-tooltip="login" tooltip-placement="bottom" tooltip-trigger="mouseenter" >Sign In</a>
                                </li>
                                <li class="btn-success radius-30 nav-btn btn-nav btn btn-small button btn-round">
                                    <a href="/home/registration"  ng-class="{active:signupPage}" target="_self">Sign Up</a>
                                  
                                </li>
                                
  
                                <!-- end menu item -->
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>



           <!-- end navigation panel -->
        <!-- slider -->

        <!--account-block-->
        <div ng-controller="HomeController" class="section">
            <div class="account-block col-md-2">
                <form ng-submit="continue()">

                <div class="form-group">
                         <input type="text" name="username_init" class="form-control width-120 input-round medium-input " placeholder='&#xf007;  Username' id="username_init" ng-model="username_init">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control width-120 input-round medium-input " name="email_init" placeholder="&#xf0e0;  Email" id="email_init" ng-model="email_init">
                    </div>
                    <div class="form-group">
                         <input type="password" name="password_init" class="form-control width-120 input-round medium-input " placeholder='&#xf023;  Password' id="password_init" ng-model="password_init">
                    </div>

                      <div class=" pull-right" style="margin-top: -10px;">
                        <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" type="submit">Join Brimav
                        </button>
                    </div>
                </form>

            </div>
        </div>


        <!--/account-block-->
         <section id="myCarousel"> 

            <div class="tp-banner-container">
                <div class="revolution-slider-full" >
                    
                    <ul>    <!-- SLIDE 1 -->
                      
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="900" data-thumb=""  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                            <img src="/uploads/images/homeslider/grow.jpg"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" style="background-color:#6D6969">
                            <div class="slider-overlay bg-slider" style=""></div>
                            <!-- LAYERS 1 -->
                            <div class="tp-caption light_medium_30_shadowed lfb ltt tp-resizeme"
                                 data-x="center" data-hoffset="0"
                                 data-y="center" data-voffset="-50"
                                 data-speed="300"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            </div>
                            <!-- LAYERS 2 -->
                            <div class="tp-caption homeslidertextleft light_heavy_70_shadowed lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="0"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;  ">Skyrocket Your<br><br>
                                  Business/Products 
                            </div>
                            <!-- LAYERS 3 -->
                            <div class="tp-caption bg-green homeslidertextleft customin tp-resizeme rs-parallaxlevel-0"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="80"
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="yes"
                                 data-splitout="none"
                                 data-elementdelay="0.1"
                                 data-endelementdelay="0.1"
                                 data-linktoslide="next"
                                 style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                 <a href="/register"  class='largeredbtn inner-link' target="_self">Try Now</a>
                            </div>
                        </li>
                        <!-- SLIDE 2 -->
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="900" data-thumb=""  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                             <img src="/uploads/images/homeslider/signup.jpg" style="background-color:#6D6969"    data-bgposition="center" data-bgfit="cover" data-bgrepeat="no-repeat">
                           <div class="slider-overlay bg-slider" ></div>
                            <!-- LAYERS 1 -->
                            <div class="tp-caption light_medium_30_shadowed lfb ltt tp-resizeme"
                                 data-x="center" data-hoffset="0"
                                 data-y="center" data-voffset="-50"
                                 data-speed="600"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            </div>
                            <!-- LAYERS 2 -->
                            <div class="tp-caption homeslidertextleft light_heavy_70_shadowed lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="0"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Earn big from <br> <br> your websites 
                            </div>
                            <!-- LAYERS 3 -->
                            <div class="tp-caption homeslidertextleft customin  tp-resizeme rs-parallaxlevel-0"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="80"
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.1"
                                 data-endelementdelay="0.1"
                                 data-linktoslide="next"
                                 style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="/register"  class='largeredbtn inner-link' target="_self">Start</a>
                            </div>
                        </li>

                          <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="900" data-thumb=""  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                            <img src="/uploads/images/homeslider/promote.jpg"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="slider-overlay bg-slider" ></div>
                            <!-- LAYERS 1 -->
                            <div class="tp-caption light_medium_30_shadowed lfb ltt tp-resizeme"
                                 data-x="center" data-hoffset="0"
                                 data-y="center" data-voffset="-50"
                                 data-speed="400"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            </div>
                            <!-- LAYERS 2 -->
                            <div class="tp-caption light_heavy_70_shadowed lfb homeslidertextleft ltt tp-resizeme"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="0"
                                 data-speed="400"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Promote Your Songs <br><br>On Top Blogs,Radios,Tvs
                            </div>
                            <!-- LAYERS 3 -->
                            <div id="about" class="tp-caption homeslidertextleft bg-dark-blue customin tp-resizeme rs-parallaxlevel-0"
                                 data-x="left" data-hoffset="0"
                                 data-y="center" data-voffset="80"
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.1"
                                 data-endelementdelay="0.1"
                                 data-linktoslide="next"
                                 style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="/register"  class='largeredbtn inner-link' target="_self">Promote Now!</a>

                            </div>
                        </li>
                    </ul> 
                </div>
            </div>      
                 
        </section>
        <!-- end slider -->
        <!-- about section -->
        <section  class=" no-margin padding-three wow fadeIn bg-gray row">
                  <div class="col-md-12"> 
                        
                        <div class="col-md-5 hometextpad no-padding text-center center-col">
                           
                            <span  class="text-med wow fadeIn black_bold_32 black-text width-90 center-col  no-margin-bottom"> Brimav is a digital platform that helps you reach your targeted audience by putting your adverts, creative works (Music, Contents, Videos and Others) on numerous of Nigeria, Ghana, SouthAfrica and Tanzania based websites.
                                There by boosting your business and also help bloggers to earn a living from their websites/blogs.
                            </span> 
                        </div>

                        
                    </div>
               
        </section>
        
        
        <!-- end about section -->

        <div>

            <section id="section_feature" class="wow fadeIn bg-gray scrollme  margin-one-bottom no-margin no-padding  border-top" >
                <div class="row col-md-8 center-col">


                    <div class="row center-col">
                        <div class="col-md-4 animateme col-sm-6" data-when="enter"
                            data-from="0.5"
                            data-to="0"
                            data-opacity="0"
                            data-translatex="-200"
                            data-rotatez="45">

                            <div class="card center-col">
                                <i class="fa fa-music"></i>
                                <div class="card-block">
                                  <h3>Music Promotion</h3>
                                  <span>Promote your music & video on blogs, tvs,radio.</span>
                                  

                               </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 animateme"
                        data-when="enter"
                            data-from="0.5"
                            data-to="0"
                            data-opacity="0"
                            data-translatex="-200"
                            data-rotatez="45">
                            <div class="card center-col">
                               <i class="fa fa-bullseye"></i>
                                <div class="card-block">
                                  <h3>Banner Advert</h3>
                                  <span>Banner adverts for your products & business.</span>
                                </div>
                             </div>
                        </div>

                        <div class="col-md-4 col-sm-6">

                            <div class="card center-col">
                              <i style="margin-top: 3px;" class="fa fa-desktop"></i>
                                <div class="card-block">
                                  <h3>TV & Radio Advert</h3>
                                  <span>Place advert on Tv and radio station with ease.</span>
                               </div>
                            </div>
                        </div>
                    </div>

                    <div class="row center-col">
                        <div class="col-md-4 col-sm-6">

                            <div class="card center-col">
                               <i class="fa fa-twitter"></i>
                                <div class="card-block">
                                  <h3>Social Advert</h3>
                                  <span>Earn money from your social account with large followers.</span>
                                  

                               </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 animateme"
                         data-when="enter"
                            data-from="0.5"
                            data-to="0"
                            data-opacity="0"
                            data-translatex="200"
                            data-rotatez="45">

                            <div class="card center-col">
                           <i class="fa fa-newspaper-o"></i>
                                <div class="card-block">
                                  <h3>Blogs Post</h3>
                                  <span>Put sponsored post on your blog and get credited.</span>
                                 

                               </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="card center-col animateme"
                                data-when="enter"
                                data-from="0.5"
                                data-to="0"
                                data-opacity="0"
                                data-translatex="-200"
                                data-rotatez="45">
                                <ul class="unique">
                                    <li>EASY TO USE</li>
                                    <li>INSTANT FEEDBACK</li>
                                    <li>ADVERT SCHEDULE</li>
                                    <li>EASY TRANSFER</li>
                                    <li>DETERMINE YOUR PRICE</li>
                                    <li>CHOICE SELECTION</li>
                                    
                                </ul>
                            </div>
                        </div> 
                    </div>
 
                </div>

            </section>
            <!-- features section -->
            <section id="how"   class="wow fadeIn bg-gray  margin-one-bottom no-margin no-padding  border-top">
               
               
                <div  class="col-md-11 col-sm-12 center-col">
                    <!-- tab -->
                    <div class="tab-style2">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <!-- tab navigation -->
                                <ul style="margin-left: -15px;"  class="nav nav-tabs nav-tabs-light text-left">
                                    <li class="active"><a id="advertiser" href="#tab2_sec1" data-toggle="tab">As Artist / Advertiser</a></li>
                                    <li><a id="publisher" href="#tab2_sec2" data-toggle="tab">As Blogger / Publisher</a></li>
                                   
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                        <!-- tab content section -->
                        <div class="row">
                            <div class="tab-content col-md-6 how-content">
                                <!-- tab content -->
                                <div class="tab-pane med-text fade in active" id="tab2_sec1">
                                    <div class="row">
                                        <div class="hometextpad col-md-12 col-sm-12">
                                            <p class="black-text">With just six steps, you get your music/advert on world platform.</p>
                                            <ul class ="how_u">
                                                <li><i class="fa fa-check-circle-o"></i><a href="https://brimav.com/register" target="_self">Sign up</a> as Artist / Advertiser</li> 
                                                <li><i class=" fa fa-check-circle-o"></i>Create an adverts(songs,banners,post/content,videos.e.t.c)</li> 
                                                <li><i class=" fa fa-check-circle-o"></i>Select websites you wish to promote your songs,content on.</li>
                                                <li><i class="fa fa-check-circle-o"></i>Make payment</li>
                                                <li><i class="fa fa-check-circle-o"></i>You get mailed when Links to your promoted content is returned by publishers</li>
                                                <li><i class="fa fa-check-circle-o"></i>Confirm the link</li>
                                            </ul>
                                            <br/>
                                             <p class="black-text">No Publisher is paid until you confirm the returned link.</p>

                                             <p class="black-text">If a Publisher fails to promote your content within 24hrs , your money will be refunded.</p>
                                        </div>
                                         
                                    </div>
                                </div>
                                    <!-- end tab content -->
                                    <!-- tab content -->
                                <div class="tab-pane fade in" id="tab2_sec2">
                                    <div class="row">
                                        <div class=" hometextpad col-md-12 col-sm-12">
                                            <p class="black-text">Just few steps to start earning from your websites .</p>
        
                                            <ul class ="how_u">
                                                <li><i class="fa fa-check-circle-o"></i><a href="https://brimav.com/register" target="_self">Sign up</a> as Publisher</li> 
                                                <li><i class="fa fa-check-circle-o"></i>Put content on your blog</li> 
                                                <li><i class="fa fa-check-circle-o"></i>Return link to the content</li>
                                                <li><i class="fa fa-check-circle-o"></i>You account get credited after the advertiser confirm the link</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> 
                                    
                            </div> 
                            <div class="col-md-6 hidden-sm-down" id="slider">

                                <div id="owl-demo" class="col-xs-12 owl-carousel owl-theme light-pagination bottom-pagination dark-pagination-without-next-prev-arrow position-relative">
                                    <!-- testimonials item -->
                                    <div class="item wow">
                                        <div class="col-md-5 col-sm-8 no-padding testimonial-style2 center-col text-center margin-three no-margin-top">
                                            <img src="/uploads/images/homeslider/lupita.jpg" alt=""/>
                                            <p class="text-light-g text-med">An efficient medium to reach out to large audience.</p>
                                            <span class="name black-text text-med">Jotana Digital Marketer </span>
                                        </div>
                                    </div>
                                            <!-- end testimonials item -->
                                    <!-- testimonials item -->
                                    <div class="item wow">
                                        <div class="col-md-5 col-sm-8 no-padding testimonial-style2 center-col text-center margin-three no-margin-top">
                                            <img src="/uploads/images/homeslider/olamide.jpg" alt=""/>
                                            <p class="text-light-g text-med">Brimav simply deliver the best with less stress.</p>
                                            <span class="name black text-med">Olamide - Artist</span>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->

                                <!--<div class="item wow">
                                    <div class="col-md-5 col-sm-8 no-padding testimonial-style2 center-col text-center margin-three no-margin-top">
                                        <img src="/uploads/images/homeslider/temmie.jpg" alt=""/>
                                        <p class="text-light-g text-med">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                        <span class="name black text-med">Dallas Groot - Facebook</span>
                                    </div>
                                </div>-->
                                <!-- end testimonials item -->
                        
                               
                            </div>
                        </div>
                        
                        <!-- end tab content section -->
                    </div>
                    <!-- end tab --> 
                         <hr/>  
                </div>
            </section>
        </div>

            
        <section class="no-margin no-padding">
           
            <!-- tab content -->
            @yield('nothing')
            <!-- end tab content -->
            <!-- tab content -->
                            
        </section>
        
    
        <!-- footer -->
        <footer>
            <div  class=" bg-gray footer-top">
                <div class="container">
                    <div class="padding-three">
                        <!-- phone -->
                        <div class="col-md-4 col-sm-4 text-center"><i class="fa fa-mobile small-icon black-text"></i><h6 class="black-text margin-two no-margin-bottom">+2347031840158</h6></div>
                        <!-- end phone -->
                        <!-- address -->
                        <div  class="col-md-4 col-sm-4 hidden-sm-down text-center"><i class="fa-map-marker small-icon black-text"></i><h6 class="black-text margin-two no-margin-bottom">Lagos - Nigeria</h6></div>
                        <!-- end address -->
                        <!-- email -->
                        <div class="col-md-4 col-sm-4 text-center"><i class="fa fa-envelope-o black-text small-icon"></i><h6 class="margin-two "><a href="mailto:no-" class="black-text">info@brimav.com</a></h6></div>
                        <!-- end email -->
                    </div>
                </div>
            </div>
           
                      
            <div id="contact"  class="container-fluid bg-home footer-bottom">
                <div class="container">
                    <div class="padding-two">
                        <!-- copyright -->
                        <div class="col-md-4 col-sm-4 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one">
                          <span class="text-lower"> Copyright © Vacvi  <?php echo date('Y'); ?>.</span> 
                        </div>

                        <div class="col-md-4 hidden-sm-down col-sm-4 col-xs-12 copyright text-center letter-spacing-1 xs-text-center xs-margin-bottom-one">
                            <!--<a href="https://www.simplepay.ng"><img style="    width: 140px;height: 40px;" src="/uploads/images/theme/logo-checkout.png"></a>-->
                        </div>
                        
                        <!-- end copyright -->
                        <!-- logo -->
                        <div class="col-md-4  col-sm-4 col-xs-12 copyright text-right xs-text-center">

                           powered by  <a href="https://vacvi.com" target="_blank"><img src="/uploads/images/logo/vacvi-logo-white.png" alt="vacvi" /></a>
                        </div>
                        <!-- end logo -->
                    </div>
                </div>
            </div>
            <!-- scroll to top -->
            <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
            <!-- scroll to top End... -->
        </footer>
        <!-- end footer -->


<!-- notification-->
    <toast></toast>
    <!-- end notification-->
    <!-- View Loading Spinner --> 
    <div ng-show="loadingView" class="spinner centerAbsolute">
        <img src="/uploads/images/theme/loadingcolor.gif">
    </div>
    <!-- End Loading Spinner-->

    <!-- Modal Loading Spinner -->
    <div id="loadingModalHolder" class="ng-hide">
        <div class="loadingModal modal-backdrop in">
            <div class="spinner">
                 <img src="/uploads/images/theme/loadingwhite.gif">
            </div>
        </div>
    </div>
    <!-- End Modal Loading Spinner--> 



 <script type="text/javascript" src="/shared/app/shared_templates.js"></script>


<!--login pop up-->
    <div   class="modal zoom-anim-dialog fade " id="loginpop" role="dialog" style="    margin-top: 40px;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div   ng-controller="HomeController" class="modal-content col-md-5 login_pop center-col col-sm-10   regmargin medium-opacity" style=" background-color: #f6f6f6 !important" >
                <div class="modal-header" style="border:none !important">
                    <div style="margin-top:5px !important;">
                            <h3>Login</h3>
                            <div class="separator-line bg-adgold no-margin-lr" ></div>
                         </div>
                    </div>
                <div class="">
                    
                <div class="validation-summary-errors margin-four-bottom" ng-show="validationErrors">
                    <ul>
                         <li ng-show="fieldErrors.verify">Please confirm your email <a style="color:green"  onclick="verification_resend()" href=""> Resend</a></li>
                        <li ng-repeat="error in validationErrors">[[error]]</li>
                    </ul>
                </div>
                <form    ng-submit="normallogin()">
           
                    <div class="form-groupdefault ">
                        
                        <label for="username" class="font-weight-500" >Username/Email</label>
                        <input type="text"  required="true" class="form-control width-120 input-round medium-input " name="username" placeholder="&#xf007;" id="username" ng-model="username" ng-class="{'input-validation-error': fieldErrors.username }">
                        <span ng-show="fieldErrors.username" class="field-validation-error-login text-center ">[[fieldErrors.username]]</span>
                      
                    </div>


                    <div ng-show="login" class="modal-backdrop in bg-adgold">
                        <div class="spinner">
                             <img  src="/uploads/images/theme/loadingcolor.gif">
                        </div>
                    </div>

                    <div class="form-groupdefault ">
                       
                        <label for="password" class="font-weight-500" >Password</label>
                        <input type="password" required="true" name="password" class="form-control width-120 input-round medium-input " placeholder='&#xf023;' id="password" ng-model="password" ng-class="{'input-validation-error': fieldErrors.password }"> 
                        <span ng-show="fieldErrors.password" class="field-validation-error-login text-center ">[[fieldErrors.password]]</span>
                    </div>

                    <div class="checkbox form-groupdefault ">
                        <label><input type="checkbox" ng-model="remember"> Remember me</label>
                    </div>

                   


                    <div class="form-groupdefault pull-left ">
                   <a ng-click="recover()" class="dark-text font-weight-400 font-10 pointer" >Recover password</a>
                
                    </div>


                    <div class=" pull-right  ">
                        <button class="btn btn-adgold no-margin-bottom btn-small btn-round no-margin-top form-control width-140 input-round medium-input" type="submit">Log in
                        </button>
                    </div>

            
                 </form>
                
                  <a class="white-text reg-link" href="home/registration" target="self">Have no account?</a>
                </div>
                 <button  title="Close (Esc)" type="button" onclick="closepop()" class=" mfp-close">×</button>
            </div>


        </div>
    </div>

         

<!--/end pop up-->


<script type="text/javascript">


    $('.scrollToTop').click(function () {
      
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    
    function closepop()
    {

        jQuery('#loginpop').modal('hide');
    }

    function verification_resend()
    {
       
        var data=
        {
            email:jQuery('#username').val(),
            type:'email'
        };
      

        jQuery.ajax(
        {
            url:'/api/register/emailverificationtoken',
            type:'post',
            data:data,
            success:function(data)
            {
                alert('Check your email for confirmation token');
            },
            error:function()
            {
                 alert('User not found');
            }
        });
    }


</script>

    
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/588b53a3bab87e66427d107b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    </body>
</html>