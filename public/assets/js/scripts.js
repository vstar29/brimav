var draweropen=false;
var offset=0;

jQuery(document).ready(function($) {

	//$.material.init();
	jQuery('.logout a').addClass('dropdown-item');
	
	jQuery('.search-button').removeClass('hidden');

	if(myajax.loggedIn)
	{

		jQuery('.mobile-login').addClass('hidden');
		jQuery('.mobile-signup').addClass('hidden');
		jQuery('.mobile-logout a').attr('href',myajax.baseUrl);
	}
	else
	{
		jQuery('.mobile-logout').addClass('hidden');
		jQuery('.mobile-profile').addClass('hidden');
	}
	

var window_width = $(window).width();

	

	/*--Image click pop up--*/
	
		jQuery('img').on('click',function()
		{
			if($(this).attr('width') !=96 && $(this).attr('height') !=96)
			{
				var picmodal=jQuery('#picture-pop');
				var pics=$(this);
				var src=$(this).attr('src');
				var clas=$(this).attr('class').split(' ')[1];
				clas=clas.split('-')[2];
				var captionclass='#attachment_'+clas+' p';

				jQuery('#img').attr('src',src);
				
				if(clas)
				{
					try
					{
						var caption=jQuery(captionclass);
						if (caption) {
							
							var captiontext=caption[0].innerText
						}
							
					}catch(e)
					{
						
					}
				}
				else
				{
					var captiontext=document.getElementsByTagName("title")[0].innerHTML;
					captiontext=captiontext.split('-')[0];
				}
				jQuery('#caption').text(captiontext);
				
				picmodal.modal('show');
				}
			

		})

		

		/*--Close Image Modal--*/

		jQuery('.btn-close').click(function()
		{
			jQuery('#picture-pop').modal('hide');
			$('.popover').addClass('hidden');  
		})

		/*----Ajax next Pagination--*/

		jQuery('#next').click(function(e)
		{
			var loader=jQuery('#loader').removeClass('hidden');
			 var inner= jQuery("#inner");
			 inner.addClass('blurbg');
			 e.preventDefault();
			jQuery.ajax({
			  type: 'POST',
			  url: myajax.ajax_url,
			  data: {
			  action: 'ajaxPagination',
			  offset:parseInt(offset)+8
			  },
			  success: function(data, textStatus, XMLHttpRequest){
			 
			  	var jsondata=jQuery.parseJSON(data);
			  	if(jsondata)
			  	{
			  		offset = offset + 8;
				  	var posts=jsondata['posts'];
				  	var thumbnails=jsondata['thumbnails'];
				  	var permalinks=jsondata.permalinks;
				  	var categories=jsondata.categories;
				  	var dates=jsondata.dates;
				  	var cat_icons=jsondata.cat_icons;
				  
				 	inner.html('');

				  for(var i=0;i< posts.length;i++)
				  {
				  	
				  	var title=posts[i].post_title.substr(0,50);
				  	var link=permalinks[i];
				  	var featured_image=thumbnails[i];
				  	var category=categories[i];
					var date=dates[i];
					var cat_icon=cat_icons[i];
				  	if(featured_image=='' || featured_image==undefined )
				  	{
				  		featured_image=myajax.placeholder;
				  	}
				  	inner.append('<div class="col-lg-4  col-md-12"><div class="card "><a href="'+link+'"title="'+title+'" class="view overlay hm-white-slight"><div class="no-dither waves-effect waves-light card-img-top" style="background-image:url('+featured_image+');"></div>'

	                   +'<div class="mask waves-effect waves-light"></div></a><span role="button" class="cat-icon"><i class="'+cat_icon+'"></i></span>'
	                   +'<div class="card-block">  <div class="row" style="margin-top: -10px;"><h6 style="color:#0b850b">'+category+'</h6><span class="pull-right date">'+date+'</span>'                    
	                   +'</div><span class="capitalize">'+title+'</span></div>');
				  }
				}
			  },
			  error: function(MLHttpRequest, textStatus, errorThrown){
			  
			  },
			  complete:function()
			  {
			  	inner.removeClass('blurbg');
			  	var loader=jQuery('#loader').addClass('hidden');

			  }
			  });
		})
	

		/*----Ajax previous Pagination--*/

		jQuery('#previous').click(function(e)
		{
			var loader=jQuery('#loader').removeClass('hidden');
			 var inner= jQuery("#inner");
			 inner.addClass('blurbg');
			 e.preventDefault();
			jQuery.ajax({
			  type: 'POST',
			  url: myajax.ajax_url,
			  data: {
			  action: 'ajaxPagination',
			  offset:parseInt(offset)-8
			  },
			  success: function(data, textStatus, XMLHttpRequest)
			  {
			  	
			  	var jsondata=jQuery.parseJSON(data);
			  	if(jsondata)
			  	{

			  		offset = offset - 8;
				  	var posts=jsondata['posts'];
				  	var thumbnails=jsondata['thumbnails'];
				  	var permalinks=jsondata.permalinks;
				  	var categories=jsondata.categories;
				  	var dates=jsondata.dates;
				  	var cat_icons=jsondata.cat_icons;
				  
				 	inner.html('');

				  	for(var i=0;i< posts.length;i++)
				  	{
				  	
					  	var title=posts[i].post_title.substr(0,90);
					  	var link=permalinks[i];
					  	var featured_image=thumbnails[i];
					  	var category=categories[i];
					  	var date=dates[i];
					  	var cat_icon=cat_icons[i];
					  	if(featured_image=='' || featured_image==undefined )
				  		{
				  			featured_image=myajax.placeholder;
				  		}

				  		inner.append('<div class="col-lg-4  col-md-12"><div class="card "><a href="'+link+'"title="'+title+'" class="view overlay hm-white-slight "><div class="no-dither card-img-top waves-effect waves-light" style="background-image:url('+featured_image+');"></div>'

	                   +'<div class="mask waves-effect waves-light"></div></a><span class="cat-icon"><i class="'+cat_icon+'"></i></span>'
	                   +'<div class="card-block">  <div class="row" style="margin-top: -10px;"><h6 style="color:#0b850b">'+category+'</h6><span class="pull-right date">'+date+'</span>'                     
	                   +'</div><span class="capitalize">'+title+'</span></div>');
					
				 	}
				   
                 }          
			  },
			  error: function(MLHttpRequest, textStatus, errorThrown){
			  
			  },
			  complete:function()
			  {
			  	inner.removeClass('blurbg');
			  	var loader=jQuery('#loader').addClass('hidden');

			  }
			  });
		})

		/*--Entertainment list hover swap--*/

		jQuery('.small-item a').hover(function()
		{
			
			var caller=jQuery(this);
			var img=caller.data('img');
			var title=caller.data('title');
			var excerpt=caller.data('excerpt');
			var link=caller.attr('href');
		
    
			
			var big=jQuery('.main-item');
			big.attr('href',link);
			big.attr('title',title);
			jQuery('.main-item .no-dither').css('background-image','url('+img+')');
			jQuery('.main-item .no-dither').attr('title',title);
			jQuery('.main-item h6').text(title);
			jQuery('.main-item +p').html(excerpt);
		
		});
	


	//    Activate bootstrap-select
	$( '.select' ).dropdown({
		'dropdownClass': 'dropdown-menu',
		'optionClass': ''
	});

	// Active Carousel
	$( '.carousel' ).carousel({
		interval: 6000
	});

	//  Activate popover
	$('[data-toggle="popover"]').popover();



	var transparent = true;

	if ($( '.navbar-color-on-scroll' ).length !== 0) {
		$(window).on( 'scroll', debounce(function () {
			if ($(document).scrollTop() > 200) {
				if (transparent) {
					transparent = false;
					$( '.navbar-color-on-scroll' ).removeClass( 'navbar-transparent' );
				}
			} else {
				if (!transparent) {
					transparent = true;
					$( '.navbar-color-on-scroll' ).addClass( 'navbar-transparent' );
				}
			}
		}, 17));
	}

	if (window_width >= 768) {
		var big_image = $( '.page-header[data-parallax="active"]' );
		if (big_image.length !== 0) {
			$(window).on( 'scroll', debounce(function () {
				if (isElementInViewport(big_image)) {
					var oVal = ($(window).scrollTop() / 3);
					big_image.css({
						'transform': 'translate3d(0,' + oVal + 'px,0)',
						'-webkit-transform': 'translate3d(0,' + oVal + 'px,0)',
						'-ms-transform': 'translate3d(0,' + oVal + 'px,0)',
						'-o-transform': 'translate3d(0,' + oVal + 'px,0)'
					});
				}
			}, 4));
		}

	}

	function debounce(func, wait, immediate) {
		var timeout;
		return function () {
			var context = this,
				args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function () {
				timeout = null;
				if (!immediate) {
					func.apply(context, args);
				}
			}, wait);
			if (immediate && !timeout) {
				func.apply(context, args);
			}
		};
	}

	function isElementInViewport(elem) {
		var $elem = $(elem);

		// Get the scroll position of the page.
		var scrollElem = ((navigator.userAgent.toLowerCase().indexOf( 'webkit' ) !== -1) ? 'body' : 'html' );
		var viewportTop = $(scrollElem).scrollTop();
		var viewportBottom = viewportTop + $(window).height();

		// Get the position of the element on the page.
		var elemTop = Math.round($elem.offset().top);
		var elemBottom = elemTop + $elem.height();

		return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
	}

	/* Smooth Scroll */
	$( '.navbar a[href*="#"], a.btn[href*="#"]' ).click(function () {
		var menuitem = $(this).attr( 'class' );
		if (menuitem === 'dropdown-toggle' ) {
			return;
		}
		if (location.pathname.replace(/^\//, '' ) === this.pathname.replace(/^\//, '' ) && location.hostname === this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $( '[name=' + this.hash.slice(1) + ']' );
			if (target.length) {
				$( 'html,body' ).animate({
					scrollTop: target.offset().top
				}, 1200);
				return false;
			}
		}
	});

	/* Apply matchHeight to match team grid */
	var byRow = $( 'body' ).hasClass( 'home' );

	if (typeof $( '#features' ) !== 'undefined' ) {
		$( '.features .row' ).each(function () {
			$(this).children( '.col-md-4' ).matchHeight(byRow);
		});
	}

	if (typeof $( '#products' ) !== 'undefined' ) {
		$( '.products .row' ).each(function () {
			$(this).children( '.col-md-3' ).matchHeight(byRow);
		});
	}

	if (typeof $( '.products' ) !== 'undefined' ) {
		$( '.products' ).each(function () {
			$(this).children( '.col-sm-6' ).matchHeight(byRow);
		});
	}

	if (typeof $( '#portfolio' ) !== 'undefined' ) {
		$( '.work .row' ).each(function () {
			$(this).children( '.col-md-4' ).matchHeight(byRow);
		});

		$( '.work .row' ).each(function () {
			$(this).children( '.col-md-6' ).matchHeight(byRow);
		});
	}

	if (typeof $( '#team' ) !== 'undefined' ) {
		$( '.team .row' ).each(function () {
			$(this).children( '.col-md-6' ).matchHeight(byRow);
		});
	}

	if (typeof $( '#testimonials' ) !== 'undefined' ) {
		$( '.testimonials .row' ).each(function () {
			$(this).children( '.col-md-4' ).matchHeight(byRow);
		});
	}

	if (typeof $( '#pricing' ) !== 'undefined' ) {
		$( '.pricing .row .col-md-7' ).each(function () {
			$(this).children( '.col-md-6' ).matchHeight(byRow);
		});
	}

	if ( typeof $( '#blog' ) !== 'undefined' ) {
		$( '.blogs .row' ).each( function() {
			$( this ).children( '.col-md-4' ).matchHeight( byRow );
		});
	}

	if (typeof $( '.contact_name_wrap' ) !== 'undefined' ) {
		if ($( '.contact_name_wrap' ).hasClass( 'col-sm-4' )) {
			$( '.contact_name_wrap' ).removeClass( 'col-sm-4' );
		}
		if ($( '.contact_name_wrap' ).hasClass( 'col-lg-4' )) {
			$( '.contact_name_wrap' ).removeClass( 'col-lg-4' );
		}
		$( '.contact_name_wrap' ).addClass( 'col-md-6' );
		$( '.contact_name_wrap .form-group' ).addClass( 'label-floating' );
		var placeholder1 = $( '.contact_name_wrap #pirate-forms-contact-name' ).attr( 'placeholder' );
		$( '.contact_name_wrap #pirate-forms-contact-name' ).removeAttr( 'placeholder' );
		$( '<label class="control-label"> '+placeholder1+' </label>' ).insertBefore( '.contact_name_wrap #pirate-forms-contact-name' );
	}

	if (typeof $( '.contact_email_wrap' ) !== 'undefined' ) {
		if ($( '.contact_email_wrap' ).hasClass( 'col-sm-4' )) {
			$( '.contact_email_wrap' ).removeClass( 'col-sm-4' );
		}
		if ($( '.contact_email_wrap' ).hasClass( 'col-lg-4' )) {
			$( '.contact_email_wrap' ).removeClass( 'col-lg-4' );
		}
		$( '.contact_email_wrap' ).addClass( 'col-md-6' );
		$( '.contact_email_wrap .form-group' ).addClass( 'label-floating' );
		var placeholder2 = $( '.contact_email_wrap #pirate-forms-contact-email' ).attr( 'placeholder' );
		$( '.contact_email_wrap #pirate-forms-contact-email' ).removeAttr( 'placeholder' );
		$( '<label class="control-label"> '+placeholder2+' </label>' ).insertBefore( '.contact_email_wrap #pirate-forms-contact-email' );
	}

	if (typeof $( '.contact_subject_wrap' ) !== 'undefined' ) {
		if ($( '.contact_subject_wrap' ).hasClass( 'col-sm-4' )) {
			$( '.contact_subject_wrap' ).removeClass( 'col-sm-4' );
		}
		if ($( '.contact_subject_wrap' ).hasClass( 'col-lg-4' )) {
			$( '.contact_subject_wrap' ).removeClass( 'col-lg-4' );
		}
		$( '.contact_subject_wrap' ).addClass( 'col-md-12' );
		$( '.contact_subject_wrap .form-group' ).addClass( 'label-floating' );
		var placeholder3 = $( '.contact_subject_wrap #pirate-forms-contact-subject' ).attr( 'placeholder' );
		$( '.contact_subject_wrap #pirate-forms-contact-subject' ).removeAttr( 'placeholder' );
		$( '<label class="control-label"> '+placeholder3+' </label>' ).insertBefore( '.contact_subject_wrap #pirate-forms-contact-subject' );
	}

	if (typeof $( '.contact_message_wrap' ) !== 'undefined' ) {
		$( '.contact_message_wrap textarea' ).attr( 'rows', '6' );
		$( '.contact_message_wrap .form-group' ).addClass( 'label-floating' );
		var placeholder4 = $( '.contact_message_wrap #pirate-forms-contact-message' ).attr( 'placeholder' );
		$( '.contact_message_wrap #pirate-forms-contact-message' ).removeAttr( 'placeholder' );
		$( '<label class="control-label"> '+placeholder4+' </label>' ).insertBefore( '.contact_message_wrap #pirate-forms-contact-message' );
	}

	if (typeof $( '.contact_submit_wrap' ) !== 'undefined' ) {
		$( '.pirate-forms-submit-button' ).addClass( 'btn btn-primary' );
	}

	if (typeof $( '.form_captcha_wrap' ) !== 'undefined' ) {
		if ($( '.form_captcha_wrap' ).hasClass( 'col-sm-4' )) {
			$( '.form_captcha_wrap' ).removeClass( 'col-sm-6' );
		}
		if ($( '.form_captcha_wrap' ).hasClass( 'col-lg-6' )) {
			$( '.form_captcha_wrap' ).removeClass( 'col-lg-6' );
		}
		$( '.form_captcha_wrap' ).addClass( 'col-md-12' );
	}

	if (typeof $( 'form' ) !== 'undefined' ) {
		$( 'form' ).addClass( 'form-group' );
	}

	if (typeof $( 'input' ) !== 'undefined' ) {
		if (typeof $( 'input[type="text"]' ) !== 'undefined' ) {
			$( 'input[type="text"]' ).addClass( 'form-control' );
		}

		if (typeof $( 'input[type="email"]' ) !== 'undefined' ) {
			$( 'input[type="email"]' ).addClass( 'form-control' );
		}

		if (typeof $( 'input[type="url"]' ) !== 'undefined' ) {
			$( 'input[type="url"]' ).addClass( 'form-control' );
		}

		if (typeof $( 'input[type="password"]' ) !== 'undefined' ) {
			$( 'input[type="password"]' ).addClass( 'form-control' );
		}

		if (typeof $( 'input[type="tel"]' ) !== 'undefined' ) {
			$( 'input[type="tel"]' ).addClass( 'form-control' );
		}

		if (typeof $( 'input[type="search"]' ) !== 'undefined' ) {
			$( 'input[type="search"]' ).addClass( 'form-control' );
		}

		if (typeof $( 'input.select2-input' ) !== 'undefined' ) {
			$( 'input.select2-input' ).removeClass( 'form-control' );
		}
	}

	if (typeof $( 'textarea' ) !== 'undefined' ) {
		$( 'textarea' ).addClass( 'form-control' );
	}

	if (typeof $( '.form-control' ) !== 'undefined' ) {
		$( '.form-control' ).parent().addClass( 'form-group' );

		$(window).on( 'scroll', function () {
			$( '.form-control' ).parent().addClass( 'form-group' );
		});
	}

	$(window).on( 'scroll', function () {
		if ($(window).width() >= 751) {
			var hestia_scrollTop = $(window).scrollTop(); // cursor position
			var headerHeight = $( '.navbar' ).outerHeight(); // header height
			var isInOneSection = 'no'; // used for checking if the cursor is in one section or not
			// for all sections check if the cursor is inside a section
			$( '#carousel-hestia-generic, section' ).each(function () {
				var thisID = '#' + $(this).attr( 'id' ); // section id
				var hestia_offset = $(this).offset().top; // distance between top and our section
				var thisHeight = $(this).outerHeight(); // section height
				var thisBegin = hestia_offset - headerHeight; // where the section begins
				var thisEnd = hestia_offset + thisHeight - headerHeight; // where the section ends  
				// if position of the cursor is inside of the this section
				if (hestia_scrollTop >= thisBegin && hestia_scrollTop <= thisEnd) {
					isInOneSection = 'yes';
					$( 'nav .active' ).removeClass( 'active' );
					$( 'nav a[href$="' + thisID + '"]' ).parent( 'li' ).addClass( 'active' ); // find the menu button with the same ID section
					return false;
				}
				if (isInOneSection === 'no' ) {
					$( 'nav .active' ).removeClass( 'active' );
				}
			});
		}
	});

	$('body').on('added_to_cart',function(){

		if( typeof $( '.added_to_cart' ) !== 'undefined' ) {

			$( '.added_to_cart' ).each( function () {
				if ( ! ( $( this ).parent().hasClass( 'hestia-view-cart-wrapper' ) ) ) {
					$( this ).wrap( '<div class="hestia-view-cart-wrapper"></div>' );
				}
			});
		}
	});

});

