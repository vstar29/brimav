<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* @LOCATION ROUTE*/



Route::get('/', 'LocationControllers\Home\HomeController@getIndex');

Route::get('dailycrone','LocationControllers\Shared\CroneController@dailyCrone');
/*Route::get('receipt/{id}', 'LocationControllers\Advertiser\AdvertiserReceiptController@getIndex')->middleware('auth','advertiser');*/


/*--Home route--*/
Route::group(array('prefix'=>'/home','as' => '/home.'),function(){

	Route::get('logins/login', 'LocationControllers\Home\HomeController@getLogin');

	Route::get('logins/passwordrecovery', 'LocationControllers\Home\HomeController@getRecover');
	Route::get('index', 'LocationControllers\Home\HomeController@getHome');
  
  


	/*--Registration route--*/
	Route::group(array('prefix'=>'/registration','as' => '/registration.'),function(){

		Route::get('', 'LocationControllers\Home\RegController@getIndex');
		Route::get('registration', 'LocationControllers\Home\RegController@getSignup');
		Route::get('publisher', 'LocationControllers\Home\RegController@getPublisherreg');
		Route::get('regsuccesfull', 'LocationControllers\Home\RegController@getRegsuccesfull');
		Route::get('telltemplate', 'LocationControllers\Home\RegController@getTelltemplate');

	});

});

	/*---redirect /register to /registration--*/
Route::group(array('prefix'=>'/register','as' => '/register.'),function(){

	Route::get('/{any?}', 'LocationControllers\Home\RegController@getIndex')->where("any", ".*");
});




/*Admin Location route*/


Route::group(array('prefix'=>'/admin','as' => '/admin.','middleware' => ['auth','admin']),function(){


	Route::get('/home', 'LocationControllers\Admin\AdminHomeController@getIndex');

	Route::get('/profile/{any?}', 'LocationControllers\Admin\AdminProfileController@getIndex')->where("any", ".*");

	Route::get('/advertiser/{any?}', 'LocationControllers\Admin\AdminAdvertiserController@getIndex')->where("any", ".*");

	Route::get('/publisher/{any?}', 'LocationControllers\Admin\AdminPublisherController@getIndex')->where("any", ".*");

	Route::get('/advert/{any?}', 'LocationControllers\Admin\AdminAdvertController@getIndex')->where("any", ".*");

	Route::get('/payment/{any?}', 'LocationControllers\Admin\AdminPaymentController@getIndex')->where("any", ".*");

	Route::get('/coupon/{any?}', 'LocationControllers\Admin\AdminCouponController@getIndex')->where("any", ".*");

	Route::get('/bannertype/{any?}', 'LocationControllers\Admin\AdminBannerTypeController@getIndex')->where("any", ".*");

	Route::get('/{any?}', 'LocationControllers\Admin\AdminHomeController@getIndex')->where("any", ".*");


});

/*--Route use by Angular location--*/

/*--home--*/
Route::group(array('prefix'=>'/adminhome','as' => '/adminhome.','middleware' => ['auth','admin']),function(){
	Route::get('', 'LocationControllers\Admin\AdminHomeController@getIndex');
	Route::get('/home', 'LocationControllers\Admin\AdminHomeController@getIndex');

});


/*--profile--*/
Route::group(array('prefix'=>'/adminprofile','as' => '/adminprofile.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminProfileController@getIndex');
	Route::get('home', 'LocationControllers\Admin\AdminProfileController@getHome');
	Route::get('basicdetails', 'LocationControllers\Admin\AdminProfileController@getBasicdetails');
	Route::get('editadminbasicdetails', 'LocationControllers\Admin\AdminProfileController@getEditadminbasicdetails');
	Route::get('changepassword', 'LocationControllers\Admin\AdminProfileController@getChangepassword');


});



/*--adverts--*/

Route::group(array('prefix'=>'/adminadvert','as' => '/adminadvert.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminAdvertController@getIndex');
	Route::get('template', 'LocationControllers\Admin\AdminAdvertController@getTemplate');
	Route::get('home', 'LocationControllers\Admin\AdminAdvertController@getHome');
	Route::get('completed', 'LocationControllers\Admin\AdminAdvertController@getCompleted');
	Route::get('uncompleted', 'LocationControllers\Admin\AdminAdvertController@getUncompleted');

	Route::get('details', 'LocationControllers\Admin\AdminAdvertController@getDetails');

	Route::get('dialogbox', 'LocationControllers\Admin\AdminAdvertController@getDialogbox');

});


/*--advertiser--*/

Route::group(array('prefix'=>'/adminadvertiser','as' => '/adminadvertiser.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminAdvertiserController@getIndex');
	
	Route::get('home', 'LocationControllers\Admin\AdminAdvertiserController@getHome');

	Route::get('details', 'LocationControllers\Admin\AdminAdvertiserController@getDetails');

	Route::get('dialogbox', 'LocationControllers\Admin\AdminAdvertiserController@getDialogbox');

});

/*--publisher--*/

Route::group(array('prefix'=>'/adminpublisher','as' => '/adminpublisher.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminPublisherController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminPublisherController@getHome');

	Route::get('template', 'LocationControllers\Admin\AdminPublisherController@getTemplate');

	Route::get('details', 'LocationControllers\Admin\AdminPublisherController@getDetails');
	Route::get('basicdetails', 'LocationControllers\Admin\AdminPublisherController@getBasicdetails');

	Route::get('accountdetails', 'LocationControllers\Admin\AdminPublisherController@getAccountdetails');


	Route::get('paymentdetails', 'LocationControllers\Admin\AdminPublisherController@getPaymentdetails');

	Route::get('editbalance', 'LocationControllers\Admin\AdminPublisherController@getEditbalance');


	Route::get('active', 'LocationControllers\Admin\AdminPublisherController@getActive');

	Route::get('inactive', 'LocationControllers\Admin\AdminPublisherController@getInactive');


	Route::get('dialogbox', 'LocationControllers\Admin\AdminPublisherController@getDialogbox');
	
});


/*--payment--*/

Route::group(array('prefix'=>'/adminpayment','as' => '/adminpayment.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminPaymentController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminPaymentController@getHome');

	Route::get('details', 'LocationControllers\Admin\AdminPaymentController@getDetails');

	Route::get('request', 'LocationControllers\Admin\AdminPaymentController@getRequest');

	Route::get('paid', 'LocationControllers\Admin\AdminPaymentController@getPaid');

	Route::get('publisherpaymount', 'LocationControllers\Admin\AdminPaymentController@getPublisherpaymount');


	
});


/*--coupon--*/

Route::group(array('prefix'=>'/admincoupon','as' => '/admincoupon.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminCouponController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminCouponController@getHome');

	Route::get('create', 'LocationControllers\Admin\AdminCouponController@getCreate');

	Route::get('edit', 'LocationControllers\Admin\AdminCouponController@getEdit');

	
});


/*--banner types--*/

Route::group(array('prefix'=>'/adminbannertype','as' => '/adminbannertype.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminBannerTypeController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminBannerTypeController@getHome');

	Route::get('create', 'LocationControllers\Admin\AdminBannerTypeController@getCreate');

	Route::get('edit', 'LocationControllers\Admin\AdminBannerTypeController@getEdit');

	
});




/*Publisher Location route*/


Route::group(array('prefix'=>'/publisher','as' => '/publisher.','middleware' => ['auth','publisher']),function(){


	Route::get('/home', 'LocationControllers\Publisher\PublisherHomeController@getIndex');

	Route::get('/profile/{any?}', 'LocationControllers\Publisher\PublisherProfileController@getIndex')->where("any", ".*");

	Route::get('/advert/{any?}', 'LocationControllers\Publisher\PublisherAdvertController@getIndex')->where("any", ".*");

	Route::get('/payment/{any?}', 'LocationControllers\Publisher\PublisherPaymentController@getIndex')->where("any", ".*");

	Route::get('/banner/{any?}', 'LocationControllers\Publisher\PublisherBannerController@getIndex')->where("any", ".*");

	Route::get('/social/{any?}', 'LocationControllers\Publisher\PublisherSocialController@getIndex')->where("any", ".*");

	Route::get('/website/{any?}', 'LocationControllers\Publisher\PublisherWebsiteController@getIndex')->where("any", ".*");

	Route::get('/tv/{any?}', 'LocationControllers\Publisher\PublisherTvController@getIndex')->where("any", ".*");

		Route::get('/programme/{any?}', 'LocationControllers\Publisher\PublisherProgrammeController@getIndex')->where("any", ".*");

	Route::get('/{any?}', 'LocationControllers\Publisher\PublisherHomeController@getIndex')->where("any", ".*");
	

});

/*--Route use by Angular location--*/

/*--home--*/
Route::group(array('prefix'=>'/publisherhome','as' => '/publisherhome.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'LocationControllers\Publisher\PublisherHomeController@getIndex');
	Route::get('/home', 'LocationControllers\Publisher\PublisherHomeController@getHome');

});


/*--profile--*/
Route::group(array('prefix'=>'/publisherprofile','as' => '/publisherprofile.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherProfileController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherProfileController@getHome');

	Route::get('basicdetails', 'LocationControllers\Publisher\PublisherProfileController@getBasic');
	Route::get('accountdetails', 'LocationControllers\Publisher\PublisherProfileController@getAccount');

	Route::get('editbasic', 'LocationControllers\Publisher\PublisherProfileController@getEditBasic');

	Route::get('editaccount', 'LocationControllers\Publisher\PublisherProfileController@getEditaccount');

	Route::get('changepassword', 'LocationControllers\Publisher\PublisherProfileController@getChangepassword');


});



/*--adverts--*/

Route::group(array('prefix'=>'/publisheradvert','as' => '/publisheradvert.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherAdvertController@getIndex');
	
	Route::get('home', 'LocationControllers\Publisher\PublisherAdvertController@getHome');

	Route::get('completed', 'LocationControllers\Publisher\PublisherAdvertController@getCompleted');

	Route::get('uncompleted', 'LocationControllers\Publisher\PublisherAdvertController@getUncompleted');

	Route::get('details', 'LocationControllers\Publisher\PublisherAdvertController@getDetails');

	Route::get('dialogbox', 'LocationControllers\Publisher\PublisherAdvertController@getDialogbox');

	Route::get('player', 'LocationControllers\Publisher\PublisherAdvertController@getPlayer');

	Route::get('insertlink', 'LocationControllers\Publisher\PublisherAdvertController@getInsertlink');

});






/*--payment--*/

Route::group(array('prefix'=>'/publisherpayment','as' => '/publisherpayment.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherPaymentController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherPaymentController@getHome');

	Route::get('details', 'LocationControllers\Admin\AdminPaymentController@getDetails');

	
});


/*--banners--*/

Route::group(array('prefix'=>'/publisherbanner','as' => '/publisherbanner.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherBannerController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherBannerController@getHome');

	Route::get('create', 'LocationControllers\Publisher\PublisherBannerController@getCreate');

	Route::get('edit', 'LocationControllers\Publisher\PublisherBannerController@getEdit');

	
});



/*--social--*/

Route::group(array('prefix'=>'/publishersocial','as' => '/publishersocial.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherSocialController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherSocialController@getHome');

	Route::get('create', 'LocationControllers\Publisher\PublisherSocialController@getCreate');

	Route::get('edit', 'LocationControllers\Publisher\PublisherSocialController@getEdit');

	
});



/*--tv--*/

Route::group(array('prefix'=>'/publishertv','as' => '/publishertv.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherTvController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherTvController@getHome');

	Route::get('create', 'LocationControllers\Publisher\PublisherTvController@getCreate');

	Route::get('edit', 'LocationControllers\Publisher\PublisherTvController@getEdit');
	
});


/*--website--*/

Route::group(array('prefix'=>'/publisherwebsite','as' => '/publisherwebsite.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherWebsiteController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherWebsiteController@getHome');

	Route::get('create', 'LocationControllers\Publisher\PublisherWebsiteController@getCreate');

	Route::get('edit', 'LocationControllers\Publisher\PublisherWebsiteController@getEdit');
	
});



/*--programs--*/

Route::group(array('prefix'=>'/publisherprogramme','as' => '/publisherprogramme.','middleware' => ['auth','publisher']),function(){

	Route::get('', 'LocationControllers\Publisher\PublisherProgrammeController@getIndex');

	Route::get('home', 'LocationControllers\Publisher\PublisherProgrammeController@getHome');

	Route::get('create', 'LocationControllers\Publisher\PublisherProgrammeController@getCreate');

	Route::get('edit', 'LocationControllers\Publisher\PublisherProgrammeController@getEdit');
	
});





/*Advertiser Location route*/


Route::group(array('prefix'=>'/advertiser','as' => '/advertiser.','middleware' => ['auth','advertiser']),function(){

	Route::get('/home', 'LocationControllers\Advertiser\AdvertiserHomeController@getIndex');

	Route::get('/profile/{any?}', 'LocationControllers\Advertiser\AdvertiserProfileController@getIndex')->where("any", ".*");


	Route::get('/advert/{any?}', 'LocationControllers\Advertiser\AdvertiserAdvertController@getIndex')->where("any", ".*");

	Route::get('/{any?}', 'LocationControllers\Advertiser\AdvertiserHomeController@getIndex')->where("any", ".*");



});

/*--Route use by Angular location--*/

/*--home--*/
Route::group(array('prefix'=>'/advertiserhome','as' => '/advertiserhome.','middleware' => ['auth','advertiser']),function(){
	Route::get('', 'LocationControllers\Advertiser\AdvertiserHomeController@getIndex');
	Route::get('/home', 'LocationControllers\Advertiser\AdvertiserHomeController@getHome');

});


/*--profile--*/
Route::group(array('prefix'=>'/advertiserprofile','as' => '/advertiserprofile.','middleware' => ['auth','advertiser']),function(){

	Route::get('', 'LocationControllers\Advertiser\AdvertiserProfileController@getIndex');

	Route::get('home', 'LocationControllers\Advertiser\AdvertiserProfileController@getHome');

	Route::get('basicdetails', 'LocationControllers\Advertiser\AdvertiserProfileController@getBasicdetails');
	

	Route::get('editbasic', 'LocationControllers\Advertiser\AdvertiserProfileController@getEditadvertiserbasicdetails');


	Route::get('changepassword', 'LocationControllers\Advertiser\AdvertiserProfileController@getChangepassword');


});



/*--adverts--*/

Route::group(array('prefix'=>'/advertiseradvert','as' => '/advertiseradvert.','middleware' => ['auth','advertiser']),function(){

	Route::get('', 'LocationControllers\Advertiser\AdvertiserAdvertController@getIndex');
	
	Route::get('home', 'LocationControllers\Advertiser\AdvertiserAdvertController@getHome');

	Route::get('completed', 'LocationControllers\Advertiser\AdvertiserAdvertController@getCompleted');

	Route::get('uncompleted', 'LocationControllers\Advertiser\AdvertiserAdvertController@getUncompleted');

	Route::get('details', 'LocationControllers\Advertiser\AdvertiserAdvertController@getDetails');

	Route::get('shared/multimodal', 'LocationControllers\Advertiser\AdvertiserAdvertController@getDialogbox');
	Route::get('create/adverttype', 'LocationControllers\Advertiser\AdvertiserAdvertController@getAdverttype');

	Route::get('create/home', 'LocationControllers\Advertiser\AdvertiserAdvertController@getCreate');

	Route::get('create/uploadsong', 'LocationControllers\Advertiser\AdvertiserAdvertController@getUploadsong');


	Route::get('create/others', 'LocationControllers\Advertiser\AdvertiserAdvertController@getOthers');

	Route::get('create/banneradvert', 'LocationControllers\Advertiser\AdvertiserAdvertController@getBanneradvert');

	Route::get('create/basicinfo', 'LocationControllers\Advertiser\AdvertiserAdvertController@getBasicinfo');

	Route::get('create/editbasicinfo', 'LocationControllers\Advertiser\AdvertiserAdvertController@getEditbasicinfo');

	Route::get('create/editothers', 'LocationControllers\Advertiser\AdvertiserAdvertController@getEditothers');

	Route::get('create/editbanner', 'LocationControllers\Advertiser\AdvertiserAdvertController@getEditbanner');

	Route::get('create/songmetadata', 'LocationControllers\Advertiser\AdvertiserAdvertController@getSongmetadata');

	Route::get('create/selectpublishers', 'LocationControllers\Advertiser\AdvertiserAdvertController@getSelectpublishers');

	Route::get('create/advertpayment', 'LocationControllers\Advertiser\AdvertiserAdvertController@getAdvertpayment');
	Route::get('create/advertsuccess', 'LocationControllers\Advertiser\AdvertiserAdvertController@getSuccess');
	});
