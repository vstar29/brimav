<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* @LOCATION ROUTE*/



Route::get('/', 'LocationControllers\Home\HomeController@getIndex');

Route::get('dailycrone','LocationControllers\Shared\CroneController@dailyCrone');
/*Route::get('receipt/{id}', 'LocationControllers\Advertiser\AdvertiserReceiptController@getIndex')->middleware('auth','advertiser');*/


/*--Home route--*/
Route::group(array('prefix'=>'/home','as' => '/home.'),function(){

	Route::get('logins/login', 'LocationControllers\Home\HomeController@getLogin');

	Route::get('logins/passwordrecovery', 'LocationControllers\Home\HomeController@getRecover');
	Route::get('index', 'LocationControllers\Home\HomeController@getHome');
  
  


	/*--Registration route--*/
	Route::group(array('prefix'=>'/registration','as' => '/registration.'),function(){

		Route::get('', 'LocationControllers\Home\RegController@getIndex');
		Route::get('registration', 'LocationControllers\Home\RegController@getSignup');
		Route::get('publisher', 'LocationControllers\Home\RegController@getPublisherreg');
		Route::get('regsuccesfull', 'LocationControllers\Home\RegController@getRegsuccesfull');
		Route::get('telltemplate', 'LocationControllers\Home\RegController@getTelltemplate');

	});

});

	/*---redirect /register to /registration--*/
Route::group(array('prefix'=>'/register','as' => '/register.'),function(){

	Route::get('/{any?}', 'LocationControllers\Home\RegController@getIndex')->where("any", ".*");
});




/*Admin Location route*/


Route::group(array('prefix'=>'/admin','as' => '/admin.','middleware' => ['auth','admin']),function(){


	Route::get('/home', 'LocationControllers\Admin\AdminHomeController@getIndex');

	Route::get('/profile/{any?}', 'LocationControllers\Admin\AdminProfileController@getIndex')->where("any", ".*");

	Route::get('/advertiser/{any?}', 'LocationControllers\Admin\AdminAdvertiserController@getIndex')->where("any", ".*");

	Route::get('/publisher/{any?}', 'LocationControllers\Admin\AdminPublisherController@getIndex')->where("any", ".*");

	Route::get('/advert/{any?}', 'LocationControllers\Admin\AdminAdvertController@getIndex')->where("any", ".*");

	Route::get('/payment/{any?}', 'LocationControllers\Admin\AdminPaymentController@getIndex')->where("any", ".*");

	Route::get('/coupon/{any?}', 'LocationControllers\Admin\AdminCouponController@getIndex')->where("any", ".*");

	Route::get('/bannertype/{any?}', 'LocationControllers\Admin\AdminBannerTypeController@getIndex')->where("any", ".*");

	Route::get('/{any?}', 'LocationControllers\Admin\AdminHomeController@getIndex')->where("any", ".*");


});

/*--Route use by Angular location--*/

/*--home--*/
Route::group(array('prefix'=>'/adminhome','as' => '/adminhome.','middleware' => ['auth','admin']),function(){
	Route::get('', 'LocationControllers\Admin\AdminHomeController@getIndex');
	Route::get('/home', 'LocationControllers\Admin\AdminHomeController@getIndex');

});


/*--profile--*/
Route::group(array('prefix'=>'/adminprofile','as' => '/adminprofile.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminProfileController@getIndex');
	Route::get('home', 'LocationControllers\Admin\AdminProfileController@getHome');
	Route::get('basicdetails', 'LocationControllers\Admin\AdminProfileController@getBasicdetails');
	Route::get('editadminbasicdetails', 'LocationControllers\Admin\AdminProfileController@getEditadminbasicdetails');
	Route::get('changepassword', 'LocationControllers\Admin\AdminProfileController@getChangepassword');


});



/*--adverts--*/

Route::group(array('prefix'=>'/adminadvert','as' => '/adminadvert.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminAdvertController@getIndex');
	Route::get('template', 'LocationControllers\Admin\AdminAdvertController@getTemplate');
	Route::get('home', 'LocationControllers\Admin\AdminAdvertController@getHome');
	Route::get('completed', 'LocationControllers\Admin\AdminAdvertController@getCompleted');
	Route::get('uncompleted', 'LocationControllers\Admin\AdminAdvertController@getUncompleted');

	Route::get('details', 'LocationControllers\Admin\AdminAdvertController@getDetails');

	Route::get('dialogbox', 'LocationControllers\Admin\AdminAdvertController@getDialogbox');

});


/*--advertiser--*/

Route::group(array('prefix'=>'/adminadvertiser','as' => '/adminadvertiser.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminAdvertiserController@getIndex');
	
	Route::get('home', 'LocationControllers\Admin\AdminAdvertiserController@getHome');

	Route::get('details', 'LocationControllers\Admin\AdminAdvertiserController@getDetails');

	Route::get('dialogbox', 'LocationControllers\Admin\AdminAdvertiserController@getDialogbox');

});

/*--publisher--*/

Route::group(array('prefix'=>'/adminpublisher','as' => '/adminpublisher.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminPublisherController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminPublisherController@getHome');

	Route::get('template', 'LocationControllers\Admin\AdminPublisherController@getTemplate');

	Route::get('details', 'LocationControllers\Admin\AdminPublisherController@getDetails');
	Route::get('basicdetails', 'LocationControllers\Admin\AdminPublisherController@getBasicdetails');

	Route::get('accountdetails', 'LocationControllers\Admin\AdminPublisherController@getAccountdetails');


	Route::get('paymentdetails', 'LocationControllers\Admin\AdminPublisherController@getPaymentdetails');

	Route::get('editbalance', 'LocationControllers\Admin\AdminPublisherController@getEditbalance');


	Route::get('active', 'LocationControllers\Admin\AdminPublisherController@getActive');

	Route::get('inactive', 'LocationControllers\Admin\AdminPublisherController@getInactive');


	Route::get('dialogbox', 'LocationControllers\Admin\AdminPublisherController@getDialogbox');
	
});


/*--payment--*/

Route::group(array('prefix'=>'/adminpayment','as' => '/adminpayment.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminPaymentController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminPaymentController@getHome');

	Route::get('details', 'LocationControllers\Admin\AdminPaymentController@getDetails');

	Route::get('request', 'LocationControllers\Admin\AdminPaymentController@getRequest');

	Route::get('paid', 'LocationControllers\Admin\AdminPaymentController@getPaid');

	Route::get('publisherpaymount', 'LocationControllers\Admin\AdminPaymentController@getPublisherpaymount');


	
});


/*--coupon--*/

Route::group(array('prefix'=>'/admincoupon','as' => '/admincoupon.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminCouponController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminCouponController@getHome');

	Route::get('create', 'LocationControllers\Admin\AdminCouponController@getCreate');

	Route::get('edit', 'LocationControllers\Admin\AdminCouponController@getEdit');

	
});


/*--banner types--*/

Route::group(array('prefix'=>'/adminbannertype','as' => '/adminbannertype.','middleware' => ['auth','admin']),function(){

	Route::get('', 'LocationControllers\Admin\AdminBannerTypeController@getIndex');

	Route::get('home', 'LocationControllers\Admin\AdminBannerTypeController@getHome');

	Route::get('create', 'LocationControllers\Admin\AdminBannerTypeController@getCreate');

	Route::get('edit', 'LocationControllers\Admin\AdminBannerTypeController@getEdit');

	
});




/*Publisher Location route*/


Route::group(array('prefix'=>'/publisher','as' => '/publisher.','middleware' => ['auth','publisher']),function(){


	Route::get('/home', 'LocationControllers\Publisher\PublisherHomeController@getIndex');

	Route::get('/profile/{any?}', 'LocationControllers\Publisher\PublisherProfileController@getIndex')->where("any", ".*");

	Route::get('/advert/{any?}', 'LocationControllers\Publisher\PublisherAdvertController@getIndex')->where("any", ".*");

	Route::get('/payment/{any?}', 'LocationControllers\Publisher\PublisherPaymentController@getIndex')->where("any", ".*");

	Route::get('/banner/{any?}', 'LocationControllers\Publisher\PublisherBannerController@getIndex')->where("any", ".*");

	Route::get('/social/{any?}', 'LocationControllers\Publisher\PublisherSocialController@getIndex')->where("any", ".*");

	Route::get('/website/{any?}', 'LocationControllers\Publisher\PublisherWebsiteController@getIndex')->where("any", ".*");

	Route::get('/tv/{any?}', 'LocationControllers\Publisher\PublisherTvController@getIndex')->where("any", ".*");

		Route::get('/programme/{any?}', 'LocationControllers\Publisher\PublisherProgrammeController@getIndex')->where("any", ".*");

	Route::get('/{any?}', 'LocationControllers\Publisher\PublisherHomeController@getIndex')->where("any", ".*");
	

});



/*Advertiser Location route*/


Route::group(array('prefix'=>'/advertiser','as' => '/advertiser.','middleware' => ['auth','advertiser']),function(){

	Route::get('/home', 'LocationControllers\Advertiser\AdvertiserHomeController@getIndex');

	Route::get('/profile/{any?}', 'LocationControllers\Advertiser\AdvertiserProfileController@getIndex')->where("any", ".*");


	Route::get('/advert/{any?}', 'LocationControllers\Advertiser\AdvertiserAdvertController@getIndex')->where("any", ".*");

	Route::get('/{any?}', 'LocationControllers\Advertiser\AdvertiserHomeController@getIndex')->where("any", ".*");



});

