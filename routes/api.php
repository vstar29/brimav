<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/* @API ROUTE*/

/*Shared API route*/

Route::get('receipt/{id}', 'APIControllers\Advertiser\AdvertAPIController@getReceipt');
Route::post('auth/login','APIControllers\Shared\AccountAPIController@Login');
Route::post('login','APIControllers\Shared\AccountAPIController@firstLogin');
Route::get('auth/logout','APIControllers\Shared\AccountAPIController@Logout');

Route::get('markread','APIControllers\Shared\AccountAPIController@markNotificationRead');


Route::get('callback/{userid}/{token}', ['uses' => 'APIControllers\Shared\ExtraAPIController@Callback', 'as' => 'callback']);

Route::get('emailcallback/{userid}/{token}', ['uses' => 'APIControllers\Shared\ExtraAPIController@Emailcallback', 'as' => 'emailcallback']);



Route::group(array('prefix'=>'/register','as' => '/register.'),function(){
	Route::post('/register', 'APIControllers\Shared\AccountAPIController@postRegister');
	Route::post('/registerpublisher', 'APIControllers\Shared\AccountAPIController@postRegisterpublisher');

	Route::post('/completereg', 'APIControllers\Shared\AccountAPIController@postCompletepublisherreg');

	Route::post('/emailverificationtoken', 'APIControllers\Shared\AccountAPIController@postEmailverificationtoken');

	Route::put('/changepassword', 'APIControllers\Shared\AccountAPIController@putChangepassword');
	

});




/*--ADMIN API--*/


/*Admin profile*/



Route::group(array('prefix'=>'/adminprofileAPI','as' => '/adminprofileAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\ProfileAPIController@getIndex');

	Route::put('/editadminbasicdetails', 'APIControllers\Admin\ProfileAPIController@putEditadminbasicdetails');

	Route::put('/changepassword', 'APIControllers\Admin\ProfileAPIController@putChangepassword');
	

});

/*Admin Advertiser*/


Route::group(array('prefix'=>'/adminadvertiserAPI','as' => '/adminadvertiserAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\AdvertiserAPIController@getIndex');
	Route::get('/advertiserbyusername/{username}', 'APIControllers\Admin\AdvertiserAPIController@getAdvertiserbyusername');

	Route::get('/advertiserbyid/{id}', 'APIControllers\Admin\AdvertiserAPIController@getAdvertiserbyid');

	Route::get('/activateadvertiser/{id}', 'APIControllers\Admin\AdvertiserAPIController@Activateadvertiser');

	Route::get('/deactivateadvertiser/{id}', 'APIControllers\Admin\AdvertiserAPIController@Deactivateadvertiser');

	Route::get('/deleteadvertiser/{id}', 'APIControllers\Admin\AdvertiserAPIController@Deleteadvertiser');
	

});

/*Admin Advert*/


Route::group(array('prefix'=>'/adminadvertAPI','as' => '/adminadvertAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\AdvertAPIController@getIndex');
	Route::get('/advertbyid/{id}', 'APIControllers\Admin\AdvertAPIController@getAdvertbyid');

	Route::get('/advertiserbyid/{id}', 'APIControllers\Admin\AdvertAPIController@getAdvertiserbyid');

	Route::get('/completedadverts', 'APIControllers\Admin\AdvertAPIController@getCompletedadverts');
	Route::get('/uncompletedadverts', 'APIControllers\Admin\AdvertAPIController@getUncompletedadverts');
	
	Route::get('/removeadvert/{id}', 'APIControllers\Admin\AdvertAPIController@Removeadvert');

	Route::get('/removeadvertbulk/{id}', 'APIControllers\Admin\AdvertAPIController@Removeadvertbulk');
	

});


/*Admin Publisher */


Route::group(array('prefix'=>'/adminpublisherAPI','as' => '/adminpublisherAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\PublisherAPIController@getIndex');
	

	Route::get('/publisherbyid/{id}', 'APIControllers\Admin\PublisherAPIController@getPublisherbyid');

	Route::get('/activepublishers', 'APIControllers\Admin\PublisherAPIController@getActivepublishers');
	Route::get('/inactivepublishers', 'APIControllers\Admin\PublisherAPIController@getInactivepublishers');
	
	Route::put('/editpublisherbalance', 'APIControllers\Admin\PublisherAPIController@putEditpublisherbalance');

	Route::get('/deactivatepublisher/{id}', 'APIControllers\Admin\PublisherAPIController@Deactivatepublisher');

	Route::get('/activatepublisher/{id}', 'APIControllers\Admin\PublisherAPIController@Activatepublisher');

	
	Route::get('/deletepublisher/{id}', 'APIControllers\Admin\PublisherAPIController@Deletepublisher');
});



	/*Admin Payment */


Route::group(array('prefix'=>'/adminpaymentAPI','as' => '/adminpaymentAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\PaymentAPIController@getIndex');
	Route::get('/confirmpayment/{id}', 'APIControllers\Admin\PaymentAPIController@getConfirmpayment');
	Route::get('/disconfirmpayment/{id}', 'APIControllers\Admin\PaymentAPIController@getDisconfirmpayment');
	

});

	/*Admin Coupon */

Route::group(array('prefix'=>'/admincouponAPI','as' => '/admincouponAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\CouponAPIController@getIndex');
	Route::get('/couponbyid/{id}', 'APIControllers\Admin\CouponAPIController@getCouponbyid');
	Route::post('/create', 'APIControllers\Admin\CouponAPIController@postCreate');
	Route::put('/edit', 'APIControllers\Admin\CouponAPIController@putEdit');
	Route::get('/couponremove/{id}', 'APIControllers\Admin\CouponAPIController@deleteCoupon');
	
});



	/*Admin  Banner type */

Route::group(array('prefix'=>'/adminbannertypeAPI','as' => '/adminbannertypeAPI.','middleware' => ['auth','admin']),function(){
	Route::get('', 'APIControllers\Admin\BannerTypeAPIController@getIndex');
	Route::get('/bannertypebyid/{id}', 'APIControllers\Admin\BannerTypeAPIController@getBannerTypebyid');
	Route::post('/create', 'APIControllers\Admin\BannerTypeAPIController@postCreate');
	Route::put('/edit', 'APIControllers\Admin\BannerTypeAPIController@putEdit');
	Route::get('/bannertyperemove/{id}', 'APIControllers\Admin\BannerTypeAPIController@deleteBannerType');
	
});


/*--PUBLISHER API--*/

/*Publisher profile*/

Route::group(array('prefix'=>'/publisherprofileAPI','as' => '/publisherprofileAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\ProfileAPIController@getIndex');

	Route::get('/shared', 'APIControllers\Publisher\ProfileAPIController@sharedAction');
	
	Route::put('/editpublisherbasicdetails', 'APIControllers\Publisher\ProfileAPIController@putEditpublisherbasicdetails');

	Route::put('/editpublisheraccountdetails', 'APIControllers\Publisher\ProfileAPIController@putEditpublisheraccountdetails');

	Route::post('/uploadavatar', 'APIControllers\Publisher\ProfileAPIController@postUploadavatar');

	Route::put('/changepassword', 'APIControllers\Publisher\ProfileAPIController@putChangepassword');
	

});

/*Publisher Advert*/


Route::group(array('prefix'=>'/publisheradvertAPI','as' => '/publisheradvertAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\AdvertAPIController@getIndex');
	Route::get('/advertbyid/{id}', 'APIControllers\Publisher\AdvertAPIController@getAdvertbyid');

	Route::get('/advertlink/{id}', 'APIControllers\Publisher\AdvertAPIController@getAdvertlink');

	Route::get('/advertiserbyid/{id}', 'APIControllers\Publisher\AdvertAPIController@getAdvertlinksbyid');

	Route::get('/advertswithoutlink', 'APIControllers\Publisher\AdvertAPIController@getAdvertswithoutlink');

	Route::put('/insertadvertlink', 'APIControllers\Publisher\AdvertAPIController@putInsertadvertlink');

	Route::get('/completedadverts', 'APIControllers\Publisher\AdvertAPIController@getCompletedadverts');

	Route::get('/uncompletedadverts', 'APIControllers\Publisher\AdvertAPIController@getUncompletedadverts');
});


	/*Publisher Payment */

Route::group(array('prefix'=>'/publisherpaymentAPI','as' => '/publisherpaymentAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\PaymentAPIController@getIndex');
	Route::post('/requestpayment', 'APIControllers\Publisher\PaymentAPIController@postRequestpayment');	
});


	/*Publisher Banner  */

Route::group(array('prefix'=>'/publisherbannerAPI','as' => '/publisherbannerAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\BannerAPIController@getIndex');
	Route::get('/getbannerbyid/{id}', 'APIControllers\Publisher\BannerAPIController@getBannerbyid');
	Route::post('/create', 'APIControllers\Publisher\BannerAPIController@addBanner');
	Route::put('/edit', 'APIControllers\Publisher\BannerAPIController@editBanner');
	Route::get('/delete/{id}', 'APIControllers\Publisher\BannerAPIController@deleteBanner');

	Route::get('/getbannertypes', 'APIControllers\Publisher\BannerAPIController@getBannerTypes');
	
});


	/*Publisher Social  */

Route::group(array('prefix'=>'/publishersocialAPI','as' => '/publishersocialAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\SocialAPIController@getIndex');
	Route::get('/getsocialbyid/{id}', 'APIControllers\Publisher\SocialAPIController@getSocialbyid');
	Route::post('/create', 'APIControllers\Publisher\SocialAPIController@addSocial');
	Route::put('/edit', 'APIControllers\Publisher\SocialAPIController@editSocial');
	Route::get('/delete/{id}', 'APIControllers\Publisher\SocialAPIController@deleteSocial');
	
});


	/*Publisher Website  */

Route::group(array('prefix'=>'/publisherwebsiteAPI','as' => '/publisherwebsiteAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\WebsiteAPIController@getIndex');
	Route::get('/getwebsitebyid/{id}', 'APIControllers\Publisher\WebsiteAPIController@getWebsitebyid');
	Route::post('/create', 'APIControllers\Publisher\WebsiteAPIController@addWebsite');
	Route::put('/edit', 'APIControllers\Publisher\WebsiteAPIController@editWebsite');
	Route::get('/delete/{id}', 'APIControllers\Publisher\WebsiteAPIController@deleteWebsite');
	
});

	/*Publisher TVs  */

Route::group(array('prefix'=>'/publishertvAPI','as' => '/publishertvAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\TvAPIController@getIndex');
	Route::get('/gettvbyid/{id}', 'APIControllers\Publisher\TvAPIController@getTvbyid');
	Route::post('/create', 'APIControllers\Publisher\TvAPIController@addTv');
	Route::put('/edit', 'APIControllers\Publisher\TvAPIController@editTv');
	Route::get('/delete/{id}', 'APIControllers\Publisher\TvAPIController@deleteTv');
	
});


/*Publisher Programmes  */

Route::group(array('prefix'=>'/publisherprogrammeAPI','as' => '/publisherprogrammeAPI.','middleware' => ['auth','publisher']),function(){
	Route::get('', 'APIControllers\Publisher\ProgrammeAPIController@getIndex');
	Route::get('/getprogrammebyid/{id}', 'APIControllers\Publisher\ProgrammeAPIController@getProgrammebyid');
	Route::post('/create', 'APIControllers\Publisher\ProgrammeAPIController@addProgramme');
	Route::put('/edit', 'APIControllers\Publisher\ProgrammeAPIController@editProgramme');
	Route::get('/delete/{id}', 'APIControllers\Publisher\ProgrammeAPIController@deleteProgramme');
	
});



/*--ADVERTISER API--*/

/*Advertiser profile*/

Route::group(array('prefix'=>'/advertiserprofileAPI','as' => '/advertiserprofileAPI.','middleware' => ['auth','advertiser']),function(){
	Route::get('', 'APIControllers\Advertiser\ProfileAPIController@getIndex');

	Route::get('/shared', 'APIControllers\Advertiser\ProfileAPIController@sharedAction');
	
	Route::put('/editadvertiserbasicdetails', 'APIControllers\Advertiser\ProfileAPIController@putEditadvertiserbasicdetails');

	Route::post('/uploadavatar', 'APIControllers\Advertiser\ProfileAPIController@postUploadavatar');

	Route::put('/changepassword', 'APIControllers\Advertiser\ProfileAPIController@putChangepassword');
	

});

/*Advertiser Advert*/


Route::group(array('prefix'=>'/advertiseradvertAPI','as' => '/advertiseradvertAPI.','middleware' => ['auth','advertiser']),function(){
	Route::get('', 'APIControllers\Advertiser\AdvertAPIController@getIndex');
	Route::get('/advertbyid/{id}', 'APIControllers\Advertiser\AdvertAPIController@getAdvertbyid');

	Route::get('/advertlinksbyid/{id}', 'APIControllers\Advertiser\AdvertAPIController@getAdvertlinksbyid');

	Route::get('/confirmadvert/{id}', 'APIControllers\Advertiser\AdvertAPIController@Confirmadvert');

	Route::put('/confirmadvertlink', 'APIControllers\Advertiser\AdvertAPIController@putConfirmadvertlink');

	Route::get('/advertplusreference/{id}', 'APIControllers\Advertiser\AdvertAPIController@getAdvertplusrefence');

	Route::get('/publisherbyid/{id}', 'APIControllers\Advertiser\AdvertAPIController@getPublisherbyid');

	Route::get('/allpublishers/{offset}', 'APIControllers\Advertiser\AdvertAPIController@getAllpublishers');

	Route::get('/bannerpublishers/{type}/{offset}', 'APIControllers\Advertiser\AdvertAPIController@getBannerpublishers');

	Route::post('/verifypayment', 'APIControllers\Advertiser\AdvertAPIController@postVerifypayment');

	Route::post('/verifycoupon', 'APIControllers\Advertiser\AdvertAPIController@postVerifycoupon');

	Route::put('/confirmpayment', 'APIControllers\Advertiser\AdvertAPIController@Confirmpayment');

	Route::put('/verifysongurl', 'APIControllers\Advertiser\AdvertAPIController@putVerifysongurl');

	Route::post('/newadvert', 'APIControllers\Advertiser\AdvertAPIController@postNewadvert');

	Route::post('/newsong', 'APIControllers\Advertiser\AdvertAPIController@postNewsong');

	Route::post('editadvert', 'APIControllers\Advertiser\AdvertAPIController@postEditadvert');

	Route::put('totalamount', 'APIControllers\Advertiser\AdvertAPIController@putTotalamount');

	Route::get('downloadpdf/{id}', 'APIControllers\Advertiser\AdvertAPIController@getDownloadpdf');

	Route::delete('removedvertbyid/{id}', 'APIControllers\Advertiser\AdvertAPIController@deleteRemovedvertbyid');

	Route::get('/completedadverts', 'APIControllers\Advertiser\AdvertAPIController@getCompletedadverts');

	Route::get('/uncompletedadverts', 'APIControllers\Advertiser\AdvertAPIController@getUncompletedadverts');


	Route::get('/unpaidadverts', 'APIControllers\Advertiser\AdvertAPIController@getUnpaidadverts');

	Route::get('/getbannertypes', 'APIControllers\Advertiser\AdvertAPIController@getBannerTypes');

});

