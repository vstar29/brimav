<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->call(function () {
        DB::table('users')->deleteInactive();
        })->dailyAt('20:00');*/
    }


    /*--function to auto confirm advert after onw week if link not empty--
    function autoConfirm()
    {
        DB::table('adverts')->join('advert_publisher','advert_publisher.advert_id','=', 'adverts.id')->join('advertlinks','advertlinks.advert_id','=','adverts.id')->where('advertlinks.publisher_id',$User->userable_id)->select('adverts.*')->where('adverts.payment',1)->take('30')->get();
    } */
}
