<?php

namespace App\Http\Controllers\LocationControllers\Shared;
use Illuminate\Http\Request;
use Validator;
use App\Models\User as User;
use App\Models\Advert as Advert;
use App\Models\Advertiser as Advertiser;
use App\Models\Publisher as Publisher;
use App\Models\Balance as Balance;
use App\Models\Payment as Payment;
use App\Models\Advertlink as Advertlink;
use redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon as Carbon;
use Hash;
use DB;


class CroneController extends Controller
{

	

	public function dailyCrone()
    {
    	$this->linksAutoConfirm();
        $this->advertsAutoConfirm();
    	
	   
	}

	public function linksAutoConfirm()
    {
    	
    	$date_str=date('Y-m-d H:i:s');
    	$date=date_create($date_str);
    	$date=date_sub($date,date_interval_create_from_date_string('2 days'));
    	$date=$date->format('Y-m-d H:i:s');
    	
    	
    	$Advertlinks=DB::select("SELECT advertlinks.id, adverts.payment, advertlinks.advert_id,advertlinks.publisher_id,advertlinks.prize FROM `advertlinks` LEFT JOIN adverts ON adverts.id=advertlinks.advert_id WHERE adverts.payment = 1 AND advertlinks.status='UNCONFIRMED' AND adverts.status='UNCONFIRMED'  AND advertlinks.link !='' AND adverts.updated_at < '$date'");

        
     
    	foreach($Advertlinks as $Advertlink)
        {
          
            if($Advertlink->status='UNCONFIRMED' && $Advertlink->payment==1)
            {
                
             
                if( DB::table('advertlinks')->where('id',$Advertlink->id)->update(['status'=>'CONFIRMED']))
                {
                    $Balance=Balance::where('publisher_id', $Advertlink->publisher_id)->first();

                    if(!empty($Balance))
                    {
                        $Balance->balance= $Balance->balance + (0.903*$Advertlink->prize);
                        $Balance->save();
                    } 
                }
                                           
            }
           
        }
    	
	}

    public function advertsAutoConfirm()
    {
        $date_str=date('Y-m-d H:i:s');
        $date=date_create($date_str);
        $date=date_sub($date,date_interval_create_from_date_string('2 days'));
        $date=$date->format('Y-m-d H:i:s');
      
        
        $Adverts=Advert::with(['links'=>function($query)
            {
                $query->where('link','<>','');
            }])->where([['status','UNCONFIRMED'],['payment',1],['adverts.updated_at','<', $date]])->get();
       

        foreach($Adverts as $Advert)
        {
           
            if(count($Advert->links)==count($Advert->publishers) && $Advert->status="UNCONFIRMED" && $Advert->payment==1)
            {
               
                $Advert->status="CONFIRMED";
            
            if($Advert->save())
            {
                
                foreach($Advert->links as $link)
                {
                    if($link->status=="UNCONFIRMED")
                    {
                        $link->status="CONFIRMED";
                        
                        if($link->save())
                        {
                            
                            foreach($Advert->publishers as $publisher_id)

                                    $Balance=Balance::where('publisher_id', $publisher_id)->first();


                                    if(!empty($Balance))
                                    {
                                        $Balance->balance= $Balance->balance + (0.903*$link->prize);
                                        $Balance->save();
                                    }


                                }

                            
                        }
                    }

                }
            }
        }
    }
}