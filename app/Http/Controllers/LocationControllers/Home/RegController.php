<?php
namespace App\Http\Controllers\LocationControllers\Home;
use App\Http\Controllers\Controller as Controller;
use Cache;

class RegController extends Controller
{
	public function getIndex()
	{
		$reg= Cache::get('reg');
        	if($reg !=null)
        	{
        		return $reg;
        	}
        	$content=view('shared/views/registration/index')->render();
        	Cache::forever('reg', $content);
		    return view('shared/views/registration/index');
	}


	public function getSignup()
	{
		return view('shared/views/registration/registration');
	}


	public function getPublisherreg()
	{
		return view('shared/views/registration/publisher');
	}

	

	public function getRegsuccesfull()
	{
		return view('shared/views/registration/regsuccesfull');
	}


	public function getTelltemplate()
	{
		return view('shared/views/registration/telltemplate');
	}

	
}