<?php
namespace App\Http\Controllers\LocationControllers\Home;
use App\Http\Controllers\Controller as Controller;
use Auth;
use Cache;
use View;


class HomeController extends Controller
{
	public function getIndex()
	{
        
        
		if(Auth::check())
		{

			if(Auth::user()->userable_type=="Admin")
	        {
	        	
	            return redirect('admin/home');   
	        }

	        elseif(Auth::user()->userable_type=="Advertiser")
	        {
	            return redirect('advertiser/home');
	            
	        }

	        elseif(Auth::user()->userable_type=="Publisher")
	        {
	            return redirect('publisher/home');
	            
	        }
	    }
        else
        {	
     		
     		
        	$home= Cache::get('home');
        	if($home !=null)
        	{
        		return $home;
        	}
        	$content=view('shared/views/index')->render();
        	Cache::forever('home', $content);
        	return view('shared/views/index');
        }
	 
	}

	public function getLogin()
	{
	return view('shared/views/login/login');
	}

	public function getRecover()
	{
	return view('shared/views/login/passwordrecovery');
	}


	public function getHome()
	{
	return view('shared/views/homepage');
	}

	public function getChangepassword()
	{
	return view('shared/views/login/changepasswordform');
	}




}