<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminPublisherController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}
	
	public function getIndex()
	{
	 return view ('areas/admin/views/publisher/index');
	}

	public function getHome()
	{
	 return view ('areas/admin/views/publisher/home');
	}

	public function getDetails()
	{
	 return view ('areas/admin/views/publisher/detailstemplate');
	}

	public function getBasicdetails()
	{
	 return view ('areas/admin/views/publisher/basicdetails');
	}

	public function getAccountdetails()
	{
	 return view ('areas/admin/views/publisher/accountdetails');
	}

	public function getPaymentdetails()
	{
	 return view ('areas/admin/views/publisher/paymentdetails');
	}

	public function getEditbalance()
	{
	 return view ('areas/admin/views/publisher/editbalance');
	}

	public function getDialogbox()
	{
		return view('shared/views/modalpopup/modalpopup');
	}

	public function getTemplate()
	{
	return view('areas/admin/views/publisher/template');
	}

	public function getActive()
	{
	return view('areas/admin/views/publisher/activepublisher');
	}

	public function getInactive()
	{
	return view('areas/admin/views/publisher/inactivepublisher');
	}
}