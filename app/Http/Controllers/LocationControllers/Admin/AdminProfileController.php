<?php

namespace App\Http\Controllers\LocationControllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller as Controller;


class AdminProfileController extends Controller
{

	public function _construct()
	{
		$this->middleware('user');
		$this->middleware('admin');
	}

	
    public function getIndex()
	{ 
		return view('/areas/admin/views/profile/index');
	}

	public function getHome()
	{ 
		return view('/areas/admin/views/profile/home');
	}

	public function getBasicdetails()
	{ 
		return view('/areas/admin/views/profile/basicdetails');
	}

	public function getEditadminbasicdetails()
	{ 
		return view('/areas/admin/views/profile/editadminbasicdetails');
	}

	public function getChangepassword()
	{ 
		return view('/areas/admin/views/profile/changepassword');
	}
}
