<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminHomeController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}
	
	public function getIndex()
	{
		return view('areas/admin/views/home/index');
	}

}