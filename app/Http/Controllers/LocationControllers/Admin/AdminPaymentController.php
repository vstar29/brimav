<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminPaymentController extends Controller
{


	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}
	
	public function getIndex()
	{
	 return view ('areas/admin/views/payment/index');
	}

	public function getHome()
	{
	 return view ('areas/admin/views/payment/home');
	}

	public function getDetails()
	{
	 return view ('areas/admin/views/payment/details');
	}

	public function getPublisherpaymount()
	{
	 return view ('areas/admin/views/payment/home');
	}

	public function getRequest()
	{
	 return view ('areas/admin/views/payment/request');
	}

	public function getPaid()
	{
	 return view ('areas/admin/views/payment/paid');
	}

}