<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminAdvertController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

	public function getIndex()
	{
	return view('areas/admin/views/advert/index');
	}

	public function getHome()
	{
	return view('areas/admin/views/advert/home');
	}

	public function getTemplate()
	{
	return view('areas/admin/views/advert/template');
	}

	public function getDetails()
	{
	return view('areas/admin/views/advert/details');
	}

	public function getCompleted()
	{
	return view('areas/admin/views/advert/advertcomplete');
	}

	public function getUncompleted()
	{
	return view('areas/admin/views/advert/advertuncomplete');
	}

	public function getDialogbox()
	{
	return view('shared/views/modalpopup/modalpopup');
	}


}