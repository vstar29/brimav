<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminAdvertiserController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}
	
	public function getIndex()
	{
	 return view ('areas/admin/views/advertiser/index');
	}

	public function getHome()
	{
	 return view ('areas/admin/views/advertiser/home');
	}

	public function getDetails()
	{
	 return view ('areas/admin/views/advertiser/details');
	}

	public function getDialogbox()
	{
		return view('shared/views/modalpopup/modalpopup');
	}
}