<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminBannerTypeController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

	public function getIndex()
	{
	return view('areas/admin/views/bannertype/index');
	}

	public function getHome()
	{
	return view('areas/admin/views/bannertype/home');
	}

	public function getCreate()
	{
	return view('areas/admin/views/bannertype/create');
	}

	public function getEdit()
	{
	return view('areas/admin/views/bannertype/edit');
	}

}