<?php
namespace App\Http\Controllers\LocationControllers\Admin;
use App\Http\Controllers\Controller as Controller;

class AdminCouponController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

	public function getIndex()
	{
	return view('areas/admin/views/coupon/index');
	}

	public function getHome()
	{
	return view('areas/admin/views/coupon/home');
	}

	public function getCreate()
	{
	return view('areas/admin/views/coupon/create');
	}

	public function getEdit()
	{
	return view('areas/admin/views/coupon/edit');
	}

}