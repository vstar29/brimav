<?php
namespace App\Http\Controllers\LocationControllers\Publisher;
use App\Http\Controllers\Controller as Controller;

class PublisherWebsiteController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

	public function getIndex()
	{
	return view('areas/publisher/views/website/index');
	}

	public function getHome()
	{
	return view('areas/publisher/views/website/home');
	}

	public function getCreate()
	{
	return view('areas/publisher/views/website/create');
	}

	public function getEdit()
	{
	return view('areas/publisher/views/website/edit');
	}

}