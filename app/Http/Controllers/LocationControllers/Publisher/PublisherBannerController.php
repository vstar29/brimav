<?php
namespace App\Http\Controllers\LocationControllers\Publisher;
use App\Http\Controllers\Controller as Controller;

class PublisherBannerController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

	public function getIndex()
	{
	return view('areas/publisher/views/banner/index');
	}

	public function getHome()
	{
	return view('areas/publisher/views/banner/home');
	}

	public function getCreate()
	{
	return view('areas/publisher/views/banner/create');
	}

	public function getEdit()
	{
	return view('areas/publisher/views/banner/edit');
	}

}