<?php
namespace App\Http\Controllers\LocationControllers\Publisher;
use App\Http\Controllers\Controller as Controller;

class PublisherHomeController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}
	
	public function getIndex()
	{
	return view('areas/publisher/views/home/index');
	}

	public function getHome()
	{
	return view('areas/publisher/views/home/home');
	}

}