<?php
namespace App\Http\Controllers\LocationControllers\Publisher;
use App\Http\Controllers\Controller as Controller;

class PublisherPaymentController extends Controller
{


	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}
	
	public function getIndex()
	{
	 return view ('areas/publisher/views/payment/index');
	}

	public function getHome()
	{
	 return view ('areas/publisher/views/payment/home');
	}

	public function getDetails()
	{
	 return view ('areas/publisher/views/payment/details');
	}

	
}