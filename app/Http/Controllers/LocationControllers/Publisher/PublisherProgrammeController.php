<?php
namespace App\Http\Controllers\LocationControllers\Publisher;
use App\Http\Controllers\Controller as Controller;

class PublisherProgrammeController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

	public function getIndex()
	{
	return view('areas/publisher/views/programme/index');
	}

	public function getHome()
	{
	return view('areas/publisher/views/programme/home');
	}

	public function getCreate()
	{
	return view('areas/publisher/views/programme/create');
	}

	public function getEdit()
	{
	return view('areas/publisher/views/programme/edit');
	}

}