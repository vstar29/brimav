<?php

namespace App\Http\Controllers\LocationControllers\Publisher;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller as Controller;


class PublisherProfileController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}
	
    public function getIndex()
	{ 
		return view('/areas/publisher/views/profile/index');
	}

	public function getHome()
	{ 
		return view('/areas/publisher/views/profile/home');
	}

	public function getBasic()
	{ 
		return view('/areas/publisher/views/profile/basicdetails');
	}

	public function getEditbasic()
	{ 
		return view('/areas/publisher/views/profile/editbasic');
	}

	public function getAccount()
	{ 
		return view('/areas/publisher/views/profile/accountdetails');
	}

	public function getEditaccount()
	{ 
		return view('/areas/publisher/views/profile/editaccount');
	}

	public function getChangepassword()
	{ 
		return view('/areas/publisher/views/profile/changepassword');
	}
}
