<?php
namespace App\Http\Controllers\LocationControllers\Publisher;
use App\Http\Controllers\Controller as Controller;

class PublisherAdvertController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}
	
	public function getIndex()
	{
	return view('areas/publisher/views/advert/index');
	}

	public function getHome()
	{
	return view('areas/publisher/views/advert/home');
	}

	

	public function getDetails()
	{
	return view('areas/publisher/views/advert/details');
	}

	public function getCompleted()
	{
	return view('areas/publisher/views/advert/completed');
	}

	public function getUncompleted()
	{
	return view('areas/publisher/views/advert/uncompleted');
	}


	public function getInsertlink()
	{
	return view('areas/publisher/views/advert/insertlink');
	}

	public function getDialogbox()
	{
	return view('areas/publisher/views/shared/modal/modalpopup');
	}

	public function getPlayer()
	{
	return view('areas/publisher/views/advert/player');
	}

	

}