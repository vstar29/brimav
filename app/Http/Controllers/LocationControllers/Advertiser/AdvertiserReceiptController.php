<?php
namespace App\Http\Controllers\LocationControllers\Advertiser;
use App\Http\Controllers\Controller as Controller;

class AdvertiserReceiptController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('advertiser');
	}

	public function getIndex()
	{
		return view('areas/advertiser/views/advert/receipt/index');
	}


	public function getHome()
	{
		return view('areas/advertiser/views/advert/receipt/home');
	}

	


}