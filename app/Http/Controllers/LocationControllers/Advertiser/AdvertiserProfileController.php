<?php

namespace App\Http\Controllers\LocationControllers\Advertiser;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller as Controller;


class AdvertiserProfileController extends Controller
{

	public function _construct()
	{
		$this->middleware('user');
		$this->middleware('advertiser');
	}
	
    public function getIndex()
	{ 
		return view('/areas/advertiser/views/profile/index');
	}

	public function getHome()
	{ 
		return view('/areas/advertiser/views/profile/home');
	}

	public function getBasicdetails()
	{ 
		return view('/areas/advertiser/views/profile/basicdetails');
	}

	public function getEditadvertiserbasicdetails()
	{ 
		return view('/areas/advertiser/views/profile/editbasic');
	}

	

	public function getChangepassword()
	{ 
		return view('/areas/advertiser/views/profile/changepassword');
	}
}
