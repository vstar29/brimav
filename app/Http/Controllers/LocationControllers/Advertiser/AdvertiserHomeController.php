<?php
namespace App\Http\Controllers\LocationControllers\Advertiser;
use App\Http\Controllers\Controller as Controller;

class AdvertiserHomeController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('advertiser');
	}
	
	public function getIndex()
	{
	return view('areas/advertiser/views/home/index');
	}

	public function getHome()
	{
	return view('areas/advertiser/views/home/home');
	}

}