<?php
namespace App\Http\Controllers\LocationControllers\Advertiser;
use App\Http\Controllers\Controller as Controller;

class AdvertiserAdvertController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('advertiser');
	}

	public function getIndex()
	{
		return view('areas/advertiser/views/advert/index');
	}

	public function getHome()
	{
		return view('areas/advertiser/views/advert/home');
	}

	public function getAdverttype()
	{
		return view('areas/advertiser/views/advert/create/adverttype');
	}

	public function getCreate()
	{
		return view('areas/advertiser/views/advert/create/home');
	}

	public function getOthers()
	{
		return view('areas/advertiser/views/advert/create/basicinfoother');
	}

	public function getBanneradvert()
	{
		return view('areas/advertiser/views/advert/create/banneradvert');
	}

	public function getEditothers()
	{
		return view('areas/advertiser/views/advert/create/editothers');
	}

	public function getBasicinfo()
	{
		return view('areas/advertiser/views/advert/create/basicinfo');
	}

	public function getEditbasicinfo()
	{
		return view('areas/advertiser/views/advert/create/basicinfoedit');
	}

	public function getEditbanner()
	{
		return view('areas/advertiser/views/advert/create/editbanner');
	}


	public function getUploadsong()
	{
		return view('areas/advertiser/views/advert/create/uploadsong');
	}

	public function getSongmetadata()
	{
		return view('areas/advertiser/views/advert/create/songmetadata');
	}

	public function getSelectpublishers()
	{
		return view('areas/advertiser/views/advert/create/selectpublishers');
	}

	public function getDetails()
	{
		return view('areas/advertiser/views/advert/details');
	}

	public function getCompleted()
	{
		return view('areas/advertiser/views/advert/complete');
	}

	public function getUncompleted()
	{
		return view('areas/advertiser/views/advert/uncomplete');
	}



	public function getAdvertpayment()
	{
		return view('areas/advertiser/views/advert/create/advertpayment');
	}

	

	public function getSuccess()
	{
		return view('areas/advertiser/views/advert/create/advertsuccess');
	}
	public function getDialogbox()
	{
		return view('areas/advertiser/views/shared/multimodal');
	}

	


}