<?php

namespace App\Http\Controllers\APIControllers\Shared;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Models\User as User;
use App\Models\Advertiser as Advertiser;
use App\Models\Publisher as Publisher;
use App\Models\Balance as Balance;
use App\Models\Payment as Payment;
use redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Auth\Passwords\DatabaseTokenRepository;
use Mail;
use Carbon\Carbon as Carbon;
use Hash;
use DB;


class ExtraAPIController extends Controller
{

	public function getIndex($userid,$token)
	{

	}

	public function callBack($userid,$token)
    {
    
	    if($userid!="" && $token!="")
	    {
	      $user= User::where('id',decrypt($userid))->first();


	      if($user!="")
	      {


	         if($user->verificationtoken==rawurldecode($token))
	         {
	         	
	          $user->status="ACTIVE";
	          $user->emailverification=1;
	          $user->save();
	          if($user->userable_type=='Publisher')
	          {
	          	$publisher=Publisher::find($user->userable_id);
	          	$publisher->status="ACTIVE";
	          	$publisher->save();
	          }
	         
	          return view('shared/views/registration/confirmationsuccess',['email'=>$user->email,'message'=>'']);

	         }
	         else
	         {
	           
	           return view('shared/views/registration/confirmationfail');
	         }

	      }
	      else
	      {
	       
	      return view('shared/views/registration/confirmationfail');
	      }
	    }
	   else
	   {
	     
	     return view('shared/views/registration/confirmationfail');
	   }
	}

	public function Emailcallback($email,$token)
    {
    	$carbon= new Carbon('WAST');
    	$carbon=$carbon->subHour(2);

	    if($email!="" && $token!="")
	    {
	      $user=DB::table('password_resets')->where([['email',decrypt($email)],['token',rawurldecode($token)]])->get();

         
	      if(!empty($user))
	      {
	      	 $user=$user[0];
	      	 if($user!="" && $user->created_at >$carbon)
		      {

		          DB::table('password_resets')->where([['email',decrypt($email)],['token',rawurldecode($token)]])->delete();
		          return view('shared/views/login/changepasswordform',['Email'=>decrypt($email)]);

		      }
		      else
		      {
		       
		        return view('shared/views/login/passwordtokenfail');
		      }
	      }
	     
	      else
	      {
	       
	        return view('shared/views/login/passwordtokenfail');
	      }
	    }
	   else
	   {
	     
	      return view('shared/views/login/passwordtokenfail');
	   }
	}
}