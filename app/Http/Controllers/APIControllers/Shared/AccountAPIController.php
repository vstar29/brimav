<?php

namespace App\Http\Controllers\APIControllers\Shared;
use App\Http\Controllers\APIControllers\Shared\ExtraAPIController as ExtraAPIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\User as User;
use App\Models\Advertiser as Advertiser;
use App\Models\Publisher as Publisher;
use App\Models\Balance as Balance;
use App\Models\Payment as Payment;
use redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Auth\Passwords\DatabaseTokenRepository;
use Mail;
use DB;
use Hash;
use Carbon\Carbon as Carbon;
use Storage;



class AccountAPIController extends Controller
{

  public function Login(Request $request)
	{
    
    $message=['username.required'=>'Username is required','password.required'=>'Password is required',];

         $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ],$message);

      if(!($validator->fails()))
      {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => 'ACTIVE', 'emailverification'=>1 ],$request->remember)) 
        {
          return Auth::user();
        }

        if (Auth::attempt(['email' => $request->username, 'password' => $request->password, 'status' => 'ACTIVE', 'emailverification'=>1 ],$request->remember)) 
        {
            
           return Auth::user();
        }

        $validator->after(function($validator) use($request)
        {

    	    if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => 'ACTIVE', 'emailverification'=>0],$request->remember,false)) 
    	    {
            
    	    	$validator->errors()->add('verify', 'Please Confirm your Email!');
                
    	    }

         else if (Auth::attempt(['email' => $request->username, 'password' => $request->password, 'status' => 'ACTIVE', 'emailverification'=>0],$request->remember,false)) 
          {

           $validator->errors()->add('verify', 'Please Confirm your Email!');
                
          }

         else if (Auth::viaRemember()) {
              return Auth::user();
          }


          /*else if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => 'INACTIVE', 'emailverification'=>1])) 
          {
            $validator->errors()->add('', 'Your Account is not Active');     
          }*/

           else 
          {
            
            $validator->errors()->add('', 'Invalid Username or Password');

          }
          
      });
      }

      if($validator->fails())
      {
        return response($validator->errors(), 422); 
      }
	}


  public function firstLogin(Request $request)
  {
    if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 'ACTIVE', 'emailverification'=>1 ],$request->remember)) 
        {
            
            return redirect('/');
        }
        else
        {
          return view('shared/views/registration/confirmationsuccess',['email'=>'','message'=>'Invalid email/password']);
        }
  }

	public function Logout()
	{

		Auth::logout();
		return redirect('/');
	}

   public function postRegister(Request $request)
  {

    $rules=['username'=>'required|alpha_dash|unique:users,username,NULL,id,emailverification,1','email'=>'required|e-mail|unique:users,email,NULL,id,emailverification,1', 'password'=>'required|min:6','type'=>'required'];


    $errormsgs=['phonenumber.required' => 'phone number  is required.','required' => ':attribute  is required.','username.unique'=>'Username taken','email.unique'=>'Email taken'];

    $validator=Validator::make($request->all(),$rules,$errormsgs);

    if($validator->fails())
    {
      return response($validator->errors(), 422);
    }
    else
    {
      $user=new User;
      $verificationtoken=hash_hmac('sha256', str_random(40), config('app.key'));

      $user->username=trim($request->username);
      $user->email=$request->email;
      $user->phonenumber= $request->phonenumber;
      $user->password=Hash::make($request->password);
      $user->userable_type=$request->type;
      $user->status="INACTIVE";
      $user->emailverification=0;
      $user->verificationtoken=$verificationtoken;
      $user->created_at=Carbon::now('WAST');
      $user->city=$request->city;
      $user->country=trim($request->country);
      $user->currency=trim($request->currency);
      $user->picture="/uploads/images/theme/unknown.jpg";
      
      if($user->save())
      {
        if($request->type=="Advertiser")
        {
          $advertiser=new Advertiser;
          $no=mt_rand(1,97);
          $advertiser->address=$no." ".$request->city
        ." ".$request->country;  
          $advertiser->save();
         $user->userable_id=$advertiser->id;
         $user->save();

        }
        else if($request->type=="Publisher")
        {
          $publisher=new Publisher;
          $publisher->status="INACTIVE";
          $publisher->save();
          $user->userable_id=$publisher->id;
          $user->save();
         

          $balance=new Balance;
          $balance->balance=0;
          $balance->publisher_id=$publisher->id;
          $balance->created_at=Carbon::now('WAST');
          $balance->save();
        }
        

        $callbackUrl=route('callback',['userid'=>encrypt($user->id),'token'=>rawurlencode($verificationtoken)]);
       


     Mail::send('shared.views.mail.emailconfirmation', ['user' => $user,'callbackUrl'=>$callbackUrl], function ($mail) use ($user) {
         
              $mail->to($user->email, $user->username)->subject('Email Confirmation');
              
            });
      }
      
    }
  }




 /* public function postCompletepublisherreg(Request $request)
  {


     $rules=['username'=>'required|alpha_dash|unique:users,username,NULL,id,emailverification,1','email'=>'required|email|unique:users,email,NULL,id,emailverification,1', 'password'=>'required|min:6|confirmed','type'=>'required','websiteurl'=>'required|active_url|unique:publishers,websiteurl','websitename'=>'required|unique:publishers,websitename|alpha_dash','accountname'=>'required','accountnumber'=>'required|numeric','banker'=>'required','prize'=>'required |numeric'];

    $errormsgs=['required' => ':attribute  is required.','websiteurl.required' => 'website url  is required.','websiteurl.unique' => 'website url  is already taken.', 'email'=>'invalid email','required' => ':attribute  is required.', 'banker.required' => 'banker name  is required.','websitename.required' => 'website name  is required.','accountnumber.required' => 'account number  is required.','accountname.required' => 'account name is required.','websitename.unique' => 'website name  is already taken.','websiteurl.unique' => 'website url  is already taken.',];

    $validator=Validator::make($request->all(),$rules,$errormsgs);

    if($validator->fails())
    {
      return response($validator->errors(), 422);
    }
    else
    {

      $user=new User;
      $verificationtoken=hash_hmac('sha256', str_random(40), config('app.key'));

      $user->username=trim($request->username);
      $user->email=$request->email;
      $user->phonenumber= $request->phonenumber;
      $user->password=Hash::make($request->password);
      $user->userable_type=$request->type;
      $user->status="INACTIVE";
      $user->emailverification=0;
      $user->verificationtoken=$verificationtoken;
      $user->created_at=Carbon::now('WAST');
      $user->picture="/uploads/images/theme/unknown.jpg";
      if($user->save())
      {
        $publisher=new Publisher;
    
        $publisher->websiteurl=trim($request->websiteurl);
        $publisher->status="INACTIVE";
        $publisher->websitename=$request->websitename;
        $publisher->accountname=$request->accountname;
        $publisher->accountnumber=$request->accountnumber;
        $publisher->banker=$request->banker;
        $publisher->prize=$request->prize;
       
        if( $publisher->save())
        {
          $user->userable_id=$publisher->id;
          $user->save();

          $balance=new Balance;
          $balance->balance=0;
          $balance->publisher_id=$publisher->id;
          $balance->created_at=Carbon::now('WAST');
          $balance->save();

          $payment=new Payment;
          $payment->publisher_id=$publisher->id;
          $payment->amount=0;
          $payment->accountnumber=$request->accountnumber;
          $payment->website=$publisher->websitename;
          $payment->created_at=Carbon::now('WAST');
          $payment->save();

          $user= User::where([['userable_id',$request->id],['userable_type','Publisher']])->first();
             
          $callbackUrl=route('callback',['userid'=>encrypt($user->id),'token'=>rawurlencode($user->verificationtoken)]);
        
      
          Mail::send('shared.views.mail.emailconfirmation', ['user' => $user,'callbackUrl'=>$callbackUrl], function ($mail) use ($user) {
           
                $mail->to($user->email, $user->username)->subject('Email Confirmation');
                
              });
      
        }
      
    }
  }

  
}*/

  public function postEmailverificationtoken(Request $request)
  {


    $message=['exists'=>'User not found','required'=>'Valid email/username is required'];
    $validator=Validator::make($request->all(),['email'=>'required'],$message);
    if($validator->fails())
    {
      return response($validator->errors(), 422);
    }
     

    $validator->after(function($validator) use($request)
    {

      $user= User::where('email',$request->email)->orWhere('username',$request->email)->first();
      if(!$user)
      {
        $validator->errors()->add('email','User not found');
      }
    });

     if($validator->fails())
    {
      return response($validator->errors(), 422);
    }

      $user= User::where('email',$request->email)->orWhere('username',$request->email)->first();
      if(!empty($user) && $user->emailverification==0 && $request->type=="email")
     {
        
        $verificationtoken=hash_hmac('sha256', str_random(40), config('app.key'));
        $user->verificationtoken=$verificationtoken;
        $user->save();
     
        $callbackUrl=route('callback',['userid'=>encrypt($user->id),'token'=>rawurlencode($verificationtoken)]);
       
        Mail::send('shared.views.mail.emailconfirmation', ['user' => $user,'callbackUrl'=>$callbackUrl], function ($mail) use ($user) {
         
              $mail->to($user->email, $user->username)->subject('Email verification');
              
            });


     }

      if(!empty($user) && $user->status=="ACTIVE" && $user->emailverification==1 && $request->type=="password")
      {
        $token=hash_hmac('sha256', str_random(40), config('app.key'));

        $exist= DB::table('password_resets')->where('email',$user->email);
        if($exist="")
        {
           DB::table('password_resets')->insert(['email'=>$user->email,'token'=>$token,'created_at'=>Carbon::now('WAST')]);
        }
        else
        {
          DB::table('password_resets')->where('email',$user->email)->delete();
          
        

          DB::table('password_resets')->insert(['email'=>$user->email,'token'=>$token,'created_at'=>Carbon::now('WAST')]);
        }
        
       

        $callbackUrl=route('emailcallback',['userid'=>encrypt($user->email),'token'=>rawurlencode($token)]);
       
       
      Mail::send('shared.views.mail.recoverpassword', ['user' => $user,'callbackUrl'=>$callbackUrl], function ($mail) use ($user) {
         
              $mail->to($user->email, $user->username)->subject('Recover password');
              
            });
     }
     else
     {
        return response("", 422);
     }
  }


  public function putChangepassword(Request $request)
  {
    
      $rules=['password'=>'required|min:6|confirmed','password_confirmation'=>'required'];
      $message=['confirmed'=>'Password must match'];

    $validator=Validator::make($request->all(),$rules,$message);

  
    if($validator->fails())
    {
      return response($validator->errors(), 422);
    }
    else
    {
      
      $user=User::where('email',$request->email)->first();
      
      if(!empty($user))
      {
        $user->password=Hash::make($request->password);
        $user->save();
      }
      else
      {
        return response('',422);
      }
    }
  }


  
  
  /*----mark a notification as read-*/
  function markNotificationRead()
  {
    
   
    $date_str=date('Y-m-d H:i:s');
    $date=date_create($date_str);
    $date=date_sub($date,date_interval_create_from_date_string('5  minutes'));
    $date=$date->format('Y-m-d H:i:s');
    $data=['status'=>1];
     DB::table('notifications')->where([['receiver_id',Auth::user()->id],['created_at','<',$date]])->update($data);
  }

}
