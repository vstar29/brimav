<?php
namespace App\Http\Controllers\APIControllers\Admin;
use Illuminate\Http\Request;
use App\Models\User as User;
use Validator;
use Hash;
use App\Http\Controllers\Controller as Controller;
use Auth;


class ProfileAPIController extends Controller
{
	
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

	 public function getIndex()
    {
    	
    	$UserId=Auth::id();
		$UserDetails= User::find($UserId);
		return $UserDetails;
	}

	


	public function putEditadminbasicdetails(Request $request)
	{
		$validator=Validator::make($request->all(),
			['username'=>'required|alpha_dash|unique:users,username,'.Auth::id(), 
			'email'=>'required|e-mail|unique:users,email,'.Auth::id(),'phonenumber'=>'numeric']);
		if($validator->fails())
		{
		      return response($validator->errors(), 422);
		}
		else
		{
			$UserId=Auth::id();
				$User= User::find($UserId);
            	if(!empty($User))
				{
				
					$User->username= $request->username;
					$User->email= $request->email;
					$User->phonenumber= $request->phonenumber;
					$User->save();
				}
			}      	
    }
            

	public function putChangepassword(Request $request)
	{
		$username=Auth::user()->username;
		$credentials=['username'=>$username,'password'=>$request->current_password];
        $errormsgs=['different'=>'choose a new different password',];
        $rules=['current_password'=>'required|different:password', 'password'=>'required|min:6|confirmed','password_confirmation'=>'required'];

		$validator=Validator::make($request->all(),$rules,$errormsgs);

		$validator->after(function($validator) use ($credentials)
		{
			if(!(Auth::validate($credentials)))
			{
				$validator->errors()->add('current_password', 'Incorrect password');
			}
		});
		if($validator->fails())
		{
			return response($validator->errors(), 422);
		}
		else
		{
			$UserId=Auth::id();
			$User=User::find($UserId);
			if(!empty($User))
			{
				$User->password=Hash::make($request->password);
				$User->save();
			}
			else
			{
				return response('',422);
			}
		}
	}
}