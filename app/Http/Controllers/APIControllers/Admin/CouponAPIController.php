<?php
namespace App\Http\Controllers\APIControllers\Admin;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\Coupon as Coupon;
use Validator;


class CouponAPIController extends Controller
{

	/*==== Constructorm to prevent uneccessary access==*/
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}


	/*==== Get all   coupon==*/
	public function getIndex()
	{
		$Coupon=Coupon::all();
		return $Coupon;
	}

		/*==== Get coupon byid ==*/
	public function getCouponbyid($id)
	{
		$Coupon=Coupon::find($id);
		return $Coupon;
	}



	/*==== Create  coupon==*/
	public function postCreate(Request $request)
	{
		$validator=Validator::make($request->all(),['name'=>'required','code'=>'required','start'=>'required','stop'=>'required','percent'=>'required|numeric','adverttype'=>'required']);
		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	    	$Coupon=new Coupon();

			$Coupon->code=trim($request->code);
			$Coupon->name=$request->name;
			$Coupon->start=$request->start;
			$Coupon->stop=$request->stop;
			$Coupon->percent=trim($request->percent);
			$Coupon->adverttype=$request->adverttype;
			$Coupon->save();
	    }
		
	}


	/*==== Edit  coupon==*/
	public function putEdit(Request $request)
	{

		$validator=Validator::make($request->all(),['name'=>'required','code'=>'required','start'=>'required','stop'=>'required','percent'=>'required|numeric|max:100','adverttype'=>'required']);
		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	    	$Coupon=Coupon::find($request->id);


			if(!empty($Coupon))
			{
				$Coupon->name=$request->name;
				$Coupon->code=trim($request->code);
				$Coupon->start=$request->start;
				$Coupon->stop=$request->stop;
				$Coupon->percent=$request->percent;
				$Coupon->adverttype=$request->adverttype;
				$Coupon->save();
				
			}
			else
			{
				return response('', 422);
			}
	    }
		
		
	}


	/*==== Delete  coupon==*/
	public function deleteCoupon($id)
	{
		$Coupon= Coupon::find($id);
		if(!empty($Coupon))
		{
			$Coupon->delete();
		}
		else
		{
			return response('', 422);
		}
	}
	
}
