<?php
namespace App\Http\Controllers\APIControllers\Admin;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Balance as Balance;
use App\Models\Payment as Payment;
use Validator;
use Mail;


class PaymentAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}


	public function getIndex()
	{
		$Payments=Payment::with('publisher')->where('status','PAID')->get();
		$Requests=Payment::with('publisher')->where('status','NOT PAID')->get();
		return ['Payments'=>$Payments,'Requests'=>$Requests];
	}

	

	public function getConfirmpayment($id)
	{
		$Request=Payment::find($id);
		$Request->status="PAID";
		$Request->save();
		$Publisher=Publisher::with('user')->find($Request->publisher_id);
		Mail::raw($Request->amount.'has been paid to '.$Publisher->accountnumber.'  '.$Publisher->banker, function ($message) {
					

				$message->to($Publisher->user->email);
				$message->subject('Payment Successful');
			});

		

	}

	public function getDisconfirmpayment($id)
	{
		$Request=Payment::find($id);
		$Request->status="NOT PAID";
		$Request->save();
	}
}