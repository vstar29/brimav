<?php
namespace App\Http\Controllers\APIControllers\Admin;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\Advert as Advert;
use App\Models\Advertiser as Advertiser;
use App\Models\User as User;
use App\Models\Publisher as Publisher;


class AdvertAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

	public function getIndex()
    {
    	$Adverts= Advert::all()->take('30');

		return $Adverts;
	}


	public function getAdvertbyid($id)
    {
		$Advert= Advert::find($id)->first();
		$Publishers= Advert::find($id)->publishers;
		$Advertiser=Advertiser::find($Advert->Advertiser->id)->user->username;
		return response (['AdvertDetails'=>$Advert,'Publishers'=>$Publishers,'Advertiser'=>$Advertiser]);
	}

		
	public function getCompletedadverts()
    {
		$Advert= Advert::where('status','CONFIRMED')->get()->take('30');
		return $Advert;
	}

	public function getUncompletedadverts()
    {
		$Advert= Advert::where('status','UNCONFIRMED')->get()->take('30');
		return $Advert;
	}


	public function Removeadvert($id)
    {
    	
    	
		$Advert= Advert::find($id);
		if(!empty($Advert))
		{
			$Advert->delete();
		}
		else
		{
			return response('', 422);
		}
		
	}

	public function Removeadvertbulk($ids)
	{
		$ids=explode(',', $ids);
		
		if(count($ids)>1)
		{
			
			foreach($ids as $id)
			{
			
				$Advert= Advert::find($id);
				
				if(!empty($Advert))
				{
					$Advert->delete();
				}
				else
				{
					return response('', 422);
				}
			}
		}
		else
		{
			$Advert= Advert::find($ids);
		
				if(!empty($Advert))
				{
					$Advert->delete();
				}
				else
				{
					return response('', 422);
				}
		}
	}

	public function putConfirmadvert($id)
    {
    	
		$Advert= Advert::find($id);
		if(!empty($Advert))
		{
			$Advert->status="COMPLETED";
			$Advert->save();
		}
		else
		{
			return response('', 422);
		}
		
	}


}