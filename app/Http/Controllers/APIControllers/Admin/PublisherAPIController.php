<?php
namespace App\Http\Controllers\APIControllers\Admin;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Balance as Balance;
use App\Models\Payment as Payment;
use Validator;


class PublisherAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}


	public function getIndex()
	{
		$Publisher=User::where('userable_type', 'Publisher')->get()->take('30');
		return $Publisher;
	}

	public function getActivepublishers()
    {
		$Publisher= User::where([['status','ACTIVE'], ['userable_type', 'Publisher'],])->get()->take('30');
		return $Publisher;
	}

	public function getInactivepublishers()
    {
		$Publisher= User::where([['status','INACTIVE'], ['userable_type', 'Publisher']])->get()->take('30');
		return $Publisher;
	}


	public function getPublisherbyid($id)
	{
		$Publisheraccountdetails=Publisher::where('id',$id)->first();
		
		$Publisherbasicdetails=User::where([['userable_type', 'Publisher'],['userable_id', $id],])->first();

		$Publisherbalance=$Publisheraccountdetails->balance;

		$Publisherpaymentsdetails=Payment::where([['publisher_id',$id],['website',$Publisheraccountdetails->websitename]])->get();

		return response(['Publisheraccountdetails'=>$Publisheraccountdetails, 'Publisherbasicdetails'=> $Publisherbasicdetails, 'Publisherbalance'=>$Publisherbalance,'Publisherpaymentsdetails'=>$Publisherpaymentsdetails]);
	}


	public function putEditpublisherbalance(Request $request)
	{
		$message=['integer'=>'The balance must be number.',];
		$validate=Validator::make($request->all(),['balance'=>'numeric'],$message);
		if($validate->fails())
		{
			return response ($validate->errors(),422);
		}
		$Balance=Balance::where([['id',$request->id],['publisher_id',$request->publisherid]])->first();

		if(!empty($Balance))
		{	
			$Balance->id=$request->id;
			$Balance->balance=$request->balance;
			$Balance->publisher_id=$request->publisherid;
			$Balance->save();
		}
		else
		{
			return response('',422);
		}
	}


	public function Activatepublisher($id)
	{
		$User=User::find($id);
		$Publisher=Publisher::find($User->userable_id);
		if(!empty($User) &&!empty($Publisher))
		{
			$User->status='ACTIVE';
			$User->save();
			$Publisher->status='ACTIVE';
			$Publisher->save();
		}
		
	}

	public function Deactivatepublisher($id)
	{
		$User=User::find($id);
		$Publisher=Publisher::find($User->userable_id);
		if(!empty($User) &&!empty($Publisher))
		{
			$User->status='INACTIVE';
			$User->save();
			$Publisher->status='INACTIVE';
			$Publisher->save();
		}
		
	}

	public function Deletepublisher($id)
	{
		$User=User::find($id);
		$Publisher=Publisher::find($User->userable_id);
		if(!empty($User) &&!empty($Publisher))
		{
			$User->delete();
			$Publisher->delete();
		}
		
	}


}