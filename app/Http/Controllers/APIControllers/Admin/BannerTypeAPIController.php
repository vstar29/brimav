<?php
namespace App\Http\Controllers\APIControllers\Admin;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\BannerType as BannerType;
use Validator;
use Cache;
use Auth;


class BannerTypeAPIController extends Controller
{

	/*==== Constructorm to prevent uneccessary access==*/
	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}


	/*==== Get all   banner types==*/
	public function getIndex()
	{
		return BannerType::all();
	}

		/*==== Get coupon byid ==*/
	public function getBannerTypebyid($id)
	{
		$BannerType=BannerType::find($id);
		return $BannerType;
	}



	/*==== Create  coupon==*/
	public function postCreate(Request $request)
	{
		$validator=Validator::make($request->all(),['name'=>'required|unique:banner_types,bannertype_name','description'=>'required','size'=>'required']);
		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	    	$BannerType=new BannerType();

			$BannerType->bannertype_name=$request->name;
			$BannerType->description=$request->description;
			$BannerType->size=$request->size;
			$BannerType->save();
			 Cache::forget('banner_types');
	    }
	  
		
	}


	/*==== Edit  BannerType==*/
	public function putEdit(Request $request)
	{

		$validator=Validator::make($request->all(),['name'=>'required|unique:banner_types,bannertype_name,'.$request->id,'description'=>'required','size'=>'required']);

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	    	$BannerType=BannerType::find($request->id);


			if(!empty($BannerType))
			{
				
				$BannerType->bannertype_name=$request->name;
				$BannerType->description=$request->description;
				$BannerType->size=$request->size;
				$BannerType->save();
				
				
			}
			else
			{
				return response('', 422);
			}
			  Cache::forget('banner_types');
	    }
		
		
	}


	/*==== Delete  BannerType==*/
	public function deleteBannerType($id)
	{
		$BannerType= BannerType::find($id);
		if(!empty($BannerType))
		{
			$BannerType->delete();
			 Cache::forget('banner_types');
		}
		else
		{
			return response('', 422);
		}
	}
	
}
