<?php
namespace App\Http\Controllers\APIControllers\Advertiser;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\Advert as Advert;
use App\Models\Advertiser as Advertiser;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Advertlink as Advertlink;
use App\Models\Balance as Balance;
use App\Models\Receipt as Receipt;
use App\Models\Coupon as Coupon;
use App\Models\BannerType as BannerType;
use App\Models\Banner as Banner;
use DB;
use Storage;
use Validator;
use Auth;
use Mail;
use Carbon\Carbon as Carbon;
use View;
use Cache;
use Config;
use  App\Lib\cloudinary;
use \Cloudinary\Uploader as Uploader;


class AdvertAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('advertiser');
	}

	/*--==== get all  adverts====--*/

	public function getIndex()
    {
    	$Adverts= Advert::take('30')->all();
		return $Adverts;
	}


		/*--==== get  advert by id====--*/
	public function getAdvertbyid($id)
    {
		$Advert= Advert::with('publishers')->find($id);
		return $Advert;
		
	}

		/*--==== get advert link====--*/

	public function getAdvertlinksbyid($id)
    {
		$Advertlinks= Advertlink::where('advert_id',$id)->get();
		return $Advertlinks;
		
		/*$Advert= Advert::find($id);
		return response (['Advertlinks'=>$Advertlinks,'Advert'=>$Advert]);*/
	}


	/*--==== delete advert by id====--*/

	public function deleteRemovedvertbyid($id)
    {
		$Advert= Advert::find($id);
		if(!empty($Advert))
		{
			$Advert->delete();
		}
		else
		{
			return response('', 422);
		}
		
	}

		/*--==== get completed  adverts====--*/
		
	public function getCompletedadverts()
    {
    	if (! is_null($Adverts = Cache::get('adv_c_adverts.'.Auth::user()->userable_id))) 
  	    {
        	return $Adverts;
        }
		$Advert= Advert::where([['status','CONFIRMED'],['payment',1],['advertiser_id',Auth::user()->userable_id]])->take('30')->get();
		Cache::forever('adv_c_adverts.'.Auth::user()->userable_id,$Adverts);
		return $Advert;
	}

	/*--==== get uncompleted  adverts====--*/

	public function getUncompletedadverts()
    {
    	if (! is_null($Adverts = Cache::get('adv_u_adverts.'.Auth::user()->userable_id))) 
  	    {
        	return $Adverts;
        }
		$Advert= Advert::where([['status','UNCONFIRMED'],['payment',1]],['advertiser_id',Auth::user()->userable_id])->take('30')->get();
		Cache::forever('adv_u_adverts.'.Auth::user()->userable_id,$Adverts);
		return $Advert;
	}


	/*--==== get unpaid  adverts====--*/

	public function getUnpaidadverts()
    {
		$Advert= Advert::where([['status','UNCONFIRMED'],['payment',0]],['advertiser_id',Auth::user()->userable_id])->orderBy('created_at','desc')->take('6')->get();

		return $Advert;
	}
	

	/*--==== get all publishers====--*/

	public function getAllpublishers($offset)
	{
		$Websites=DB::table('websites')->join('publishers','websites.publisher_id','=','publishers.id')->where('publishers.status','ACTIVE')->select('websites.*')->where('websites.status','Approved')->skip($offset)->take(30)->get();
		$Tvs=DB::table('tvs')->join('publishers','tvs.publisher_id','=','publishers.id')->where('publishers.status','ACTIVE')->select('tvs.*')->where('tvs.status','Approved')->skip($offset)->take(30)->get();

		$Socials=DB::table('socials')->join('publishers','socials.publisher_id','=','publishers.id')->where('publishers.status','ACTIVE')->select('socials.*')->where('socials.status','Approved')->skip($offset)->take(30)->get();

		$Rates=DB::table('configs')->select('configs.*')->first();
	
		return ['Websites'=>$Websites,'Tvs'=>$Tvs,'Socials'=>$Socials,'Rates'=>$Rates];
		
	}

	/*--=== get banners, publishers based on banner type selected--*/

	public function getBannerpublishers($typeIds=0,$offset=0)
	{
		if($typeIds!=0)
		{
			$typeIds=explode(',', $typeIds);
			$Publisher=DB::table('banners')->join('banner_types','banner_types.id','=','banners.bannertype_id')->join('publishers','publishers.id','=','banners.publisher_id')->where('publishers.status','ACTIVE')->whereIn('banner_types.id',$typeIds)->skip($offset)->take('30')->select('banners.*','publishers.id','banner_types.size')->get();
		}
		else
		{

			$Publisher=DB::table('banners')->join('banner_types','banner_types.id','=','banners.bannertype_id')->join('publishers','publishers.id','=','banners.publisher_id')->where('publishers.status','ACTIVE')->skip($offset)->take('30')->select('banners.*','publishers.id','banner_types.size')->get();
		}

		$Rates=DB::table('configs')->select('configs.*')->first();
		
		return ['Banners'=>$Publisher,'Rates'=>$Rates];
		
	}

	/*--=== get all availlable banners type--*/

	public function getBannerTypes()
	{
		if (! is_null($BannerTypes = Cache::get('banner_types.'.Auth::user()->userable_id))) 
  	    {
        	return $BannerTypes;
        }
		$BannerTypes=BannerType::all();
		Cache::forever('banner_types',$BannerTypes);
		return $BannerTypes;
	}
		

	/*--==== get publisher by id====--*/
	public function getPublisherbyid($id)
	{
		$publisher=Publisher::find($id);
		return $publisher->prize;
	}


	/*--==== validate song url====--*/
	public function putVerifysongurl(Request $request)
	{
		$message=['url.url'=>'Invalid url (must include http or https )',];
	    $validator=Validator::make($request->all(),['url'=>'required|url'],$message);

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	    	return $request->url;
	    }
	}


	/*--==== upload  new song====--*/
	public function postNewsong(Request $request)
	{
		$message=['song.max'=>'The song must not be greater than :max',];
	    $validator=Validator::make($request->all(),['song'=>'required|max:10000'],$message);

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	   
	    $validator->after(function($validator) use($request)
	    {
	    	$mimetype=$request->file('song')->getClientMimeType();
	    	if($mimetype!="audio/mp3" && $mimetype!="audio/mpeg")
	    	{
	    		$validator->errors()->add('song','Please select an mp3 audio');
	    	}
	    });

	     if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {	
	    	
	    	$file=$request->file('song');
    		$originalname=  $file->getClientOriginalName();
	    	$originalname=str_slug(str_replace('.mp3','',$originalname));
	    	$returnPath='uploads/adverts/songs/'.date('Y').'/'.date('m').'/';
	    	$songdestinationPath='../brimav.com/uploads/adverts/songs/'.date('Y').'/'.date('m');
	    	$file->move($songdestinationPath,$originalname.'.mp3');
	    	return url($returnPath.$originalname.'.mp3');
	    	
	    }
	}

	 /*--==== verify coupon====--*/

	public function postVerifycoupon(Request $request)
	{
		$today=new Carbon('WAST');
		$Advert=Advert::find($request->advertid);


		$Coupon=Coupon::where('code',trim($request->couponcode))->first();


		
		if($Coupon!="" && $today >=$Coupon->start && $today<=$Coupon->stop && $Coupon->adverttype==$Advert->type)
		{
			$totalamount=(int)$request->totalamount;
			$discount=round(($Coupon->percent*0.01) *$totalamount);
			$newprize=round($totalamount-$discount);
		
			
			if(!empty($Advert) && $Advert->discount ==0)
			{
				$Advert->discount=$discount;
				$Advert->totalamount=$newprize;
				$Advert->save();
				$newDetails=['discount'=>$discount, 'newprize'=>$newprize];
				return $newDetails;
			}
			else
			{
				return response('',422);
			}
					
		}
		else
		{
			return response('',422);
		}
		
	}

	 /*--==== create adert====--*/

	public function postNewadvert(Request $request)
	{

	 	$message=['image.max'=>'The image must not be greater than :max kb','image.image'=>'Please select an image','image.mimes'=>'Only jpeg & png'];
	    $validator=Validator::make($request->all(),['title'=>'required','description'=>'required_if:type,music','image'=>'required|image|mimes:jpeg,png,gif|max:2000'],$message);
	    
	    $validator->after(function($validator) use ($request)
	    {
	    	if ($request->title=="undefined")
	    	{
	    		$validator->errors()->add('title','The title field is required');
	    	}
	    	if ($request->description=="undefined" && $request->type=="music")
	    	{
	    		$validator->errors()->add('description','The description field is required');
	    	}

	    	if ($request->genre=="undefined" && $request->type=="music")
	    	{
	    		$validator->errors()->add('genre','The genre field is required');
	    	}

	    	if($request->hasFile('attachment'))

	    	{
	    		
	    		$format= $request->file('attachment')->extension();

	    		if ($format =="pdf" || $format =="docx")
	    		{
	    			
	    		}
	    		else
	    		{
	    			$validator->errors()->add('attachment','Only pdf & docx');
	    		}
    		}

	
	    	if ($request->image=="true")
	    	{
	    		$validator->errors()->add('image','The image field is required');
	    	}
	    });

	    if($validator->fails())
	    {
	    	 
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	  		
	    	if(!$request->hasFile('image'))
	    	{
	    		return response('Please select a valid image',422);
	    	}

	    	 if(!$request->file('image')->isValid())
	    	 {
	    	 	return response('Please select a valid image',422);
	    	 }
	    	
	    	$destination=$request->destination=='undefined'? '':$request->destination;
	  		$itunes=$request->itunes=='undefined'? '':$request->itunes;
	  		$youtube=$request->youtube=='undefined'? '':$request->youtube;
	  		
	    	$Advert =new Advert;
	    	$Advert->title=$request->title;
	    	$Advert->description=$request->description;
    		$Advert->destination=$destination;
	    	$Advert->youtube=$youtube;
	    	$Advert->itunes=$itunes;
	    	$Advert->genre=$request->genre;
	    	$Advert->status="UNCONFIRMED";
	    	$Advert->type=$request->type;
	    	$Advert->totalamount=$request->totalamount;
	    	$Advert->advertiser_id=Auth::user()->userable_id;
	    	$Advert->payment=false;
	    	$Advert->songurl=$request->song;
	    	$Advert->created_at=Carbon::now('WAST');

	    	if($Advert->save())
	    	{
	    	
	    		/*$imagedestinationPath='../brimav.com/uploads/adverts/images/'.date('Y').'/'.date('m');
	    		$extension=$request->file('image')->extension();
	    		$returnPath='uploads/adverts/images/'.date('Y').'/'.date('m').'/';
	    		$filename=$Advert->id.'.'.$extension;
		    	
	    		$request->file('image')->move($imagedestinationPath,$filename);
	    		$picture= url($returnPath.$filename);
	    		$Advert->imageurl=$picture;*/
	    	   $response=Uploader::upload($request->file('image'));
	    		$Advert->imageurl=$response['secure_url'];
	    		

	    		if($request->hasFile('attachment'))
	    		{
	    		
	    			/*$filedestinationPath='../brimav.com/uploads/adverts/attachments/'.date('Y').'/'.date('m');

		    		$extension=$request->file('attachment')->extension();
		    		$returnPath='uploads/adverts/attachments/'.date('Y').'/'.date('m').'/';
		    		$filename=$Advert->id.'.'.$extension;
			    	
		    		$request->file('attachment')->move($filedestinationPath,$filename);
		    		$fileattach= url($returnPath.$filename);
		    		$Advert->attachment=$fileattach;*/

		    		$response=Uploader::upload($request->file('attachment'));
	    			$Advert->attachment=$response['secure_url'];
	    		}
	    		$Advert->save();
	    		return $Advert->id;

	    	}	

	    	else
	    	{
	    		return response('',422);
	    		
	    	}	
	    }

	}


	/*--==== edit advert====--*/

	public function postEditadvert(Request $request)
	{
	 	$message=['image.max'=>'The image must not be greater than :max kb','image.image'=>'Please select an image','image.mimes'=>'Only jpeg & png'];
	    $validator=Validator::make($request->all(),['title'=>'required','description'=>'required_if:type,music','image'=>'required'],$message);
	    
	    $validator->after(function($validator) use ($request)
	    {
	    	if ($request->title=="undefined")
	    	{
	    		$validator->errors()->add('title','The title filed is required');
	    	}

	    	if($request->hasFile('image'))
	    	{

	    		$format= $request->file('image')->extension();

	    		if ($format =="jpg" || $format =="png" || $format =="jpeg")
	    		{
	    			
	    		}
	    		else
	    		{
	    			$validator->errors()->add('image','Only jpeg & png');
	    		}
	    	}

	    	if ($request->description=="undefined" && $request->type=="music")
	    	{
	    		$validator->errors()->add('description','The description filed is required');
	    	}

	    	if ($request->genre=="undefined" && $request->type=="music")
	    	{
	    		$validator->errors()->add('genre','The genre filed is required');
	    	}

	    	if($request->hasFile('attachment'))

	    	{
	    		
	    		$format= $request->file('attachment')->extension();

	    		if ($format =="pdf" || $format =="docx")
	    		{
	    			
	    		}
	    		else
	    		{
	    			$validator->errors()->add('attachment','Only pdf & docx');
	    		}
    		}

	
	    	if ($request->image=="true")
	    	{
	    		$validator->errors()->add('image','The image filed is required');
	    	}
	    });

	    if($validator->fails())
	    {
	    	 
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	  		$destination=$request->destination=='undefined'? '':$request->destination;
	  		$itunes=$request->itunes=='undefined'? '':$request->itunes;
	  		$youtube=$request->youtube=='undefined'? '':$request->youtube;
	  		

	    	$Advert =Advert::find($request->id);
	    	$Advert->title=$request->title;
	    	$Advert->description=$request->description;
	    	$Advert->destination=$destination;
	    	$Advert->youtube=$youtube;
	    	$Advert->itunes=$itunes;
	    	$Advert->genre=$request->genre;
	    	$Advert->status="UNCONFIRMED";
	    	$Advert->type=$request->type;
	    	$Advert->totalamount=$request->totalamount;
	    	$Advert->advertiser_id=Auth::user()->userable_id;
	    	$Advert->payment=false;
	    	$Advert->songurl=$request->song;
	    	$Advert->updated_at=Carbon::now('WAST');

	    	if($Advert->save())
	    	{
	    		if($request->hasFile('image') && $request->file('image')->isValid())
	    		{
	    			/*$imagedestinationPath='../brimav.com/uploads/adverts/images/'.date('Y').'/'.date('m');

		    		$extension=$request->file('image')->extension();
		    		$returnPath='uploads/adverts/images/'.date('Y').'/'.date('m').'/';
		    		$filename=$Advert->id.'.'.$extension;
			    	
		    		$request->file('image')->move($imagedestinationPath,$filename);
		    		$picture= url($returnPath.$filename);
		    		$Advert->imageurl=$picture;*/
		    		
		    		$response=Uploader::upload($request->file('image'));
		    		$Advert->imageurl=$response['secure_url'];
	    		}
	    		else
	    		{

	    			$Advert->imageurl=$request->image;
	    		}
	    		if($request->hasFile('attachment') && $request->file('attachment')->isValid())
	    		{
	    			/*$filedestinationPath='../brimav.com/uploads/adverts/attachments/'.date('Y').'/'.date('m');

	    			
		    		$extension=$request->file('attachment')->extension();
		    		$returnPath='uploads/adverts/attachments/'.date('Y').'/'.date('m').'/';
		    		$filename=$Advert->id.'.'.$extension;
			    	
		    		$request->file('attachment')->move($filedestinationPath,$filename);
		    		$fileattach= url($returnPath.$filename);
		    		$Advert->attachment=$fileattach;*/
		    		$response=Uploader::upload($request->file('attachment'));
	    			$Advert->attachment=$response['secure_url'];
	    		}
	    		else
	    		{
	    			$Advert->attachment=$request->attachment;
	    		}
	    		$Advert->save();
	    		return $Advert->id;

	    	}	

	    	else
	    	{
	    		return response('',422);
	    		
	    	}	
	    }

	}

		/*--==== save advert total prize & initial links====--*/

	public function putTotalamount(Request $request)
	{
		
		$message=['required.publishers'=>'Please select website'];
		$validator=Validator::make($request->all(),['publishers'=>'required','totalamount'=>'required'],$message);

		 if($validator->fails())
	    {
	    	 
	    	return response($validator->errors(), 422);
	    }
	    else
	    {
	    	$Advert=Advert::find($request->id);
		if(!empty($Advert))
		{

			if($request->status=='edit')
			{
				DB::table('advert_publisher')->where('advert_id',$request->id)->delete();
			}
			$Advert->discount=0;
	    	if($Advert->save())
	    	{
	    		$totalamount=0;
	    		foreach($request->publishers as $publisher=>$val)
		    	{
		    		$values=explode(",", $val);
		    		$prize=$values[0];
		    		$publisher_id=$values[1];
		    		$name=$values[2];
		    		$medium=$values[3];
		    		$totalamount=$totalamount+$prize;
		    		$Publisher=Publisher::find($publisher_id);
		    		$data=['advert_id'=>$Advert->id,'publisher_id'=>$publisher_id,'status'=>'UNCONFIRMED','link'=>'','prize'=>$prize,'mediumname'=>$name,'medium'=>$medium];
  				    DB::table('advertlinks')->insert($data);
		    		$Publisher->adverts()->save($Advert,['prize' => $val]);
		    	}
		    	
		    	$Advert->totalamount=$totalamount;
		    	$Advert->save();
		    	return $Advert->id;
	    	}

	    	else
	    	{
	    		return response('', 422);
	    	}
		}
	    }
		
	}


	/*----verify payment from payment gateway--*/

	public function postVerifypayment(Request $request)
	{
		
		$result = array();

         $data=['token'=>$request->token,
            	'amount'=>$request->amount,
            	'amount_currency'=>'NGN'];

		$url = 'https://api.simplepay.ng/v1/payments/verify';

		$api_key='test_pr_5996c4f02397440fb3821199048827c6';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		
		curl_setopt($ch, CURLOPT_USERPWD, "$api_key:");
		$process = curl_exec($ch);
		curl_close($ch);
		
		if ($process) {
		  $result = json_decode($process, true);

		}

		if($result['captured']="true")
		{
				$CardDetails=$result['source'];
				$PaymentDetails=['amount'=>$result['amount'],'currency'=>$result['currency'],'websiteno'=>$request->websiteno,'id'=>$request->id,'advertiser'=>$request->advertiser,'reference'=>$result['payment_reference']];
			$res=$this->Confirmpayment($CardDetails,$PaymentDetails);
		
			if($res)
			{
				return $res;
			}
			else
			{
				return response('Payment failed',422);
			}

		}
		else
		{
			return response('Payment failed',422);
		}



		/*$url = 'https://api.paystack.co/transaction/verify/'.$request->reference;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt(
		  $ch, CURLOPT_HTTPHEADER, [
		    'Authorization: sk_test_57cf292f4de1886249320d14d33ba9ef0024ed24']
		);
		$request = curl_exec($ch);
		curl_close($ch);

		if ($request) {
		  $result = json_decode($request, true);
		}

		if($result->status="true")
		{
			$data=$result->data;
			if($data->status="success")
			{
				$CardDetails=$data->authorization;
				$PaymentDetails=['amount'=>$data->amount,'currency'=>$data->currency,'websiteno'=>$request->websiteno,'id'=>$request->id,'advertiser'=>$request->advertiser,'reference'=>$data->reference];
				Confirmpayment($CardDetails,$PaymentDetails);

				

			}
		}
		else
		{
			return response('Payment failed',422);
		}*/

	}


	/*--==== advert payment confirmation====--*/

	public function Confirmpayment($card,$payment)
	{
		
		$Advert=Advert::find($payment['id']);

		if(!empty($Advert))
		{
			
			if( (int)$Advert->totalamount."00"== $payment['amount'])
			{
				$Advert->payment=1;
				if($Advert->save())
				{

					$user=Auth::user();
	    			Mail::send('shared.views.mail.newadvertadvertiser', ['user' => $user], function ($mail) use ($user) {
           
            		$mail->to($user->email, $user->username)->subject('New promotion!');
            		
        			});

	    			
	    			foreach ($Advert->publishers as $Publisher) 
	    			{
	    				$publisher=Publisher::with('user')->find($Publisher->id);
	    			

	    				Mail::send('shared.views.mail.newadvertpublisher', ['user' => $publisher->user], function ($mail) use ($publisher) {
           
            			$mail->to($publisher->user->email, $publisher->user->username)->subject('New promotion!');
            		
        				});
        				
		    	        $data=['sender_id'=>Auth::id(),'receiver_id'=>$publisher->user->id, 'advert_id'=>$Advert->id,'content'=>'New advert: ','status'=>0,'advert_link'=>'/publisher/advert/details/'.$Advert->id];

        		     $this->putNotification($data);
        		     Cache::forget('pub_u_adverts.'.$Advert->advert_id);
        		     Cache::forget('adv_u_adverts.'.$publisher->id);
        		     Cache::forget('pub_u_adverts_l.'.$publisher->id);
	    			}

	    				$Receipt=new Receipt;
	    				$Receipt->advert_id=$payment['id'];
	    				$Receipt->receipt_number= rand(020,345205);
	    				$Receipt->advertiser_id=$payment['advertiser'];
	    				$Receipt->cardtype=$card['brand'];
	    				$Receipt->cardnumber=$card['last4'];
	    				$Receipt->cardtoken=$card['id'];
	    				$Receipt->websitenumber=$payment['websiteno'];
	    				$Receipt->reference=$payment['reference'];
	    				$Receipt->created_at=Carbon::now('WAST');
	    				$Receipt->save();
	    				return $Receipt;
        			
				}
				
			}
			else
			{
				
			}
		}
	}

	/*--==== confirm advert as completed====--*/

	public function Confirmadvert($id)
    {
    	
		$Advert= Advert::find($id);
		$Advertlinks=Advertlinks::where([['advert_id',$id],['links','<>','']])->get();
		
		if(count($Advertlinks)==count($Advert->publishers) && $Advert->status="UNCONFIRMED" && $Advert->payment==1)
		{
			$Advert->status="CONFIRMED";
			
			if($Advert->save())
			{
				Cache::forget('adv_u_adverts.'.Auth::user());
				foreach($Advertlinks as $Advertlink)
				{
					if($Advertlink->status=="UNCONFIRMED")
					{
						$Advertlink->status="CONFIRMED";
						
						if($Advertlink->save())
						{
							foreach($Advert->publishers as $publisher_id)
							{

								$Balance=Balance::where('publisher_id', $publisher_id)->first();

								if(!empty($Balance))
								{
									$Balance->balance= $Balance->balance + (0.903*$Advertlink->prize);
									$Balance->save();
								}
							}
						}
					}	
				}
			
			}	
		}
		else
		{
			return response('Advert links yet to complete', 422);
		}
	}
		

	/*--==== confirm advert returned link====--*/

	public function putConfirmadvertlink(Request $request)
    {
 
		$Advertlink= Advertlink::find($request->link_id);
		$Advert=Advert::find($Advertlink->advert_id);
		
		if(!empty($Advertlink) && $Advert->status=="UNCONFIRMED" && $Advert->payment==1)
		{
			if($Advertlink->status=="UNCONFIRMED")
			{
				$Advertlink->status="UNCONFIRMED";
				
				if($Advertlink->save())
				{
					$Balance=Balance::where('publisher_id', $request->publisher_id)->first();
					if(!empty($Balance))
					{
						$Balance->balance= $Balance->balance + (0.903*$Advertlink->prize);
						$Balance->save();
					}
												
				}
			}

		
		}

	}
	
	/*--==== get receipt  ====--*/

	public function getReceipt($id)
	{
		if(!empty($id))
		{
			$receipt=Receipt::with('advert','advertiser.user')->find($id);
			$totalamount=(int)$receipt->advert->totalamount +(int)$receipt->advert->discount; 
			$receiptDetails=
			[
				'date'=>$receipt->created_at->format('M d,Y'),
				'no'=>$receipt->receipt_number,
				'websiteno'=>$receipt->websitenumber,
				'cardnumber'=>$receipt->cardnumber,
				'cardtype'=>$receipt->cardtype,
				'username'=>$receipt->advertiser->user->username,
				'email'=>$receipt->advertiser->user->email,
				'city'=>$receipt->advertiser->user->city,
				'country'=>$receipt->advertiser->user->country,
				'currency'=>$receipt->advertiser->user->currency,
				'phonenumber'=>$receipt->advertiser->user->phonenumber,
				'amount'=>$receipt->advert->totalamount,
				'totalamount'=>$totalamount,
				'discount'=>$receipt->advert->discount,
			];
			return view('areas/advertiser/views/advert/receipt/index', $receiptDetails);
		}
	}

	


	/*--==== delete advert====--*/

	public function deleteRemoveadvertbyid($id)
	{
		$Advert=Advert::find($id);
		if(!empty($Advert))
		{
			$Advert->delete();
		}
	}


  	function putNotification($data)
 	{
   		DB::table('notifications')->insert($data);
  	}
	
}