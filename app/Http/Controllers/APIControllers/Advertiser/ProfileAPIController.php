<?php
namespace App\Http\Controllers\APIControllers\Advertiser;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Advertiser as Advertiser;
use Auth;
use Validator;
use Hash;
use Image;
use DB;

class ProfileAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('advertiser');
	}

	public function sharedAction()
	{
		
		$UserDetails= Advertiser::with('user')->find(Auth::user()->userable_id);

		$notifications=DB::table('notifications')->where([['receiver_id',Auth::id()],['status',0]])->orderBy('created_at', 'desc')->get();

		return ['Advertiser'=>$UserDetails,'Notifications'=>$notifications];

	}


	 public function getIndex()
    {
		$UserDetails= Advertiser::with('user')->find(Auth::user()->userable_id);
		return $UserDetails;
	}

	public function postUploadavatar(Request $request)
	{

		$validator=Validator::make($request->all(),['avatar'=>'required|image|mimes:jpeg,png,gif|max:1400']);

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {	
	    	$UserId=Auth::id();
	    	$User=User::find($UserId);
	    	if(!empty($UserId))
	    	{
	    		$file=$request->file('avatar');
		    	$extension=$file->extension();
	    		$originalname=  $file->getClientOriginalName();
		    	//$avatardestinationPath=public_path('uploads\images\avatars/\/');
		    	$avatardestinationPath='../brimav.com/uploads/images/avatars/';
		    	$returnPath='uploads/images/avatars/';
		    	$filename=$User->username.'.'.$extension;
		    	$file->move($avatardestinationPath,$filename);
		    	$picture= $returnPath.$filename;
		    	$img = Image::make($picture)->resize(220, 172);
		    	$img->save($avatardestinationPath.$filename);
		    	$User->picture=$picture;
		    	$User->save();
		    	return $picture;
		    }
	    	
	    }

	}

	

	public function putEditadvertiserbasicdetails(Request $request)
	{
		$validator=Validator::make($request->all(),
			['username'=>'required|alpha_dash|unique:users,username,'.Auth::id(), 
			'email'=>'required|e-mail|unique:users,email,'.Auth::id(),'phonenumber'=>'required|numeric']);
		if($validator->fails())
		{
		      return response($validator->errors(), 422);
		}
		else
		{
			$UserId=Auth::id();
				$User= User::find($UserId);
				$Advertiser=Advertiser::find($User->userable_id);
            	if(!empty($Advertiser))
				{
					$Advertiser->facebook=$request->facebook;
					$Advertiser->twitter=$request->twitter;
					$Advertiser->save();
					
					$User->username= trim($request->username);
					$User->email= $request->email;
					$User->phonenumber= $request->phonenumber;
					$User->save();

				}
			}      	
    }
            

	public function putChangepassword(Request $request)
	{
		$username=Auth::user()->username;
		$credentials=['username'=>$username,'password'=>$request->current_password];
        $errormsgs=['different'=>'choose a new different password',];
        $rules=['current_password'=>'required|different:password', 'password'=>'required|min:6|confirmed','password_confirmation'=>'required'];

		$validator=Validator::make($request->all(),$rules,$errormsgs);

		$validator->after(function($validator) use ($credentials)
		{
			if(!(Auth::validate($credentials)))
			{
				$validator->errors()->add('current_password', 'Incorrect password');
			}
		});
		if($validator->fails())
		{
			return response($validator->errors(), 422);
		}
		else
		{
			$UserId=Auth::id();
			$User=User::find($UserId);
			if(!empty($User))
			{
				$User->password=Hash::make($request->password);
				$User->save();
			}
			else
			{
				return response('',422);
			}
		}
	}
}