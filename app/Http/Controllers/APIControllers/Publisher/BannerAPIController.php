<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Banner as Banner;
use App\Models\Website as Website;
use App\Models\BannerType as BannerType;
use App\Models\Balance as Balance;
use Validator;
use Auth;
use DB;

class BannerAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

		/*==== Get all banners ==*/


	public function getIndex()
    {
  
        $Banners=DB::table('banners')->join('banner_types','banners.bannertype_id','=','banner_types.id')->where('banners.publisher_id',Auth::user()->userable_id)->get();
        return $Banners;
      
	}

		/*==== Get banner byid ==*/
	public function getBannerbyid($name)
	{
		$Banner=Banner::where('name',$name)->first();
		return $Banner;
	}

	/*==== Add new banner ==*/
	public function addBanner(Request $request)
	{
		$Validator=Validator::make($request->all(),['name'=>'required|unique:Banners,name','type'=>'required','prize'=>'required|numeric','website'=>'required']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		
    		$Banner=new Banner;
    		$Banner->name=$request->name;
    		$Banner->prize=$request->prize;
    		$Banner->bannertype_id=$request->type;
    		$Banner->website=$request->website;
    		$Banner->publisher_id=Auth::user()->userable_id;
    		$Banner->save();
    	}
	}



		/*==== Edit banner ==*/
	public function editBanner(Request $request)
	{
		$Validator=Validator::make($request->all(),['name'=>'required|unique:Banners,name,'.$request->id,'type'=>'required','prize'=>'required|numeric','website'=>'required']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Banner=Banner::find($request->id);
    		if($Banner)
    		{
    			$Banner->name=$request->name;
    			$Banner->prize=$request->prize;
    			$Banner->website=$request->website;
    			$Banner->bannertype_id=$request->type;
    			$Banner->save();
    		}
    		else
    		{
    			return response($Validator->errors(), 422);
    		}
    		
    		
    	}
	}


		/*==== Delete banner ==*/
	public function deleteBanner($name)
	{

	   $Banner=Banner::where('name',$name)->first();

		if($Banner)

		{
			$Banner->delete();
		}
    		
	}


		/*==== Get all   banner types && websites==*/
	
	public function getBannerTypes()
	{
		$BannerTypes=BannerType::all();
		$Websites=Website::where('publisher_id',Auth::user()->userable_id)->get();

		return['BannerTypes'=>$BannerTypes,'Websites'=>$Websites];
	}

	
}