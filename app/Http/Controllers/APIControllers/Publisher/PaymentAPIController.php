<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Balance as Balance;
use App\Models\Payment as Payment;
use Validator;
use Auth;
use Mail;


class PaymentAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}


	public function getIndex()
	{
		
		$Payments=Payment::with('publisher')->where('publisher_id',Auth::user()->userable_id)->take('10')->skip('1')->get();
		return $Payments;
	}



	public function postRequestpayment(Request $request)
	{

		$Publisher=Publisher::find(Auth::user()->userable_id);
		
		$message=['amount.min'=>'Minimum of N2000 '];
		$validator=Validator::make($request->all(),['amount'=>'required|numeric|min:2000'],$message);
		

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	   

	   
	    $validator->after(function($validator) use($request,$Publisher)
	    {
	    	
			$Balance=Balance::where('publisher_id',$Publisher->id)->first();
			$balance=(int)$Balance->balance;
			$amount=(int)$request->amount;

			if($balance < $amount)
			{
				$validator->errors()->add('amount','Insufficient balance');
			}
	    });
		 

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }

	    else
	    {
	    	
	  
	    		$Payment =new Payment;
	    		$Payment->status ='NOT PAID';
	    		$Payment->publisher_id=$Publisher->id;
	    		$Payment->amount=$request->amount;
	    		$Payment->website=$Publisher->websitename;
	    		$Payment->save();

	    		$NewBalance=Balance::where('publisher_id',$Publisher->id)->first();
	    		$NewBalance->balance=$NewBalance->balance-$request->amount;
	    		$NewBalance->save();

	    		Mail::raw('New payment request', function ($message) {
					

				$message->to('brimav@gmail.com');
				$message->subject('New payment request');

				});
	    		//return true;
	    	
		    	
		    }
		
			
	}

}