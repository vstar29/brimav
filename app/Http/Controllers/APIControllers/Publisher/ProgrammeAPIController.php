<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Tv as Tv;
use Validator;
use Auth;
use DB;
use App\Models\Programme as Programme;
use Cache;

class ProgrammeAPIController extends Controller
{

    public function _construct()
    {
        $this->middleware('auth');
        $this->middleware('publisher');
    }

        /*==== Get all programmes ==*/


    public function getIndex($skip=0)
    {
    
        $Programmes=DB::select("SELECT programmes.*,tvs.channel_name FROM programmes LEFT JOIN tvs ON programmes.tv_id=tvs.id LIMIT 30 OFFSET $skip");
        
        return $Programmes;
    }

        /*==== Get programme byid ==*/
    public function getProgrammebyid($id)
    {
        $Programme=Programme::find($id);
        return $Programme;
    }

    /*==== Add new programme ==*/
    public function addProgramme(Request $request)
    {
        $msg=["regex"=>"Invalid time format (12:30 AM)"];
        $Validator=Validator::make($request->all(),['name'=>'required|unique:programmes,name','station'=>'required','price'=>'required|numeric','category'=>'required','start_time'=>['required','regex:/[0,1]{1}[0-9]{1}:[0-5]{1}[0-9]{1}\s[A,P]M/'],'stop_time'=>['required','regex:/[0,1]{1}[0-9]{1}:[0-5]{1}[0-9]{1}\s[A,P]M/','different:start_time'],'days'=>'required','interval'=>'required'],$msg);

        if ($Validator->fails())
        {
            return response($Validator->errors(), 422);
        }
        else
        {
         
            $Programme=new Programme;
            $Programme->name=$request->name;
            $Programme->price=$request->price;
            $Programme->category=$request->category;
            $Programme->tv_id=$request->station;
            $Programme->start_time=$request->start_time;
            $Programme->stop_time=$request->stop_time;
            $Programme->interval=$request->interval;
            $Programme->days=$request->days;
            $Programme->publisher_id=Auth::user()->userable_id;
            $Programme->save();
           
        }
    }



        /*==== Edit programme ==*/
    public function editProgramme(Request $request)
    {
        $msg=["regex"=>"Invalid time format (12:30 AM)"];
        $Validator=Validator::make($request->all(),['name'=>'required|unique:programmes,name,'.$request->id,'station'=>'required','category'=>'required','price'=>'required|numeric','start_time'=>['required','regex:/[0,1]{1}[0-2]{1}:[0-5]{1}[0-9]{1}\s[A,P]M/'],'stop_time'=>['required','regex:/[0,1]{1}[0-2]{1}:[0-5]{1}[0-9]{1}\s[A,P]M/','different:start_time'],'days'=>'required','interval'=>'required'],$msg);

        if ($Validator->fails())
        {
            return response($Validator->errors(), 422);
        }
        else
        {
            $Programme=Programme::find($request->id);
            if($Programme)
            {
                $Programme->name=$request->name;
                $Programme->price=$request->price;
                $Programme->category=$request->category;
                $Programme->tv_id=$request->station;
                $Programme->start_time=$request->start_time;
                $Programme->stop_time=$request->stop_time;
                $Programme->interval=$request->interval;
                $Programme->days=$request->days;
                $Programme->save(); 
                Cache::forget('pub_programs.'.Auth::user()->userable_id); 
            }
            else
            {
                return response($Validator->errors(), 422);
            }
            
            
        }
    }


        /*==== Delete programme--==*/
    public function deleteProgramme($id)
    {
       
        $Programme=Programme::find($id);
        if($Programme)
        {
            $Programme->delete();
             Cache::forget('pub_programs.'.Auth::user()->userable_id);
        }
            
    }

    
}