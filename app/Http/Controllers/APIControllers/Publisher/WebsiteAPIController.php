<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Website as Website;
use Validator;
use Auth;
use DB;
use Cache;

class WebsiteAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

		/*==== Get all social medium ==*/


	public function getIndex()
    {
    	
        $Id=Auth::user()->userable_id;
      
        if (! is_null($Websites = Cache::get('pub_websites.'.$Id))) 
        {
            return $Websites;
        }
        else
        {
            $Websites=Website::all()->take(30);
            Cache::forever('pub_websites.'.$Id,$Programmes);
            return $Websites;
        }
  
	}

		/*==== Get website byid ==*/
	public function getWebsitebyid($id)
	{
		$Website=Website::find($id);
		return $Website;
	}

	/*==== Add new website ==*/
	public function addWebsite(Request $request)
	{
        $message=['websitename.required'=>'Website name is required','websiteurl.required'=>'Website url is required'];
		$Validator=Validator::make($request->all(),['websitename'=>'required|unique:websites,websitename','websiteurl'=>'required|active_url','prize|unique:websites,websiteurl','category'=>'required'],$message);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Website=new Website;
    		$Website->prize=$request->prize;
    		$Website->websitename=$request->websitename;
            $Website->websiteurl=$request->websiteurl;
    		$Website->category=$request->category;
    		$Website->publisher_id=Auth::user()->userable_id;
    		$Website->save();
            Cache::forget('pub_websites.'.Auth::user()->userable_id);
    	}
	}



		/*==== Edit website-- ==*/
	public function editWebsite(Request $request)
	{
		$Validator=Validator::make($request->all(),['websitename'=>'required|unique:websites,websitename,'.$request->id,'websiteurl'=>'required|active_url|unique:websites,websiteurl,'.Auth::user()->userable_id.',publisher_id','prize'=>'required|numeric','category'=>'required']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Website=Website::find($request->id);
    		if($Website)
    		{
    		    $Website->prize=$request->prize;
                $Website->websitename=$request->websitename;
                $Website->websiteurl=$request->websiteurl;
                $Website->category=$request->category;
                $Website->save();
                Cache::forget('pub_websites.'.Auth::user()->userable_id);
    			
    		}
    		else
    		{
    			return response($Validator->errors(), 422);
    		}
    		
    		
    	}
	}


		/*==== Delete website--==*/
	public function deleteWebsite($id)
	{
	    $Website=Website::find($id);
    	if($Website)
		{
			$Website->delete();
            Cache::forget('pub_websites.'.Auth::user()->userable_id);
		}
    		
	}

	
}