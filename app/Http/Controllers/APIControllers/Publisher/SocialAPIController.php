<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Social as Social;
use App\Models\BannerType as BannerType;
use App\Models\Balance as Balance;
use Validator;
use Auth;
use DB;

class SocialAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

		/*==== Get all social medium ==*/


	public function getIndex()
    {
    	
        $Socials=Social::all()->take(30);
        return $Socials;
  
	}

		/*==== Get social byid ==*/
	public function getSocialbyid($id)
	{
		$Social=Social::find($id);
		return $Social;
	}

	/*==== Add new social ==*/
	public function addSocial(Request $request)
	{
		$Validator=Validator::make($request->all(),['type'=>'required','handler'=>'required|active_url|unique:socials,handler','prize'=>'required|numeric','followers|numeric|min:7000']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Social=new Social;
    		$Social->type=$request->type;
    		$Social->prize=$request->prize;
    		$Social->handler=$request->handler;
    		$Social->followers=$request->followers;
    		$Social->publisher_id=Auth::user()->userable_id;
    		$Social->save();
    	}
	}



		/*==== Edit social ==*/
	public function editSocial(Request $request)
	{
		$Validator=Validator::make($request->all(),['type'=>'required','handler'=>'required|active_url|unique:socials,handler,'.$request->id,'prize'=>'required|numeric','followers|numeric|min:7000']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Social=Social::find($request->id);
    		if($Social)
    		{
    			$Social->type=$request->type;
    			$Social->prize=$request->prize;
    			$Social->handler=$request->handler;
    			$Social->followers=$request->followers;
    			$Social->publisher_id=Auth::user()->userable_id;
    			$Social->save();
    		}
    		else
    		{
    			return response($Validator->errors(), 422);
    		}
    		
    		
    	}
	}


		/*==== Delete social ==*/
	public function deleteSocial($id)
	{
	    $Social=Social::find($id);
    	if($Social)
		{
			$Social->delete();
		}
    		
	}

	
}