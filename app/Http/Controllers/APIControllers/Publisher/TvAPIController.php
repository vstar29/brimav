<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Tv as Tv;
use Validator;
use Auth;
use DB;
use App\Models\Programme as Programme;

class TvAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

		/*==== Get all tv/radio medium ==*/


	public function getIndex()
    {
    	
        $Tvs=Tv::all()->take(30);
        return $Tvs;
  
	}

		/*==== Get tv byid ==*/
	public function getTvbyid($id)
	{
		$Tv=Tv::find($id);
		return $Tv;
	}

	/*==== Add new tv ==*/
	public function addTv(Request $request)
	{
		$Validator=Validator::make($request->all(),['name'=>'required|unique:tvs,channel_name','frequency'=>'required|unique:tvs,channel_frequency','prize'=>'required|numeric','category'=>'required']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Tv=new Tv;
    		$Tv->prize=$request->prize;
    		$Tv->channel_name=$request->name;
            $Tv->channel_frequency=$request->frequency;
    		$Tv->category=$request->category;
    		$Tv->publisher_id=Auth::user()->userable_id;
    		$Tv->save();
    	}
	}



		/*==== Edit tv ==*/
	public function editTv(Request $request)
	{
		$Validator=Validator::make($request->all(),['name'=>'required|unique:tvs,channel_name,'.$request->id,'frequency'=>'required|unique:tvs,channel_frequency,'.Auth::user()->userable_id.',publisher_id','prize'=>'required|numeric','category'=>'required']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{
    		$Tv=Tv::find($request->id);
    		if($Tv)
    		{
    		    $Tv->prize=$request->prize;
                $Tv->channel_name=$request->name;
                $Tv->channel_frequency=$request->frequency;
                $Tv->category=$request->category;
                $Tv->save();
    			
    		}
    		else
    		{
    			return response($Validator->errors(), 422);
    		}
    		
    		
    	}
	}


		/*==== Delete tv--==*/
	public function deleteTv($id)
	{
	    $Tv=Tv::find($id);
    	if($Tv)
		{
			$Tv->delete();
		}
    		
	}

	
}

