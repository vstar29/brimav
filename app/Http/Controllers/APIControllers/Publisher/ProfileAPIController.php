<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use Auth;
use Validator;
use Hash;
use Image;
use DB;
use Cache;

class ProfileAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

	public function sharedAction()
	{
		
		$UserDetails= Publisher::with('payments','balance','user')->find(Auth::user()->userable_id);

		$notifications=DB::table('notifications')->where([['receiver_id',Auth::id()],['status',0]])->orderBy('created_at', 'desc')->get();

		return ['Publisher'=>$UserDetails,'Notifications'=>$notifications];

	}

	 public function getIndex()
    {
    	
		$Publisher= Publisher::with('payments','balance','user')->find(Auth::user()->userable_id);

		return $Publisher;
	}


	public function postUploadavatar(Request $request)
	{

		$validator=Validator::make($request->all(),['avatar'=>'required|image|mimes:jpeg,png,gif|max:1400']);

		if($validator->fails())
	    {
	    	return response($validator->errors(), 422);
	    }
	    else
	    {	
	    	$UserId=Auth::id();
	    	$User=User::find($UserId);
	    	if(!empty($UserId))
	    	{
	    		$file=$request->file('avatar');
		    	$extension=$file->extension();
	    		$originalname=  $file->getClientOriginalName();
	    		$avatardestinationPath='../brimav.com/uploads\images\avatars/\/';
		    	//$avatardestinationPath=public_path('uploads\images\avatars/\/');
		    	$returnPath='uploads/images/avatars/';
		    	$filename=$User->username.'.'.$extension;
		    	$file->move($avatardestinationPath,$filename);
		    	$picture= $returnPath.$filename;
		    	$img = Image::make($picture)->resize(220, 172);
		    	$img->save($avatardestinationPath.$filename);
		    	$User->picture=$picture;
		    	$User->save();
		    	return $picture;
		    }
	    	
	    }

	}

	

	public function putEditpublisherbasicdetails(Request $request)
	{
		$validator=Validator::make($request->all(),
			['username'=>'required|alpha_dash|unique:users,username,'.Auth::id(), 
			'email'=>'required|email|unique:users,email,'.Auth::id(),'phonenumber'=>'numeric','websiteurl'=>'url']);
		if($validator->fails())
		{
		      return response($validator->errors(), 422);
		}
		else
		{
			$User= User::find(Auth::id());
			$User->username=$request->username;
			$User->email=$request->email;
			$User->phonenumber=$request->phonenumber;
			$User->save();
				
		}      	
    }


    public function putEditpublisheraccountdetails(Request $request)
	{
		$validator=Validator::make($request->all(),
			['banker'=>'required', 
			'accountname'=>'required','accountnumber'=>'required|numeric']);
		if($validator->fails())
		{
		      return response($validator->errors(), 422);
		}
		else
		{
			
			$Publisher=Publisher::find(Auth::user()->userable_id);
            	if(!empty($Publisher))
				{
				
					$Publisher->banker= $request->banker;
					$Publisher->accountname= $request->accountname;
					$Publisher->accountnumber= $request->accountnumber;
					$Publisher->save();
				}
			}      	
    }
            

	public function putChangepassword(Request $request)
	{
		$username=Auth::user()->username;
		$credentials=['username'=>$username,'password'=>$request->current_password];
        $errormsgs=['different'=>'choose a new different password',];
        $rules=['current_password'=>'required|different:password', 'password'=>'required|min:6|confirmed','password_confirmation'=>'required'];

		$validator=Validator::make($request->all(),$rules,$errormsgs);

		$validator->after(function($validator) use ($credentials)
		{
			if(!(Auth::validate($credentials)))
			{
				$validator->errors()->add('current_password', 'Incorrect password');
			}
		});
		if($validator->fails())
		{
			return response($validator->errors(), 422);
		}
		else
		{
			$UserId=Auth::id();
			$User=User::find($UserId);
			if(!empty($User))
			{
				$User->password=Hash::make($request->password);
				$User->save();
			}
			else
			{
				return response('',422);
			}
		}
	}


}