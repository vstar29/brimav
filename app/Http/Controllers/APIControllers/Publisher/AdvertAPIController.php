<?php
namespace App\Http\Controllers\APIControllers\Publisher;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\Advert as Advert;
use App\Models\Advertiser as Advertiser;
use App\Models\User as User;
use App\Models\Publisher as Publisher;
use App\Models\Advertlink as Advertlink;
use App\Models\Balance as Balance;
use Storage;
use Validator;
use Auth;
use DB;
use Mail;
use Carbon\Carbon as Carbon;
use Cache;


class AdvertAPIController extends Controller
{

	public function _construct()
	{
		$this->middleware('auth');
		$this->middleware('publisher');
	}

	
	
	public function getCompletedadverts()
    {
 		$Id=Auth::user()->userable_id;
      
	  	  if (! is_null($Adverts = Cache::get('pub_adverts.'.$Id))) 
	  	  {
	        return $Adverts;
	      }

        $Adverts= DB::select("SELECT advertlinks.advert_id,'advertlinks.status','advertlinks.id','advertlinks.prize',adverts.title,adverts.duration,adverts.type,adverts.created_at  FROM advertlinks LEFT JOIN advert_publisher ON advert_publisher.advert_id=advertlinks.advert_id LEFT JOIN adverts ON advertlinks.advert_id =adverts.id WHERE advertlinks.link !='' AND advert_publisher.publisher_id=$Id  AND adverts.payment=1 AND advertlinks.publisher_id=$Id AND advertlinks.prize=advert_publisher.prize  LIMIT 30");
			Cache::forever('pub_adverts.'.$Id,$Adverts);
			return $Adverts;
	
	}

	/*---Get unplished adverts i.e(adverts with empty link)--*/

	public function getUncompletedadverts()
    {
    	
      $Id=Auth::user()->userable_id;
      
  	  if (! is_null($Adverts = Cache::get('pub_u_adverts.'.$Id))) 
  	  {
        return $Adverts;
      }
        $Adverts= DB::select("SELECT advertlinks.advert_id,'advertlinks.status','advertlinks.id','advertlinks.prize',adverts.title,adverts.duration,adverts.type,adverts.created_at  FROM advertlinks LEFT JOIN advert_publisher ON advert_publisher.advert_id=advertlinks.advert_id LEFT JOIN adverts ON advertlinks.advert_id =adverts.id WHERE advertlinks.link='' AND advert_publisher.publisher_id=$Id  AND adverts.payment=1 AND advertlinks.publisher_id=$Id AND advertlinks.prize=advert_publisher.prize  LIMIT 30");
			Cache::forever('pub_u_adverts.'.$Id,$Adverts);
			return $Adverts;
	}

	public function getAdvertswithoutlink()
	{
		 $Id=Auth::user()->userable_id;
      
  	  if (! is_null($Adverts = Cache::get('pub_u_adverts_l.'.$Id))) 
  	  {
        return $Adverts;
      }

        $Adverts= DB::select("SELECT advertlinks.advert_id,'advertlinks.status','advertlinks.id','advertlinks.prize',adverts.title,adverts.duration,adverts.type,adverts.created_at  FROM advertlinks LEFT JOIN advert_publisher ON advert_publisher.advert_id=advertlinks.advert_id LEFT JOIN adverts ON advertlinks.advert_id =adverts.id WHERE advertlinks.link='' AND advert_publisher.publisher_id=$Id  AND adverts.payment=1 AND advertlinks.publisher_id=$Id AND advertlinks.prize=advert_publisher.prize  LIMIT 6");
			Cache::forever('pub_u_adverts_l.'.$Id,$Adverts);
			return $Adverts;
               
	}

	public function getAdvertbyid($id)
    {
		$Advert= Advert::with('advertiser.user')->find($id);
		return $Advert;
	}

	/*--get Advert link id--*/
	public function getAdvertlink($link_id)
	{
		$Advertlink= Advertlink::find($link_id);
		if($Advertlink)
		{
			return $Advertlink->link;
		}

		
	}
	

	public function putInsertadvertlink(Request $request)
    {
    	$Validator=Validator::make($request->all(),['link'=>'required|active_url']);

    	if ($Validator->fails())
    	{
    		return response($Validator->errors(), 422);
    	}
    	else
    	{

    		
	 		$Publisher=Publisher::find(Auth::user()->userable_id);
	 		$Link=Advertlink::find($request->link_id);
	 		if(! empty($Link))
	 		{
				
				$Link->link=$request->link;
				$Link->edited_at=new Carbon();
				if($Link->save())
				{
					$Advert=Advert::find($Link->advert_id);
					$Advertiser=Advertiser::with('user')->find($Advert->advertiser_id);

		    		Mail::send('areas.publisher.views.mail.linkinserted', ['user' => $Advertiser->user,'link'=>$request->link,'Publisher'=>$Publisher->websitename], function ($mail) use ($Advertiser) {
	           
	            	$mail->to($Advertiser->user->email, $Advertiser->user->username)->subject('New promotion link');
	            		
	        			});

		    		$data=['sender_id'=>Auth::id(),'receiver_id'=>$Advert->advertiser_id, 'advert_id'=>$Advert->id,'content'=>'New link','status'=>0,'advert_link'=>'/advertiser/advert/links/'.$Advert->id];

        		     $this->putNotification($data);

        		     Cache::forget('pub_u_adverts.'.$Publisher->id);
        		     Cache::forget('pub_adverts.'.$Publisher->id);
        		     Cache::forget('pub_u_adverts_l.'.$Publisher->id);
				}
				else
				{
					return response('', 422);
				}
			}
			
		}
    }


    /*---insert notification--*/ 
  function putNotification($data)
  {
    
    DB::table('notifications')->insert($data);
  }
 		
}