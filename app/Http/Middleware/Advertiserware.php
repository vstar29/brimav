<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Advertiserware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->userable_type!=="Advertiser" && Auth::user()->userable_type!=="Admin")
        {
            return redirect('/');
            
        }
        
         return $next($request);
    }
}
