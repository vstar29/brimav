<?php

namespace App\Providers;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
    }
  

    
       
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         Relation::morphMap([
        'Advertiser'=>\App\Models\Advertiser::class,
        'Publisher'=>\App\Models\Publisher::class,
        'Admin'=>\App\Models\Admin::class]);
    }
}
