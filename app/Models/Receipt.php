<?php
namespace App\Models ;
use  Illuminate\Database\Eloquent\Model;
use App\Models\Advert as Advert;
use App\Models\Advertiser as Advertiser;
use App\Models\Coupon as Coupon;

class Receipt extends Model
{
	
	public function advert()
	{
		return $this->belongsTo('App\Models\Advert');
	}

	public function advertiser()
	{
		return $this->belongsTo('App\Models\Advertiser');
	}

	public function coupon()
	{
		return $this->belongsTo('App\Models\Coupon');
	}
}
