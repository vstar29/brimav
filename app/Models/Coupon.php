<?php
namespace App\Models;
use  Illuminate\Database\Eloquent\Model;

Class Coupon extends Model
{
	public function receipts()
	{
		return $this->hasMany('App\Models\Receipt');
	}
}