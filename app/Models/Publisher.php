<?php
namespace App\Models ;
use  Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Advert as Advert;
use App\Models\Payment as Payment;
use App\Models\Balance as Balance;

class Publisher extends Model
{
	

  public function user()
  {
      return $this->morphOne('App\Models\User', 'userable');
  }

	public function adverts()
	{
		return $this->belongsToMany('App\Models\Advert')->withPivot('prize');
	}

  public function payments()
  {
      return $this->hasMany('App\Models\Payment');
  }

   public function programmes()
  {
      return $this->hasMany('App\Models\Programme');
  }


  public function banners()
  {
        return $this->hasMany('App\Models\Banner');
  }

   public function websites()
  {
        return $this->hasMany('App\Models\Website');
  }

  public function socials()
  {
        return $this->hasMany('App\Models\Social');
  }

   public function tvs()
  {
        return $this->hasMany('App\Models\Tv');
  }

  public function balance()
  {
      return $this->hasOne('App\Models\Balance');
  }
}



	



