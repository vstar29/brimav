<?php
namespace App\Models ;
use  Illuminate\Database\Eloquent\Model;
use App\Models\Advert as Advert;
use App\Models\Receipt as Receipt;

class Advertiser extends Model
{
	
	public function adverts()
	{
		return $this->hasMany('App\Models\Advert');
	}
     public function user()
    {
        return $this->morphOne('App\Models\User', 'userable');
    }

    public function receipts()
	{
		return $this->hasMany('App\Models\Receipt');
	}
}



	



