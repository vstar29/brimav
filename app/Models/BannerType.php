<?php
namespace App\Models;
use  Illuminate\Database\Eloquent\Model;

Class BannerType extends Model
{
	public function banners()
	{
		return $this->hasMany('App\Models\Banners');
	}
}