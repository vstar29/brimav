<?php
namespace App\Models ;
use  Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\BannerType as BannerType;
use App\Models\Publisher as Publisher;


class Banner extends Model
{
	

	public function bannertype()
	{
		return $this->belongsTo('App\Models\BannerType');
	}

    public function publisher()
    {
        return $this->belongsTo('App\Models\Publisher');
    }


}



	



