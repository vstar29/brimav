<?php
namespace App\Models ;
use  Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\BannerType as BannerType;
use App\Models\Publisher as Publisher;


class Tv extends Model
{
    public function publisher()
    {
        return $this->belongsTo('App\Models\Publisher');
    }

    public function programmes()
    {
        return $this->hasMany('App\Models\Programme');
    }

}

