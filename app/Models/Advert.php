<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Advert Extends Model
{
	
	public function advertiser()
	{
		return $this->belongsTo('App\Models\Advertiser');
	}

	public function publishers()
	{
		return $this->belongsToMany('App\Models\Publisher')->withPivot(['prize']);
	}

	public function links()
	{
		return $this->hasMany('App\Models\Advertlink');
	}

	public function receipt()
	{
		return $this->hasOne('App\Models\Receipt');
	}

}