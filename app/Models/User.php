<?php
namespace App\Models ;
use  Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable
{
	protected $fillable = [
        'username', 'email', 'password',
    ];

	protected $hidden = [
        'password', 'verificationtoken',
    ];


    public function getUserId()
    {
        return $this->getKey();
    }

    public function getUserPassword()
    {
        return $this->password;
    }

    public function getUserEmail()
    {
        return $this->email;
    }

    
     public function getUserName()
    {
        return $this->username;
    }

     public function getUserPhoneNumber()
    {
        return $this->phonenumber;
    }


    public function userable()
    {
        return $this->morphTo();
    }

}



	



